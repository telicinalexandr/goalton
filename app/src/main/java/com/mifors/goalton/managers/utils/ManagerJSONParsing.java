package com.mifors.goalton.managers.utils;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mifors.goalton.R;
import com.mifors.goalton.interfaces.InterfaceCallbackResponseServer;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.model.Company;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.model.myteams.Invite;
import com.mifors.goalton.model.myteams.InviteProject;
import com.mifors.goalton.model.myteams.MyTeam;
import com.mifors.goalton.model.myteams.MyTeamProject;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectAssign;
import com.mifors.goalton.model.project.ProjectDeals;
import com.mifors.goalton.model.project.ProjectRight;
import com.mifors.goalton.model.project.ProjectRightUser;
import com.mifors.goalton.model.project.ProjectStage;
import com.mifors.goalton.model.project.ProjectSubject;
import com.mifors.goalton.model.team.Team;
import com.mifors.goalton.model.team.TeamAccount;
import com.mifors.goalton.model.team.TeamPermissionProfile;
import com.mifors.goalton.model.team.TeamProfile;
import com.mifors.goalton.model.team.TeamProject;
import com.mifors.goalton.model.user.Profile;
import com.mifors.goalton.network.HttpParam;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by gly8 on 13.05.16.
 */
@SuppressWarnings("ALL")
public class ManagerJSONParsing {

    private static final String TAG = "Goalton [" + ManagerJSONParsing.class.getSimpleName() + "]";
    private static long myId;

    //==========================================
    // PARSE PROFILE
    //==========================================
    public static void parseProfile(JSONObject obj, boolean isMyProfile) {
        GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        try {
            JSONObject user = obj.getJSONObject("user");
            if (!user.isNull("bg")) {
                ManagerAccount.saveSelectedBackgroundImageId(user.getString("bg"));
            }
            Profile p = gson.fromJson(user.toString(), Profile.class);

            Profile profile = Profile.findByServerId(Profile.class, user.getLong("id"));
            // Если пользователя нет в базе создаем того которого распарсили
            if (profile == null) {
                p.setServerId(user.getLong("id"));
                p.setIsMyProfile(isMyProfile);
                p.save();
            } else {
                // Если пользователь есть то перписываем ему поля пришедшие с сервера
                saveMyProfile(profile, p, true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void saveMyProfile(Profile oldProfile, Profile newProfile, boolean isMyProfile) {
        oldProfile.setUsername(newProfile.getUsername());
        oldProfile.setAuthKey(newProfile.getAuthKey());
        oldProfile.setPasswordHash(newProfile.getPasswordHash());
        oldProfile.setPasswordResetToken(newProfile.getPasswordResetToken());
        oldProfile.setEmail(newProfile.getEmail());
        oldProfile.setStatus(newProfile.getStatus());
        oldProfile.setCreatedAt(newProfile.getCreatedAt());
        oldProfile.setUpdatedAt(newProfile.getUpdatedAt());
        oldProfile.setBg(newProfile.getBg());
        oldProfile.setFirstname(newProfile.getFirstname());
        oldProfile.setLastname(newProfile.getLastname());
        oldProfile.setTimezone(newProfile.getTimezone());
        oldProfile.setCountry(newProfile.getCountry());
        oldProfile.setGetTodolist(newProfile.getGetTodolist());
        oldProfile.setCompanyName(newProfile.getCompanyName());
        oldProfile.setCompanySize(newProfile.getCompanySize());
        oldProfile.setIsMyProfile(isMyProfile);
        oldProfile.save();
    }

    //==========================================
    // TEAMS
    //==========================================
    public static void parseTeamAccounts(String jsonStr) {
        ActiveAndroid.beginTransaction();

        try {
            GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithoutExposeAnnotation();
            Gson gson = builder.create();
            JSONObject obj = new JSONObject(jsonStr);
            JSONArray objAccounts = obj.getJSONArray("accounts");
            Collection<TeamAccount> accounts = gson.fromJson(objAccounts.toString(), new TypeToken<Collection<TeamAccount>>() {
            }.getType());

            Iterator itrAccounts = accounts.iterator();

            while (itrAccounts.hasNext()) {
                TeamAccount account = (TeamAccount) itrAccounts.next();
                TeamAccount tempAccount = TeamAccount.finByServerId(account.getJsonServerId());
                if (tempAccount == null) {
                    account.setServerId(account.getJsonServerId());
                    account.save();
                } else {
                    tempAccount.setName(account.getName());
                    tempAccount.setJsonServerId(account.getJsonServerId());
                    tempAccount.setOwnerUserId(account.getOwnerUserId());
                    tempAccount.setTariffId(account.getTariffId());
                    tempAccount.setTariffName(account.getTariffName());
                    tempAccount.setTariffLimitItem(account.getTariffLimitItem());
                    tempAccount.setTariffLimitUser(account.getTariffLimitUser());
                    tempAccount.setCountUser(account.getCountUser());
                    tempAccount.setCountProject(account.getCountProject());
                    tempAccount.setCountItem(account.getCountItem());
                    tempAccount.setTeams(account.getTeams());
                    tempAccount.save();
                    account = tempAccount;
                }

                if (account.getTeams() != null) {
                    Iterator itrTeams = account.getTeams().iterator();
                    while (itrTeams.hasNext()) {
                        Team team = (Team) itrTeams.next();
                        Team tempTeam = Team.findByServerId(Team.class, team.getJsonServerId());
                        if (tempTeam == null) {
                            team.setServerId(team.getJsonServerId());
                            team.setTeamAccount(account);
                            team.save();
                        } else {
                            tempTeam.setName(team.getName());
                            tempTeam.setProfiles(team.getProfiles());
                            tempTeam.setTeamAccount(account);
                            tempTeam.save();
                            team = tempTeam;
                        }

                        if (team.getProfiles() != null) {
                            Iterator itrProfiles = team.getProfiles().iterator();
                            while (itrProfiles.hasNext()) {
                                TeamProfile teamProfile = (TeamProfile) itrProfiles.next();
                                TeamProfile tempTeamProfile = TeamProfile.findByUserTeamId(teamProfile.getUsersTeamId());
                                if (tempTeamProfile == null) {
                                    teamProfile.setTeam(team);
                                    teamProfile.save();
                                } else {
                                    // переносим данные из объект который пришел с сервера
                                    // в объект который хранится в базе
//                                    tempTeamProfile.setJsonServerId(teamProfile.getJsonServerId());
                                    tempTeamProfile.setUsername(teamProfile.getUsername());
                                    tempTeamProfile.setEmail(teamProfile.getEmail());
                                    tempTeamProfile.setIsActive(teamProfile.getIsActive());
                                    tempTeamProfile.setTeamProjects(teamProfile.getTeamProjects());
                                    tempTeamProfile.save();


                                    teamProfile = tempTeamProfile;
                                }

                                if (teamProfile.getTeamProjects() != null) {
                                    Iterator itrProjects = teamProfile.getTeamProjects().iterator();
                                    while (itrProjects.hasNext()) {
                                        TeamProject teamProject = (TeamProject) itrProjects.next();
                                        TeamProject tempTeamProject = ExtendedModelUnique.findByServerId(TeamProject.class, teamProject.getProjectId());
                                        if (tempTeamProject == null) {
                                            teamProject.setTeamProfileId(teamProfile.getUsersTeamId());
                                            teamProject.setTeamId(teamProject.getTeamId());
                                            teamProject.setServerId(teamProject.getProjectId());
                                            teamProject.save();
                                        } else {
                                            tempTeamProject.setName(teamProject.getName());
                                            tempTeamProject.setProjectId(teamProject.getProjectId());
                                            tempTeamProject.setTeamId(teamProject.getTeamId());
                                            tempTeamProject.setProfilesPermissions(teamProject.getProfilesPermissions());
                                            tempTeamProject.save();
                                            teamProject = tempTeamProject;
                                        }
                                        try {
                                            TeamPermissionProfile tempProfilePermission = TeamPermissionProfile.findOrCreate(TeamPermissionProfile.class, teamProject.getProfilesPermissions().getJsonServerId());
                                            tempProfilePermission.setJsonServerId(teamProject.getProfilesPermissions().getJsonServerId());
                                            tempProfilePermission.setTeamProject(teamProject);
                                            tempProfilePermission.setProjectId(teamProject.getProfilesPermissions().getProjectId());
                                            tempProfilePermission.setRightToEdit(teamProject.getProfilesPermissions().getRightToEdit());
                                            tempProfilePermission.setRightToEditProject(teamProject.getProfilesPermissions().getRightToEditProject());
                                            tempProfilePermission.setRightToInvite(teamProject.getProfilesPermissions().getRightToInvite());
                                            tempProfilePermission.setRightToView(teamProject.getProfilesPermissions().getRightToView());
                                            tempProfilePermission.setUserId(teamProject.getProfilesPermissions().getUserId());
                                            tempProfilePermission.setServerId(teamProject.getProfilesPermissions().getJsonServerId());
                                            tempProfilePermission.save();
                                        } catch (Exception e) {
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    //==========================================
    // Projects
    //==========================================
    public static void parseProjects(JSONObject obj) {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();

        try {
            ActiveAndroid.beginTransaction();
            Project[] pr = gson.fromJson(obj.getString("projects"), Project[].class);
            if (pr.length > 0) {
                for (int i = 0; i < pr.length; i++) {
                    Project project = pr[i];
                    Project tempProject = Project.findByServerId(Project.class, project.getJsonServerId());
                    if (tempProject == null) {
                        project.setServerId(project.getJsonServerId());
                        project.setUrlImage(HttpParam.checkImageProject(project));
                        project.save();
                    } else {
                        tempProject.setCountItems(project.getCountItems());
                        tempProject.setCountItemsIsCompleted(project.getCountItemsIsCompleted());
                        tempProject.setCountItemsIsOverdue(project.getCountItemsIsOverdue());
                        tempProject.setImg(project.getImg());
                        tempProject.setName(project.getName());
                        tempProject.setOtherUserName(project.getOtherUserName());
                        tempProject.setOwnerUserId(project.getOwnerUserId());
                        tempProject.setPercentComplete(project.getPercentComplete());
                        tempProject.setRightToEdit(project.getRightToEdit());
                        tempProject.setRightToEditProject(project.getRightToEditProject());
                        tempProject.setType(project.getType());
                        tempProject.setUrlImage(HttpParam.checkImageProject(tempProject));
                        tempProject.save();
                    }
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static void parseProjectSetting(JSONObject obj) {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        myId = ManagerAccount.getMyProfile().getServerId();
        ActiveAndroid.beginTransaction();
        try {

            try {
                long parentProjectServerId = 0;

                if (!obj.isNull("project")) {
                    JSONObject objProject = obj.getJSONObject("project");
                    Project project = gson.fromJson(objProject.toString(), Project.class);
                    Project tempProject = Project.findByServerId(Project.class, project.getJsonServerId());
                    if (tempProject == null) {
                        project.setServerId(project.getJsonServerId());
                        project.save();
                        parentProjectServerId = project.getJsonServerId();

                    } else {

                        if (project.getOwnerUserId() != 0l) {
                            tempProject.setOwnerUserId(project.getOwnerUserId());
                        }

                        if (!TextUtils.isEmpty(project.getRightToEditProject())) {
                            tempProject.setRightToEditProject(project.getRightToEditProject());
                        }

                        if (!TextUtils.isEmpty(project.getRightToEdit())) {
                            tempProject.setRightToEdit(project.getRightToEdit());
                        }

                        if (!TextUtils.isEmpty(project.getName())) {
                            tempProject.setName(project.getName());
                        }
                        if (!TextUtils.isEmpty(project.getImg())) {
                            tempProject.setImg(project.getImg());
                        }

                        if (!TextUtils.isEmpty(project.getType())) {
                            tempProject.setType(project.getType());
                        }

                        if (!TextUtils.isEmpty(project.getCountItems())) {
                            tempProject.setCountItems(project.getCountItems());
                        }
                        if (!TextUtils.isEmpty(project.getCountItemsIsOverdue())) {
                            tempProject.setCountItemsIsOverdue(project.getCountItemsIsOverdue());
                        }
                        if (!TextUtils.isEmpty(project.getCountItemsIsCompleted())) {
                            tempProject.setCountItemsIsCompleted(project.getCountItemsIsCompleted());
                        }

                        if (!TextUtils.isEmpty(project.getUrlImage())) {
                            tempProject.setUrlImage(project.getUrlImage());
                        }

                        tempProject.setOtherUserName(project.getOtherUserName());
                        if (!objProject.isNull("percent_complete")) {
                            tempProject.setPercentComplete(project.getPercentComplete());
                        }

                        // Поля приходящие при загрузке настроек
                        tempProject.setCreatorUserId(project.getCreatorUserId());
                        tempProject.setTeamId(project.getTeamId());
                        tempProject.setIsActive(project.getIsActive());
                        tempProject.setSubjectId(project.getSubjectId());
                        tempProject.setShowCompleted(project.getShowCompleted());
                        tempProject.setShowColumns(project.getShowColumns());
                        tempProject.setShowColumnsTl(project.getShowColumnsTl());
                        tempProject.save();
                        parentProjectServerId = tempProject.getJsonServerId();
                    }
                } else {
                    Project pr = Project.findOrCreate(Project.class, 0);
                    pr.setServerId(0);
                    pr.save();
                }

                if (!obj.isNull("ProjectStages")) {
                    parseProjectStage(obj.getString("ProjectStages"), gson, parentProjectServerId);
                }


                // Парсинг элементов которые меняют статус проекта (отображаются выпадющимся списком)
                if (!obj.isNull("Subjects")) {
                    ProjectSubject[] subjects = gson.fromJson(obj.getString("Subjects"), ProjectSubject[].class);

                    for (int i = 0; i < subjects.length; i++) {
                        ProjectSubject subj = subjects[i];
                        ProjectSubject tempSubj = ProjectSubject.findByFieldAndLongValue(ProjectSubject.class, "server_id", subj.getServerId());
                        if (tempSubj == null) {
                            subj.setParentProjectId(parentProjectServerId);
                            subj.setProjectId(parentProjectServerId);
                            subj.save();
                        } else {
                            tempSubj.setParentProjectId(parentProjectServerId);
                            tempSubj.setName(subj.getName());
                            tempSubj.setProjectId(parentProjectServerId);
                            tempSubj.save();
                        }
                    }
                }

                // Парсинг всех прав для проектов
                if (!obj.isNull("UserRightsForProjects")) {
                    ProjectRight[] projects = gson.fromJson(obj.getString("UserRightsForProjects"), ProjectRight[].class);
                    for (int i = 0; i < projects.length; i++) {
                        ProjectRight projectRight = projects[i];
                        ProjectRight tempProjectRight = ProjectRight.findByTeamIdAndOwnerUser(projectRight.getTeamId(), projectRight.getOwnerUserId());

                        if (tempProjectRight == null) {
                            projectRight.setParentProjectId(parentProjectServerId);
                            projectRight.save();
                        } else {
                            tempProjectRight.setEmail(projectRight.getEmail());
                            tempProjectRight.setListRights(projectRight.getListRights());
                            tempProjectRight.setName(projectRight.getName());
                            tempProjectRight.setOwnerUserId(projectRight.getOwnerUserId());
                            tempProjectRight.setTeamId(projectRight.getTeamId());
                            tempProjectRight.setUsername(projectRight.getUsername());
                            tempProjectRight.setParentProjectId(parentProjectServerId);
                            tempProjectRight.save();
                            projectRight = tempProjectRight;
                        }

                        parseProjectRightUser(projectRight, parentProjectServerId);
                    }
                }

                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void parseProject(JSONObject obj) {
        try {
            Project tempProject = Project.findByServerId(Project.class, obj.getLong("id"));
            if (tempProject != null) {
                tempProject.setName(obj.getString("name"));
                tempProject.setUrlImage(HttpParam.checlImgUrlByType(obj.getString("type"), obj.getString("img")));

                if (!obj.isNull("show_completed")) {
                    tempProject.setShowCompleted(obj.getInt("show_completed"));
                }

                tempProject.setTeamId(obj.getLong("team_id"));
                tempProject.setIsActive(obj.getInt("is_active"));
                tempProject.save();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void parseProjectStage(String objArrayStages, Gson gson, long parentProjectServerId) {
        // Парсинг состояний проекта которые есть у всех проектов
        ProjectStage[] list = gson.fromJson(objArrayStages, ProjectStage[].class);
        for (int i = 0; i < list.length; i++) {
            ProjectStage st = list[i];
            ProjectStage tempStage = ProjectStage.findByServerId(ProjectStage.class, st.getJsonServerid());
            if (tempStage == null) {
                if (parentProjectServerId == 0) {
                    st.setServerId(ManagerApplication.getRandomLong());
                } else {
                    st.setServerId(st.getJsonServerid());
                }

                st.setParentProjectId(parentProjectServerId);
                st.save();
            } else {
                tempStage.setLimitItem(st.getLimitItem());
                tempStage.setName(st.getName());
                tempStage.setPeriod(st.getPeriod());
                tempStage.setProjectId(st.getProjectId());
                tempStage.setSort(st.getSort());
                tempStage.setParentProjectId(parentProjectServerId);
                tempStage.save();
            }
        }
    }

    private static void parseProjectAssigns(JSONArray arAssigns, long projectId) throws JSONException {
        Profile profile = ManagerAccount.getMyProfile();
        for (int i = 0; i < arAssigns.length(); i++) {
            JSONObject obj = arAssigns.getJSONObject(i);
            ProjectAssign assign = ProjectAssign.findOrCreate(ProjectAssign.class, obj.getLong("id"));
            assign.setUserName(obj.getString("username"));
            assign.setProjectID(projectId);
            assign.save();
        }
    }

    // Парсинг этапов проекта
    private static void parseProjectRightUser(ProjectRight projectRight, long parentProjectId) {
        for (ProjectRightUser user : projectRight.getListRights()) {
            ProjectRightUser tempProjectRightUser = ProjectRightUser.findByProjectRight(user.getTeamId(), user.getUserId(), projectRight);
            if (tempProjectRightUser == null) {
                // Костыль. Не возвращаются с сервера параметры прав пользователя.
                if (user.getRightToEdit() == null
                        || user.getRightToEditProject() == null
                        || user.getRightToInvite() == null
                        || user.getRightToView() == null) {
                    if (user.getUserId() == myId) {
                        user.setRightToEdit("1");
                        user.setRightToEditProject("1");
                        user.setRightToInvite("1");
                        user.setRightToView("1");
                    } else {
                        user.setRightToEdit("0");
                        user.setRightToEditProject("0");
                        user.setRightToInvite("0");
                        user.setRightToView("0");
                    }
                }

                user.setParentProjectId(parentProjectId);
                user.setProjectRight(projectRight);
                user.save();
            } else {
                tempProjectRightUser.setParentProjectId(parentProjectId);
                tempProjectRightUser.setEmail(user.getEmail());
                tempProjectRightUser.setRightToEdit(user.getRightToEdit());
                tempProjectRightUser.setRightToEditProject(user.getRightToEditProject());
                tempProjectRightUser.setRightToInvite(user.getRightToInvite());
                tempProjectRightUser.setRightToView(user.getRightToView());
                tempProjectRightUser.setTeamId(user.getTeamId());
                tempProjectRightUser.setUserId(user.getUserId());
                tempProjectRightUser.setUsername(user.getUsername());
                tempProjectRightUser.setProjectRight(projectRight);
                tempProjectRightUser.save();
            }
        }
    }

    //==========================================
    // PARSE STATUS
    //==========================================
    public static boolean checkAccessToken(JSONObject obj) {
        if (!obj.isNull("error")) {
            try {
                if ("ACCESS_TOKEN".equals(obj.getString("error"))) {
                    return false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Проверяет стаутс запроса с сервера
     * Если есть ошибки вызывает метод интерфейса
     *
     * @param strJSON                         - ответ сервера
     * @param interfaceCallbackResponseServer - интерфейс которой реализует контейнер желающий
     *                                        обрабатывать ошибки из запроса по поляем (для показа сообщений под полем ввода)
     * @return boolean - валидные или не валидные данные
     */
    public static boolean isCheckStatusServerResponse(String strJSON, InterfaceCallbackResponseServer interfaceCallbackResponseServer) {
        boolean isCheck = false;
        try {
            JSONObject obj = new JSONObject(strJSON);
            // Есть ошибки в запросе
            if (!isCheckStatusServerResponse(obj)) {
                if (!obj.isNull("errors")) {
                    JSONObject jsonObjectErrors = obj.getJSONObject("errors");
                    Iterator<String> iter = jsonObjectErrors.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {
                            String value = jsonObjectErrors.getString(key);
                            if ("email".equals(key) || "username".equals(key) || "password".equals(key)) {
                                // Показ ошибок под полями ввода
                                if (interfaceCallbackResponseServer != null) {
                                    interfaceCallbackResponseServer.completeResponse(key, getZeroValueFromString(value));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                isCheck = true;
            }

            // Если есть сообщение которое нужно показать пользователю
            if (!obj.isNull("msg")) {
                ManagerApplication.getInstance().showToast(obj.getString("msg"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return isCheck;
    }

    /**
     * Преобразовывает к массиву и
     * возвращает всегда первый елемент из переданной строки
     *
     * @param str - строка которую надо преобразовать к массиву
     */
    private static String getZeroValueFromString(String str) throws JSONException {
        try {
            JSONArray arr = new JSONArray(str);
            return arr.get(0).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * Проверяет стаутс запроса с сервера
     *
     * @param strJSON - ответ сервера
     * @return boolean - валидные или не валидные данные
     */
    public static boolean isCheckStatusServerResponse(String strJSON) {
        try {
            JSONObject obj = new JSONObject(strJSON);
            return isCheckStatusServerResponse(obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Проверяет стаутс запроса с сервера
     *
     * @param obj - запрос сервера преобразованный в JSONObject
     * @return boolean - валидные или не валидные данные
     */
    public static boolean isCheckStatusServerResponse(JSONObject obj) throws JSONException {
        String status;
        if (!obj.isNull("status")) {
            status = obj.getString("status");
            switch (status) {
                case "FAIL":
//                    Crashlytics.logException(new Exception("Status fail"));
                    return false;
                case "OK":
                    return true;
            }
            // Если произошла ошибка с внутруенним движком сервера
        } else if (!obj.isNull("name")) {
            status = obj.getString("name");
            switch (status) {
                case "PHP Fatal Error":
                    return false;
            }
        } else {
            return true;
        }

        return false;
    }

    //==========================================
    // Outline
    //==========================================
    private static JSONObject obj;
    private static Handler callbackOutlineCompleteParse;
    private static ManagerOutline.IManagerOutline iManagerOutline;

    public static void parseOutline(ManagerOutline.IManagerOutline liManagerOutline, JSONObject jsonObject, Handler callback) throws JSONException {
        iManagerOutline = liManagerOutline;
        obj = jsonObject;
        callbackOutlineCompleteParse = callback;
        ManagerProgressDialog.changeTitle(String.format(ManagerApplication.getInstance().getString(R.string.parse_outline), 0, 0));
        new Thread(new Runnable() {
            @Override
            public void run() {
                ActiveAndroid.beginTransaction();
                try {
                    JSONObject objProject = obj.getJSONObject("project");
                    long projectId = objProject.getLong("id");

                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithoutExposeAnnotation();
                    Gson gson = builder.create();

                    if (isCheckStatusServerResponse(obj)) {
                        JSONArray array = obj.getJSONArray("arTree");
                        ActiveAndroid.beginTransaction();
                        try {
                            Project project = Project.findByServerId(Project.class, objProject.getLong("id"));
                            ManagerParseLine managerParseLine = new ManagerParseLine();
                            String mesage = ManagerApplication.getInstance().getString(R.string.parse_outline);
                            for (int i = 0; i < array.length(); i++) {
                                managerParseLine.startParseLine(array.getJSONObject(i), 0, false);
                                if (iManagerOutline != null) {
                                    iManagerOutline.updateTitleProgressParseOutline(String.format(mesage, managerParseLine.c, project.getCountItems()));
                                }
                            }
                            ActiveAndroid.setTransactionSuccessful();
                        } finally {
                            ActiveAndroid.endTransaction();
                        }
                    }

                    if (!obj.isNull("arProjectStages")) {
                        JSONArray array = obj.getJSONArray("arProjectStages");
                        parseProjectStage(array.toString(), gson, projectId);
                    }

                    if (!obj.isNull("arAssigns")) {
                        parseProjectAssigns(obj.getJSONArray("arAssigns"), projectId);
                    }

                    Project pr = Project.findByServerId(Project.class, objProject.getLong("id"));
                    if (pr != null) {
                        if (!obj.isNull("show_completed")) {
                            pr.setShowCompleted(obj.getInt("show_completed"));
                            pr.save();
                        }
                    }

                    ActiveAndroid.setTransactionSuccessful();
                } catch (JSONException e) {
                    if (!obj.isNull("status") && !obj.isNull("errors")) {
                        try {
                            JSONObject erObj = obj.getJSONObject("errors");
                            if (erObj.getString("error").equals("Access denied")) {
                                if (callbackOutlineCompleteParse != null) {
                                    Integer i = new Integer(HttpParam.RESPONSE_ACCESS_DENIED);
                                    Message msg = callbackOutlineCompleteParse.obtainMessage(HttpParam.RESPONSE_ERROR, i);
                                    callbackOutlineCompleteParse.sendMessage(msg);
                                }
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        if (callbackOutlineCompleteParse != null) {
                            callbackOutlineCompleteParse.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                        }
                    }
                    return;
                } finally {
                    ActiveAndroid.endTransaction();
                }

                if (callbackOutlineCompleteParse != null) {
                    callbackOutlineCompleteParse.sendEmptyMessage(HttpParam.RESPONSE_OK);
                }
            }
        }).start();
    }

    public static void parseCompanies(JSONObject object) {
        obj = object;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GsonBuilder builder = new GsonBuilder();
                    builder.excludeFieldsWithoutExposeAnnotation();
                    Gson gson = builder.create();
                    Company.deleteAll(Company.class);
                    if (!obj.isNull("arCompanies")) {
                        Company[] arrCompany = gson.fromJson(obj.getString("arCompanies"), Company[].class);
                        for (Company jsonCompany : arrCompany) {
                            Company tempCompany = Company.findByServerId(Company.class, jsonCompany.getJsonServerId());

                            if (tempCompany == null) {
                                jsonCompany.setIsProjects(true);
                                jsonCompany.setServerId(jsonCompany.getJsonServerId());
                                jsonCompany.save();
                            } else {
                                jsonCompany.setIsProjects(true);
                                tempCompany.setAddress(jsonCompany.getAddress());
                                tempCompany.setAssigned_to(jsonCompany.getAssigned_to());
                                tempCompany.setBirthdate_company(jsonCompany.getBirthdate_company());
                                tempCompany.setComment(jsonCompany.getComment());
                                tempCompany.setComment2(jsonCompany.getComment2());
                                tempCompany.setIndustry(jsonCompany.getIndustry());
                                tempCompany.setIndustry_id(jsonCompany.getIndustry_id());
                                tempCompany.setName(jsonCompany.getName());
                                tempCompany.setOwner_team_id(jsonCompany.getOwner_team_id());
                                tempCompany.setOwner_user_id(jsonCompany.getOwner_user_id());
                                tempCompany.setUppername(jsonCompany.getUppername());
                                tempCompany.save();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void parseAttributes(JSONObject obj, long projectId) {
        try {
            if (!obj.isNull("arProjectDeals")) {
                ProjectDeals.clearTable(ProjectDeals.class);
                JSONArray array = obj.getJSONArray("arProjectDeals");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject o = array.getJSONObject(i);
                    ProjectDeals projectDeals = ProjectDeals.findOrCreate(ProjectDeals.class, o.getLong("id"));
                    projectDeals.setName(o.getString("name"));
                    projectDeals.setProjectId(o.getLong("project_id"));
                    projectDeals.setOwnerUserId(o.getLong("owner_user_id"));
                    projectDeals.setCreationDate(o.getString("creation_date"));
                    projectDeals.setAmount(o.getString("amount"));
                    projectDeals.setStage(o.getInt("status"));
                    projectDeals.setSort(o.getInt("sort"));
                    if (!o.isNull("actual_close_date")) {
                        projectDeals.setActualCloseDate(o.getString("actual_close_date"));
                    }
                    if (!o.isNull("close_date")) {
                        projectDeals.setCloseDate(o.getString("close_date"));
                    }
                    if (!o.isNull("stage")) {
                        projectDeals.setStage(o.getLong("stage"));
                    }
                    if (!o.isNull("company_id")) {
                        projectDeals.setCompanyId(o.getLong("company_id"));
                    }

                    projectDeals.save();
                }
            }

            if (!obj.isNull("assign_list")) {
                parseProjectAssigns(obj.getJSONArray("assign_list"), projectId);
            }

            if (!obj.isNull("arProjectStages")) {
                JSONArray array = obj.getJSONArray("arProjectStages");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject objStage = array.getJSONObject(i);
                    ProjectStage projectStage = ProjectStage.findOrCreate(ProjectStage.class, objStage.getLong("id"));
                    projectStage.setProjectId(objStage.getLong("project_id"));
                    projectStage.setName(objStage.getString("name"));
                    projectStage.setSort(objStage.getString("sort"));
                    projectStage.setPeriod(objStage.getString("period"));
                    projectStage.setLimitItem(objStage.getString("limit_item"));
                    projectStage.save();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //==========================================
    // MY TEAMS
    //==========================================
    public static void parseMyTeams(JSONObject jsonObject) throws JSONException {
        ActiveAndroid.beginTransaction();
        try {
            JSONArray arrayGroups = jsonObject.getJSONArray("groups");
            for (int i = 0; i < arrayGroups.length(); i++) {
                JSONObject obj = arrayGroups.getJSONObject(i);
                JSONObject team = obj.getJSONObject("arTeam");
                MyTeam myTeam = MyTeam.findOrCreate(MyTeam.class, team.getLong("id"));
                myTeam.setUserId(team.getLong("user_id"));
                myTeam.setTeamId(team.getLong("team_id"));
                myTeam.setIsActive(team.getInt("is_active"));
                myTeam.setName(team.getString("name"));
                myTeam.setUsername(team.getString("username"));
                myTeam.setEmail(team.getString("email"));
                myTeam.save();

                if (!obj.isNull("obProjects")) {
                    JSONArray arrayProjects = obj.getJSONArray("obProjects");
                    for (int j = 0; j < arrayProjects.length(); j++) {
                        JSONObject project = arrayProjects.getJSONObject(j);
                        MyTeamProject myTeamProject = MyTeamProject.findOrCreate(MyTeamProject.class, project.getLong("id"));
                        myTeamProject.setTeamId(myTeam.getServerId());
                        myTeamProject.setUserId(project.getLong("user_id"));
                        myTeamProject.setProjectId(project.getLong("project_id"));
                        myTeamProject.setRightToView(project.getInt("right_to_view"));
                        myTeamProject.setRightToEdit(project.getInt("right_to_edit"));
                        myTeamProject.setRightToEditProject(project.getInt("right_to_edit_project"));
                        myTeamProject.setRightToInvite(project.getInt("right_to_invite"));
                        myTeamProject.setName(project.getString("name"));
                        myTeamProject.save();
                    }
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }

    }

    public static ArrayList<InviteProject> parseInviteProjects(JSONObject jsonObject) throws JSONException {
        ArrayList<InviteProject> inviteProjects = new ArrayList<>();
        ActiveAndroid.beginTransaction();
        try {
            JSONArray arrayGroups = jsonObject.getJSONArray("projects");
            for (int i = 0; i < arrayGroups.length(); i++) {
                JSONObject obj = arrayGroups.getJSONObject(i);
                InviteProject myTeam = new InviteProject();
                myTeam.setValue(obj.getString("id"));
                myTeam.setName(obj.getString("name"));
                inviteProjects.add(myTeam);
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return inviteProjects;
    }

    public static ArrayList<Invite> parseMyInviteList(JSONObject jsonObject) throws JSONException {
        ArrayList<Invite> invites = new ArrayList<>();
        ActiveAndroid.beginTransaction();
        try {
            JSONArray arrayGroups = jsonObject.getJSONArray("invites");
            for (int i = 0; i < arrayGroups.length(); i++) {
                JSONObject obj = arrayGroups.getJSONObject(i);

                JSONObject objInvite = obj.getJSONObject("invite");
                JSONObject objTeam = obj.getJSONObject("team");
                JSONObject objProject = obj.getJSONObject("project");

                Invite invite = new Invite();
                invite.setId(objInvite.getLong("id"));
                invite.setOwnerUserId(objInvite.getLong("owner_user_id"));
                invite.setTeamId(objInvite.getLong("team_id"));
                invite.setProjectId(objInvite.getLong("project_id"));
                if (!objInvite.isNull("user_id")) {
                    invite.setUserId(objInvite.getLong("user_id"));
                }

                invite.setEmail(objInvite.getString("email"));
                invite.setStatus(objInvite.getString("status"));
                invite.setCreatedAtText(objInvite.getString("created_at"));
                invite.setUpdatedAtText(objInvite.getString("updated_at"));
                invite.setCreatedA(ManagerCalendar.convertStringToCalendar(invite.getCreatedAtText()));
                invite.setUpdateAt(ManagerCalendar.convertStringToCalendar(invite.getUpdatedAtText()));

                invite.setNameProject(objProject.getString("name"));
                invite.setNameTeam(objTeam.getString("name"));

                invites.add(invite);
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return invites;
    }


}