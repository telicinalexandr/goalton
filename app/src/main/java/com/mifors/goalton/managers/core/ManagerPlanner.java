package com.mifors.goalton.managers.core;

import android.os.Handler;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.managers.utils.ManagerNetHelper;
import com.mifors.goalton.managers.utils.ManagerParseLine;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.Api.ApiPlanner;
import com.mifors.goalton.utils.calendar.PlannerFilter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mifors.goalton.network.HttpParam.getCsrfToken;

/**
 * Created by gly8 on 21.08.17.
 */

@SuppressWarnings("ALL")
public class ManagerPlanner extends ManagerNetHelper
{
    private static final String TAG = "Goalton ["+ ManagerPlanner.class.getSimpleName()+"]";
    private static PlannerFilter plannerFilter;
    private static int heightItemLine = Integer.MAX_VALUE;
    private static int heightFloatinActionBar = 0;
    private static int widthItemCalendar =0;
    private static int heightSteepMinute = 0;

    public static void responseSetColor(final long itemServerId, String color, final Handler callback) {
        ApiPlanner apiInterface = ApiModule.getApiCalendar();
        apiInterface.updateColor(getCsrfToken(), itemServerId, color).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                responseOkSendObject(callback, new ManagerNet().readResponse(response));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    // Вызывается после авторизации пользователя для загрузки задач календаря
    public static void responseGetDataCalendarLoginUser() {
        Calendar calendar = Calendar.getInstance();
        ManagerPlanner.responseGetPlannerData(calendar.get(Calendar.WEEK_OF_YEAR), calendar.get(Calendar.YEAR), null);
        ManagerPlanner.responseGetPlannerData((calendar.get(Calendar.WEEK_OF_YEAR)+1), calendar.get(Calendar.YEAR), null);
        ManagerPlanner.responseGetPlannerData((calendar.get(Calendar.WEEK_OF_YEAR)-1), calendar.get(Calendar.YEAR), null);
    }

    public static void responseGetPlannerData(final int weekYear, final int year, final Handler callback) {
        try{
            ApiPlanner apiInterface = ApiModule.getApiCalendar();
            apiInterface.getDataPlanning(getCsrfToken(), weekYear, year, "2","").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> r) {
                    String data = new ManagerNet().readResponse(r);
                    try {
                        JSONObject object = new JSONObject(data);
                        if (ManagerJSONParsing.isCheckStatusServerResponse(object)) {
                            new ManagerParseLine().parsePlanner(object, weekYear, year);
                            responseOkSendObject(callback, weekYear);
                        } else {
                            responseError(callback);
                        }
                    } catch (JSONException  e) {
                        Log.v(TAG, "[responseGetPlannerData] data - "+data);
                        responseError(callback);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public static void responseSetDuration(long itemId, int duration, final Handler callback) {
        ApiPlanner apiInterface = ApiModule.getApiCalendar();
        apiInterface.setDuration(getCsrfToken(), itemId, duration).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                responseOk(callback);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public static void responseSetAssign(long lineId, long assignUser, final Handler callback) {
        ApiPlanner apiInterface = ApiModule.getApiCalendar();
        apiInterface.setAssign(getCsrfToken(), lineId, assignUser).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                checkResultNotParseDataSendCallback(callback, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public static void responseSetCompany(long lineId, long companyId, final Handler callback) {
        ApiPlanner apiInterface = ApiModule.getApiCalendar();
        apiInterface.setCompany(getCsrfToken(), lineId, companyId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                responseOk(callback);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public static void responseDeleteStageItem(long itemId, final Handler callback){
        ApiPlanner apiInterface = ApiModule.getApiCalendar();
        apiInterface.deleteStageItem(getCsrfToken(), itemId, "item").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                checkResultNotParseDataSendCallback(callback, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public static void responseUpdateStageItem(long itemId, long stageId, String isComplete, final Handler callback) {
        ApiPlanner apiInterface = ApiModule.getApiCalendar();
        apiInterface.updateStageItem(getCsrfToken(), itemId, "item", stageId, isComplete, "1", "1").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                responseOk(callback);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    //==========================================
    // Geter setter
    //==========================================
    public static PlannerFilter getFilter() {
        if (plannerFilter == null){
            plannerFilter = new PlannerFilter();
        }
        return plannerFilter;
    }

    public static int getHeightItemLine() {
        if (heightItemLine == Integer.MAX_VALUE || heightItemLine == 0) {
            heightItemLine = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.HEIGHT_ITEM_LINE, Integer.MAX_VALUE);
        }
        return heightItemLine;
    }

    public static void saveHeightItem(int height) {
        if (heightItemLine == Integer.MAX_VALUE || heightItemLine == 0) {
            heightItemLine = height;
            ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.HEIGHT_ITEM_LINE, height);
        }
    }

    public static int getHeightFloatingActionBar(){
        if (heightFloatinActionBar == 0) {
            heightFloatinActionBar = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.HEIGHT_FLOATING_ACTION_BUTTON, 0);
        }
        return heightFloatinActionBar;
    }

    public static int getHeightFloatingActionBar50Percentage(){
        if (heightFloatinActionBar == 0) {
            heightFloatinActionBar = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.HEIGHT_FLOATING_ACTION_BUTTON, 0);
        }

        if (heightFloatinActionBar > 0) {
            return heightFloatinActionBar/2;
        }

        return heightFloatinActionBar;
    }

    public static void saveHeightFloatingActionBar(int height){
        if (heightFloatinActionBar == 0) {
            heightFloatinActionBar = height;
            ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.HEIGHT_FLOATING_ACTION_BUTTON, height);
        }
    }

    public static void saveWidthItemCalendar(int width){
        if (widthItemCalendar == 0) {
            widthItemCalendar = width;
            ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.WIDTH_ITEM_CALENDAR, width);
        }
    }

    public static int getWidthItemCalendar(){
        if (widthItemCalendar == 0) {
            widthItemCalendar = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.WIDTH_ITEM_CALENDAR, 0);
        }
        return widthItemCalendar;
    }

    public static int getHeightSteepMinute() {
        if (heightSteepMinute == 0) {
            heightSteepMinute = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.HEIGHT_STEEP_MINUTE, 0);
        }
        return heightSteepMinute;
    }

    public static void setHeightSteepMinute(int height) {
        if (heightSteepMinute == 0) {
            heightSteepMinute = height;
            ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.HEIGHT_STEEP_MINUTE, height);
        }
    }
}