package com.mifors.goalton.managers.core;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import com.mifors.goalton.DebugKeys;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.managers.utils.ManagerNetHelper;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectRight;
import com.mifors.goalton.model.project.ProjectRightUser;
import com.mifors.goalton.model.project.ProjectStage;
import com.mifors.goalton.model.project.ProjectSubject;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.Api.ApiProjectSettings;
import com.mifors.goalton.network.CookiesRetrofit;
import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gly8 on 30.03.17.
 */

@SuppressWarnings("ALL")
public class ManagerProjects {

    private static final String TAG = "Goalton [" + ManagerProjects.class.getSimpleName() + "]";

    // Получение всех проектов
    public static void responseGetProjects(final Handler callback) {
        ApiProjectSettings apiInterface = ApiModule.getApiProjectSettings();
        apiInterface.getProjects().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                try {
                    managerNet.checkReponse(new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            if (msg.what == HttpParam.RESPONSE_OK) {
                                if (msg.obj != null) {
                                    JSONObject obj = (JSONObject) msg.obj;
                                    Project.deleteAll(Project.class);
                                    ManagerJSONParsing.parseProjects(obj);
                                    if (callback != null) {
                                        callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
                                    }
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            ManagerOutline.responseGetCompanies();
                                        }
                                    }, 1000);

                                }
                            } else {
//                                Crashlytics.logException(new Exception("responseGetProjects onCompleted RESPONSE_ERROR"));
                                if (callback != null) {
                                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                                }
                            }
                        }
                    }, response);
                } catch (JSONException e) {

//                    if (TextUtils.isEmpty(result)) {
//                        Crashlytics.logException(new Exception("responseGetProjects  error parse projects"));
//                    } else if (result.length() > 10) {
//                        Crashlytics.logException(new Exception("responseGetProjects  error parse projects - " + result.substring(0, 9)));
//                    } else {
//                        Crashlytics.logException(new Exception("responseGetProjects  error parse projects"));
//                    }

                    e.printStackTrace();
                    if (callback != null) {
                        callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Crashlytics.logException(new Exception("responseGetProjects  onFailure " + t.getMessage()));
                t.printStackTrace();
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    // Получение настроек проекта
    public static void responseGetSettingsProject(final Handler callback, final long idProject) {
        ApiProjectSettings apiInterface = ApiModule.getApiProjectSettings();
        apiInterface.getProjectSettings(String.valueOf(idProject)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                try {
                    ProjectStage.deleteAllByProject(idProject);
                    JSONObject obj = managerNet.getResultFromResponse(response);
                    ManagerJSONParsing.parseProjectSetting(obj);
                    if (callback != null) {
                        callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Crashlytics.logException(new Exception("responseGetSettingsProject  onFailure - " + t.getMessage()));
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    // Удаление проекта
    public static void responseDeleteProject(final Handler callback, final long projectId) {
        ApiProjectSettings apiInterface = ApiModule.getApiProjectSettings();
        apiInterface.deleteProjectSettings(String.valueOf(projectId), HttpParam.getCsrfToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                try {
                    if (ManagerJSONParsing.isCheckStatusServerResponse(managerNet.readResponse(response))) {
                        deleteProjectByProjectId(projectId);
                        if (callback != null) {
                            callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
                        }
                    } else {
                        if (callback != null) {
                            callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (callback != null) {
                        callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Crashlytics.logException(new Exception("responseDeleteProject  onFailure - " + t.getMessage()));
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    // Удаление проекта а также его настроек если были скачаны
    public static void deleteProjectByProjectId(long projectId) {
        Project.deleteByServerId(Project.class, projectId);
        ProjectRight.deleteByKeyAndValue(ProjectRight.class, "parent_project_id", projectId);
        ProjectRightUser.deleteByKeyAndValue(ProjectRightUser.class, "parent_project_id", projectId);
        ProjectStage.deleteByKeyAndValue(ProjectStage.class, "parent_project_id", projectId);
        ProjectSubject.deleteByKeyAndValue(ProjectSubject.class, "parent_project_id", projectId);
    }

    // Обновление настроек проекта
    public static void responseUpdateSettings(MultipartBody.Builder buidler, long projectId, Handler callback) {
        String url = HttpParam.PROTO_URL_FULL + "/project/update/" + projectId;
        new AsyncTaskUpdateProjectSettings(url, buidler, projectId, callback).execute();
    }

    // Делает запрос на обновление данных
    static class AsyncTaskUpdateProjectSettings extends AsyncTask<Void, Void, String> {

        private String url;
        private MultipartBody.Builder buidler;
        private long projectId;
        private Handler callback;

        public AsyncTaskUpdateProjectSettings(String url, MultipartBody.Builder buidler, long projectId, Handler callback) {
            this.url = url;
            this.buidler = buidler;
            this.projectId = projectId;
            this.callback = callback;
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                if (DebugKeys.isShowLogsRetrofit) {
                    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                } else {
                    interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
                }

                OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
                OkHttpClient client = clientBuilder
                        .connectTimeout(30000, TimeUnit.MILLISECONDS)
                        .cookieJar(new CookiesRetrofit())
                        .addInterceptor(interceptor)
                        .build();
                RequestBody requestBody = buidler.build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(requestBody)
                        .build();

                ManagerNet managerNet = new ManagerNet();
                okhttp3.Response response = client.newCall(request).execute();
                result = managerNet.readResultFromResponse(response.body());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (ManagerJSONParsing.isCheckStatusServerResponse(result)) {
                // Смотрим количество новых созданных этапов в базе
                int countNewStages = ProjectStage.countNewStagesToProject(projectId);
                if (countNewStages > 0) {
                    // Этапы пришли в JSON удаляем старые из базы
                    // делаем запрос к серверу на обновление настроек проекта
                    List<ProjectStage> list = ProjectStage.getAllNewStages(projectId);
                    for (ProjectStage st : list) {
                        st.delete();
                    }
                    responseGetSettingsProject(new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            ManagerApplication.getInstance().showToast(R.string.update_data_complete);
                            ManagerProgressDialog.dismiss();
                            if (callback != null) {
                                callback.sendEmptyMessage(0);
                            }
                        }
                    }, projectId);

                } else {
                    try {
                        JSONObject obj = new JSONObject(result);
                        ManagerJSONParsing.parseProject(obj.getJSONObject("project"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (callback != null) {
                        callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
                    }
                    ManagerProgressDialog.dismiss();
                }


            } else {
                ManagerApplication.getInstance().showToast(R.string.update_data_error);
                ManagerProgressDialog.dismiss();
            }
        }
    }

    public static void responseCreateProject(MultipartBody.Builder buidler, Handler callback) {
        String url = HttpParam.PROTO_URL_FULL + "/project/create";
        new AsyncTaskCreateProject(url, buidler, callback).execute();
    }

    // Fixme REFACTOR: объеденить два класса в один  AsyncTaskUpdateProjectSettings and AsyncTaskCreateProject
    static class AsyncTaskCreateProject extends AsyncTask<Void, Void, String> {
        private String url;
        private MultipartBody.Builder buidler;
        private Handler callback;
        private String result = "";

        public AsyncTaskCreateProject(String url, MultipartBody.Builder buidler, Handler callback) {
            this.url = url;
            this.buidler = buidler;
            this.callback = callback;
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

                OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
                OkHttpClient client = clientBuilder
                        .connectTimeout(30000, TimeUnit.MILLISECONDS)
                        .cookieJar(new CookiesRetrofit())
                        .addInterceptor(interceptor)
                        .build();
                RequestBody requestBody = buidler.build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(requestBody)
                        .build();

                ManagerNet managerNet = new ManagerNet();
                okhttp3.Response response = client.newCall(request).execute();
                result = managerNet.readResultFromResponse(response.body());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                try {
                    if (ManagerJSONParsing.isCheckStatusServerResponse(new JSONObject(result))) {
                        responseGetProjects(new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                super.handleMessage(msg);
                                if (callback != null) {
                                    callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
                                }
                            }
                        });
                    } else {
                        if (callback != null) {
                            callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
                }
            }
        }
    }

    // Удаление нового проекта у которого нет ServerId
    public static void deleteLastNewProject() {
        Project project = Project.findByServerId(Project.class, 0);
        if (project != null) {
            ProjectStage.deleteAllByProjectNewItems(0);
            ProjectSubject.deleteAllByProject(0);
        }
    }

    public static void saveSelectedProjects(long selectedProjectId) {
        ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_SELECTED, selectedProjectId);
    }

    public static long getSelectedProject() {
        return ManagerApplication.getInstance().getSharedManager().getValueLong(SharedKeys.ID_PROJECT_SELECTED);
    }
}