package com.mifors.goalton.managers.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;


/**
 * Created by gly8 on 19.01.16.
 */
@SuppressWarnings("ALL")
public class ManagerAlertDialog {
    private static AlertDialog dialog;

    public static void showDialog(Context context, int resourceMessage) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(resourceMessage)
                .setPositiveButton("OK", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .create()
                .show();
    }

    public static void showDialog(Context context, String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(message)
                .setPositiveButton("OK", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(false)
                .create()
                .show();
    }

    public static void showDialog(Context context, int messageResource, DialogInterface.OnDismissListener dismissListener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(messageResource)
                .setPositiveButton("OK", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create()
                .setOnDismissListener(dismissListener);
        builder.show();
    }

    public static void showDialog(Context context, int messageResource, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(messageResource)
                .setPositiveButton("OK", onClickListener)
                .setIcon(android.R.drawable.ic_dialog_alert).create().show();
    }


    public static void showDialog(Context context, int messageResource, int resTitleBtnOk, int resTitleBtnCancle,
                                  DialogInterface.OnClickListener clickOnPositiove, DialogInterface.OnClickListener clickOnNigative) {
        try {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder.setMessage(messageResource)
                    .setPositiveButton(resTitleBtnOk, clickOnPositiove)
                    .setNegativeButton(resTitleBtnCancle, clickOnNigative)
                    .setIcon(android.R.drawable.ic_dialog_alert).create().show();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    public static void showDialog(Context context, String messageResource, String resTitleBtnOk, String resTitleBtnCancle,
                                  DialogInterface.OnClickListener clickOnPositiove, DialogInterface.OnClickListener clickOnNigative) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(messageResource)
                .setPositiveButton(resTitleBtnOk, clickOnPositiove)
                .setNegativeButton(resTitleBtnCancle, clickOnNigative)
                .setIcon(android.R.drawable.ic_dialog_alert).create().show();
    }

    public static void showDialog(Context context, int messageResource, int resTitleBtnOk, int resTitleBtnCancle, int resNetral,
                                  DialogInterface.OnClickListener clickOnPositiove, DialogInterface.OnClickListener clickOnNigative, boolean cancelable) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(messageResource)
                .setPositiveButton(resTitleBtnOk, clickOnPositiove)
                .setNegativeButton(resTitleBtnCancle, clickOnNigative)
                .setCancelable(cancelable)
                .setIcon(android.R.drawable.ic_dialog_alert).create().show();
    }

    public static void showDialog(Context context, String message, int resTitleBtnOk, int resTitleBtnCancle,
                                  DialogInterface.OnClickListener clickOnPositiove, DialogInterface.OnClickListener clickOnNigative) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }
        builder.setMessage(message)
                .setPositiveButton(resTitleBtnOk, clickOnPositiove)
                .setNegativeButton(resTitleBtnCancle, clickOnNigative)
                .setIcon(android.R.drawable.ic_dialog_alert).create().show();
    }

    //==========================================
    // EDIT TEXT
    //==========================================

    /**
     * Показывает EditText в диалогововм окне
     *
     * @param context         - контекст в котором показывается окно
     * @param lastText        - текст который надо вставить в EditText
     * @param title           - ресурс заголовка в строках
     * @param resBtnOk        - ресуср кнопки готово
     * @param resBtnNegative  - ресурс кнопки отмены
     * @param clickOnPositive - обработчик нажатия готово
     * @param clickOnNegative - обработчик нажатия отмена
     * @return EditText - возвращает EditText из диалогового овна для дальнейшей работы после подтверждения или отмены
     */
    public static EditText showEditTextDialogNumber(Activity context,
                                                    String lastText,
                                                    int title,
                                                    int resBtnOk,
                                                    int resBtnNegative,
                                                    DialogInterface.OnClickListener clickOnPositive,
                                                    DialogInterface.OnClickListener clickOnNegative) {
        return showEditTextDialog(context, lastText, InputType.TYPE_CLASS_NUMBER, title, resBtnOk, resBtnNegative, clickOnPositive, clickOnNegative, null);
    }

    /**
     * Показывает EditText в диалогововм окне
     *
     * @param context         - контекст в котором показывается окно
     * @param lastText        - текст который надо вставить в EditText
     * @param title           - ресурс заголовка в строках
     * @param resBtnOk        - ресуср кнопки готово
     * @param resBtnNegative  - ресурс кнопки отмены
     * @param clickOnPositive - обработчик нажатия готово
     * @param clickOnNegative - обработчик нажатия отмена
     * @return EditText - возвращает EditText из диалогового овна для дальнейшей работы после подтверждения или отмены
     */
    public static EditText showEditTextDialogText(Activity context,
                                                  String lastText,
                                                  int title,
                                                  int resBtnOk,
                                                  int resBtnNegative,
                                                  DialogInterface.OnClickListener clickOnPositive,
                                                  DialogInterface.OnClickListener clickOnNegative) {
        return showEditTextDialog(context, lastText, InputType.TYPE_CLASS_TEXT, title, resBtnOk, resBtnNegative, clickOnPositive, clickOnNegative, null);
    }

    /**
     * Показывает EditText в диалогововм окне
     *
     * @param context         - контекст в котором показывается окно
     * @param lastText        - текст который надо вставить в EditText
     * @param title           - ресурс заголовка в строках
     * @param resBtnOk        - ресуср кнопки готово
     * @param resBtnNegative  - ресурс кнопки отмены
     * @param clickOnPositive - обработчик нажатия готово
     * @param clickOnNegative - обработчик нажатия отмена
     * @return EditText - возвращает EditText из диалогового овна для дальнейшей работы после подтверждения или отмены
     */
    public static EditText showEditTextDialog(Activity context,
                                              String lastText,
                                              int title,
                                              int resBtnOk,
                                              int resBtnNegative,
                                              DialogInterface.OnClickListener clickOnPositive,
                                              DialogInterface.OnClickListener clickOnNegative,
                                              DialogInterface.OnDismissListener dismissListener) {
        return showEditTextDialog(context, lastText, InputType.TYPE_CLASS_TEXT, title, resBtnOk, resBtnNegative, clickOnPositive, clickOnNegative, dismissListener);
    }

    public static EditText showEditTextDialog(Activity context,
                                              String lastText,
                                              int title,
                                              int resBtnOk,
                                              int resBtnNegative,
                                              DialogInterface.OnClickListener clickOnPositive) {
        return showEditTextDialog(context, lastText, title, resBtnOk, resBtnNegative, clickOnPositive, null, null);
    }

    /**
     * Показывает EditText в диалогововм окне
     *
     * @param context           - контекст в котором показывается окно
     * @param lastText          - текст который надо вставить в EditText
     * @param typeInputEditText - тип поля (текст, число...)
     * @param title             - ресурс заголовка в строках
     * @param resBtnOk          - ресуср кнопки готово
     * @param resBtnNegative    - ресурс кнопки отмены
     * @param clickOnPositive   - обработчик нажатия готово
     * @return EditText - возвращает EditText из диалогового овна для дальнейшей работы после подтверждения или отмены
     */
    public static EditText showEditTextDialog(Activity context,
                                              String lastText,
                                              int typeInputEditText,
                                              int title,
                                              int resBtnOk,
                                              int resBtnNegative,
                                              DialogInterface.OnClickListener clickOnPositive) {
        return showEditTextDialog(context, lastText, typeInputEditText, title, resBtnOk, resBtnNegative, clickOnPositive, null, null);
    }

    /**
     * Показывает EditText в диалогововм окне
     *
     * @param context           - контекст в котором показывается окно
     * @param lastText          - текст который надо вставить в EditText
     * @param typeInputEditText - тип поля (текст, число...)
     * @param title             - ресурс заголовка в строках
     * @param resBtnOk          - ресуср кнопки готово
     * @param resBtnNegative    - ресурс кнопки отмены
     * @param clickOnPositive   - обработчик нажатия готово
     * @param clickOnNegative   - обработчик нажатия отмена
     * @return EditText - возвращает EditText из диалогового овна для дальнейшей работы после подтверждения или отмены
     */
    public static EditText showEditTextDialog(final Activity context,
                                              String lastText,
                                              int typeInputEditText,
                                              int title,
                                              int resBtnOk,
                                              int resBtnNegative,
                                              final DialogInterface.OnClickListener clickOnPositive,
                                              DialogInterface.OnClickListener clickOnNegative,
                                              DialogInterface.OnDismissListener dismissListener) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(context);
        }

        LayoutInflater inflater = context.getLayoutInflater();
        View dialogView;

        if ((typeInputEditText & InputType.TYPE_CLASS_NUMBER) == InputType.TYPE_CLASS_NUMBER) {
            dialogView = inflater.inflate(R.layout.dialog_edit_number, null);
        } else {
            dialogView = inflater.inflate(R.layout.dialog_edit_text, null);
        }

        final EditText editText = dialogView.findViewById(R.id.edit_project_name_stage);
        if (!TextUtils.isEmpty(lastText)) {
            editText.setText(lastText);
        }

        if ((typeInputEditText & InputType.TYPE_CLASS_NUMBER) == InputType.TYPE_CLASS_NUMBER) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            editText.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
        } else {
            editText.setInputType(typeInputEditText);
        }


        builder.setView(dialogView);
        builder.setTitle(title);
        builder.setPositiveButton(resBtnOk, clickOnPositive);
        builder.setNegativeButton(resBtnNegative, clickOnNegative);
        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.setOnDismissListener(dismissListener);
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText() != null) {
                    if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
                        clickOnPositive.onClick(dialog, AlertDialog.BUTTON_POSITIVE);
                        dialog.dismiss();
                    } else {
                        ManagerApplication.getInstance().showToast(R.string.enter_the_data);
                    }
                }

            }
        });
        return editText;
    }

    public static void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
