package com.mifors.goalton.managers.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.ArrayList;

public class ManagerShared {
    private Context context;
    private SharedPreferences settings;
    private final String DIVIDER_ARRAY = "::::::::::::";
    public ManagerShared(Context c) {
        context = c;
        setSettings(context.getSharedPreferences("mode", Context.MODE_PRIVATE));
    }

    public void putKeyString(String key, String value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void putKeyBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void putKeyInteger(String key, int value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public String getValueString(String key) {
        return getSettings().getString(key, "");
    }

    public boolean getValueBoolean(String key) {
        return getSettings().getBoolean(key, false);
    }

    public int getValueInteger(String key) {
        return getValueInteger(key, 0);
    }

    public int getValueInteger(String key, int returnValue) {
        return getSettings().getInt(key, returnValue);
    }

    public void removeKey(String key) {
        if (getSettings().contains(key)) {
            SharedPreferences.Editor editor = getSettings().edit();
            editor.remove(key);
            editor.apply();
        }
    }

    public void putKeyLong(String key, long value) {
        SharedPreferences.Editor editor = getSettings().edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public long getValueLong(String key) {
        return getSettings().getLong(key, 0);
    }

    public SharedPreferences getSettings() {
        if (settings == null) {
            settings = context.getSharedPreferences("mode", Context.MODE_PRIVATE);
        }
        return settings;
    }

    public void setSettings(SharedPreferences settings) {
        this.settings = settings;
    }

    public void saveArrayLong(ArrayList<Long> arrayList, String key) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arrayList.size(); i++) {
            builder.append(arrayList.get(i));
            builder.append(DIVIDER_ARRAY);
        }
        putKeyString(key, builder.toString());
    }

    public ArrayList<Long> getArrayLong(String key) {
        ArrayList<Long> arrayList = new ArrayList<>();
        String stringArray = getValueString(key);
        if (!TextUtils.isEmpty(stringArray)) {
            String[] arrStr = stringArray.split(DIVIDER_ARRAY);
            for (String item: arrStr){
                if (!TextUtils.isEmpty(item)) {
                    arrayList.add(Long.parseLong(item));
                }
            }
        }
        return arrayList;
    }

}