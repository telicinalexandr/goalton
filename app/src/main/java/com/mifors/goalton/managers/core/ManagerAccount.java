package com.mifors.goalton.managers.core;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivityMain;
import com.mifors.goalton.fragment.projects.FragmentMyProjects;
import com.mifors.goalton.interfaces.InterfaceCallbackResponseServer;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.managers.utils.ManagerNetHelper;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.Company;
import com.mifors.goalton.model.myteams.MyTeam;
import com.mifors.goalton.model.myteams.MyTeamProject;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectAssign;
import com.mifors.goalton.model.project.ProjectRight;
import com.mifors.goalton.model.project.ProjectRightUser;
import com.mifors.goalton.model.project.ProjectStage;
import com.mifors.goalton.model.project.ProjectSubject;
import com.mifors.goalton.model.team.Team;
import com.mifors.goalton.model.team.TeamAccount;
import com.mifors.goalton.model.team.TeamPermissionProfile;
import com.mifors.goalton.model.team.TeamProfile;
import com.mifors.goalton.model.team.TeamProject;
import com.mifors.goalton.model.user.Profile;
import com.mifors.goalton.network.Api.ApiInterface;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.utils.calendar.PlannerFilter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mifors.goalton.SharedKeys.NAME_SELECTED_BG;
import static com.mifors.goalton.network.HttpParam.getCsrfToken;

/**
 * Created by gly8 on 20.03.17.
 */
@SuppressWarnings("ALL")
public class ManagerAccount extends ManagerNetHelper {
    private static final String TAG = "Goalton [" + ManagerAccount.class.getSimpleName() + "]";

    // Авторизован ли пользователь
    public static boolean isLogin() {
        return ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.IS_LOGIN_USER);
    }

    public static Profile getMyProfile() {
        return Profile.getMyProfile();
    }

    /**
     * Выход из профиля стирание текущей сессии
     */
    public static void logout() {
        ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.IS_LOGIN_USER, false);
    }

    public static void logoutFull() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ActiveAndroid.beginTransaction();
                try {
                    TeamPermissionProfile.clearTable(TeamPermissionProfile.class);
                    TeamProject.clearTable(TeamProject.class);
                    TeamProfile.clearTable(TeamProfile.class);
                    TeamAccount.clearTable(TeamAccount.class);
                    Team.clearTable(Team.class);
                    MyTeamProject.clearTable(MyTeamProject.class);
                    MyTeam.clearTable(MyTeam.class);
                    Profile.clearTable(Profile.class);
                    Line.clearTable(Line.class);
                    Company.clearTable(Company.class);
                    ProjectRight.clearTable(ProjectRight.class);
                    ProjectRightUser.clearTable(ProjectRightUser.class);
                    ProjectStage.clearTable(ProjectStage.class);
                    ProjectSubject.clearTable(ProjectSubject.class);
                    Project.clearTable(Project.class);
                    Team.clearTable(Team.class);
                    TeamAccount.clearTable(TeamAccount.class);
                    TeamPermissionProfile.clearTable(TeamPermissionProfile.class);
                    TeamProfile.clearTable(TeamProfile.class);
                    TeamProject.clearTable(TeamProject.class);
                    ActiveAndroid.setTransactionSuccessful();
                } finally {
                    ActiveAndroid.endTransaction();
                }

                // Очистка shared manager
                // Удаление профилей из базы
                ManagerApplication.getInstance().getSharedManager().removeKey("_csrf");
                ManagerApplication.getInstance().getSharedManager().removeKey("CurLanguageCode");
                ManagerApplication.getInstance().getSharedManager().removeKey("PHPSESSID");
                ManagerApplication.getInstance().getSharedManager().removeKey("login");
                ManagerApplication.getInstance().getSharedManager().removeKey("_csrf_shared");
                ManagerApplication.getInstance().getSharedManager().removeKey("password");
                ManagerApplication.getInstance().getSharedManager().removeKey("NAME_SELECTED_BG");
                ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.TYPE_PROJECT_SORT, FragmentMyProjects.TypeSort.DATE.getType());
                ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE, true);
                PlannerFilter.reset();
                ActivityMain.IS_LOAD_PROJECTS_RUN_SESSION = false;
            }
        }).run();
    }

    //==========================================
    // Регистрация Авторизация
    //==========================================
    public static void relogin(Handler handler) {
        ManagerApplication.getInstance().getSharedManager().putKeyString(HttpParam._CSRF, "");
        ManagerApplication.getInstance().getSharedManager().putKeyString(HttpParam._CSRF_GENERATE_SERVER, "");

        String username = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.LOGIN);
        String password = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.PASSWORD);
        login(username, password, handler);
    }

    /**
     * Авторизация пользователя
     *
     * @param username - имя пользователя
     * @param password - пароль пользователя
     * @param callback - вызывается после завершения запроса
     */
    public static void login(final String username, final String password, final Handler callback) {
        init(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                responseLogin(username, password, callback);
            }
        });
    }

    private static void responseLogin(final String username, final String password, final Handler callback) {
        ApiInterface apiInterface = ApiModule.getApiInterface();
        apiInterface.login(getCsrfToken(), username, password, "0").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String result = (new ManagerNet()).readResponse(response);
                Crashlytics.setUserIdentifier(username);
                ManagerProgressDialog.dismiss();
                switch (response.code()) {
                    case 200:

                        if (callback != null) {
                            ManagerPlanner.responseGetDataCalendarLoginUser();
                            ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.NOTIFICATIONS_SOUND, true);
                            ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.NOTIFICATIONS_VIBRO, true);
                            ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.PASSWORD, password);
                            ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.LOGIN, username);
                            PlannerFilter.reset();
                            ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.TYPE_PROJECT_SORT, FragmentMyProjects.TypeSort.DATE.getType());
                            ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE, true);
                            callback.sendMessage(callback.obtainMessage(HttpParam.RESPONSE_OK, result));
                        }
                        break;
                    case 400:
                        if (callback != null) {
                            callback.sendMessage(callback.obtainMessage(HttpParam.RESPONSE_ERROR, response.errorBody().toString()));
                        }
                        break;
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Crashlytics.logException(new Exception("responseLogin  onFailure - "+t.getMessage()));
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    private static String typeSocial, token, userId;

    public static void responseLoginSocial(String tSocial, String tok, String userID, final String email, final String name, final String firstName, final String lastName, final Handler callback) {
        typeSocial = tSocial;
        token = tok;
        userId = userID;
        init(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                String md5HashStr = HttpParam.getCsrfToken() + "#" + typeSocial + "#" + userId;
                String md5Hash = ManagerApplication.md5(md5HashStr);
                ApiModule.getApiInterface().loginSocial(HttpParam.getCsrfToken(),
                        HttpParam.getCsrfToken(),
                        typeSocial,
                        String.valueOf(userId),
                        token,
                        md5Hash,
                        email,
                        name,
                        firstName,
                        lastName).enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (ManagerNetHelper.checkResultResponse(response)) {
                            responseGetDataUser(new Handler() {
                                @Override
                                public void handleMessage(Message msg) {
                                    if (msg.what == HttpParam.RESPONSE_OK) {
                                        responseOk(callback);
                                    } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                                        responseError(callback);
                                    }
                                }
                            });
                        } else {
                            responseError(callback);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });
    }

    public static void register(final String username, final String email, final String password, final InterfaceCallbackResponseServer interfaceCallbackResponseServer) {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ApiInterface apiInterface = ApiModule.getApiInterface();
            apiInterface.register(getCsrfToken(), username, email, password).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String str = new ManagerNet().readResponse(response);
                        if (ManagerJSONParsing.isCheckStatusServerResponse(str, interfaceCallbackResponseServer)) {
                            if (interfaceCallbackResponseServer != null) {
                                interfaceCallbackResponseServer.completeResponse(InterfaceCallbackResponseServer.KEY_END_RESPONSE, "");
                            }
                        } else {
                            if (interfaceCallbackResponseServer != null) {
                                interfaceCallbackResponseServer.completeResponse(InterfaceCallbackResponseServer.KEY_ERROR_RESPONSE, "");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    Crashlytics.logException(new Exception("register  onFailure - "+t.getMessage()));
                    if (interfaceCallbackResponseServer != null) {
                        interfaceCallbackResponseServer.completeResponse(InterfaceCallbackResponseServer.KEY_ERROR_RESPONSE, "");
                    }
                }
            });
        }
    }

    public static void remindPassword(final String email, final InterfaceCallbackResponseServer interfaceCallbackResponseServer) {
        init(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                responseRemindPassword(email, interfaceCallbackResponseServer);
            }
        });
    }

    private static void responseRemindPassword(final String email, final InterfaceCallbackResponseServer interfaceCallbackResponseServer) {
        ApiInterface apiInterface = ApiModule.getApiInterface();
        apiInterface.remindPassword(getCsrfToken(), email).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    String str = new String(response.body().bytes());
                    if (ManagerJSONParsing.isCheckStatusServerResponse(str, interfaceCallbackResponseServer)) {
                        if (interfaceCallbackResponseServer != null) {
                            interfaceCallbackResponseServer.completeResponse(InterfaceCallbackResponseServer.KEY_FINISH_ACTIVITY, "");
                        }
                    } else {
                        if (interfaceCallbackResponseServer != null) {
                            interfaceCallbackResponseServer.completeResponse(InterfaceCallbackResponseServer.KEY_ERROR_RESPONSE, "");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (interfaceCallbackResponseServer != null) {
                    interfaceCallbackResponseServer.completeResponse(InterfaceCallbackResponseServer.KEY_ERROR_RESPONSE, "");
                }
            }
        });
    }

    public static void init(final Handler callback) {
        try {
            ApiInterface apiInterface = ApiModule.getApiInterface();
            apiInterface.init().enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    try {
                    String result = new ManagerNet().readResponse(response);
//                        if (!TextUtils.isEmpty(result) && ManagerJSONParsing.isCheckStatusServerResponse(new JSONObject(result))) {
                    if (!TextUtils.isEmpty(result)) {
                        // Запрос был вызван первый раз без куков от этого редеректило на главную страницу и в ответе ничего нету
                        // После запроса куки сохранились вызываем еще раз метод инициализации для получения валидного CSRF токена
                        if (result.contains("<html>")) {
                            init(callback);
                        } else {
                            JSONObject obj;
                            try {
                                obj = new JSONObject(result);
                                HttpParam.saveCSRFTokent(obj.getString("token"));
                                responseOk(callback);
                            } catch (JSONException e) { // Не может преобразовать целую html страницу к JSONObject
                                init(callback);
                            }
                        }
                    } else {
                        responseError(callback);
                    }
//                    } catch (JSONException e) {
//                        responseError(callback);
//                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    //==========================================
    // Работа с данными
    //==========================================
    public static void responseChangeBackGround(String bgId) {
        ApiInterface apiInterface = ApiModule.getApiInterface();
        apiInterface.changeBackground(getCsrfToken(), bgId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String json = "";
                try {
                    json = new String(response.body().bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public static void responseSubscriptNotify(int value) {
        ApiInterface apiInterface = ApiModule.getApiInterface();
        apiInterface.subscriptNotify(getCsrfToken(), "get_todolist", value).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String json = "";
                try {
                    json = new String(response.body().bytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public static void responseUpdateProfile(Profile profile, final Handler callbackCompleteUpdate) {
        ApiInterface apiInterface = ApiModule.getApiInterface();
        apiInterface.updateDataUser(getCsrfToken(),
                profile.getUsername(),
                profile.getEmail(),
                profile.getPassword(),
                profile.getFirstname(),
                profile.getLastname(),
                profile.getCountry(),
                profile.getTimezone(),
                profile.getCompanyName(),
                profile.getCompanySize()
        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (ManagerJSONParsing.isCheckStatusServerResponse(new String(response.body().bytes()))) {
                        callbackCompleteUpdate.sendEmptyMessage(HttpParam.RESPONSE_OK);
                    } else {
                        callbackCompleteUpdate.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                    }
                } catch (IOException | NullPointerException e) {
                    callbackCompleteUpdate.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public static void getDataUser(@Nullable final Handler callback) {
        responseGetDataUser(callback);
    }

    // Если нет для переданного проекта профиля то он создается
    public static ProjectAssign getProfileAssignMy(long projectId) {
        ProjectAssign projectAssign = ProjectAssign.findByServerId(ProjectAssign.class, getMyProfile().getServerId());
        if (projectAssign == null) {
            projectAssign = new ProjectAssign(getMyProfile().getUsername(), projectId);
            projectAssign.setServerId(getMyProfile().getServerId());
            projectAssign.save();
        }
//        else if (TextUtils.isEmpty(projectAssign.getUserName().trim())) {
//            projectAssign.setUserName(getMyProfile().getUsername());
//            projectAssign.save();
//        }

        return projectAssign;
    }

    public static void responseGetDataUser(final Handler callback) {
        ApiInterface apiInterface = ApiModule.getApiInterface();
        apiInterface.getUserData().enqueue(new Callback<ResponseBody>() {
            private Response<ResponseBody> response;

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> r) {
                response = r;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject obj = new JSONObject(new String(response.body().bytes()));
                            if (ManagerJSONParsing.checkAccessToken(obj)) {
                                ManagerJSONParsing.parseProfile(obj, true);
                                responseOk(callback);
                            } else {
                                ManagerAccount.relogin(new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        responseOkSendEmptyHandler(callback, msg.what);
                                    }
                                });
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                            responseError(callback);
                        }
                    }
                }).run();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public static void syncDataUser() {
        ManagerAccount.getDataUser(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    ManagerMyTeams.responseGetAccounts(null);
                } else {
//                    Crashlytics.logException(new Exception("syncDataUser RESPONSE_ERROR"));
                }
            }
        });
    }


    public static void saveSelectedBackgroundNameImage(String value) {
        ManagerApplication.getInstance().getSharedManager().putKeyString(NAME_SELECTED_BG, value);
    }

    public static String getSelectedNameBackground() {
        return ManagerApplication.getInstance().getSharedManager().getValueString(NAME_SELECTED_BG);
    }


    /**
     * Сохранение выбраного аватара для текущего профиля
     * Уменьшаем значение чтобы в окне настроек корректно отрисовался выбраный фон в AdapterGridBgProfileSettings
     *
     * @param name - имя фотографии пришедшей ссервера (bg1)
     */
    public static void saveSelectedBackgroundImageId(String name) {
        if (!TextUtils.isEmpty(name)) {
            ManagerApplication.getInstance().getSharedManager().putKeyString(NAME_SELECTED_BG, name);
        }
    }

    /**
     * Возвращает название картинки в ресурсах
     *
     * @param context - контекст в котором создается картинки
     */
    public static int getResourceSelectedBackgroundImage(Context context) {
        int res = context.getResources().getIdentifier(getSelectedNameBackground(), "drawable", "com.mifors.goalton");
        if (res == 0) {
            res = R.drawable.bg1;
        }
        return res;
    }

    public static void saveEmailVk(String email) {
        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.VK_EMAIL, email);
    }

    public static void removeEmailVk() {
        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.VK_EMAIL, "");
    }

    public static String getEmailVk() {
        return ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.VK_EMAIL);
    }


    public static void saveTokenVk(String email) {
        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.VK_TOKEN, email);
    }

    public static void removeTokenVk() {
        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.VK_TOKEN, "");
    }

    public static String getTokenVk() {
        return ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.VK_TOKEN);
    }

}