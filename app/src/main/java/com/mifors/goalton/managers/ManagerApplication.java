package com.mifors.goalton.managers;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivitySplash;
import com.mifors.goalton.activity.ActivitySyncLines;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerPlanner;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerScreens;
import com.mifors.goalton.managers.utils.ManagerShared;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.utils.ReciverCoonnectNetwork;
import com.vk.sdk.VKSdk;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import io.fabric.sdk.android.Fabric;


/**
 * Created by gly8 on 05.11.15.
 */
@SuppressWarnings("ALL")
public class ManagerApplication extends MultiDexApplication implements Observer {

    private static final String TAG = "Goalton [" + ManagerApplication.class.getSimpleName() + "]";
    private static ManagerApplication instance;
    private ManagerShared sharedManager;
    private int widthScreen = Integer.MAX_VALUE, heightScreen = Integer.MAX_VALUE, heightActionBar = Integer.MAX_VALUE, heightStatusBar = Integer.MAX_VALUE, heightRemoveTopCalendar = Integer.MAX_VALUE;

    public static ManagerApplication getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        getSharedManager().putKeyBoolean(SharedKeys.IS_FIRST_OPEN_CALENDAR_SESSION, false);
        getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_SETTINGS, 0);
        getSharedManager().putKeyString(SharedKeys.EDIT_LINE_MODE, "");
        FirebaseApp.initializeApp(this);
        Fabric.with(this, new Crashlytics());
        instance = this;
        try {
            ActiveAndroid.initialize(this);
        } catch (NoClassDefFoundError e) {
            e.printStackTrace();
        }

        if (ManagerAccount.isLogin()) {
            ManagerProjects.saveSelectedProjects(0);
            ManagerAccount.syncDataUser();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ManagerPlanner.responseGetDataCalendarLoginUser();
                }
            }).run();
        }
        setupLanguage();
        VKSdk.initialize(this);
    }

    public void registerReciver() {
        Log.v(TAG, "[onCreate] display = " + ManagerScreens.getDeviceDensity(getApplicationContext()) + "  " + ManagerScreens.differentDensityAndScreenSize(getApplicationContext()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(new ReciverCoonnectNetwork() {
                                 @Override
                                 public void onReceive(Context context, Intent intent) {
                                     boolean isOnline = ManagerApplication.getInstance().isConnectToInternet(false);
                                     ReciverCoonnectNetwork.NetworkObservable.getInstance().setChanged();
                                     ReciverCoonnectNetwork.NetworkObservable.getInstance().notifyObservers(isOnline);
                                     if (isOnline) {
                                         checkOfflineLines();
                                     }
                                 }
                             },
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } else {
            ReciverCoonnectNetwork.NetworkObservable.getInstance().addObserver(this); // Ставим наблюдатель на включение отключение интернета
        }
    }

    public static void showToastStatic(int resString) {
        instance.showToast(resString);
    }


    public static void showToastStatic(String string) {
        instance.showToast(string);
    }

    /**
     * Показ всплывающего окна в контексте прилояжения
     *
     * @param resString - id строки из ресурсов
     */
    public void showToast(int resString) {
        Toast toast = Toast.makeText(getApplicationContext(), resString, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Показ всплывающего окна в контексте прилояжения
     *
     * @param string - сообщение
     */
    public void showToast(String string) {
        Toast toast = Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Проверка на наличие интернета
     *
     * @param isVisibleError - показывать ли ошибку что интернета нету
     * @return boolean - true - есть интернет
     * false - нет интернета
     */
    public boolean isConnectToInternet(boolean isVisibleError) {
        try {
            ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            if (manager != null) {
                NetworkInfo[] info = manager.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (isVisibleError) {
                showToast(R.string.message_error_no_connect_network);
            }
            return false;
        }
        if (isVisibleError) {
            showToast(R.string.message_error_no_connect_network);
        }
        return false;
    }

    /**
     * Проверка валидации email
     *
     * @return boolean - валидный ли email
     */
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //================================
    // GETTER SETTER
    //==================Î==============
    public ManagerShared getSharedManager() {
        if (sharedManager == null) {
            setSharedManager(new ManagerShared(getApplicationContext()));
        }
        return sharedManager;
    }

    public void setSharedManager(ManagerShared sharedManager) {
        this.sharedManager = sharedManager;
    }

    //==========================================
    // Help methods
    //==========================================

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp) {
        Resources resources = ManagerApplication.getInstance().getApplicationContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px) {
        Resources resources = ManagerApplication.getInstance().getApplicationContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public int getScreenHeight50Percentage() {
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        }
        height -= actionBarHeight;

        return height / 2;
    }

    public int getScreenWidth() {
        if (widthScreen == Integer.MAX_VALUE) {
            updateHeightWidthScreen();
        }
        return widthScreen;
    }

    public int getScreenHeight() {
        if (heightScreen == Integer.MAX_VALUE) {
            updateHeightWidthScreen();
        }
        return heightScreen;
    }

    public int getHeightRemoveTopCalendar() {
        if (heightRemoveTopCalendar == Integer.MAX_VALUE || heightRemoveTopCalendar == 0) {
            int pixels = (int) ManagerApplication.getInstance().convertDpToPixel(5);
            heightRemoveTopCalendar = ManagerApplication.getInstance().getHeightActionBar() + ManagerApplication.getInstance().getHeightStatusBar() + pixels;
        }
        return heightRemoveTopCalendar;
    }

    private void updateHeightWidthScreen() {
        WindowManager windowManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        widthScreen = size.x;
        heightScreen = size.y;
    }

    public void setHeightScreen(int heightScreen) {
        this.heightScreen = heightScreen;
    }

    public void setHeightStatusBar(int heightStatusBar) {
        if (heightStatusBar > 0) {
            getSharedManager().putKeyInteger(SharedKeys.HEIGHT_STATUS_BAR, heightStatusBar);
        }
        this.heightStatusBar = heightStatusBar;
    }

    public int getHeightStatusBar() {
        if (heightStatusBar == Integer.MAX_VALUE) {
            heightStatusBar = getSharedManager().getValueInteger(SharedKeys.HEIGHT_STATUS_BAR, Integer.MAX_VALUE);
        }
        return heightStatusBar;
    }

    public void setHeightActionBar(int heightActionBar) {
        if (heightActionBar > 0) {
            getSharedManager().putKeyInteger(SharedKeys.HEIGHT_ACTION_BAR, heightActionBar);
        }
        this.heightActionBar = heightActionBar;
    }

    public int getHeightActionBar() {
        if (heightActionBar == Integer.MAX_VALUE) {
            heightActionBar = getSharedManager().getValueInteger(SharedKeys.HEIGHT_ACTION_BAR, Integer.MAX_VALUE);
        }
        return heightActionBar;
    }


    /**
     * Возвращает рандомный long
     *
     * @return long - сгенерированое число
     */
    public static int getRandomInt(int min, int max) {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }


    /**
     * Возвращает рандомный long
     *
     * @return long - сгенерированое число
     */
    public static long getRandomLong() {
        long LOWER_RANGE = 1; //assign lower range value
        long UPPER_RANGE = 90000; //assign upper range value
        return getRandomLong(LOWER_RANGE, UPPER_RANGE);
    }

    public static long getRandomLong(long min, long max) {
        long LOWER_RANGE = min; //assign lower range value
        long UPPER_RANGE = max; //assign upper range value
        Random random = new Random();
        return LOWER_RANGE + (long) (random.nextDouble() * (UPPER_RANGE - LOWER_RANGE));
    }

    public static void hideKeyboard(Activity activity) {
        if (activity == null) {
            return;
        }
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getInstance().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg != null && (boolean) arg == true) {
            checkOfflineLines();
        }
    }

    public void checkOfflineLines() {
        if (Line.getOfflineLinesCount() > 0 || Line.getOfflineLinesUpdateSortCount() > 0) {
            if (!ActivitySyncLines.isRunActivity) {
                Intent i = new Intent();
                i.setClassName("com.mifors.goalton", "com.mifors.goalton.activity.ActivitySyncLines");
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ManagerApplication.getInstance().startActivity(i);
                ActivitySyncLines.isRunActivity = true;
            }
        }
    }

    public void restartApplication() {
        Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage(getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    //==========================================
    // Language
    //==========================================
    private void setupLanguage() {
        if (TextUtils.isEmpty(getCurrentLangiage())) {
            String locale;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                locale = getApplicationContext().getResources().getConfiguration().getLocales().get(0).toString();
            } else {
                locale = getApplicationContext().getResources().getConfiguration().locale.toString();
            }
            String arr[] = locale.split("_");
            saveCurrentLanguage(arr[0]);
        } else {
            setLanguageApplication(getCurrentLangiage());
        }
    }

    public void setLanguageApplication(String currentLangiage) {
        saveCurrentLanguage(currentLangiage);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        if (conf != null && !TextUtils.isEmpty(currentLangiage)) {
            try {
                conf.setLocale(new Locale(currentLangiage.toLowerCase())); // API 17+ only.
                res.updateConfiguration(conf, dm);
            } catch (NoSuchMethodError e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isEnglishLocale() {
//        String locale;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            locale = getApplicationContext().getResources().getConfiguration().getLocales().get(0).toString();
//        } else {
//            locale = getApplicationContext().getResources().getConfiguration().locale.toString();
//        }
//
//        if (!TextUtils.isEmpty(locale)) {
//            if (locale.contains("en")) {
//                return false;
//            }
//        }

        return "en".equals(getCurrentLangiage());
    }

    public Locale stringToLocale(String s) {
        return new Locale(getCurrentLangiage());
    }

    public String getCurrentLangiage() {
        return ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.CURRENT_LANGUAGE);
    }

    public void saveCurrentLanguage(String currentLanguage) {
        if ("ru".equals(currentLanguage)) {
            ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.CURRENT_LANGUAGE, "ru");
        } else {
            ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.CURRENT_LANGUAGE, "en");
        }
    }


    //==========================================
    // Logout profile
    //==========================================
    public void clearApplicationData() {
        try {
            File cache = getCacheDir();
            File appDir = new File(cache.getParent());
            if (appDir.exists()) {
                String[] children = appDir.list();
                for (String s : children) {
                    if (!s.equals("lib")) {
                        deleteDir(new File(appDir, s));
                    }
                }
            }
        } finally {
            restartApp();
        }
    }

    private void restartApp() {
        Intent intent = new Intent(getApplicationContext(), ActivitySplash.class);
        int mPendingIntentId = 213;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}



