package com.mifors.goalton.managers.core;

import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.DebugKeys;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivityMyInvites;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.model.myteams.Invite;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.Api.ApiTeams;
import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mifors.goalton.network.HttpParam.RESPONSE_ERROR;

/**
 * Created by gly8 on 10.07.17.
 */

@SuppressWarnings("ALL")
public class ManagerInvite extends Observable {
    private static ManagerInvite instance;
    private static final String TAG = "Goalton [" + ManagerInvite.class.getSimpleName() + "]";
    private Timer mTimer;
    private TimerTaskGetInvites timerTaskGetInvites;
    private long delayResponseSeconds = 300;

    public static ManagerInvite getInstance() {
        if (instance == null) {
            instance = new ManagerInvite();
        }
        return instance;
    }

    public ManagerInvite() {
        startTimer();
    }

    // Возвращает количество приглашений
    public static int getCountInvites() {
        return ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.COUNT_INVITES);
    }

    // Приглашения которые высланы мне
    public void responseGetListMyInvites(final Handler callback) {
        if (!ManagerApplication.getInstance().isConnectToInternet(false)) {
            return;
        }

        ApiTeams apiTeams = ApiModule.getApiInterfaceTeams();
        apiTeams.getListMyInvite().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                String result = managerNet.readResponse(response);
                try {
                    JSONObject obj = new JSONObject(result);
                    if (ManagerJSONParsing.isCheckStatusServerResponse(obj)) {
                        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.INVITES, result);
                        ArrayList<Invite> list = ManagerJSONParsing.parseMyInviteList(obj);
                        saveCountInvites(list.size());
                        // Есть приглашения и активность не запущена
                        if (list.size() > 0 && !ActivityMyInvites.isRunActivity) {
                            ManagerInvite.getInstance().startActivityInvity();
                        }

                        ManagerInvite.getInstance().callingNotify(0);
                        if (callback != null) {
                            callback.sendMessage(callback.obtainMessage(HttpParam.RESPONSE_OK, list));
                        }
                    } else {
                        if (callback != null) {
                            callback.sendEmptyMessage(RESPONSE_ERROR);
                        }
                    }
                } catch (JSONException e) {
//                    if (TextUtils.isEmpty(result)) {
//                        Crashlytics.logException(new Exception("responseGetListMyInvites  error parse invites"));
//                    } else if (result.length() > 10) {
//                        Crashlytics.logException(new Exception("responseGetListMyInvites  error parse invites " + result.substring(0, 9)));
//                    } else {
//                        Crashlytics.logException(new Exception("responseGetListMyInvites  error parse invites "));
//                    }

                    e.printStackTrace();
                    if (callback != null) {
                        callback.sendEmptyMessage(RESPONSE_ERROR);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (callback != null) {
                    callback.sendEmptyMessage(RESPONSE_ERROR);
                }
            }
        });
    }

    public static void saveCountInvites(int count) {
        ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.COUNT_INVITES, count);
    }

    public void startActivityInvity() {
        Intent i = new Intent();
        i.setClassName("com.mifors.goalton", "com.mifors.goalton.activity.ActivityMyInvites");
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ManagerApplication.getInstance().startActivity(i);
    }

    public void callingNotify(int status) {
        setChanged();
        notifyObservers(status);
    }

    private void startTimer() {
        if (DebugKeys.isStartTimerResponseInvites) {
            mTimer = new Timer();
            timerTaskGetInvites = new TimerTaskGetInvites();
            mTimer.schedule(timerTaskGetInvites, 0, delayResponseSeconds * 1000);
        }
    }

    private class TimerTaskGetInvites extends TimerTask {
        @Override
        public void run() {
            ManagerInvite.getInstance().responseGetListMyInvites(null);
        }
    }
}