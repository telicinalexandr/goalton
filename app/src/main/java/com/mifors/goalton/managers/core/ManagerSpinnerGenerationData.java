package com.mifors.goalton.managers.core;

import android.content.Context;
import android.text.TextUtils;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.model.user.CompanySize;
import com.mifors.goalton.model.user.Country;
import com.mifors.goalton.model.user.TimeZone;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItemColors;
import com.mifors.goalton.ui.spinner.view.DefaultObject;
import com.mifors.goalton.ui.spinner.view.DefaultObjectColors;
import com.mifors.goalton.utils.OutlineLineUIHelper;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by Sergey on 12.08.16.
 */
@SuppressWarnings("ALL")
public class ManagerSpinnerGenerationData {
    private static final String TAG = "Goalton [" + ManagerSpinnerGenerationData.class.getSimpleName() + "]";

    // Преключатель показывать или нет демо данные в списке чата
    // Отвечает за вход данных на актививи передается из демо менеджера
    // Останавливается таймер запроса getChats в ManagerChats.
    // При запросе к серверу типа работаем в демо данных
    private static final boolean isSetDemoDataListChats = false;

    // Количество демо чатов для списка чатов
    private static final int countGenerationChats = 20;

    // Количество минут между ответами
    private static final long minuteDivivder = 60 * 24; // 1 день - (60*60)*24   1 час- (60*60);

    // Сгенерировать количество непрочианных сообщений от
    private static final int randomCountMessageStart = -2;

    // до
    private static final int randomCountMessageEnd = 1;
    // Количество сделок
    private static final int countOrders = 15;

    //==========================================
    // TODO: В дальнейшем планируется реализация механизма update пока все забито гвоздями
    // Демо данные выпадающих списков
    //==========================================
    public static ArrayList<InterfaceSpinnerItem> initCounters(Context context) {
        String[] arrayCountrys = context.getResources().getStringArray(R.array.countrys);
        String[] arrayCountrysValues = context.getResources().getStringArray(R.array.countrys_values);
        ArrayList<InterfaceSpinnerItem> list = new ArrayList<>();

        if (Country.getCount(Country.class) == 0) {
            for (int i = 0; i < arrayCountrys.length; i++) {
                Country c = Country.findCountryByValue(arrayCountrysValues[i]);
                if (c == null) {
                    c = new Country();
                    c.setName(arrayCountrys[i]);
                    c.setValue(arrayCountrysValues[i]);
                    c.save();
                }
                list.add(c);
            }
        } else {
            list.addAll(Country.getAll(Country.class));
        }
        return list;
    }

    public static ArrayList<InterfaceSpinnerItem> initTimezones(Context context) {
        String[] timezones = context.getResources().getStringArray(R.array.timezones);
        String[] timezonesValues = context.getResources().getStringArray(R.array.timezones_my_values);
        ArrayList<InterfaceSpinnerItem> list = new ArrayList<>();
        if (TimeZone.getCount(TimeZone.class) == 0) {
            for (int i = 0; i < timezones.length; i++) {
                TimeZone timeZone = TimeZone.findCountryByValue(timezonesValues[i]);
                if (timeZone == null) {
                    timeZone = new TimeZone();
                    timeZone.setName(timezones[i]);
                    timeZone.setValue(timezonesValues[i]);
                    timeZone.save();
                }
                list.add(timeZone);
            }
        } else {
            list.addAll(TimeZone.getAll(TimeZone.class));
        }
        return list;
    }

    public static ArrayList<InterfaceSpinnerItem> initCompanySize(Context context) {
        String[] timezones = context.getResources().getStringArray(R.array.company_size);
        String[] timezonesValues = context.getResources().getStringArray(R.array.company_size_values);
        ArrayList<InterfaceSpinnerItem> list = new ArrayList<>();
        if (CompanySize.getCount(CompanySize.class) == 0) {
            for (int i = 0; i < timezones.length; i++) {
                CompanySize companySize = CompanySize.findCountryByValue(timezonesValues[i]);
                if (companySize == null) {
                    companySize = new CompanySize();
                    companySize.setName(timezones[i]);
                    companySize.setValue(timezonesValues[i]);
                    companySize.save();
                }
                list.add(companySize);
            }
        } else {
            list.addAll(CompanySize.getAll(CompanySize.class));
        }
        return list;
    }

    public static String getNameByPositionFromArray(int resArray, int position) {
        String valuesearch = "";
        try {
            String[] arr = ManagerApplication.getInstance().getResources().getStringArray(resArray);
            return arr[position];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valuesearch;
    }

    public static int getPositionFromArrayString(int resArray, String value) {
        boolean isFindField = false;
        int searchInt = 0;

        try {
            if (TextUtils.isEmpty(value)) {
                return 0;
            }
            String[] arr = ManagerApplication.getInstance().getResources().getStringArray(resArray);
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].equals(value)) {
                    isFindField = true;
                    searchInt = i;
                }

                if (isFindField) {
                    break;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchInt;
    }

    public static int getPositionFromArrayStringContains(int resArray, String value) {
        boolean isFindField = false;
        int searchInt = 0;

        try {
            if (TextUtils.isEmpty(value)) {
                return 0;
            }
            String[] arr = ManagerApplication.getInstance().getResources().getStringArray(resArray);
            for (int i = 0; i < arr.length; i++) {
                if (arr[i].toLowerCase().contains(value)) {
                    isFindField = true;
                    searchInt = i;
                }

                if (isFindField) {
                    break;
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return searchInt;
    }

    public static ArrayList<InterfaceSpinnerItem> getDataSpinnerDuration() {
//        <!--15 мин, 30 мин, 1 час, 1,5 часа, 2 часа,3 часа и 4 часа-->
        ArrayList<InterfaceSpinnerItem> arrayList = new ArrayList();
        String[] arrDuration = ManagerApplication.getInstance().getResources().getStringArray(R.array.duration);
        for (int i = 0; i < arrDuration.length; i++) {
            DefaultObject defaultObject = new DefaultObject();
            defaultObject.setName(arrDuration[i]);
            switch (i) {
                case 0:
                    defaultObject.setValue(String.valueOf(15));
                    break;
                case 1:
                    defaultObject.setValue(String.valueOf(30));
                    break;
                case 2:
                    defaultObject.setValue(String.valueOf(60));
                    break;
                case 3:
                    defaultObject.setValue(String.valueOf(90));
                    break;
                case 4:
                    defaultObject.setValue(String.valueOf(120));
                    break;
                case 5:
                    defaultObject.setValue(String.valueOf(180));
                    break;
                case 6:
                    defaultObject.setValue(String.valueOf(240));
                    break;
            }
            arrayList.add(defaultObject);
        }
        return arrayList;
    }

    public static ArrayList<InterfaceSpinnerItemColors> getColorArray() {
        ArrayList<InterfaceSpinnerItemColors> arrayList = new ArrayList<>();
        OutlineLineUIHelper outlineLineUIHelper = new OutlineLineUIHelper();
        for (int i = 1; i < 7; i++) {
            DefaultObjectColors defaultObject = new DefaultObjectColors();
            defaultObject.setValue(String.valueOf(i));
            switch (i) {
                case 1:
                    defaultObject.setResColor(R.drawable.shape_outline_circle_color_1);
                    break;
                case 2:
                    defaultObject.setResColor(R.drawable.shape_outline_circle_color_2);
                    break;
                case 3:
                    defaultObject.setResColor(R.drawable.shape_outline_circle_color_3);
                    break;
                case 4:
                    defaultObject.setResColor(R.drawable.shape_outline_circle_color_4);
                    break;
                case 5:
                    defaultObject.setResColor(R.drawable.shape_outline_circle_color_5);
                    break;
                case 6:
                    defaultObject.setResColor(R.drawable.shape_outline_circle_color_6);
                    break;
            }


            arrayList.add(defaultObject);
        }
        return arrayList;

    }

    //==========================================
    // OUTLINE
    //==========================================

    public static String getOutlineDemo(Context contex) {
        return loadJSONFromAsset(contex, "demo_outline.json");
    }


    private static String loadJSONFromAsset(Context context, String nameFile) {
        String json;
        try {
            InputStream is = ManagerApplication.getInstance().getApplicationContext().getAssets().open(nameFile);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
