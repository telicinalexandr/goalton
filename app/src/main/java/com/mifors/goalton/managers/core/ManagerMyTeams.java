package com.mifors.goalton.managers.core;

import android.os.Handler;

import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.managers.utils.ManagerNetHelper;
import com.mifors.goalton.model.myteams.MyTeam;
import com.mifors.goalton.model.myteams.MyTeamProject;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.Api.ApiTeams;
import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mifors.goalton.managers.utils.ManagerJSONParsing.parseInviteProjects;

/**
 * Created by gly8 on 27.03.17.
 */

@SuppressWarnings("ALL")
public class ManagerMyTeams extends ManagerNetHelper {
    public static void responseGetAccounts(final Handler callback) {
        try{
            ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
            apiInterface.getAcccounts(HttpParam.getCsrfToken()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.body() == null) {
                        return;
                    }
                    String result = (new ManagerNet()).readResponse(response);
                    if (ManagerJSONParsing.isCheckStatusServerResponse(result)) {
                        ManagerJSONParsing.parseTeamAccounts(result);
                        responseOk(callback);
                    } else {
                        responseError(callback);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
    }

    public static void responseCreateTeam(final Handler callback, long accountId, String teamName) {
        ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
        apiInterface.createTeam(HttpParam.getCsrfToken(), ManagerAccount.getMyProfile().getServerId(), accountId, teamName)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.body().bytes()));
                            if (jsonObject.has("status")) {
                                if (jsonObject.getString("status").equals("OK")) {
                                    responseOk(callback);
                                } else {
                                    responseError(callback);
                                }
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        responseError(callback);
                    }
                });
    }

    public static void responseCreateAccount(final Handler callback, String accountName) {
        ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
        apiInterface.createAccount(HttpParam.getCsrfToken(), ManagerAccount.getMyProfile().getServerId(), accountName)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.body().bytes()));
                            if (jsonObject.has("status")) {
                                if (jsonObject.getString("status").equals("OK")) {
                                    responseOk(callback);
                                } else {
                                    responseError(callback);
                                }
                            }
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
//                        Crashlytics.logException(new Exception("responseCreateAccount onFailure - "+t.getMessage()));
                        responseError(callback);
                    }
                });
    }

    public static void responseChangeNameTheme(final Handler callback, long teamId, String teamName) {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
            apiInterface.changeNameTeam(HttpParam.getCsrfToken(), teamId, teamName).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (ManagerJSONParsing.isCheckStatusServerResponse(new String(response.body().bytes()))) {
                            responseOk(callback);
                        } else {
                            responseError(callback);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        }
    }

    // Удаление профиля из команды
    public static void responseUserUnlinkFromTeam(final Handler callback, long userTeamId) {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
            apiInterface.unlinkUserFromTeam(HttpParam.getCsrfToken(), userTeamId).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String result = new String(response.body().bytes());
                        if (ManagerJSONParsing.isCheckStatusServerResponse(result)) {
                            responseOk(callback);
                        } else {
                            responseError(callback);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        }
    }

    // Удаление профиля из команды
    public static void responseUserUnlinkFromTeamProject(final Handler callback, long teamId) {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
            apiInterface.unlinkUserFromTeamProject(HttpParam.getCsrfToken(), teamId).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String result = new String(response.body().bytes());
                        if (ManagerJSONParsing.isCheckStatusServerResponse(result)) {

                            responseOk(callback);
                        } else {
                            responseError(callback);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        }
    }

    // Смена прав профиля
    public static void responseChangePermissionTeamProfile(final Handler callback, long userId, long projectId, String rightToView, String rightToEdit, String rightToEditProject, String rightToInvite) {
        ApiTeams apiInterface = ApiModule.getApiInterfaceTeams();
        apiInterface.changePermissionTeamProfile(HttpParam.getCsrfToken(),
                userId,
                projectId,
                rightToView,
                rightToEdit,
                rightToEditProject,
                rightToInvite).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                try {
                    managerNet.checkReponse(callback, response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    // Получение членство в коммандах
    public static void responseGetMemberships(final Handler callback) {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ApiTeams apiTeams = ApiModule.getApiInterfaceTeams();
            apiTeams.getMemberships().enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    MyTeam.clearTable(MyTeam.class);
                    MyTeamProject.clearTable(MyTeamProject.class);
                    String result = new ManagerNet().readResponse(response);
                    try {
                        ManagerJSONParsing.parseMyTeams(new JSONObject(result));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    responseOk(callback);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        }
    }

    // Получение списка проектов куда можно пригласить пользователя
    public static void responseGetInviteList(final Handler callback) {
        ApiTeams apiTeams = ApiModule.getApiInterfaceTeams();
        apiTeams.getListInvites().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    ManagerNet managerNet = new ManagerNet();
                    String result = managerNet.readResponse(response);
                    responseOkSendObject(callback, parseInviteProjects(new JSONObject(result)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    // Отправка приглашений
    public static void responseSendInvite(final Handler callback, long projectId, String email) {
        ApiTeams apiTeams = ApiModule.getApiInterfaceTeams();
        apiTeams.sendInvite(HttpParam.getCsrfToken(), projectId, email).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                if (ManagerJSONParsing.isCheckStatusServerResponse(managerNet.readResponse(response))) {
                    responseOk(callback);
                } else {
                    responseError(callback);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }


    // Принять/отклонить приглашение
    public static void responseChangeStatusInvite(final Handler callback, long teamId, String status) {
        ApiTeams apiTeams = ApiModule.getApiInterfaceTeams();
        apiTeams.changeStatusInvite(HttpParam.getCsrfToken(), teamId, status).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet managerNet = new ManagerNet();
                String result = managerNet.readResponse(response);
                try {
                    JSONObject obj = new JSONObject(result);
                    if (ManagerJSONParsing.isCheckStatusServerResponse(obj)) {
                        responseOk(callback);
                    } else {
                        responseError(callback);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }
}