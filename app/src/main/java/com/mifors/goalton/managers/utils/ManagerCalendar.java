package com.mifors.goalton.managers.utils;

import android.content.Context;
import android.text.TextUtils;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by gly8 on 14.07.16.
 * Методя для работы с классом Calendar
 */
public class ManagerCalendar {

    /**
     * Преобразование строки в объект календарь
     *
     * @param dateStr - строка для преобразования
     * @return Calendar - результат преобразования. При ошибке парсинга даты
     * возвращает текущее время.
     */
    public static Calendar convertStringToCalendar(String dateStr) {
        Calendar calendar = Calendar.getInstance();
        if (TextUtils.isEmpty(dateStr) || dateStr.equals("null")) {
            return null;
        }

        try {
            calendar.setTimeInMillis(new SimpleDateFormat("dd.MM.yyyy hh:mm").parse(dateStr).getTime());
        } catch (ParseException e) {
            try {
                calendar.setTimeInMillis(new SimpleDateFormat("dd.MM.yyyy").parse(dateStr).getTime());
            } catch (ParseException e1) {
                try {
                    calendar.setTimeInMillis(new SimpleDateFormat("dd.MM.yyyy hh:mm:ss.SSS").parse(dateStr).getTime());
                } catch (ParseException e2) {
                    try {
                        calendar.setTimeInMillis(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS").parse(dateStr).getTime());
                    } catch (ParseException e3) {
                        try {
                            calendar.setTimeInMillis(new SimpleDateFormat("dd.MM.yyyy").parse(dateStr).getTime());
                        } catch (ParseException e4) {
                            try {
                                calendar.setTimeInMillis(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(dateStr).getTime());
                            } catch (Exception e5) {
                                try {
                                    calendar.setTimeInMillis(new SimpleDateFormat("yyyy-MM-dd").parse(dateStr).getTime());
                                } catch (ParseException e6) {
                                    try {
                                        calendar.setTimeInMillis(new SimpleDateFormat("yyyy-MM-dd hh:mm").parse(dateStr).getTime());
                                    } catch (ParseException e7) {

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return calendar;
    }

    public static int roundMinutes10(Calendar tempCalendar) {
        int minutes10 = 0;
        if (tempCalendar.get(Calendar.MINUTE) > 0) {
            minutes10 = tempCalendar.get(Calendar.MINUTE) / 10;
        }

        if (minutes10 == 6) {
            minutes10 = 5;
        }

        minutes10 *= 10;
        return minutes10;
    }

    public static String getMonthByNumber(Context context, int numberMonth) {
        String[] arr = context.getResources().getStringArray(R.array.months);
        return arr[numberMonth];
    }

    public static Calendar convertStringToCalendar(String source, String dateFormat) {
        if (TextUtils.isEmpty(source) || dateFormat.isEmpty()) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTimeInMillis(new SimpleDateFormat(dateFormat, Locale.getDefault()).parse(source).getTime());
            return calendar;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String convertCalendarToString(Calendar calendar) {
        if (calendar == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return format.format(calendar.getTime());
    }

    public static String convertCalendarToString(Calendar calendar, String dateFormat) {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat, new Locale(ManagerApplication.getInstance().getCurrentLangiage()));
        return format.format(calendar.getTime());
    }

    /**
     * Преобразовывает дату к строке вида dd.mm.yyyy hh:mm:ss
     *
     * @param calendar      - дата для преобразования
     * @param isAppeandTime - true -  return dd.mm.yyyy hh:mm:ss
     *                      false -return dd.mm.yyyy
     * @return String - результат преобразования
     */
    public static String convertCalendarToString(Calendar calendar, boolean isAppeandTime) {
        if (calendar == null) {
            return "null date";
        }
        String result = "";
        int month = calendar.get(Calendar.MONTH) + 1;
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);

        result += bulBul(dayOfMonth) + ".";
        result += bulBul(month) + ".";

        if (isAppeandTime) {
            result += year + " " + getTime(calendar, true);
        } else {
            result += String.valueOf(year);
        }

        return result;
    }

    public static String bulBul(int number) {
        String result = "";
        if (number <= 9) {
            result += "0" + number;
        } else {
            result += number;
        }
        return result;
    }

    public static Calendar getInstance() {
        return Calendar.getInstance(Locale.ENGLISH);
    }

    public static String convertTimeStampToStringDate(long timeStamp, String dateFormat) {
        if (timeStamp == 0) {
            return "";
        }
        Calendar checkDate = Calendar.getInstance();
        checkDate.setTimeInMillis(timeStamp);
        return convertCalendarToString(checkDate, dateFormat);
    }

    /**
     * Возвращает количество дней между датами
     *
     * @param currentDate - дата начала
     * @param dateTwo     - конечная дата
     * @return long - количество дней между датами
     */
    public static long getBeetwenDays(Calendar currentDate, Calendar dateTwo) {
        long milis1 = toCalendar(currentDate.getTimeInMillis()).getTimeInMillis();
        long milis2 = toCalendar(dateTwo.getTimeInMillis()).getTimeInMillis();
        long diff = milis1 - milis2;
        return (int) (diff / (24 * 60 * 60 * 1000));
    }

    public static long getBeetwenMinutes(long startDate, long endDate) {
        long different = endDate - startDate;
        return TimeUnit.MILLISECONDS.toMinutes(different);
    }

    private static Calendar toCalendar(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * Возвращает время формата hh:mm
     *
     * @param c - дата начала
     * @return String - количество дней между датами
     * @params isSetSecond - нужно ли добавлять секунды
     */
    public static String getTime(Calendar c, boolean isSetSecond) {
        String time = "";
        int minute = c.get(Calendar.MINUTE);
        int hourse = c.get(Calendar.HOUR_OF_DAY);
        int second = c.get(Calendar.SECOND);

        if (hourse < 10) {
            time += "0" + hourse;
        } else {
            time += "" + hourse;
        }

        if (minute < 10) {
            time += ":0" + minute;
        } else {
            time += ":" + minute;
        }

        if (isSetSecond) {
            if (second < 10) {
                time += ":0" + second;
            } else {
                time += ":" + second;
            }
        }

        return time;
    }
}