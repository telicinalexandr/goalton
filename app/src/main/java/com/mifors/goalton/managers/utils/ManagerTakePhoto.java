package com.mifors.goalton.managers.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gly8 on 12.07.17.
 */

@SuppressWarnings("ALL")
public class ManagerTakePhoto {
    private static final String TAG = "Goalton [" + ManagerTakePhoto.class.getSimpleName() + "]";
    private Activity activity;
    private int resTitle;
    public final static int REQUEST_PERMISSIONS = 200;
    public final static int REQUEST_IMAGE_CAPTURE = 300;
    public final static int REQUEST_CODE_GALLERY = 301;
    private final static String PREFIX_TEMP_FILE = "tempfile_";

    private String mCurrentPhotoPath;

    public ManagerTakePhoto(Activity activity) {
        this.activity = activity;
    }

    public ManagerTakePhoto() {
    }

    public void takePhoto(int resTitle) {
        this.resTitle = resTitle;
        takePhoto();
    }

    public void takePhoto() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkPermissions()) {
                startTakePhoto();
            }
        } else {
            startTakePhoto();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS) {
            int countGranted = 0;
            for (int gr : grantResults) {
                if (gr == PackageManager.PERMISSION_GRANTED) {
                    countGranted++;
                }
            }

            if (countGranted == grantResults.length) {
                startTakePhoto();
            }
        }
    }

    public boolean checkPermissions() {
        ArrayList<String> permissions = new ArrayList<>();

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CAMERA);
        }

        if (permissions.size() > 0) {
            ActivityCompat.requestPermissions(activity, permissions.toArray(new String[permissions.size()]), REQUEST_PERMISSIONS);
        } else {
            return true;
        }

        return false;
    }

    public void startTakePhoto() {
        if (resTitle == 0) {
            resTitle = R.string.source_cover;
        }
        ManagerAlertDialog.showDialog(activity, resTitle, R.string.galery, R.string.camera, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startGallery();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    startCamera();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

//    private Uri mImageUri;

    private void startCamera() throws IOException {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {
            return;
        }
        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(createImageFile());
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File createImageFile() throws IOException {
        String imageFileName = "JPEG_1111111_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    public void startGallery() {
        Intent selectPictureintent = new Intent();
        selectPictureintent.setType("image/*");
        selectPictureintent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(selectPictureintent, "Select Picture"), REQUEST_CODE_GALLERY);
    }

    // Сохранение картинки в директорию для отправки на сервак
    public void saveBitmapToFileDirectory(Bitmap bmp, long lineServerId) {
        File file = new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lineServerId));
        if (bmp == null) {
            if (file.exists()) {
                file.delete();
            }
        } else {
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(file);
                bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean isExsistImage(long lineServerId) {
        return new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lineServerId)).exists();
    }

    public void deleteImage(long lineServerId) {
        File file = new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lineServerId));
        if (file.exists()) {
            Log.e(TAG, "[deleteImage]  uri = " + file.getAbsolutePath());
            file.delete();
        }
    }

    public void changeNameImage(long lastServerId, long newServerId) {
        File fileLast = new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lastServerId));
        File fileNew = new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(newServerId));
        Log.e(TAG, "[deleteImage]  fileLast = " + fileLast.getAbsolutePath() + "    fileNew = " + fileNew.getAbsolutePath());
        if (fileLast.exists()) {
            fileLast.renameTo(fileNew);
        }
    }

    public Bitmap getBitmapFromDirectory(long lineServerId) {
        File file = new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lineServerId));
        if (file.exists()) {
            return BitmapFactory.decodeFile(file.getAbsolutePath());
        }
        return null;
    }

    public String getStringBitmapFromDirectory(long lineServerId) {
        File file = new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lineServerId));
        if (file.exists()) {
            return BitMapToString(BitmapFactory.decodeFile(file.getAbsolutePath()));
        }
        return "";
    }

    public File getFileFromDirectory(long lineServerId) {
        return new File(ManagerApplication.getInstance().getFilesDir(), PREFIX_TEMP_FILE + String.valueOf(lineServerId));
    }

    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    //====================================================================================
    // Методы которые возвращают картинку в нормальной ориентации
    //====================================================================================
    public Bitmap getBitmapFromData(int requestCode, Intent data) {
        switch (requestCode) {
            case (REQUEST_IMAGE_CAPTURE):
                return getBitmapFromCameraData();
            case (REQUEST_CODE_GALLERY):
                return getBitmapFromGalleryData(data);
        }
        return null;
    }

    // Возвращает картинку растянутую до размеров обложки
    public Bitmap getBitmapFromGalleryData(Intent data) {
        Bitmap bitmap = null;
        try {
            Uri uri = data.getData();
             bitmap = MediaStore.Images.Media.getBitmap(ManagerApplication.getInstance().getApplicationContext().getContentResolver(), uri);
            return modifyOrientation(bitmap, Uri.parse("file:"+getRealPathFromURI(uri)));
        } catch (IOException | OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    public Bitmap getBitmapFromCameraData() {
        Bitmap bmp = null;
        try {
            Uri imageUri = Uri.parse(mCurrentPhotoPath);
            activity.getContentResolver().notifyChange(imageUri, null);
            ContentResolver cr = activity.getContentResolver();
            bmp = android.provider.MediaStore.Images.Media.getBitmap(cr, imageUri);
            return modifyOrientation(bmp, imageUri);
        } catch (Exception e) {
            e.printStackTrace();
            return bmp;
        }
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    public Bitmap modifyOrientation(Bitmap bmp, Uri uri) throws IOException {
        Matrix matrix = new Matrix();
        ExifInterface exif = new ExifInterface(uri.getPath());
        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int rotationInDegrees = exifToDegrees(rotation);
        if (rotation != 0f) {
            matrix.preRotate(rotationInDegrees);
            return Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        } else {
            return bmp;
        }
    }


    public String getRealPathFromURI(Uri contentUri) throws Exception{
        try{
            String wholeID = DocumentsContract.getDocumentId(contentUri);
            String id = wholeID.split(":")[1];
            String[] column = { MediaStore.Images.Media.DATA };
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = activity.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{ id }, null);
            String filePath = "";
            int columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } catch (NoClassDefFoundError e) {
            Cursor cursor = null;
            try {
                String[] proj = { MediaStore.Images.Media.DATA };
                cursor = ManagerApplication.getInstance().getContentResolver().query(contentUri, proj, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }


    private Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author paulburke
     */
    public String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // DownloadsProvider
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
                // MediaProvider
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
            // MediaStore (and general)
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
            // File
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public void setResTitle(int resTitle) {
        this.resTitle = resTitle;
    }
}
