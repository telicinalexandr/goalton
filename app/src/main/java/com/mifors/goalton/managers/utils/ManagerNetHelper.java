package com.mifors.goalton.managers.utils;

import android.os.Handler;
import android.os.Message;

import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by gly8 on 22.08.17.
 */

@SuppressWarnings("ALL")
public class ManagerNetHelper {
    public static void responseError(Handler callback) {
        if (callback != null) {
            callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
        }
    }

    public static void responseErrorSendObject(Handler callback, Object object) {
        if (callback != null) {
            Message msg = callback.obtainMessage(HttpParam.RESPONSE_ERROR);
            msg.obj = object;
            callback.sendMessage(msg);
        }
    }

    public static void responseOk(Handler callback) {
        if (callback != null) {
            callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
        }
    }

    public static void responseOkSendObject(Handler callback, Object object) {
        if (callback != null) {
            Message msg = callback.obtainMessage(HttpParam.RESPONSE_OK);
            msg.obj = object;
            callback.sendMessage(msg);
        }
    }

    public static void responseOkSendEmptyHandler(Handler callback, int what) {
        if (callback != null) {
            callback.sendEmptyMessage(what);
        }
    }

    public static void checkResultNotParseDataSendCallback(Handler callback, Response<ResponseBody> response) {
        try {
            JSONObject object = new JSONObject(new ManagerNet().readResponse(response));
            if (ManagerJSONParsing.isCheckStatusServerResponse(object)) {
                responseOk(callback);
            } else {
                responseError(callback);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            responseError(callback);
        }
    }

    public static boolean checkResultResponse(Response<ResponseBody> response) {
        try {
            JSONObject object = new JSONObject(new ManagerNet().readResponse(response));
            if (ManagerJSONParsing.isCheckStatusServerResponse(object)) {
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            return false;
        }
    }

}
