package com.mifors.goalton.managers.utils;

import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.mifors.goalton.model.outline.Line;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by gly8 on 24.10.17.
 */

public class ManagerParseLine {

    private static ArrayList<Line> tempArrayParseLinesPlanner;
    private static HashMap<Long, Integer> weekLines;

    public int c = 0;

    public void startParseLine(JSONObject object, int level, boolean isFromPlanner) {
        try {
            c++;
            long serverId = 0;
            if (!object.isNull("key")) {
                serverId = object.getLong("key");
            } else if (!object.isNull("id")) {
                serverId = object.getLong("id");
            }

            Line line = Line.findOrCreate(Line.class, serverId);

            if (!object.isNull("title")) {
                line.setTitle(object.getString("title"));
            } else if (!object.isNull("name")) {
                line.setTitle(object.getString("name"));
            }

            line.updateReplaceTitleFromCurentTitle();

            if (!object.isNull("sort")) {
                line.setSort(object.getInt("sort"));
            }
            if (!object.isNull("parent_item_id")) {
                line.setParentItemId(object.getLong("parent_item_id"));
            } else {
                line.setParentItemId(0);
            }

            if (!object.isNull("img")) {
                line.setImg(object.getString("img"));
            }

            if (!object.isNull("creator_user_id")) {
                line.setCreatorUserId(object.getLong("creator_user_id"));
            }
            if (!object.isNull("is_completed")) {
                line.setIsCompleted(object.getString("is_completed"));
            }
            if (!object.isNull("is_important")) {
                line.setIsImportant(object.getString("is_important"));
            }

            if (!object.isNull("project_id")) {
                line.setProjectId(object.getLong("project_id"));
            }
            if (!object.isNull("company_id")) {
                line.setCompanyId(object.getLong("company_id"));
            }
            if (!object.isNull("company_name")) {
                line.setCompanyName(object.getString("company_name"));
            }
            if (!object.isNull("assign_user_id")) {
                line.setAssignUserId(object.getString("assign_user_id"));
            }
            if (!object.isNull("assign_user_username")) {
                line.setAssignUserUsername(object.getString("assign_user_username"));
            }
            if (!object.isNull("project_stage_id") && !TextUtils.isEmpty(object.getString("project_stage_id"))) {
                line.setProjectStageId(object.getLong("project_stage_id"));
            }
            if (!object.isNull("project_stage_name")) {
                line.setProjectStageName(object.getString("project_stage_name"));
            }

            // Даты
            if (!object.isNull("deadline_date_format")) {
                line.setDeadlineDateFormat(object.getString("deadline_date_format"));
            }

            if (!object.isNull("deadline_time_format")) {
                line.setDeadlineDateFormatTime(object.getString("deadline_time_format"));
            }

            if (!object.isNull("deadline_date")) {
                line.setDeadlineDateStartStr(object.getString("deadline_date"));
            }

            if (!object.isNull("deadline_time")) {
                line.setDeadlineDateStartTimeStr(object.getString("deadline_time"));
            }
            /////////////////////////////////////////////////////////////////////

            if (!object.isNull("color")) {
                String color = object.getString("color");
                if (!TextUtils.isEmpty(color)) {
                    line.setColor(Integer.parseInt(color));
                }
            }

            if (!object.isNull("duration")) {
                line.setDuration(object.getInt("duration"));
            }

            //================================= DATE ===================================================
            if (!TextUtils.isEmpty(line.getDeadlineDateStartStr())) {
                String deadlineDate = line.getDeadlineDateStartStr();
                if (!TextUtils.isEmpty(line.getDeadlineDateStartTimeStr())) {
                    deadlineDate += " " + line.getDeadlineDateStartTimeStr();
                    line.setDeadlineDateStart(ManagerCalendar.convertStringToCalendar(deadlineDate, Line.DATE_FORMAT_DATE_END_AND_TIME));
                } else {
                    line.setDeadlineDateStart(ManagerCalendar.convertStringToCalendar(deadlineDate, Line.DATE_FORMAT_DATE_END_NOT_TIME));
                }
            }

            if (!object.isNull("completed_date")) {
                line.setCompleteDate(object.getString("completed_date"));
            }

            line.updateFinishDateForCurrentDuration();
            line.updateDatesTextByCalendarNotSave();

            //====================================================================================

            level++;
            line.setLevel(level);
            if (isFromPlanner) {
                if (tempArrayParseLinesPlanner != null) {
                    tempArrayParseLinesPlanner.add(line);
                }

                if (Line.INBOX_ITEM.equals(line.getTitle())) {
                    line.setLevel(1);
                } else {
                    line.setLevel(2);
                }
            }

            if (!object.isNull("children")) {
                JSONArray array = object.getJSONArray("children");
                for (int i = 0; i < array.length(); i++) {
                    startParseLine(array.getJSONObject(i), level, isFromPlanner);
                }
            }
            if (line.getParentItemId() == 0) {
                line.setLevel(1);
            }
            line.save();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void parsePlanner(final JSONObject object, int weekYear, int year) throws JSONException {
        tempArrayParseLinesPlanner = new ArrayList<>();
        weekLines = Line.getHasmapLevels(weekYear, year);
        try {
            ActiveAndroid.beginTransaction();
            try {
                if (!object.isNull("arPlanning")) {
                    JSONArray array = object.getJSONArray("arPlanning");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        if (!obj.isNull("children")) {
                            JSONArray arrChildrens = obj.getJSONArray("children");
                            for (int j = 0; j < arrChildrens.length(); j++) {
                                JSONObject o = arrChildrens.getJSONObject(j);
                                startParseLine(o, 0, true);
                            }
                        }
                    }

                    // Выгружаем все задачи за неделю
                    updateLevelArrayTemps();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    private void updateLevelArrayTemps() {
        // Перед парсингом получить задачи за неделю
        // После парсинга узнать какие задачи были обновлены
        // Задачи которые были обновлены обновляем уровень из базы
        try {
            if (tempArrayParseLinesPlanner != null) {
                for (Iterator<Map.Entry<Long, Integer>> it = weekLines.entrySet().iterator(); it.hasNext(); ) {
                    Map.Entry<Long, Integer> entry = it.next();
                    long serverId = entry.getKey();
                    int level = entry.getValue();
                    for (Line lineTemp : tempArrayParseLinesPlanner) {
                        if (lineTemp.getServerId() == serverId) {
                            lineTemp.setLevel(level);
                            lineTemp.save();
                            it.remove();
                        }
                    }
                }
            }

            for (Iterator<Map.Entry<Long, Integer>> it = weekLines.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<Long, Integer> entry = it.next();
                long serverId = entry.getKey();
                Line line = Line.findByServerId(Line.class, serverId);
                if (line != null) {
                    line.delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        tempArrayParseLinesPlanner = null;
        weekLines = null;
    }
}
