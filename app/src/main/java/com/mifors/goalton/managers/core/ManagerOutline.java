package com.mifors.goalton.managers.core;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.mifors.goalton.DebugKeys;
import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.managers.utils.ManagerNetHelper;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.Api.ApiOutline;
import com.mifors.goalton.network.CookiesRetrofit;
import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mifors.goalton.network.HttpParam.RESPONSE_OK;
import static com.mifors.goalton.network.HttpParam.getCsrfToken;

/**
 * Created by gly8 on 14.04.17.
 */

@SuppressWarnings("ALL")
public class ManagerOutline extends ManagerNetHelper {
    private static final String TAG = "Goalton [" + ManagerOutline.class.getSimpleName() + "]";
    public static volatile String result = "";
    private static Handler callbackReadComplete;
    private static Response<ResponseBody> response;
    private static JSONObject jsonObject;
    private static IManagerOutline iManagerOutline;

    public interface IManagerOutline {
        void updateTitleProgressParseOutline(String title);
    }

    public static void getOutlineProject(IManagerOutline liManagerOutline, long projectId, final Handler callback) {
        iManagerOutline = liManagerOutline;
        ApiOutline apiOutline = ApiModule.getApiOutline();
        apiOutline.getOutlineProject(String.valueOf(projectId)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                readResponse(response, new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        try {
                            if (ManagerJSONParsing.checkAccessToken(jsonObject)) {
                                try {
                                    ManagerJSONParsing.parseOutline(iManagerOutline, jsonObject, new Handler() {
                                        @Override
                                        public void handleMessage(Message msg) {
                                            super.handleMessage(msg);
                                            if (callback != null) {
                                                if (msg.obj != null) {
                                                    callback.sendMessage(callback.obtainMessage(msg.what, msg.obj));
                                                } else {
                                                    callback.sendEmptyMessage(msg.what);
                                                }
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                ManagerAccount.relogin(new Handler() {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        super.handleMessage(msg);
                                        if (callback != null) {
                                            callback.sendEmptyMessage(msg.what);
                                        }
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    private static void readResponse(final Response<ResponseBody> responseResult, Handler callback) {
        callbackReadComplete = callback;
        response = responseResult;
        new Thread(new Runnable() {
            @Override
            public void run() {
                ManagerNet managerNet = new ManagerNet();
                result = managerNet.readResponse(response);
                try {
                    jsonObject = new JSONObject(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (callbackReadComplete != null) {
                    callbackReadComplete.sendEmptyMessage(0);
                }
            }
        }).start();
    }

    public static void responseUpdateSort(List<Line> list, final Handler callback) {
        HashMap<String, Object> map = new HashMap<>(list.size() * 2);
        for (Line l : list) {
            if (l.getServerId() != 0) {
                map.put("Items[" + l.getServerId() + "][parent_item_id]", l.getParentItemId());
                map.put("Items[" + l.getServerId() + "][sort]", l.getSort());
//                map.put("Items[" + l.getServerId() + "][name]", l.getTitle());
            }
        }
        responseUpdateSort(map, callback);
    }

    // Обновление сортировки
    public static void responseUpdateSort(HashMap<String, Object> map, final Handler callback) {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            String xCsrf = ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam._CSRF);
            ApiOutline api = ApiModule.getApiOutline();
            api.updateSort(xCsrf, map).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    ManagerProgressDialog.dismiss();
                    responseOk(callback);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    ManagerProgressDialog.dismiss();
                    responseError(callback);
                }
            });
        }
    }

    // Показать/скрыть выполненые задачи
    public static void responseToogleTaskComplete(long serverId, int isShow, final Handler callback) {
        String csrf = ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam._CSRF);
        ApiOutline api = ApiModule.getApiOutline();
        api.setShowCompleted(csrf, serverId, isShow).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet m = new ManagerNet();
                String read = m.readResponse(response);
                if (callback != null) {
                    callback.sendEmptyMessage(RESPONSE_OK);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    public static void responseSetCompletedLine(Line line, String status, final Handler callback) {
        ApiModule.getApiOutline().setItemCompleted(getCsrfToken(), line.getServerId(), status).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                responseOk(callback);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    // Отправка отмеченных линий на сервак
    public static void responseSendCheckLine(long projectId, final Handler callback) {
        try {
            String url = HttpParam.PROTO_URL_FULL + "/tree-ajax/set-completed";
            MultipartBody.Builder builderMultipart = new MultipartBody.Builder();
            builderMultipart.setType(MultipartBody.FORM);
            builderMultipart.addFormDataPart("_csrf", HttpParam.getCsrfToken());

            List<Line> listOfflineCheck = Line.getChildsByProjectIdAndStatusCompleted(projectId);
            for (Line l : listOfflineCheck) {
                builderMultipart.addFormDataPart("Item['id_" + l.getServerId() + "']", String.valueOf(l.getServerId()));
                builderMultipart.addFormDataPart("Item_" + l.getServerId() + "[id]", String.valueOf(l.getServerId()));
                builderMultipart.addFormDataPart("Item_" + l.getServerId() + "[is_completed]", l.getIsCompleted());
            }

            new AsyncTaskUpdate(url, builderMultipart, projectId, callback).execute();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    // Установка задачи как важная
    public static void responseSetImportant(long lineId, String status, final Handler callback) {
        ApiOutline api = ApiModule.getApiOutline();
        api.setImportant(getCsrfToken(), lineId, status).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                ManagerNet m = new ManagerNet();
                String read = m.readResponse(response);
                if (callback != null) {
                    callback.sendEmptyMessage(RESPONSE_OK);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });

    }

    // Делает запрос на обновление данных
    static class AsyncTaskUpdate extends AsyncTask<Void, Void, String> {

        private String url;
        private MultipartBody.Builder buidler;
        private long projectId;
        private Handler callback;

        public AsyncTaskUpdate(String url, MultipartBody.Builder buidler, long projectId, Handler callback) {
            this.url = url;
            this.projectId = projectId;
            this.buidler = buidler;
            this.callback = callback;
        }

        @Override
        protected String doInBackground(Void... params) {
            String result = "";
            try {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                if (DebugKeys.isShowLogsRetrofit) {
                    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                } else {
                    interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
                }

                OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
                OkHttpClient client = clientBuilder
                        .connectTimeout(30000, TimeUnit.MILLISECONDS)
                        .cookieJar(new CookiesRetrofit())
                        .addInterceptor(interceptor)
                        .build();

                RequestBody requestBody = buidler.build();
                Request request = new Request.Builder()
                        .url(url)
                        .addHeader("X-Requested-With", "XMLHttpRequest")
                        .addHeader("X-CSRF-Token", HttpParam.getCsrfToken())
                        .post(requestBody)
                        .build();

                ManagerNet managerNet = new ManagerNet();
                okhttp3.Response response = client.newCall(request).execute();
                result = managerNet.readResultFromResponse(response.body());
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (callback != null) {
                callback.sendEmptyMessage(HttpParam.RESPONSE_OK);
            }

            if (!TextUtils.isEmpty(result)) {
                try {
                    Project currentProject = Project.findByServerId(Project.class, projectId);
                    JSONObject obj = new JSONObject(result);
                    currentProject.setPercentComplete(obj.getInt("percent_complete"));
                    currentProject.save();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public static void responseUpdateName(long itemId, String name, final Handler callback) {
        ApiOutline apiInterface = ApiModule.getApiOutline();
        apiInterface.updateName(getCsrfToken(), itemId, name).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                responseOkSendObject(callback, new ManagerNet().readResponse(response));
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });

    }

    public static void responseDeleteLine(Line line, final Handler callback) {
        if (line != null) {
            responseDeleteLine(line.getServerId(), line.getProjectId(), callback);
        }
    }

    public static void responseDeleteLine(final long serverId, long projectId, final Handler callback) {
        ApiModule.getApiOutline().delete(HttpParam.getCsrfToken(), serverId, projectId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String result = new String(response.body().bytes());
                    JSONObject obj = new JSONObject(result);
                    if (ManagerJSONParsing.isCheckStatusServerResponse(obj)) {
                        updateProjectFromObject(obj);
                        responseOkSendObject(callback, serverId);
                    } else {
                        responseError(callback);
                    }
                } catch (JSONException | IOException | NullPointerException e) {
                    e.printStackTrace();
                    responseError(callback);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (callback != null) {
                    callback.sendEmptyMessage(HttpParam.RESPONSE_ERROR);
                }
            }
        });
    }

    // Вернуть список актуальных компаний
    public static void responseGetCompanies() {
        ApiModule.getApiOutline().getCompanies().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                final String result = new ManagerNet().readResponse(response);
                if (ManagerJSONParsing.isCheckStatusServerResponse(result)) {
                    try {
                        ManagerJSONParsing.parseCompanies(new JSONObject(result));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    // Получить картинку добавленую к задаче
    public static void responseGetImage(long itemId, final Handler callback) {
        ApiModule.getApiOutline().getImageLine(getCsrfToken(), itemId).enqueue(new Callback<ResponseBody>() {
//        ApiModule.getApiOutline().getImageLine(getCsrfToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    responseOkSendObject(callback, bmp);
                } catch (Exception e) {
                    e.printStackTrace();
                    responseError(callback);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    // Установка даты дедлайна
    public static void responseSetDeadlineDate(long itemId, String date, String time, int duration, final Handler callback) {
        ApiOutline apiInterface = ApiModule.getApiOutline();
        apiInterface.setDeadlinDate(getCsrfToken(), itemId, date, time, duration).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                checkResultNotParseDataSendCallback(callback, response);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public static void responseCreateItem(final long projectId, long parentId, String title, final Handler callback) {
        String xCsrf = ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam._CSRF);
        ApiOutline api = ApiModule.getApiOutline();
        api.addItemOutline(xCsrf, "0", projectId, parentId, title).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null) {
                    try {
                        JSONObject object = new JSONObject(new String(response.body().bytes()));
                        if (!object.isNull("err")) {
                            if ("Text not found".equals(object.getString("err"))) {
                                ManagerApplication.getInstance().showToast(R.string.error_add_new_item_outline_empty_text);
                            } else {
//                                Crashlytics.logException(new Exception("[ManagerOutline:responseCreateItem] user -"+ManagerAccount.getMyProfile().getFio()+"  response -   "+object.toString()));
                                ManagerApplication.getInstance().showToast(R.string.error_add_new_item_outline);
                            }
                            ManagerProgressDialog.dismiss();
                        } else {
                            Line newLine = null;
                            if (object.has("id")) {
                                newLine = Line.findOrCreate(Line.class, object.getLong("id"));
                            }

                            if (!object.isNull("name")) {
                                newLine.setTitle(object.getString("name"));
                                newLine.setTitleReplacingTags(newLine.getTitle());
                            }

                            if (object.has("post")) {
                                JSONObject post = object.getJSONObject("post");
                                if (post.has("parent_item_id")) {
                                    newLine.setParentItemId(post.getLong("parent_item_id"));
                                }
                                newLine.setProjectId(projectId);
                                if (!post.isNull("name")) {
                                    newLine.setTitle(post.getString("name"));
                                }
                                if (!object.has("id") && !post.isNull("id")) {
                                    newLine.setServerId(post.getLong("id"));
                                }
                            }
                            updateProjectFromObject(object);
                            responseOkSendObject(callback, newLine);
                        }
                    } catch (JSONException | IOException e) {
                        try {
                            Writer writer = new StringWriter();
                            e.printStackTrace(new PrintWriter(writer));
                            String s = writer.toString();
//                            Crashlytics.logException(new Exception("[ManagerOutline:responseCreateItem] exception user - "+ManagerAccount.getMyProfile().getFio()+"     "+s));
                        } catch (Exception exp) {
                            exp.printStackTrace();
                        }
                        ManagerApplication.getInstance().showToast(R.string.error_add_new_item_outline);
                        ManagerProgressDialog.dismiss();
                        responseError(callback);
                    }
                } else {
                    responseError(callback);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                try {
                    Writer writer = new StringWriter();
                    t.printStackTrace(new PrintWriter(writer));
                    String s = writer.toString();
//                    Crashlytics.logException(new Exception("[ManagerOurtline:ResponseCreateItem error exception - "+ManagerAccount.getMyProfile().getFio()+"     "+s));
                } catch (Exception exp) {
                    exp.printStackTrace();
                }

                ManagerApplication.getInstance().showToast(R.string.error_add_new_item_outline);
                ManagerProgressDialog.dismiss();
                responseError(callback);
            }
        });
    }

    public static void updateProjectFromObject(JSONObject object) throws JSONException {
        if (!object.isNull("project_id") && !object.isNull("count_items")) {
            Project project = Project.findByServerId(Project.class, object.getLong("project_id"));
            if (project != null) {
                project.setCountItems(String.valueOf(object.getLong("count_items")));
                project.save();
            }
        }

    }

    public static void responseGetAttributesProject(final long projectId, final Handler callback) {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ApiModule.getApiOutline().getAttributesProject(getCsrfToken(), projectId).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    ManagerNet managerNetHelper = new ManagerNet();
                    try {
                        JSONObject obj = new JSONObject(managerNetHelper.readResponse(response));
                        if ("ok".equals(obj.getString("status").toLowerCase())) {
                            ManagerJSONParsing.parseAttributes(obj, projectId);
                            responseOk(callback);
                        } else {
                            responseError(callback);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        responseError(callback);
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseError(callback);
                }
            });
        } else {
            responseError(callback);
        }
    }
}