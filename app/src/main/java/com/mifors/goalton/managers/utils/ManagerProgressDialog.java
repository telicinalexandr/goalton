package com.mifors.goalton.managers.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.mifors.goalton.managers.ManagerApplication;

/**
 * Created by gly8 on 14.07.16.
 */
@SuppressWarnings("ALL")
public class ManagerProgressDialog {
    private static ProgressDialog progressDialog;

    /**
     * Показывает диалог прогресса c шариком
     *
     * @param resMessage - сообщение прогресса
     * @param context    - контекст в котором показывать диалог
     */
    public static void showProgressBar(Context context, int resMessage, boolean isCancelable) {
        showProgressBar(context, context.getResources().getString(resMessage), isCancelable);
    }

    public static void showProgressBar(Context context, String resMessage, boolean isCancelable) {
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
            } catch (IllegalArgumentException e) { // Activity уничтоженно
                return;
            }
        }
        try {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage(resMessage);
            progressDialog.setCancelable(isCancelable);
            progressDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changeTitle(int resTitle) {
        if (progressDialog != null) {
            progressDialog.setMessage(ManagerApplication.getInstance().getApplicationContext().getString(resTitle));
        }
    }

    public static void changeTitle(String title) {
        if (progressDialog != null) {
            progressDialog.setMessage(title);
        }
    }

    /**
     * @return isShowDialog
     * */
    public static boolean changeTitleIsSet(String title) {
        if (progressDialog != null) {
            progressDialog.setMessage(title);
            return true;
        }
        return false;
    }

    /**
     * Скрывает диалог прогресса
     */
    public static void dismiss() {
        if (progressDialog != null) {
            try {
                progressDialog.dismiss();
                progressDialog = null;
            } catch (IllegalArgumentException e1) {
            } catch (Exception e) {
            }
        }
    }

    public static boolean isRun() {
        return progressDialog != null;
    }

}
