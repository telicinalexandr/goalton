package com.mifors.goalton.managers.utils;

import android.os.Handler;

import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by gly8 on 21.03.17.
 */

@SuppressWarnings("ALL")
public class ManagerNet {

    /**
     * Проверка пришедшего ответа на наличие ошибок
     *
     * @param callback - вызывается после обработки
     * @param response - ответ сервера
     */
    public void checkReponse(Handler callback, Response<ResponseBody> response) throws JSONException {

            JSONObject obj = getResultFromResponse(response);
            if (ManagerJSONParsing.isCheckStatusServerResponse(obj)) {
                if (callback != null) {
                    callback.sendMessage(callback.obtainMessage(HttpParam.RESPONSE_OK, obj));
                }
            } else {
                if (callback != null) {
                    callback.sendMessage(callback.obtainMessage(HttpParam.RESPONSE_ERROR, obj));
                }
            }
    }

    /**
     * Чтение ответа сервера и преобразование к JSONObject
     *
     * @param response - ответ с сервера
     * @return преобразованый ответ
     **/
    public JSONObject getResultFromResponse(Response<ResponseBody> response) throws JSONException {
        return new JSONObject(readResponse(response));
    }


    /**
     * Чтение ответа сервера и преобразование к JSONObject
     *
     * @param response - ответ с сервера
     * @return считанная строка
     **/
    public String readResponse(Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            return readResultFromResponse(response.body());
        } else {
            return readResultFromResponse(response.errorBody());
        }
    }

    /**
     * Чтение ответа сервера и преобразование к JSONObject
     *
     * @param response - ответ с сервера
     * @return InputStream
     **/
    public InputStream getInputStream(Response<ResponseBody> response) {
        if (response.isSuccessful()) {
            return response.body().byteStream();
        } else {
            return response.errorBody().byteStream();
        }
    }


    /**
     * Чтение тела ответа
     *
     * @param body - тело запроса ошибочный или нормальный
     * @return считанная строка
     **/
    public String readResultFromResponse(ResponseBody body) {
        return new String(getBytesFromInputStream(body.byteStream()));
    }

    public byte[] getBytesFromInputStream(InputStream inputStream) {
        try {
            String line;
            StringBuilder sb = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString().getBytes();
        } catch (OutOfMemoryError | IOException e) {
            return null;
        }
    }
}