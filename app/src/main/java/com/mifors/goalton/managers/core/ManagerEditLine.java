package com.mifors.goalton.managers.core;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.R;
import com.mifors.goalton.interfaces.InterfaceManagerCreateLine;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerNet;
import com.mifors.goalton.managers.utils.ManagerNetHelper;
import com.mifors.goalton.managers.utils.ManagerParseLine;
import com.mifors.goalton.managers.utils.ManagerTakePhoto;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.ProjectAssign;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.utils.ResponseRunnabel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mifors.goalton.network.HttpParam.getCsrfToken;

/**
 * Created by gly8 on 02.11.17.
 */
public class ManagerEditLine extends ManagerNetHelper {

    private ArrayList<Runnable> arrRunableResponse = new ArrayList<>();
    private InterfaceManagerCreateLine interfaceManagerCreateLine;
    private Line updateLine;
    private Line tempLine;
    private ManagerTakePhoto managerTakePhoto = new ManagerTakePhoto();

    public ManagerEditLine(InterfaceManagerCreateLine interfaceManagerCreateLine, Line updateLine) {
        this.interfaceManagerCreateLine = interfaceManagerCreateLine;
        this.updateLine = updateLine;
    }

    public ManagerEditLine(InterfaceManagerCreateLine interfaceManagerCreateLine) {
        this.interfaceManagerCreateLine = interfaceManagerCreateLine;
    }

    public void setTempLine(Line tempLine) {
        this.tempLine = tempLine;
    }

    private void callingRunableArray() {
        if (arrRunableResponse != null && arrRunableResponse.size() > 0) {
            arrRunableResponse.get(0).run();
        }
    }

    public void responseCreateItemOutline() {
        interfaceManagerCreateLine.showProgressBar(R.string.create);
        ManagerOutline.responseCreateItem(updateLine.getProjectId(), updateLine.getParentItemId(), updateLine.getTitle(), new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    try {
                        // Успешно создали задачу на сервере
                        tempLine = (Line) msg.obj;
                        // Переписываем параметры задачи которую отправляем в ту которую создали
                        // т.к. если задача была создана офлайн то она хранится в базе под другим id
                        // ищем всех детей старой задачи созданной офлайн
                        if (updateLine.getOfflineStatus() == Line.StatusOffline.CREATE) {
                            if (updateLine.getCountChild() > 0) {
                                // Устанавливаем им новый parentId
                                ActiveAndroid.beginTransaction();
                                try {
                                    List<Line> listChilds = Line.getChildrenParentLine(updateLine.getServerId());
                                    for (Line child : listChilds) {
                                        child.setParentItemId(tempLine.getServerId());
                                        child.save();
                                    }
                                    ActiveAndroid.setTransactionSuccessful();
                                } finally {
                                    ActiveAndroid.endTransaction();
                                }
                            }
                        }
                        tempLine.update(updateLine);
                        tempLine.updateFinishDateForCurrentDuration();
                        tempLine.updateDatesTextByCalendarNotSave();
                        checkChangeNameImageTempLine();
                        responseUpdateItem();
                    } catch (Exception e) {
                        e.printStackTrace();
                        finishSendLine();
                    }
                } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                    finishSendLine();
                }
            }
        });
    }

    private void checkChangeNameImageTempLine() {
        // Проверяем есть ли offlineId
        if (tempLine.getOfflineId() != 0) {
            // Проверяем есть ли картинка для этого id
            if (managerTakePhoto.isExsistImage(tempLine.getOfflineId())) {
                // Меняем название на новый для того чтобы при отправки картинки брал нужную
                managerTakePhoto.changeNameImage(tempLine.getOfflineId(), tempLine.getServerId());
            }
        }
    }

    public void responseCreateItemPlanner() {
        interfaceManagerCreateLine.showProgressBar(R.string.create);
        responseCreateCalendarItem(tempLine,
                new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.what == HttpParam.RESPONSE_OK) {
                            try {
                                JSONObject obj = (JSONObject) msg.obj;
                                JSONObject model = obj.getJSONObject("model");
                                tempLine.setServerId(model.getLong("id"));
                                checkChangeNameImageTempLine();
                                tempLine.setLevel(2);
                                if (!TextUtils.isEmpty(tempLine.getAssignUserId())) {
                                    try {
                                        ProjectAssign assign = ProjectAssign.findByServerId(ProjectAssign.class, Long.parseLong(tempLine.getAssignUserId()));
                                        if (assign != null) {
                                            tempLine.setAssignUserUsername(assign.getUserName());
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                tempLine.save();
                                if (!obj.isNull("inbox")) {
                                    new ManagerParseLine().startParseLine(obj.getJSONObject("inbox"), 0, true);
                                }

                                // Обновление количества задач в проекте по пришедшему с сервера полю
                                ManagerOutline.updateProjectFromObject(obj);
                                responseUpdateImage();
                                callingRunableArray();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                            Crashlytics.logException(new Exception("[ManagerEditLine:responseCreateItemPlanner] user -" + ManagerAccount.getMyProfile().getFio() + "   msg.what == HttpParam.RESPONSE_ERROR  "));
                            ManagerApplication.getInstance().showToast(R.string.error_add_new_item_outline);
                            finishSendLine();
                        }
                        super.handleMessage(msg);
                    }
                });
    }

    private static void responseCreateCalendarItem(final Line line, final Handler callback) {
        String itemId;
        if (line.getOfflineId() != 0) {
            itemId = "";
        } else {
            itemId = line.getServerId() == 0 ? "" : String.valueOf(line.getServerId()); // Если редактируем задачу то шлем ее id
        }

        String finishDatetime = line.getDeadlineDateEndStr();
        if (!TextUtils.isEmpty(finishDatetime) && finishDatetime.length() < 8) {
            finishDatetime += ":00";
        } else {
            finishDatetime = "";
        }

        String assignUserId = String.valueOf(line.getAssignUserId());
        if (TextUtils.isEmpty(assignUserId)) {
            assignUserId = "0";
        }

        int duration = 0;
        String durationStr = String.valueOf(line.getDuration());
        if (!TextUtils.isEmpty(durationStr)) {
            duration = Integer.parseInt(durationStr);
        }

        String deadlineTime = line.getDeadlineDateStartTimeStr();
        if (duration == 0) {
            deadlineTime = "";
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put("array[0][field]", "item_id");
        map.put("array[0][value]", itemId);

        map.put("array[1][field]", "parent_item_id");
        map.put("array[1][value]", "0");

        map.put("array[2][field]", "name");
        map.put("array[2][value]", line.getTitle());

        map.put("array[3][field]", "project_id");
        map.put("array[3][value]", String.valueOf(line.getProjectId()));

        map.put("array[4][field]", "project_stage_id");
        map.put("array[4][value]", String.valueOf(line.getProjectStageId()));

        map.put("array[5][field]", "deal_id"); // Сделка
        map.put("array[5][value]", "");

        map.put("array[6][field]", "company_id");
        map.put("array[6][value]", String.valueOf(line.getCompanyId()));

        map.put("array[7][field]", "assign_user_id");
        map.put("array[7][value]", assignUserId);

        map.put("array[8][field]", "deadline_date"); // "2017-09-16"
        map.put("array[8][value]", String.valueOf(line.getDeadlineDateStartStr()));

        map.put("array[9][field]", "deadline_time");
        map.put("array[9][value]", deadlineTime);

        map.put("array[10][field]", "finish_date");
        map.put("array[10][value]", finishDatetime);

        map.put("array[11][field]", "duration");
        map.put("array[11][value]", duration);

        map.put("array[12][field]", "is_completed");
        map.put("array[12][value]", line.getIsCompleted());

        map.put("array[13][field]", "is_important");
        map.put("array[13][value]", line.getIsImportant());

        map.put("array[14][field]", "color");
        map.put("array[14][value]", line.getColor());

        ApiModule.getApiOutline().createPlannerItem(getCsrfToken(), map).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String readString = new ManagerNet().readResponse(response);
                    try {
                        JSONObject obj = new JSONObject(readString);
                        if (!obj.isNull("ok")) {
                            responseOkSendObject(callback, obj);
                        } else {
                            Crashlytics.logException(new Exception("ResponseCreateCalendarItem error " + obj.toString()));
                            responseError(callback);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        try {
                            Writer writer = new StringWriter();
                            e.printStackTrace(new PrintWriter(writer));
                            String s = writer.toString();
                            Crashlytics.logException(new Exception("ResponseCreateCalendarItem error parse - " + ManagerAccount.getMyProfile().getFio() + "   resultRsponse =  " + readString + "  prinStack - " + s));
                        } catch (Exception exp) {
                            exp.printStackTrace();
                        }

                        responseError(callback);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    e.printStackTrace();
                    try {
                        Writer writer = new StringWriter();
                        e.printStackTrace(new PrintWriter(writer));
                        String s = writer.toString();
                        Crashlytics.logException(new Exception("ResponseCreateCalendarItem error parse 2 - " + ManagerAccount.getMyProfile().getFio() + "  resultNotRead - prinStack - " + s));
                    } catch (Exception exp) {
                        exp.printStackTrace();
                    }

                    responseError(callback);

                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                responseError(callback);
            }
        });
    }

    public void responseUpdateItem() {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            interfaceManagerCreateLine.showProgressBar(R.string.saves_changes);
            responseSetComplete();
            responseSetImportant();
            responseUpdateName();
            responseSetCompany();
            responseUpdateAssign();
            responseSetDeadlineDate();

            if (tempLine.getProjectStageId() == 0) {
                responseDeleteStage();
            } else {
                responseUpdateProjectStage();
            }

            if (tempLine.getColor() != Integer.MIN_VALUE) {
                responseSetColor();
            }
            responseUpdateImage();
            callingRunableArray();
        }
    }

    // Дата начала задачи так же отправляется и время выполнение
    private void responseSetDeadlineDate() {
        arrRunableResponse.add(new ResponseRunnabel("responseSetDeadlineDate") {
            @Override
            public void run() {
                String time = tempLine.getDeadlineDateStartTimeStr();
                if (tempLine.getDuration() == 0) {
                    time = "";
                }
                ManagerOutline.responseSetDeadlineDate(tempLine.getServerId(), tempLine.getDeadlineDateStartStr(), time, tempLine.getDuration(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseUpdateImage() {
        arrRunableResponse.add(new ResponseRunnabel("responseUpdateImage") {
            @Override
            public void run() {
                final ManagerTakePhoto managerTakePhoto = new ManagerTakePhoto();
                File file = managerTakePhoto.getFileFromDirectory(tempLine.getServerId());
                if (file.exists()) {
                    RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                    MultipartBody.Part body = MultipartBody.Part.createFormData("Item[img]", file.getName() + ".jpg", requestFile);
                    RequestBody longValue = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(tempLine.getServerId()));
                    ApiModule.getApiCalendar().updateImage(getCsrfToken(), longValue, body).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            checkFinishSendData(getRunnable());
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            checkFinishSendData(getRunnable());
                        }
                    });
                } else {
                    ApiModule.getApiCalendar().deleteImage(getCsrfToken(), tempLine.getServerId()).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            checkFinishSendData(getRunnable());
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            checkFinishSendData(getRunnable());
                        }
                    });
                }
            }
        });
    }

    private void responseUpdateAssign() {
        arrRunableResponse.add(new ResponseRunnabel("responseUpdateAssign") {
            private long assignId = 0;

            @Override
            public void run() {
                if (!TextUtils.isEmpty(tempLine.getAssignUserId())) {
                    assignId = Long.parseLong(tempLine.getAssignUserId());
                }
                ManagerPlanner.responseSetAssign(tempLine.getServerId(), assignId, new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseDeleteStage() {
        arrRunableResponse.add(new ResponseRunnabel("responseDeleteStage") {
            @Override
            public void run() {
                ManagerPlanner.responseDeleteStageItem(tempLine.getServerId(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseUpdateProjectStage() {
        arrRunableResponse.add(new ResponseRunnabel("responseUpdateProjectStage") {


            @Override
            public void run() {
                ManagerPlanner.responseUpdateStageItem(tempLine.getServerId(), tempLine.getProjectStageId(), tempLine.getIsCompleted(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseSetCompany() {
        arrRunableResponse.add(new ResponseRunnabel("responseSetCompany") {
            @Override
            public void run() {
                ManagerPlanner.responseSetCompany(tempLine.getServerId(), tempLine.getCompanyId(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseSetColor() {
        arrRunableResponse.add(new ResponseRunnabel("responseSetColor") {
            @Override
            public void run() {
                ManagerPlanner.responseSetColor(tempLine.getServerId(), String.valueOf(tempLine.getColor()), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseUpdateName() {
        arrRunableResponse.add(new ResponseRunnabel("responseUpdateName") {
            @Override
            public void run() {
                ManagerOutline.responseUpdateName(tempLine.getServerId(), tempLine.getTitle(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
            }
        });
    }

    private void responseSetImportant() {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            arrRunableResponse.add(new ResponseRunnabel("responseSetImportant") {
                @Override
                public void run() {
                    ManagerOutline.responseSetImportant(tempLine.getServerId(), tempLine.getIsImportant(), new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            interfaceManagerCreateLine.updateStatusImportantCompletion();
                            checkFinishSendData(getRunnable());
                            super.handleMessage(msg);
                        }
                    });
                    super.run();
                }
            });
        }
    }

    private void responseSetComplete() {
        arrRunableResponse.add(new ResponseRunnabel("responseSetComplete") {
            @Override
            public void run() {
                interfaceManagerCreateLine.updateStatusImportantCompletion();
                ManagerOutline.responseSetCompletedLine(tempLine, tempLine.getIsCompleted(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        interfaceManagerCreateLine.updateStatusImportantCompletion();
                        checkFinishSendData(getRunnable());
                        super.handleMessage(msg);
                    }
                });
                super.run();
            }
        });
    }

    private void finishSendLine() {
        ActiveAndroid.beginTransaction();
        try {
            updateLine = null;
            if (tempLine != null) {
                if (tempLine.getOfflineStatus() == Line.StatusOffline.CREATE) {
                    // Если задача была создана офлайн то удаляем из базы уже ненужную временную задачу
                    // Она создается для того чтобы можно было корректно сохранить картинку и отправить на сервак
                    Line line = Line.findByServerId(Line.class, tempLine.getOfflineId());
                    if (line != null) {
                        line.delete();
                    }
                    tempLine.setOfflineId(0);
                }

                tempLine.setOfflineStatus(null);
                tempLine.setModeEdit(null);
                tempLine.setNew(false);
                tempLine.save();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
        interfaceManagerCreateLine.finishResponse(tempLine);
    }

    private void checkFinishSendData(Runnable runnable) {
        if (runnable != null) {
            if (arrRunableResponse != null) {
                arrRunableResponse.remove(runnable);
                if (arrRunableResponse.size() == 0) {
                    finishSendLine();
                } else if (arrRunableResponse.size() >= 1) {
                    arrRunableResponse.get(0).run();
                } else {
                    arrRunableResponse = null;
                    finishSendLine();
                }
            } else {
                finishSendLine();
            }
        } else {
            if (arrRunableResponse != null) {
                arrRunableResponse.clear();
            }
            finishSendLine();
        }
    }
}
