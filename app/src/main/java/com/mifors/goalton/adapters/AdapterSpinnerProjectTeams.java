package com.mifors.goalton.adapters;


import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.project.ProjectRight;

public class AdapterSpinnerProjectTeams extends ArrayAdapter<ProjectRight> {

    ProjectRight[] projectRights;
    LayoutInflater inflater;
    int resourse;

    public AdapterSpinnerProjectTeams(@NonNull Context context, @LayoutRes int resource, @NonNull ProjectRight[] objects) {
        super(context, resource, objects);
        this.projectRights = objects;
        this.resourse = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return projectRights.length;
    }

    @Nullable
    @Override
    public ProjectRight getItem(int position) {
        return projectRights[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourse,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(projectRights[position].getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourse,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(projectRights[position].getName());
        return convertView;
    }

    private class ViewHolder{
        TextView name;

        public ViewHolder(View holder) {
            this.name = holder.findViewById(R.id.item_spinner_acount_text);
        }
    }
}
