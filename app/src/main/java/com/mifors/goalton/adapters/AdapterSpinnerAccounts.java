package com.mifors.goalton.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.team.TeamAccount;


public class AdapterSpinnerAccounts extends ArrayAdapter<TeamAccount> {
    Context context;
    TeamAccount[] accounts;
    int resourse;
    LayoutInflater inflater;
    public AdapterSpinnerAccounts(@NonNull Context context, @LayoutRes int resource, @NonNull TeamAccount[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.accounts = objects;
        this.resourse = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return accounts.length;
    }

    @Nullable
    @Override
    public TeamAccount getItem(int position) {
        return accounts[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourse,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(accounts[position].getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(resourse,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(accounts[position].getName());
        return convertView;
    }

    private class ViewHolder{
        TextView name;

        ViewHolder(View holder) {
            this.name = holder.findViewById(R.id.item_spinner_acount_text);
        }
    }
}


