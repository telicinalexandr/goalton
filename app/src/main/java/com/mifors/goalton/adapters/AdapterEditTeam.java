package com.mifors.goalton.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.interfaces.InterfaceOnGroupMenuClickListener;
import com.mifors.goalton.model.team.TeamProfile;

import java.util.List;


public class AdapterEditTeam extends BaseExpandableListAdapter {

    Context context;
    List<TeamProfile> list;
    InterfaceOnGroupMenuClickListener onclick;

    public AdapterEditTeam(Context context, List<TeamProfile> list, InterfaceOnGroupMenuClickListener onclick) {
        this.context = context;
        this.list = list;
        this.onclick = onclick;
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (list.get(groupPosition).getTeamProjects() == null){
            return 0;
        }else {
            return list.get(groupPosition).getTeamProjects().size();
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return list.get(groupPosition).getTeamProjects().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_edit_team_group,null);
        }
        ((TextView) convertView.findViewById(R.id.item_edit_team_group_title)).setText(list.get(groupPosition).getUsername());
        if (list.get(groupPosition).getTeamProjects() == null){
            ((TextView) convertView.findViewById(R.id.item_edit_team_child_count)).setText("0");
        } else {
            ((TextView) convertView.findViewById(R.id.item_edit_team_child_count)).setText(String.valueOf(list.get(groupPosition).getTeamProjects().size()));
        }

        if (list.size() == 1 && list.get(0).getTeamProjects().size() == 0){
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_round_view_gray_light);
        } else if (groupPosition == 0){
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_view_gray_light_round_top);
        } else if (groupPosition == list.size()-1 && !isExpanded){
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_view_gray_light_round_bottom);
        } else {
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_view_gray_light);
        }

        convertView.findViewById(R.id.item_team_context_menu_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick.onClickMenuItem(v,groupPosition);
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_team_child,null);
        }
        if (list.get(groupPosition).getTeamProjects() == null){
            return null;
        } else {
            ((TextView) convertView.findViewById(R.id.item_team_child_title)).setText(list.get(groupPosition).getTeamProjects().get(childPosition).getName());
        }

        if (isLastChild && groupPosition == (list.size() - 1)){
            convertView.findViewById(R.id.item_team_child_root).setBackgroundResource(R.drawable.shape_view_gray_light_round_bottom);
        } else {
            convertView.findViewById(R.id.item_team_child_root).setBackgroundResource(R.drawable.shape_view_gray_light);
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
