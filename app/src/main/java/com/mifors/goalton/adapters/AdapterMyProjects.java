package com.mifors.goalton.adapters;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.mifors.goalton.DebugKeys;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.interfaces.InterfaceOnClickProjectPanelBtns;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.utils.RoundedTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterMyProjects extends RecyclerView.Adapter<AdapterMyProjects.ViewHolder> {
    private static final String TAG = "Goalton [" + AdapterMyProjects.class.getSimpleName() + "]";

    private Context context;
    private List<Project> list;
    private AdapterView.OnItemClickListener onItemClickListener;
    private InterfaceOnClickProjectPanelBtns interfaceOnClickProjectPanelBtns;
    private ViewHolder holderSelected;
    private final long DEFAULT_FLIP_DURATION = 200;

    public AdapterMyProjects(List<Project> list, AdapterView.OnItemClickListener onItemClickListener, InterfaceOnClickProjectPanelBtns interfaceOnClickProjectPanelBtns) {
        this.list = list;
        this.onItemClickListener = onItemClickListener;
        this.interfaceOnClickProjectPanelBtns = interfaceOnClickProjectPanelBtns;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView btnDelete, btnEdit;
        public ImageView cover;
        private RelativeLayout coverLayout, settingsLayout;
        private DonutProgress progress;
        private View containerPublicStatus;
        private TextView items;
        private long projectId;
        private View bigHead;
        private TextView anotherUserName;

        public ViewHolder(View itemView) {
            super(itemView);
            containerPublicStatus = itemView.findViewById(R.id.container_public);
            bigHead = itemView.findViewById(R.id.project_big_head);
            coverLayout = itemView.findViewById(R.id.container_cover);
            settingsLayout = itemView.findViewById(R.id.container_layout_settings);
            title = itemView.findViewById(R.id.project_name);
            cover = itemView.findViewById(R.id.project_cover);
            btnDelete = itemView.findViewById(R.id.item_project_delete);
            btnEdit = itemView.findViewById(R.id.item_project_edit);
            progress = itemView.findViewById(R.id.project_progress);
            items = itemView.findViewById(R.id.project_items);
            anotherUserName = itemView.findViewById(R.id.other_user_name);

            coverLayout.setOnClickListener(this);
            settingsLayout.setOnClickListener(this);
            btnDelete.setOnClickListener(this);
            btnEdit.setOnClickListener(this);
        }

        public void flip(final View viewFrom, final View viewTo, long duration) {
            ObjectAnimator animator = ObjectAnimator.ofFloat(viewFrom, "rotationY", 0f, 90f);
            animator.setDuration(duration);
            animator.start();
            viewFrom.setEnabled(false);
            final ObjectAnimator animator2 = ObjectAnimator.ofFloat(viewTo, "rotationY", -90f, 0f);
            animator2.setDuration(duration);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    if (animation.getAnimatedFraction() == 1f) {
                        viewFrom.setEnabled(true);
                        viewFrom.setVisibility(View.GONE);
                        animator2.start();
                        viewTo.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.container_cover:
                    ActiveAndroid.beginTransaction();
                    try {
                        for (Project p : list) { // Установили значение что этот проект выбран
                            if (p.getServerId() == projectId) {
                                p.setRevertCover(true);
                                p.save();
                            } else {
                                p.setRevertCover(false);
                                p.save();
                            }
                        }
                        ActiveAndroid.setTransactionSuccessful();
                    } finally {
                        ActiveAndroid.endTransaction();
                    }

                    flip(v, settingsLayout, DEFAULT_FLIP_DURATION); // Переворачиваем поебень
                    if (holderSelected != null) {
                        recoverItem(holderSelected);
                    }
                    setSelected();
                    break;
                case R.id.container_layout_settings:
                    interfaceOnClickProjectPanelBtns.onClickOpenOutline(projectId);
                    break;
                default:
                    onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
            }
        }

        public void showCover() {
            coverLayout.setVisibility(View.GONE);
            settingsLayout.setVisibility(View.VISIBLE);
        }

        public void hideCover() {
            coverLayout.setVisibility(View.VISIBLE);
            settingsLayout.setVisibility(View.GONE);
        }

        private void setSelected() {
            holderSelected = this;
        }

        private void recoverItem(ViewHolder holderSelected) {
            flip(holderSelected.settingsLayout, holderSelected.coverLayout, DEFAULT_FLIP_DURATION);
        }

        public void saveWidthCoverToShared() {
            coverLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (Build.VERSION.SDK_INT < 16) {
                        coverLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        coverLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }

                    int width = coverLayout.getWidth();
                    int height = coverLayout.getHeight();
                    if (width > 0 && height > 0) {
                        ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.WIDTH_COVER, width);
                        ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.HEIGHT_COVER, height);
                    }
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Project p = list.get(position);
        if (position == 0) {
            holder.saveWidthCoverToShared();
        }
        holder.projectId = p.getServerId();

        // cover side
        holder.title.setText(p.getName());
        if (ManagerApplication.getInstance().isConnectToInternet(false) && !TextUtils.isEmpty(p.getUrlImage())) {
            holder.bigHead.setVisibility(View.GONE);
            if (ManagerApplication.getInstance().getSharedManager().getValueLong(SharedKeys.ID_PROJECT_RELOAD_IMAGE) == p.getServerId()) {
                loadImageByNoMemory(holder, p);
                ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_RELOAD_IMAGE, -200);
            } else {
                loadImage(holder, p);
            }
        } else {
            holder.bigHead.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(R.drawable.bg_default)
                    .transform(new RoundedTransformation(15, 0))
                    .into(holder.cover);
        }

        //dark side
        holder.progress.setProgress(p.getPercentComplete());
        holder.items.setText(context.getString(R.string.project_items) + " " + p.getCountItems());
        if ("0".equals(p.getRightToEditProject())) {
            holder.btnDelete.setVisibility(View.INVISIBLE);
            holder.btnEdit.setVisibility(View.INVISIBLE);
        }

        if ("public".equals(p.getType())) {
            holder.containerPublicStatus.setVisibility(View.VISIBLE);
        } else {
            holder.containerPublicStatus.setVisibility(View.GONE);
        }

        holder.btnDelete.setTag(p.getServerId());
        if (TextUtils.isEmpty(p.getOtherUserName())) {
            holder.anotherUserName.setVisibility(View.GONE);
        } else {
            holder.anotherUserName.setVisibility(View.VISIBLE);
            holder.anotherUserName.setText(p.getOtherUserName());
        }

        if (p.isRevertCover()) {
            holder.showCover();
        } else {
            holder.hideCover();
        }
    }

    private void loadImageByNoMemory(final ViewHolder holder, Project p) {
        Picasso.Builder builder = new Picasso.Builder(ManagerApplication.getInstance().getApplicationContext());
        builder.build()
                .load(p.getUrlImage())
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .transform(new RoundedTransformation(9, 0))
                .into(holder.cover, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.bigHead.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        Log.v(TAG, "[loadImageByNoMemory:onError] error");
                        holder.bigHead.setVisibility(View.VISIBLE);
                        Picasso.with(ManagerApplication.getInstance().getApplicationContext())
                                .load(R.drawable.bg_default)
                                .transform(new RoundedTransformation(11, 0))
                                .into(holder.cover);
                    }
                });
    }

    private void loadImage(final ViewHolder holder, Project p) {
        Picasso.Builder builder = new Picasso.Builder(ManagerApplication.getInstance().getApplicationContext());
        builder.build()
                .load(p.getUrlImage())
                .transform(new RoundedTransformation(11, 0))
                .into(holder.cover, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.bigHead.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                        Log.v(TAG, "[loadImage:onError] error");
                        holder.bigHead.setVisibility(View.VISIBLE);
                        Picasso.with(ManagerApplication.getInstance().getApplicationContext())
                                .load(R.drawable.bg_default)
                                .transform(new RoundedTransformation(11, 0))
                                .into(holder.cover);
                    }
                });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.context = recyclerView.getContext();
    }

    public void revertCover() {
        if (holderSelected != null) {
            holderSelected.recoverItem(holderSelected);
            ActiveAndroid.beginTransaction();
            try {
                for (Project p : list) { // Установили значение что этот проект выбран
                    p.setRevertCover(false);
                    p.save();
                }
                holderSelected = null;
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }
        }
    }

    public void reloadDataProjectFromDb(long projectID) {
        for (Project pr : list) {
            if (pr.getServerId() == projectID) {
                Project prFind = Project.findByServerId(Project.class, projectID);
                pr.setUrlImage(prFind.getUrlImage());
                pr.setName(prFind.getName());
                pr.setType(prFind.getType());
                pr.setCountItems(prFind.getCountItems());
                pr.save();
                return;
            }
        }
    }
}