package com.mifors.goalton.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.project.ProjectSubject;


public class AdapterSpinnerProjectSubject extends ArrayAdapter<ProjectSubject> {
    ProjectSubject[] projectSubjects;
    int resource;
    LayoutInflater inflater;

    public AdapterSpinnerProjectSubject(@NonNull Context context, @LayoutRes int resource, @NonNull ProjectSubject[] objects) {
        super(context, resource, objects);
        this.projectSubjects = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return projectSubjects.length;
    }

    @Nullable
    @Override
    public ProjectSubject getItem(int position) {
        return projectSubjects[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(resource,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(projectSubjects[position].getName());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null){
            convertView = inflater.inflate(resource,parent,false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(projectSubjects[position].getName());
        return convertView;
    }

    private class ViewHolder{
        TextView name;
        public ViewHolder(View holder) {
            this.name = holder.findViewById(R.id.item_spinner_acount_text);
        }
    }
}
