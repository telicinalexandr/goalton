package com.mifors.goalton.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mifors.goalton.R;
import com.mifors.goalton.utils.ImageViewRounder;

/**
 * Created by gly8 on 23.03.17.
 */

@SuppressWarnings("ALL")
public class AdapterGridBgProfileSettings extends BaseAdapter {

    private static final String TAG = "Goalton [" + AdapterGridBgProfileSettings.class.getSimpleName() + "]";
    private String[] arrNamesBg;
    private Context mContext;
    private int selectedPosition; // На один меньше чем в название
    final float roundPx;

    public AdapterGridBgProfileSettings(Context c, String[] arrNamesBg) {
        this.mContext = c;
        this.arrNamesBg = arrNamesBg;
        Resources r = mContext.getResources();
        roundPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics());
    }

    public int getCount() {
        return arrNamesBg.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView img;
        View checkView;

        public ViewHolder(View parent) {
            img = parent.findViewById(R.id.img);
            checkView = parent.findViewById(R.id.check_view);
        }
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.item_grid_bg_profile_settings, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) convertView.getTag();
        }

        String nameImage = arrNamesBg[position] + "_mini";
        int drawableResourceId = view.getContext().getResources().getIdentifier(nameImage, "drawable", "com.mifors.goalton");
        holder.img.setImageResource(drawableResourceId);
        Drawable dr = ContextCompat.getDrawable(view.getContext(), drawableResourceId);
        Drawable currentState = dr.getCurrent();
        if (currentState instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) currentState).getBitmap();
            try {
                holder.img.setImageBitmap(ImageViewRounder.getRoundedCornerBitmap(bitmap, roundPx));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (selectedPosition == position) {
            holder.checkView.setVisibility(View.VISIBLE);
        } else {
            holder.checkView.setVisibility(View.GONE);
        }
        return view;
    }


    public String getValue(int position) {
        try {
            return arrNamesBg[position];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}