package com.mifors.goalton.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.myteams.Invite;

import java.util.List;

public class AdapterMyInvites extends RecyclerView.Adapter<AdapterMyInvites.ViewHolder> {
    private static final String TAG = "Goalton [" + AdapterMyInvites.class.getSimpleName() + "]";

    private List<Invite> list;
    private View.OnClickListener onClickListener;

    public AdapterMyInvites(List<Invite> list, View.OnClickListener onClickListener, Context context) {
        this.list = list;
        this.onClickListener = onClickListener;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleTeam, titleProject;
        private View btnAccept, btnFailure;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTeam = itemView.findViewById(R.id.title_team);
            titleProject = itemView.findViewById(R.id.title_project);
            btnAccept = itemView.findViewById(R.id.btn_accept);
            btnFailure = itemView.findViewById(R.id.btn_failure);
            btnAccept.setOnClickListener(onClickListener);
            btnFailure.setOnClickListener(onClickListener);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_invites, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Invite team = list.get(position);
        holder.btnAccept.setTag(team.getId());
        holder.btnFailure.setTag(team.getId());

        holder.titleTeam.setText(team.getNameTeam());
        holder.titleProject.setText(team.getNameProject());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

}