package com.mifors.goalton.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.project.ProjectStage;
import com.mifors.goalton.ui.ItemTouchHelper.ItemTouchHelperAdapter;
import com.mifors.goalton.ui.ItemTouchHelper.ItemTouchHelperProjectStage;

import java.util.Collections;
import java.util.List;

public class AdapterProjectStages extends RecyclerView.Adapter<AdapterProjectStages.ViewHolder> implements ItemTouchHelperAdapter {
    private static final String TAG = "Goalton [" + AdapterProjectStages.class.getSimpleName() + "]";
    private List<com.mifors.goalton.model.project.ProjectStage> list;
    private AdapterView.OnItemClickListener onItemClickListener;

    public AdapterProjectStages(List<com.mifors.goalton.model.project.ProjectStage> list, AdapterView.OnItemClickListener onItemClickListener) {
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    public void setList(List<com.mifors.goalton.model.project.ProjectStage> list) {
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperProjectStage, View.OnClickListener {
        private View root;
        private TextView stageName;
        private TextView stageMaxMinDays;
        private ImageView btnMenu;
        private TextView titleNumberStage;
        private View menuProjectStages;

        ViewHolder(View itemView) {
            super(itemView);
            root =itemView.findViewById(R.id.root_view);
            stageName = itemView.findViewById(R.id.project_stage_name);
            btnMenu = itemView.findViewById(R.id.project_stage_menu);
            stageMaxMinDays = itemView.findViewById(R.id.project_stage_max_min_days);
            titleNumberStage = itemView.findViewById(R.id.title_number_stage);
            btnMenu.setOnClickListener(this);
        }

        @Override
        public void onItemSelected() {
        }

        @Override
        public void onItemClear() {
            notifyDataSetChanged();
        }

        // Конец перемещения - обновление поля сортировка
        @Override
        public void endDrag() {
            root.setBackgroundColor(Color.parseColor("#FFFFFF"));
            for (int i = 0; i < list.size(); i++) {
                ProjectStage st = list.get(i);
                st.setSort(String.valueOf(i));
                st.save();
            }
        }

        @Override
        public void startDrag() {
            root.setBackgroundColor(Color.parseColor("#A9A9A9"));
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project_stage, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProjectStage projectStage = list.get(position);
        holder.titleNumberStage.setText("" + position + ".");
        holder.stageName.setText(projectStage.getName());
        StringBuilder builderMaxMinDays = new StringBuilder();
        if (!TextUtils.isEmpty(projectStage.getPeriod())) {
            builderMaxMinDays.append("MAX DAYS = " + projectStage.getPeriod());
        }

        if (!TextUtils.isEmpty(projectStage.getLimitItem())) {

            // Если было добавлено поле MAX -DAYS добавляем пробел
            if (!TextUtils.isEmpty(builderMaxMinDays)) {
                builderMaxMinDays.append(", ");
            }

            builderMaxMinDays.append("WIP LIMIT = " + projectStage.getLimitItem());
        }

        holder.stageMaxMinDays.setText(builderMaxMinDays);
        holder.btnMenu.setTag(projectStage.getServerId());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(list, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
    }
}