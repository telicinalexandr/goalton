package com.mifors.goalton.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.myteams.MyTeam;
import com.mifors.goalton.model.myteams.MyTeamProject;

import java.util.List;

public class AdapterMemberships extends RecyclerView.Adapter<AdapterMemberships.ViewHolder> {
    private static final String TAG = "Goalton [" + AdapterMemberships.class.getSimpleName() + "]";

    private List<MyTeam> list;
    private View.OnClickListener onClickListener;
    private LayoutInflater inflater;

    public AdapterMemberships(List<MyTeam> list, View.OnClickListener onClickListener, Context context) {
        this.list = list;
        this.onClickListener = onClickListener;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleNameteam, titleUserName;
        private View btnDeleteMyFromTeams;
        private LinearLayout containerProjects;

        public ViewHolder(View itemView) {
            super(itemView);
            titleNameteam = itemView.findViewById(R.id.title_team_name);
            titleUserName = itemView.findViewById(R.id.title_username);
            containerProjects = itemView.findViewById(R.id.container_my_projects);
            btnDeleteMyFromTeams = itemView.findViewById(R.id.btn_delete_me_from_teams);
            btnDeleteMyFromTeams.setOnClickListener(onClickListener);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_memberships, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        MyTeam team = list.get(position);
        holder.btnDeleteMyFromTeams.setTag(team.getServerId());
        holder.titleNameteam.setText(team.getName());
        holder.titleUserName.setText(team.getUsername() + ", " + team.getEmail());
        addProjectsLines(holder, team.getServerId());
    }

    private void addProjectsLines(ViewHolder holder, long serverId) {
        List<MyTeamProject> list = MyTeamProject.findItemsByTeamServerId(serverId);
        holder.containerProjects.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            MyTeamProject pr = list.get(i);
            View row = inflater.inflate(R.layout.item_membership_project, null);
            ((TextView) row.findViewById(R.id.title_name_team)).setText(pr.getName());
            View btnExitFromProject = row.findViewById(R.id.btn_delete_me_from_team_project);
            btnExitFromProject.setTag(pr.getServerId());
            btnExitFromProject.setOnClickListener(onClickListener);

            if (i == 0) {
                row.findViewById(R.id.root_view).setBackgroundColor(Color.parseColor("#edf6ff"));
            } else if (i % 2 == 0) {
                row.findViewById(R.id.root_view).setBackgroundColor(Color.parseColor("#edf6ff"));
            } else {
                row.findViewById(R.id.root_view).setBackgroundColor(Color.parseColor("#ffffff"));
            }

            holder.containerProjects.addView(row);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    //==========================================
    // Getter setter
    //==========================================
    public void setList(List<MyTeam> list) {
        this.list = list;
    }

}