package com.mifors.goalton.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.model.project.ProjectRightUser;

import java.util.List;

public class AdapterProfileSettingsUsers extends RecyclerView.Adapter<AdapterProfileSettingsUsers.ViewHolder> {
    private List<ProjectRightUser> list;
    private View.OnClickListener onClickListener;

    public AdapterProfileSettingsUsers(List<ProjectRightUser> list, View.OnClickListener onClickListener) {
        this.list = list;
        this.onClickListener = onClickListener;
    }

    public void setList(List<ProjectRightUser> list) {
        this.list = list;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView stageName;
        private CheckBox checkBoxViewItems, checkBoxEditItems, checkBoxInvite;
        private TextView textViewViewItems, textViewEditItems, textViewInvite, textViewTeamMember;
        private int position;

        public ViewHolder(View itemView) {
            super(itemView);
            stageName = itemView.findViewById(R.id.title_name);
            checkBoxViewItems = itemView.findViewById(R.id.permission_view_items);
            checkBoxEditItems = itemView.findViewById(R.id.permission_edit_items);
            checkBoxInvite = itemView.findViewById(R.id.permission_invite);
            checkBoxViewItems.setOnCheckedChangeListener(onEditViews);
            checkBoxEditItems.setOnCheckedChangeListener(onEditItems);
            checkBoxInvite.setOnCheckedChangeListener(onEditInvite);
            textViewViewItems = itemView.findViewById(R.id.title_view_items);
            textViewEditItems = itemView.findViewById(R.id.title_edit_items);
            textViewInvite = itemView.findViewById(R.id.title_invite);
            textViewTeamMember = itemView.findViewById(R.id.title_team_member);
        }

        // Редактирование вьюх
        private CompoundButton.OnCheckedChangeListener onEditViews = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    setChecked(checkBoxEditItems, onEditItems, false);
                }
                saveData();
            }
        };

        // редактирование Item
        private CompoundButton.OnCheckedChangeListener onEditItems = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setChecked(checkBoxViewItems, onEditViews, true);
                }
                saveData();
            }
        };

        // Инвайт
        private CompoundButton.OnCheckedChangeListener onEditInvite = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                saveData();
            }
        };

        // Инициализация чекбокса
        public void initCheckBoxData(ProjectRightUser projectRightUser) {

            if (projectRightUser.getUserId() == ManagerAccount.getMyProfile().getServerId()) {
                checkBoxEditItems.setEnabled(false);
                checkBoxViewItems.setEnabled(false);
                checkBoxInvite.setEnabled(false);
                setChecked(checkBoxEditItems, onEditItems, true);
                setChecked(checkBoxViewItems, onEditViews, true);
                setChecked(checkBoxInvite, onEditInvite, true);
            } else {
                checkBoxEditItems.setEnabled(true);
                checkBoxViewItems.setEnabled(true);
                checkBoxInvite.setEnabled(true);

                if(projectRightUser.getRightToEdit().equals(String.valueOf(projectRightUser.getUserId()))){
                    setChecked(checkBoxEditItems, onEditItems, true);
                    setChecked(checkBoxViewItems, onEditViews, true);
                    setChecked(checkBoxInvite, onEditInvite, true);
                } else {
                    if (projectRightUser.getRightToEdit().equals("1")) {
                        setChecked(checkBoxEditItems, onEditItems, true);
                    }

                    if (projectRightUser.getRightToView().equals("1")) {
                        setChecked(checkBoxViewItems, onEditViews, true);
                    }

                    if (projectRightUser.getRightToInvite().equals("1")) {
                        setChecked(checkBoxInvite, onEditInvite, true);
                    }
                }
            }
        }

        // Сохранение изменений в базу
        private void saveData() {
            ProjectRightUser projectRightUser = list.get(position);
            String viewI = checkBoxViewItems.isChecked() ? "1" : "0";
            String editI = checkBoxEditItems.isChecked() ? "1" : "0";
            String inv = checkBoxInvite.isChecked() ? "1" : "0";

            projectRightUser.setRightToView(viewI);
            projectRightUser.setRightToEdit(editI);
            projectRightUser.setRightToInvite(inv);
            projectRightUser.save();
        }

        private void setChecked(CheckBox appCompatCheckBox, CompoundButton.OnCheckedChangeListener onChecked, boolean isChecked) {
            appCompatCheckBox.setOnCheckedChangeListener(null);
            appCompatCheckBox.setChecked(isChecked);
            appCompatCheckBox.setOnCheckedChangeListener(onChecked);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project_settings_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ProjectRightUser projectRightUser = list.get(position);
        holder.stageName.setText(list.get(position).getUsername());
        holder.stageName.setOnClickListener(onClickListener);
        holder.stageName.setTag(list.get(position).getUserId());
        holder.position = position;
        holder.initCheckBoxData(projectRightUser);
        if (position == 0) {
            holder.textViewEditItems.setVisibility(View.VISIBLE);
            holder.textViewViewItems.setVisibility(View.VISIBLE);
            holder.textViewInvite.setVisibility(View.VISIBLE);
            holder.textViewTeamMember.setVisibility(View.VISIBLE);
        } else {
            holder.textViewEditItems.setVisibility(View.GONE);
            holder.textViewViewItems.setVisibility(View.GONE);
            holder.textViewInvite.setVisibility(View.GONE);
            holder.textViewTeamMember.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}