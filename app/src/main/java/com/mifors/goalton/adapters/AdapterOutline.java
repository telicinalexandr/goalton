package com.mifors.goalton.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.mifors.goalton.R;
import com.mifors.goalton.activity.ActivityEditLineOutline;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.ui.ItemTouchHelper.ItemTouchHelperAdapter;
import com.mifors.goalton.utils.OutlineLineUIHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AdapterOutline extends MultiLevelExpIndListAdapter implements ItemTouchHelperAdapter {
    private static final String TAG = "Goalton [" + AdapterOutline.class.getSimpleName() + "]";

    private Context context;
    private List<Line> list;
    private AdapterView.OnItemClickListener onClickListener;
    private int rootLevel;
    private OutlineLineUIHelper outlineLineUIHelper;



    public AdapterOutline(Context context, List<Line> list, int rootLevel, AdapterView.OnItemClickListener onClickListener, boolean isShowComplete) {
        this.context = context;
        this.list = list;
        this.onClickListener = onClickListener;
        this.rootLevel = rootLevel;
        this.outlineLineUIHelper = new OutlineLineUIHelper();
        super.setIsShowCompleted(isShowComplete);
    }

    public List<Line> getList() {
        return list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
        private TextView title, info;
        private ImageView btnExpand, btnNext;
        private LinearLayout root, vLineContainer, titleLayout;
        private AppCompatCheckBox checkBox;
        private View shadowLayout;
        private float dY;
        long serverId;
        RelativeLayout.LayoutParams lpTop;
        RelativeLayout.LayoutParams lpBottom;
        private ImageView imgImportant;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            imgImportant = itemView.findViewById(R.id.img_important);
            shadowLayout = itemView.findViewById(R.id.layout_shadow);
            btnExpand = itemView.findViewById(R.id.btn_expande);
            btnNext = itemView.findViewById(R.id.btn_next);
            root = itemView.findViewById(R.id.outline_root_layout);
            info = itemView.findViewById(R.id.line_info);
            vLineContainer = itemView.findViewById(R.id.vline_container);
            titleLayout = itemView.findViewById(R.id.title_layout);

            btnExpand.setOnClickListener(this);
            btnNext.setOnClickListener(this);
            title.setOnClickListener(this);
            info.setOnClickListener(this);

            checkBox = itemView.findViewById(R.id.check_box_check_line);
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {
                        if (!isShowComplete()) { // Если скрывать выполненые - то при чеке перезагрузка всех задач
                            hideCompleted(isShowComplete(), false);
                        }
                    }
                }
            });

            lpTop = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lpTop.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            lpBottom = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            lpBottom.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        }

        @Override
        public void onClick(View v) {
            onClickListener.onItemClick(null, v, getAdapterPosition(), v.getId());
        }

        public void setPaddingLeft(int paddingLeft) {
            itemView.setPadding(paddingLeft, 0, 0, 0);
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Line line = ((Line) getItemAt(getAdapterPosition()));
            if (line != null) {
                line.setIsCompleted(isChecked ? "1" : "0");
                line.save();
                outlineLineUIHelper.checkTitle(line, title);

                boolean isNetwork = ManagerApplication.getInstance().isConnectToInternet(false);
                // Есть дети и задача выполнена
                if (line.getCountChild() > 0 && line.isComplete()) {
                    if (recursiveCheckCompleteAllChild(line, isNetwork)) {
                    } // Ставим всем детям что они выполнены
                }

                if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                    // Отправка статуса
                    ManagerOutline.responseSendCheckLine(line.getProjectId(), null);
                } else {
                    if (!isNetwork) {
                        if (line.getOfflineStatus() != Line.StatusOffline.CREATE || line.getOfflineStatus() != Line.StatusOffline.REMOVE) {
                            line.setOfflineStatus(Line.StatusOffline.UPDATE);
                            line.setModeEdit(ActivityEditLineOutline.Mode.UPDATE_LINE_OUTLINE);
                            line.save();
                        }
                    }
                }
                // Проверяет надо ли скрывать выполненые задачи
                checkCurrentHide();
                if (isShowComplete()) {
                    notifyDataSetChanged();
                }
            }
        }

        // Хак чтобы не вызывался onCheckedChangeListener и не делался лишний запрос
        void updateCheckBoxValue(boolean value) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(value);
            checkBox.setOnCheckedChangeListener(this);
        }

        // Вызывается из Helper при начале перемещение
        public void startDrag() {
            root.getBackground().setColorFilter(Color.parseColor("#A9A9A9"), PorterDuff.Mode.MULTIPLY);
        }

        // Вызывается из Helper при завершение перемещения
        public void endSelectedDrag() {
            root.getBackground().setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
        }

        // Вызывается при начале перемещения
        public void checkColapseGroupStartDrag() {
            Line l = (Line) getItemAt(this.getAdapterPosition());
            int countChild = l.getCountChild();
            if (countChild > 0) {
                if (!l.isGroup()) {
                    collapseGroup(this.getAdapterPosition(), false);
                    btnExpand.setImageResource(R.drawable.plus);
                    setShadowViewholderBackgroundAll(this, l);
                    startDrag();
                }
            }
        }

        public void setBackgroundRoundAll() {
            Line l = Line.findByServerId(Line.class, serverId);
            setShadowViewholderBackgroundAll(this, l);
        }

        public void setBackgroundCenter() {
            Line l = Line.findByServerId(Line.class, serverId);
            setShadowViewHolderBackgroundCenter(this, l);
        }

        public void setdY(float dY) {
            this.dY = dY;
        }

        public float getdY() {
            return dY;
        }

        public long getServerId() {
            return serverId;
        }

        public void setServerId(long serverId) {
            this.serverId = serverId;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_outline_task, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Line line = (Line) getItemAt(position);
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.title.setTag(line.getServerId());
        viewHolder.info.setTag(line.getServerId());
        viewHolder.itemView.invalidate();
        ((ViewHolder) holder).setServerId(line.getServerId());


        StringBuilder builder = new StringBuilder();
        builder.append(outlineLineUIHelper.getAssignName(line));

        if (!TextUtils.isEmpty(line.getDeadlineDateFormat())){
            builder.append(" ").append(line.getDeadlineDateFormat());
        }

        if (line.getDeadlineDateEnd() != null && !TextUtils.isEmpty(line.getDeadlineDateStartTimeStr())) {
            try{
                String[] arr = line.getDeadlineDateStartTimeStr().split(":");
                builder.append(" ").append(arr[0]).append(":").append(arr[1]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        viewHolder.info.setText(builder);

        // чекбокс
        viewHolder.updateCheckBoxValue(line.isComplete());
        outlineLineUIHelper.checkTitle(line, viewHolder.title);
        outlineLineUIHelper.checkDeadlineProsecution(viewHolder.checkBox, line);

        // Проверка на просрок по дедлайну
        drawableItemBackground(viewHolder, line, position);

        // Кнопка раскрытия\скрытия чилдов
        RelativeLayout.LayoutParams titleLayoutParams = (RelativeLayout.LayoutParams) viewHolder.titleLayout.getLayoutParams();
        drawableItemBtnCollapse(viewHolder, line, titleLayoutParams);
        viewHolder.titleLayout.setLayoutParams(titleLayoutParams);

        // Полоса уровня
        drawableLineLevels(viewHolder, line);

        if (line.isImportant() && !viewHolder.checkBox.isChecked()) {
            viewHolder.imgImportant.setVisibility(View.VISIBLE);
        } else {
            viewHolder.imgImportant.setVisibility(View.GONE);
        }
    }

    //==========================================
    // Drawaing items
    //==========================================
    // Фон
    private void drawableItemBackground(ViewHolder viewHolder, Line line, int position) {
        // Background для корневых Элементов
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) viewHolder.root.getLayoutParams();
        if (line.getLevel() == rootLevel) {
            params.setMargins(0, convertDpInPx(5), 0, 0);
            viewHolder.root.setLayoutParams(params);
            if (!line.isGroup() && line.getCountChild() > 0) {  // Если линия открыта и у нее есть дети то рисуем так
                int countChildInParent;
                if (isShowCompleted()) { // Если показываем скрытые выгружаем все задачи
                    countChildInParent = line.getCountChild();
                } else { // Инчае выгружаем из базы только завершенные
                    countChildInParent = Line.getCountChildByLineIdAndStatusCompleted(line.getServerId(), "0");
                }

                if (countChildInParent > 0) {
                    setShadowViewHolderBackgroundTop(viewHolder, line);
                } else {
                    setShadowViewholderBackgroundAll(viewHolder, line);
                }
            } else { // Не группа или нет детей то риуем так
                setShadowViewholderBackgroundAll(viewHolder, line);
            }
        } else {
            // Background для дочерних элементов
            params.setMargins(0, 0, 0, 0);

            int countChildInParent;
            if (isShowCompleted()) { // Если показываем скрытые выгружаем все задачи
                countChildInParent = Line.getCountChildByLineId(line.getParentItemId());
            } else { // Инчае выгружаем из базы только незавершенные
                countChildInParent = Line.getCountChildByLineIdAndStatusCompleted(line.getParentItemId(), "0");
            }

            int positionInChildList = getPositionChild(line);
            if (positionInChildList == countChildInParent) { // Ребенок является последним элементом в списке с детьми
                if (line.getCountChild() > 0 && !line.isGroup()) { // Если есть дети и группа открыта
                    setShadowViewHolderBackgroundCenter(viewHolder, line);
                } else { // Нет детей
                    if (line.getParentItemId() != 0) { // Если есть родитель то смотрим насколько он является вложеным
                        int tempPosition = position + 1;
                        if (tempPosition < mData.size()) {
                            Line l = (Line) mData.get(tempPosition); // Берем следующий элемент чтобы проверить какой bg отрисовывать
                            if (l.getLevel() > rootLevel) { // Типа следующий элемент по уровню находится ниже - значит у него будет стоять белый фон и не надо ставить скругления
                                setShadowViewHolderBackgroundCenter(viewHolder, line);
                            } else {
                                setShadowViewHolderBackGroundBottom(viewHolder, line);
                            }
                        } else {
                            setShadowViewHolderBackGroundBottom(viewHolder, line);
                        }
                    } else {
                        setShadowViewHolderBackGroundBottom(viewHolder, line);
                    }
                }
            } else { // Елемент находится посередине
                setShadowViewHolderBackgroundCenter(viewHolder, line);
            }

            viewHolder.root.setLayoutParams(params);
        }
    }

    private void setShadowViewHolderBackgroundTop(ViewHolder viewHolder, Line line) {
        if (line == null) {
            setBackGround(viewHolder.root, 0);
        } else {
            setBackGround(viewHolder.root, outlineLineUIHelper.getColorRoundTop(line.getColor())); // Header
        }
        viewHolder.shadowLayout.setBackgroundResource(R.drawable.shadow_top);
    }

    private void setShadowViewholderBackgroundAll(ViewHolder viewHolder, Line line) {
        if (line == null) {
            setBackGround(viewHolder.root, 0);
        } else {
            setBackGround(viewHolder.root, outlineLineUIHelper.getColorRoundAll(line.getColor()));
        }

        viewHolder.shadowLayout.setBackgroundResource(R.drawable.shadow_all);
    }

    private void setShadowViewHolderBackGroundBottom(ViewHolder viewHolder, Line line) {
        if (line != null) {
            setBackGround(viewHolder.root, outlineLineUIHelper.getColorRoundBottom(line.getColor()));
        }

        viewHolder.shadowLayout.setBackgroundResource(R.drawable.shadow_bottom);
    }

    private void setShadowViewHolderBackgroundCenter(ViewHolder viewHolder, Line line) {
        if (line != null) {
            setBackGround(viewHolder.root, outlineLineUIHelper.getColorRoundCenter(line.getColor()));
        }

        viewHolder.shadowLayout.setBackgroundResource(R.drawable.shadow_center);
    }

    private int getPositionChild(Line line) {
        int counter = 0;
        int position = 0;

        for (int i = 0; i < mData.size(); i++) {
            Line l = (Line) mData.get(i);
            if (l.getLevel() == line.getLevel() &&
                    l.getParentItemId() == line.getParentItemId()) { // Уровень линии и совпадают по родителю значит дети из одной группы
                counter++;
                if (l.getServerId() == line.getServerId()) { // Нашли переданный элемент в массиве
                    position = counter;
                }
            }
        }
        return position;
    }

    // Кнопка скрыть раскрыть детей
    private void drawableItemBtnCollapse(ViewHolder viewHolder, Line line, RelativeLayout.LayoutParams titleLayoutParams) {
        if (line.getCountChild() > 0) {
            viewHolder.btnExpand.setVisibility(View.VISIBLE);
            titleLayoutParams.addRule(RelativeLayout.LEFT_OF, viewHolder.btnExpand.getId());
            if (line.isGroup()) {
                viewHolder.btnExpand.setImageResource(R.drawable.plus);
            } else {
                viewHolder.btnExpand.setImageResource(R.drawable.minus);
            }
        } else {
            viewHolder.btnExpand.setVisibility(View.GONE);
            titleLayoutParams.addRule(RelativeLayout.LEFT_OF, viewHolder.btnNext.getId());
        }
    }

    // Уровень ребенка
    private void drawableLineLevels(ViewHolder viewHolder, Line line) {
        drawableLineLevels(viewHolder, line.getLevel());
    }

    public void drawableLineLevelsMoving(ViewHolder viewHolder, int level) {
        if (rootLevel == 1) {
            if (viewHolder.vLineContainer.getChildCount() + 1 != level) { // Чтобы не рисовать по 2 раза
                drawableLineLevels(viewHolder, level);
            }
        } else {
            if ((level - rootLevel) != viewHolder.vLineContainer.getChildCount()) {
                drawableLineLevels(viewHolder, level);
            }
        }
    }

    private void drawableLineLevels(ViewHolder viewHolder, int level) {
        viewHolder.vLineContainer.removeAllViews();
        viewHolder.checkBox.measure(0, 0);
        int myLeft = viewHolder.checkBox.getMeasuredWidth() / 2;
        LinearLayout.LayoutParams vp = new LinearLayout.LayoutParams(2, LinearLayout.LayoutParams.MATCH_PARENT);
        vp.setMargins(myLeft, 0, myLeft, 0);

        for (int i = rootLevel; i < level; i++) {
            View view = new View(context);
            view.setBackgroundResource(R.drawable.outline_dash_line_vertical);
            view.setLayoutParams(vp);
            viewHolder.vLineContainer.addView(view);
        }
    }

    private void setBackGround(View view, int resId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            view.setBackground(context.getResources().getDrawable(resId, context.getTheme()));
        } else {
            if (resId != 0) {
                view.setBackgroundDrawable(context.getResources().getDrawable(resId));
            }
        }
    }

    //==========================================
    // Logic methods
    //==========================================
    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
    }

    private int convertDpInPx(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }

    /**
     * Показ скрытие выполненых задач.
     * Делатся выборка из данных загруженных с сервера
     * и перезагружается адаптер с новыми данными.
     * вызывается метод colapseAll()
     *
     * @param showComplete - показать/скрыть
     */
    public void setShowComplete(boolean showComplete) {
        setIsShowCompleted(showComplete);
        if (showComplete) { // Показать все линии
            mData.clear();
            mGroups.clear();
            mData.addAll(list);
            collapseAll();
            notifyDataSetChanged();
        } else { // Скрыть завершенные
            hideCompleted(showComplete, true);
        }
    }

    private void checkCurrentHide() {
        if (!isShowComplete()) {
            hideCompleted(isShowComplete(), false);
        }
    }

    private void hideCompleted(boolean showComplete, boolean isColapseGroup) {
        setIsShowCompleted(showComplete);
        List<Long> complitedList = new ArrayList<>();
        Iterator<ExpIndData> iter = mData.iterator();
        while (iter.hasNext()) {
            Object obj = iter.next();
            if (obj instanceof Line) {
                Line l = (Line) obj;
                if (l.isComplete()) { // Если линия выполнена
                    complitedList.add(l.getServerId());
                    iter.remove();
                } else { // Линия не выполнена
                    if (l.getParentItemId() != 0) {
                        if (complitedList.contains(l.getParentItemId())) {
                            iter.remove();
                        }
                    }
                }
            }
        }

        if (isColapseGroup) {
            collapseAll();
        }

        notifyDataSetChanged();
    }

    // Обновление индеска сортировки у элементов списка
    public void updateItemsSortIndex(Line line) {
        updateItemsSortIndex(line.getParentItemId());
    }

    // Выставление индекса сортировки по порядку
    private void updateItemsSortIndex(long lineUpdateServerId) {
        ActiveAndroid.beginTransaction();
        try {
            int counter = 0;
            for (int i = 0; i < mData.size(); i++) {
                Line l = (Line) mData.get(i);
                if (l.getParentItemId() == lineUpdateServerId) {
                    l.setSort(counter);
                    l.save();
                    counter++;
                }
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    //==========================================
    // Move lines methods
    //==========================================

    /**
     * Перемещение линии или группы линий на новую позицию
     * В линии которую перемещаем уже стоит сервер id нового родителя
     *
     * @param swipeLine         - линия которую смещаем
     * @param lastParent        - родитель из которого удалили ребенка
     * @param positionSwipeLine - стартовая позиция линии которую смещаем
     */
    public void moveLineToNewPosition(Line swipeLine, Line lastParent, int positionSwipeLine) {
        // Группа элементов
        if (swipeLine.getCountChild() > 0) {
            // Количество отрисованных детей у родителя из которого удалили линию
            if (swipeLine.isGroup()) { // Перемещаем закрытую группу элементов. Один элемент.
                moveLineOne(swipeLine, lastParent, positionSwipeLine);
            } else { // перемещаем открытую группу элементов
                moveLineGroup(swipeLine, lastParent);
            }
            // Один элемент
        } else {
            moveLineOne(swipeLine, lastParent, positionSwipeLine);
        }
    }

    private void moveLineOne(Line swipeLine, Line lastParent, int positionSwipeLine) {
        // Перемещаемый элемент на нужном уровне или выше
        if (swipeLine.getLevel() >= rootLevel) {
            mData.remove(positionSwipeLine);

            // Родитель перемещаемого элемента не рутовый
            if (swipeLine.getParentItemId() > 0) {
                // перемещаемый элемент находится в фокусе
                if (swipeLine.getLevel() > rootLevel) {
                    Line newParent = Line.findByServerId(Line.class, swipeLine.getParentItemId()); // Ищем нового родителя
                    int positionNewParent = mData.indexOf(newParent); // Находим его местоположение в массиве
                    int countChildVisibilityNewParent = getCountVisibilityLine(newParent); // Считаем сколько детей отрисованно + сам родитель
                    int newPosition = positionNewParent + countChildVisibilityNewParent;// Получаем новую позицию для перемещения
                    mData.add(newPosition, swipeLine);
                } else {
                    android.util.Log.v(TAG, "[moveLineOne] empty");
                }
            } else {
                // Ищем вверх первый элемент у которого уровень рутовый
                // Получаем его позицию в массиве
                // Считаем сколько детей у него отрисованно
                // Новая позиция = позиция родителя + количество детей
                Line rootParent = getLineFirstRoot(lastParent);
                if (rootParent != null) {
                    int positionRootParent = mData.indexOf(rootParent);
                    int countChildVisibility = getCountVisibilityLine(rootParent);
                    int newPosition = positionRootParent + countChildVisibility;
                    mData.add(newPosition, swipeLine);
                }
            }
        } else {
            // Просто удаляем линию из массива
            // так как уровень куда перемещаем не в фокусе
            mData.remove(positionSwipeLine);
        }
    }

    /**
     * Перемещение раскрытой группы элементов на новое место
     *
     * @param header     - элемент заголовок группы
     * @param lastParent - родитель у которого удаляем группу
     */
    private void moveLineGroup(Line header, Line lastParent) {
        // Дети которых надо переместить
        ArrayList<Line> listRemovingItems = getListVisibilityLines(header);
        mData.removeAll(listRemovingItems); // Удаляем элементы из массива

        // Родитель перемещаемого элемента не рутовый
        if (header.getParentItemId() > 0) {
            // перемещаемый элемент находится в фокусе
            if (header.getLevel() >= rootLevel) {
                Line newParent = Line.findByServerId(Line.class, header.getParentItemId()); // Ищем нового родителя
                int positionNewParent = mData.indexOf(newParent); // Находим его местоположение в массиве
                int countChildVisibilityNewParent = getCountVisibilityLine(newParent); // Считаем сколько детей отрисованно + сам родитель
                int newPosition = positionNewParent + countChildVisibilityNewParent; // Получаем новую позицию для перемещения
                if (newPosition >= 0) {
                    try {
                        mData.addAll(newPosition, listRemovingItems);
                    } catch (IndexOutOfBoundsException e) {
                    }
                }
            } else {
                // Смещаем на уровень который ниже и не в фокусе
            }
        } else { // Перемещения в рутовый элемент расчитывается от старого родителя
            Line rootParent = getLineFirstRoot(lastParent);
            if (rootParent != null) {
                int positionRootParent = mData.indexOf(rootParent);
                int countChildVisibility = getCountVisibilityLine(rootParent);
                int newPosition = positionRootParent + countChildVisibility;
                mData.addAll(newPosition, listRemovingItems);
            }
        }
    }

    // Ищет от указанного элемента вверх до первого элемента у которого родитель root
    private Line getLineFirstRoot(Line startLine) {
        if (startLine == null) {
            return null;
        }

        // Надо ли начинать считать линии
        // Линии начинаются считатся после прохождения той линии которую передали в функцию
        boolean isStartCountLines = false;
        for (int i = mData.size() - 1; i > -1; i--) {
            if (i == -1) {
                break;
            }
            Line l = (Line) mData.get(i);
            if (l.getServerId() == startLine.getServerId()) {
                isStartCountLines = true;
            }

            if (isStartCountLines) {
                if (l.getParentItemId() == 0) {
                    return l;
                }
            }
        }
        return null;
    }

    private int getCountVisibilityLine(Line line) {
        if (line == null) {
            return 0;
        }

        boolean isStartCountLines = false; // Надо ли начинать считать линии
        int counter = 0;

        for (int i = 0; i < mData.size(); i++) {
            Line l = (Line) mData.get(i);
            if (l.getServerId() == line.getServerId()) {
                // Нашли линию от которой нужно отсчитать отрисованных детей
                // Линия нарисованна ее тоже подсчитываем
                isStartCountLines = true;
                counter++;
                continue;
            }

            if (isStartCountLines) {
                if (l.getLevel() <= line.getLevel() && // Уровень линии больше той что передали и он линия не та что ищем.
                        l.getServerId() != line.getServerId()) {
                    break;
                } else if (l.getParentItemId() == 0) { // У элемента нет родителя значит он лежит на самом верхнем уровне.
                    break;
                } else {
                    counter++;
                }
            }
        }

        return counter;
    }

    // Возвращает всех детей у переданной линии
    public ArrayList<Line> getListVisibilityLines(Line line) {
        if (line == null) {
            return new ArrayList<>();
        }

        ArrayList<Line> arrayList = new ArrayList<>();
        int positionLine = mData.indexOf(line);
        positionLine++;
        arrayList.add(line);
        for (int i = positionLine; i < mData.size(); i++) {
            try {
                Line l = (Line) mData.get(i);
                if (l.getLevel() <= line.getLevel()) {
                    break;
                } else {
                    arrayList.add(l);
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        return arrayList;
    }

    // Выставить у всех детей что задача выполнена
    private boolean recursiveCheckCompleteAllChild(Line line, boolean isConnectNetwrk) {
        List<Line> listChilds = Line.getChildrenParentLine(line.getServerId());
        for (Line l : listChilds) {
            if (!l.isComplete()) { // Если задача не была выполнена ставим крыжик делаем запрос
                l.setIsCompleted("1");
                checkLineInMData(l);
            }
            if (!isConnectNetwrk) {
                if (l.getOfflineStatus() != Line.StatusOffline.CREATE || l.getOfflineStatus() != Line.StatusOffline.REMOVE) {
                    l.setOfflineStatus(Line.StatusOffline.UPDATE);
                    l.setModeEdit(ActivityEditLineOutline.Mode.UPDATE_LINE_OUTLINE);
                }
            }
            l.save();
            // Если у задачи есть дети то рекурсивоно проходим их
            if (l.getCountChild() > 0) {
                recursiveCheckCompleteAllChild(l, isConnectNetwrk);
            }
        }

        return true;
    }

    private void checkLineInMData(Line line) {
        for (int i = 0; i < mData.size(); i++) {
            Line l = (Line) mData.get(i);
            if (l.getServerId() == line.getServerId()) {
                l.setIsCompleted("1");
                l.save();
                break;
            }
        }
    }

    /**
     * Обновление уровня у всех детей в массиве mData
     *
     * @param parent - линия у которой чекаем детей
     */
    public void updateMdataLevel(Line parent) {
        if (parent == null) {
            return;
        }
        int indexParent = mData.indexOf(parent);
        for (int i = indexParent; i < mData.size(); i++) {
            Line line;
            try {
                line = (Line) mData.get(i);
            } catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }

            if (line != null) {

                if (line.getServerId() == parent.getServerId()) { // Родитель

                } else if (line.getParentItemId() == parent.getServerId()) { // Ребенок
                    line.setLevel(parent.getLevel() + 1);
                    line.save();
                } else { // Другие элементы
                    if (line.getLevel() == parent.getLevel() // Уровень как у родитяеля значит следующий элемент за ним
                            || line.getLevel() == rootLevel) { // Уровень корневой значит закончилась группа
                        break;
                        // Конец группы
                    } else {
                        // Элемент у которого надо поменять уровень
                        Line parentTrue = Line.findByServerId(Line.class, line.getParentItemId());
                        line.setLevel(parentTrue.getLevel() + 1);
                        line.save();
                    }
                }
            }
        }
    }

    public void updateLineToList(Line l) {
        if (l == null) {
            return;
        }
        try{
            Line line = list.get(list.indexOf(l));
            line.update(l);
//        line.setLevel(l.getLevel());
//        line.setSort(l.getSort());
//        line.setColor(l.getColor());
//        line.setIsImportant(l.getIsImportant());
//        line.setIsCompleted(l.getIS);
            line.save();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public void updateItemToMData(Line l) {
        if (l == null) {
            return;
        }
        Line line = (Line) mData.get(mData.indexOf(l));
        line.setLevel(l.getLevel());
        line.setParentItemId(l.getParentItemId());
        line.setSort(l.getSort());
    }

    public void deleteLineNotNotify(Line line) {
        if (line != null) {
            list.remove(line);
            mData.remove(line);
            mGroups.remove(line);
        }
    }

    //==========================================
    // Setter getter
    //==========================================
    public void setList(List<Line> list) {
        this.list = list;
    }

    private boolean isShowComplete() {
        return isShowCompleted();
    }

    public int getRootLevel() {
        return rootLevel;
    }
}