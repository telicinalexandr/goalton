package com.mifors.goalton.adapters;

import android.support.v7.widget.RecyclerView;

import com.activeandroid.ActiveAndroid;
import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.model.outline.Line;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public abstract class MultiLevelExpIndListAdapter extends RecyclerView.Adapter {

    private static final String TAG = "Goalton [" + MultiLevelExpIndListAdapter.class.getSimpleName() + "]";

    private boolean isShowCompleted;

    /**
     * Indicates whether or not the observers must be notified whenever
     * {@link #mData} is modified.
     */
    private boolean mNotifyOnChange;

    /**
     * List of items to display.
     */
    public List<MultiLevelExpIndListAdapter.ExpIndData> mData;

    /**
     * Map an item to the relative group.
     * e.g.: if the user click on item 6 then mGroups(item(6)) = {all items/groups below item 6}
     * <p>
     * Содержит элементы которые не открыты вместе с их детьми
     */
    public HashMap<MultiLevelExpIndListAdapter.ExpIndData, List<? extends MultiLevelExpIndListAdapter.ExpIndData>> mGroups;

    /**
     * Interface that every item to be displayed has to implement. If an object implements
     * this interface it means that it can be expanded/collapsed and has a level of indentation.
     * Note: some methods are commented out because they're not used here, but they should be
     * implemented if you want your data to be expandable/collapsible and indentable.
     * See MyComment in the sample app to see an example of how to implement this.
     */
    public interface ExpIndData {
        /**
         * @return The children of this item.
         */
        List<? extends MultiLevelExpIndListAdapter.ExpIndData> getChildren();

        /**
         * @return The count children to item.
         */
        int getCountChildren();

        /**
         * @return True if this item is a group.
         */
        boolean isGroup();

        /**
         * @param value True if this item is a group
         */
        void setIsGroup(boolean value);

        boolean isComplete();
    }

    public MultiLevelExpIndListAdapter() {
        mData = new ArrayList<>();
        mGroups = new HashMap<>();
        mNotifyOnChange = true;
    }

    public void add(MultiLevelExpIndListAdapter.ExpIndData item) {
        if (item != null) {
            mData.add(item);
            if (mNotifyOnChange)
                notifyItemChanged(mData.size() - 1);
        }
    }

    public void addAll(int position, Collection<? extends MultiLevelExpIndListAdapter.ExpIndData> data) {
        if (data != null && data.size() > 0) {
            mData.addAll(position, data);
            if (mNotifyOnChange)
                notifyItemRangeInserted(position, data.size());
        }
    }

    public void addAll(Collection<? extends MultiLevelExpIndListAdapter.ExpIndData> data) {
        addAll(mData.size(), data);
    }

    public MultiLevelExpIndListAdapter.ExpIndData getItemAt(int position) {
        try {
            return mData.get(position);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            return mData.get((position - 1));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void collapseAll() {
        for (int i = getItemCount() - 1; i >= 0; i--) {
            collapseGroup(i, true);
        }
    }

    public void expandAll() {
        for (int i = 0; i < getItemCount(); i++) {
            expandGroup(i);
        }
    }

    /**
     * Expand the group at position "posititon".
     *
     * @param position The position (range [0,n-1]) of the group that has to be expanded
     */
    void expandGroup(int position) {
        MultiLevelExpIndListAdapter.ExpIndData firstItem = getItemAt(position);
        expandeGroup(firstItem, position);
    }


    private List<ExpIndData> restoreChildRecursive(ExpIndData firstItem, List<ExpIndData> childList) {
        List<ExpIndData> childs = (List<ExpIndData>) mGroups.get(firstItem);
        if (childs != null) {
            for (int i = 0; i < childs.size(); i++) {
                childList.add(childs.get(i));
                if (!childs.get(i).isGroup() && childs.get(i).getCountChildren() > 0) {
                    restoreChildRecursive(childs.get(i), childList);
                }
            }
            mGroups.remove(firstItem);
        }
        return childList;
    }

    private void expandeGroup(MultiLevelExpIndListAdapter.ExpIndData firstItem, int position) {
        if (!firstItem.isGroup()) {
            return;
        }
        firstItem.setIsGroup(false);
        List<ExpIndData> childs = restoreChildRecursive(firstItem, new ArrayList<ExpIndData>());
        mData.addAll(position + 1, childs);
        notifyDataSetChanged();
    }

    private void addChildrenToGroupRecursive(int start, int end) {
        try {
            MultiLevelExpIndListAdapter.ExpIndData firstItem = getItemAt(start);
            int level = ((Line) firstItem).getLevel();
            List<ExpIndData> childs = new ArrayList<>();
            for (int i = start + 1; start < end; i++) {
                if (i == mData.size() || ((Line) mData.get(i)).getLevel() <= level) { //на том же уровне или выше - выходим из цикла
                    break;
                }
                if (((Line) mData.get(i)).getLevel() - level == 1) {  // больше на 1 уровень - значит ребенок
                    childs.add(mData.get(i));
                    // Проверяем Есть ли у ребенка дети чтобы свернуть и его
                    if(((Line) mData.get(i)).getCountChild() > 0){
                        addChildrenToGroupRecursive(i, end);
                    }
                } else if (((Line) mData.get(i)).getLevel() - level > 1) {
                    addChildrenToGroupRecursive(i, end);
                }
            }

            if (childs.size() != 0) {
                mGroups.put(firstItem, childs);
            }
        } catch (Exception e) {
//            Crashlytics.logException(e);
        }
    }

    /**
     * Collapse the descendants of the item at position "position".
     *
     * @param position The position (range [0,n-1]) of the element that has to be collapsed
     */
    public void collapseGroup(int position, boolean isUpdateHeader) {
        MultiLevelExpIndListAdapter.ExpIndData firstItem = getItemAt(position);
        if (firstItem != null) {
            int level = ((Line) firstItem).getLevel();
            int childCount = 0;                         // кол-во детей и их детей
            List<ExpIndData> group = new ArrayList<>(); // список удаляемых детей
            if (position + 1 < mData.size()) {
                for (int i = position + 1; i < mData.size(); i++) { //идем вниз по списку, узнаем childCount
                    if (((Line) mData.get(i)).getLevel() > level) {
                        ++childCount;
                        group.add(mData.get(i));
                    } else {
                        break;
                    }
                }
                addChildrenToGroupRecursive(position, position + childCount);
                mData.removeAll(group);
            }
            firstItem.setIsGroup(true);
            if (isUpdateHeader) {       // нужно для того что бы не сбрасывался фокус при перетаскивании
                notifyItemChanged(position);
            }
            notifyItemRangeRemoved(position + 1, group.size());
        } else {
//            Crashlytics.logException(new Exception("collapseGroup firstItem null"));
        }
    }

    // Обновление уровня всех детей переданной линии
    public void updateChildsLevelParentLineToMGroups(Line firstItem) {
        List<ExpIndData> childs = (List<ExpIndData>) mGroups.get(firstItem);
        if (childs != null) {
            ActiveAndroid.beginTransaction();
            try {
                for (int i = 0; i < childs.size(); i++) {
                    Line child = (Line) childs.get(i);
                    child.setLevel(firstItem.getLevel() + 1);
                    child.save();
                    if (child.getCountChildren() > 0) {
                        updateChildsLevelParentLineToMGroups(child);
                    }
                }
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }

        }
    }

    public void toggleGroup(final int position) {
        /*
            Collpase/expand the item at position "position"
        */
        if (getItemAt(position).isGroup()) {
            expandGroup(position);
        } else {
            collapseGroup(position, true);
        }
    }

    public boolean isShowCompleted() {
        return isShowCompleted;
    }

    public void setIsShowCompleted(boolean showCompleted) {
        isShowCompleted = showCompleted;
    }
}