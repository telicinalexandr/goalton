package com.mifors.goalton.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.project.ProjectRightUser;

import java.util.List;

/**
 * Created by Altair on 04.04.2017.
 */

@SuppressWarnings("ALL")
public class AdapterProjectUserRights extends RecyclerView.Adapter<AdapterProjectUserRights.ViewHolder> {

    List<ProjectRightUser> list;
    AdapterView.OnItemClickListener onItemClickListener;

    public AdapterProjectUserRights(List<ProjectRightUser> list, AdapterView.OnItemClickListener onItemClickListener) {
        this.list = list;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project_user_right,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.name.setText(list.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.item_project_user_right_name);
            name.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(null,v,getAdapterPosition(),v.getId());
        }
    }
}
