package com.mifors.goalton.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.List;

/**
 * Created by gly8 on 23.03.17.
 */

@SuppressWarnings("ALL")
public class AdapterSpinner extends ArrayAdapter<InterfaceSpinnerItem> {
    private static final String TAG = "Goalton [" + AdapterSpinner.class.getSimpleName() + "]";

    private List<InterfaceSpinnerItem> data;
    private LayoutInflater inflater;
    private int resLayout;
    private int selectedPosition;
    private boolean isHideArrowRight; // Надо ли скрывать правую стрелочку
    private boolean isColorTranparent; // Надо ли скрывать правую стрелочку

    //==========================================
    // Constructors
    //==========================================
    public AdapterSpinner(@NonNull Context context, @LayoutRes int resource, @NonNull List<InterfaceSpinnerItem> data) {
        super(context, resource, data);
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
    }

    public AdapterSpinner(@NonNull Context context, @LayoutRes int resource, @NonNull List<InterfaceSpinnerItem> data, Spinner spinner) {
        super(context, resource, data);
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
        initSpinner(spinner);
    }

    public AdapterSpinner(@NonNull Context context, @LayoutRes int resource, @NonNull List<InterfaceSpinnerItem> data, Spinner spinner, boolean isHideArrowRight, boolean isColorTranparent) {
        super(context, resource, data);
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
        this.isHideArrowRight = isHideArrowRight;
        this.isColorTranparent = isColorTranparent;
        initSpinner(spinner);
    }

    private void initSpinner(Spinner spinner) {
        if (spinner != null) {
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                boolean isInitCalling = true;

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!isInitCalling) {
                        setSelectedPosition(position);
                        notifyDataSetChanged();
                    }
                    isInitCalling = false;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    //==========================================
    // View created
    //==========================================
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(parent, true, data.get(position), convertView, position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            return getCustomView(parent, false, data.get(position), convertView, position);
        } catch (IndexOutOfBoundsException e) {
            if (convertView == null || convertView != null) {
                return getCustomView(parent, false, data.get(0), convertView, position);
            }
        }
        return convertView;
    }

    private class ViewHolder {
        private TextView title;
        private ImageView imgCheck;
        private ImageView imgArrow;
        private View imgBotomLine;

        public ViewHolder(View holder) {
            this.title = holder.findViewById(R.id.title);
            this.imgCheck = holder.findViewById(R.id.img_check);
            this.imgArrow = holder.findViewById(R.id.img_arrow);
            this.imgBotomLine = holder.findViewById(R.id.bottom_plashka);
        }
    }

    public View getCustomView(ViewGroup parent, boolean isDropDown, InterfaceSpinnerItem option, View convertView, int position) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(resLayout, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        convertView.setPadding(0, convertView.getPaddingTop(), convertView.getPaddingRight(), convertView.getPaddingBottom());
        if (isColorTranparent) {
            convertView.setBackgroundColor(android.R.color.transparent);
        }
        if (isDropDown) {
            if (holder.imgBotomLine != null) {
                holder.imgBotomLine.setVisibility(View.GONE);
            }

            if (selectedPosition == position) {
                holder.imgCheck.setAlpha(1f);
            } else {
                holder.imgCheck.setAlpha(0f);
            }
        } else {
            if (holder.imgBotomLine != null) {
                holder.imgBotomLine.setVisibility(View.VISIBLE);
            }

            holder.imgCheck.setVisibility(View.GONE);
            if (holder.imgArrow != null) {
                holder.imgArrow.setVisibility(View.VISIBLE);
            }
        }

        if (isHideArrowRight && holder.imgArrow != null) {
            holder.imgArrow.setVisibility(View.GONE);
        }


        holder.title.setText(option.getName());

        return convertView;
    }

    //==========================================
    // Help methods
    //==========================================
    public InterfaceSpinnerItem getItemPosition(int position) {
        return data.get(position);
    }

    public int getItemCount() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    public void setSelectedPositionByValue(Object value) {
        for (int i = 0; i < data.size(); i++) {
            try {
                if (data.get(i).getValue().equals(value.toString())) {
                    setSelectedPosition(i);
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getValueByPosition(int position) throws ArrayIndexOutOfBoundsException {
        return data.get(position).getValue();
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }
}