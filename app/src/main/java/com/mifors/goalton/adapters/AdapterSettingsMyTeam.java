package com.mifors.goalton.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.interfaces.InterfaceOnGroupMenuClickListener;
import com.mifors.goalton.model.team.Team;
import com.mifors.goalton.model.team.TeamAccount;

import java.util.List;

public class AdapterSettingsMyTeam extends BaseExpandableListAdapter{
    private List<TeamAccount> accountList;
    private Context context;
    InterfaceOnGroupMenuClickListener onclick;

    public AdapterSettingsMyTeam(Context context, List<TeamAccount> accountList, InterfaceOnGroupMenuClickListener interfaceOnGroupMenuClick) {
        this.accountList = accountList;
        this.context = context;
        this.onclick = interfaceOnGroupMenuClick;
    }

    @Override
    public int getGroupCount() {
        return accountList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return accountList.get(groupPosition).getTeams().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return accountList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return accountList.get(groupPosition).getTeams().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, final ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_team_group,null);
        }

        if (groupPosition == 0){
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_view_gray_light_round_top);
        } else if (groupPosition == accountList.size()-1 && !isExpanded){
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_view_gray_light_round_bottom);
        } else {
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_view_gray_light);
        }
        if (accountList.size() == 1 && !isExpanded && accountList.get(0).getTeams().size() == 0){
            convertView.findViewById(R.id.item_team_group_root).setBackgroundResource(R.drawable.shape_round_view_gray_light);
        }

        ((TextView) convertView.findViewById(R.id.item_team_group_title)).setText(accountList.get(groupPosition).getName());
        ((TextView) convertView.findViewById(R.id.item_team_child_count)).setText(String.valueOf(accountList.get(groupPosition).getTeams().size()));
        final ImageView menu = convertView.findViewById(R.id.item_team_context_menu_btn);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onclick.onClickMenuItem(menu,groupPosition);
            }
        });
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_team_child,null);
        }
        Team team = accountList.get(groupPosition).getTeams().get(childPosition);
        ((TextView) convertView.findViewById(R.id.item_team_child_title)).setText(team.getName());

        if (isLastChild && groupPosition == (accountList.size() - 1)){
            convertView.findViewById(R.id.item_team_child_root).setBackgroundResource(R.drawable.shape_view_gray_light_round_bottom);
        } else {
            convertView.findViewById(R.id.item_team_child_root).setBackgroundResource(R.drawable.shape_view_gray_light);
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
