package com.mifors.goalton.interfaces;

import android.view.View;

/**
 * Created by Altair on 29.03.2017.
 */

@SuppressWarnings("ALL")
public interface InterfaceOnGroupMenuClickListener {
    void onClickMenuItem(View view, int position);
}
