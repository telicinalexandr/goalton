package com.mifors.goalton.interfaces;

import java.util.Date;

/**
 * Created by gly8 on 21.08.17.
 */

@SuppressWarnings("ALL")
public interface InterfaceViewPagerDays {
    void scrollToNextPage();
    void scrollToPrevPage();
    void cancelScroll();
    int getSelectedPageIndex();
    void scrollToDate(Date date);
}
