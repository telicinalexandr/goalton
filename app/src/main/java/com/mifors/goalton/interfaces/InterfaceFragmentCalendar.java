package com.mifors.goalton.interfaces;

import com.mifors.goalton.utils.calendar.PlannerFilter;

import java.util.Calendar;

/**
 * Created by gly8 on 21.08.17.
 */

@SuppressWarnings("ALL")
public interface InterfaceFragmentCalendar
{
    void updateSelectedDate(Calendar date);
    void openTask(long serverId);
    void openOutline(long serverId);
    void openCreateTask();
    void scrollNextDay();
    void scrollPrevDay();
    void responseGetPlannerSelectedDate(); // Перезагрузка задач на выбранную неделю
    void notifyUpdateDays();
    void finishDragItem();
    PlannerFilter getFilter();
}
