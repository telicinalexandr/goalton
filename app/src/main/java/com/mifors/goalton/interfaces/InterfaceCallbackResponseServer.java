package com.mifors.goalton.interfaces;

/**
 * Created by gly8 on 23.03.17.
 */

@SuppressWarnings("ALL")
public interface InterfaceCallbackResponseServer {
    String KEY_ERROR_EMAIL = "email"; // Неправильный email
    String KEY_ERROR_USERNAME = "username"; // Неправильное имя
    String KEY_ERROR_PASSWORD = "password"; // Неправильный или пустой пароль
    String KEY_END_RESPONSE = "endresponse"; // конец запроса
    String KEY_ERROR_RESPONSE = "error"; // Запрос не удался или в ответе была ошибка
    String KEY_FINISH_ACTIVITY = "finish_activity"; // Закрыть текущую активность (Востановление пароля)

    void completeResponse(String fieldError, String valueError);
}
