package com.mifors.goalton.interfaces;

/**
 * Created by gly8 on 14.04.17.
 */

@SuppressWarnings("ALL")
public interface InterfaceOnClickProjectPanelBtns {
    void onClickOpenOutline(long projectID);
}
