package com.mifors.goalton.interfaces;

import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.ui.calendar.ItemLine;

import java.util.Calendar;

/**
 * Created by gly8 on 09.10.17.
 */

public interface InterfaceFragmentDay
{
    ItemLine createViewItemLine(Line l, int positionnotDeadlineDate);
    Calendar getDateCurrentDay();
//    void setHeightItemLine(int heightItemLine);
//    int getHeightItemLine();
}
