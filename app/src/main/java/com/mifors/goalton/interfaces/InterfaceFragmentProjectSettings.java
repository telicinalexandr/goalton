package com.mifors.goalton.interfaces;

import com.mifors.goalton.utils.exceptions.ErrorCreateProjectSettings;

import okhttp3.MultipartBody;

/**
 * Created by gly8 on 11.04.17.
 */
@SuppressWarnings("ALL")
public interface InterfaceFragmentProjectSettings {
    MultipartBody.Builder getSettings(MultipartBody.Builder builder) throws ErrorCreateProjectSettings;
    void updateInterface();
}
