package com.mifors.goalton.interfaces;


import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

public interface InterfaceCallbackActivityMain {
    void replaceFragment(Fragment fragment, boolean addToBackStack, String TAG);
    void setActionBarTitle(String title, boolean hideCustom);
    void setActionBarTitle(int resId);
    void setActionBarTitle(int resId, boolean hideCustom);

    void showProgressBar();
    void showProgressBar(int resMessage);
    void hideProgressBar();
    void updateNavHeader();

    ActionBar getSupportActionBar();

    void hideSpinnerSort();
    void showSpinnerSort();

    /**
     * Открыть фрагмент с задачами выбранного проекта
     * @param projectId - Проект который надо открыть
     * @param startParentLineServerId - serverId задачи которая будет показанна сначала
     *                                по умолчанию 0
     * */
    void replaceFragmentOutline(long projectId, long startParentLineServerId);
    void replaceFragmentCalendar();
}
