package com.mifors.goalton.interfaces;

import com.mifors.goalton.model.outline.Line;

/**
 * Created by gly8 on 02.11.17.
 */

public interface InterfaceManagerCreateLine {
    void finishResponse(Line line);
    void showProgressBar(int resString);
    void updateStatusImportantCompletion();
}
