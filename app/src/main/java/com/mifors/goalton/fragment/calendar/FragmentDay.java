package com.mifors.goalton.fragment.calendar;


import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivityEditLineOutline;
import com.mifors.goalton.interfaces.InterfaceFragmentCalendar;
import com.mifors.goalton.interfaces.InterfaceFragmentDay;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.managers.core.ManagerPlanner;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.ui.InfiniteViewPager;
import com.mifors.goalton.ui.calendar.ItemLine;
import com.mifors.goalton.ui.calendar.RelativeLayoutCalendarDeadlineDates;
import com.mifors.goalton.ui.calendar.ScrollViewMaxHeight;
import com.mifors.goalton.utils.OutlineLineUIHelper;
import com.mifors.goalton.utils.calendar.DragShadowBuilderPositionItemCenter;
import com.mifors.goalton.utils.notifications.ReciverTimeNotifications;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class FragmentDay extends Fragment implements Observer, InterfaceFragmentDay {
    private static final String TAG = "Goalton [" + FragmentDay.class.getSimpleName() + "]";

    private Calendar dateCurrentDay; // Дата к которой относиться страница

    public ViewGroup rootContainer; // Контайнер страницы с задач с датами
    private ScrollView scrollViewDeadlineDates;
    private ScrollViewMaxHeight scrollViewMaxHeight;
    private LinearLayout containerNotDeadlineDate;
    private RelativeLayoutCalendarDeadlineDates relativeLayoutCalendarDeadlineDates;
    private View lineDividerVertical; // Ответсвенная штука не удалять, по ней растягивается контейнер с датами
    private LayoutInflater inflater;

    public static int NOTIFY_CURRENT_HOUR = 15263;
    public static int NOTIFY_HIDE_MINUTES_TITLE = 15285;
    private int position; // Позиция страница на скролле LEFT, RIGHT, MIDDLE
    private int realPosition;

    private List<Line> arrLinesNotExsistEndDeadlineDate;
    private OutlineLineUIHelper outlineLineUIHelper;
    //    private ItemLine tempLineEndDrag = null; // Хранит элемент который закончили перетаскивать.
    private Calendar tempCalendarEndDragtime; // Хранит последнее время при перемещение задачи

    private int heightTextViewMinutes;
    private int bottomScroll = 0;
    private long tempBeetwenResponseScroll = 0; // Время между запросами на скролл к предыдущей или следующей задачи
    private final int DELAY_MILLISECOND_BEETWEN_RESPONSE_SCROLL = 2500;
    private final String NAME_SCROLLY_ANIMATION = "scrollY";
    private final long DURATION_ANIMATION_SCROLL_BOTTOM = 5000;
    private final int PADDING_BOTTOM_SCROLL;
    private InterfaceFragmentCalendar interfaceFragmentCalendar;
    private DragShadowBuilderPositionItemCenter shadowBuilder;

    public FragmentDay() {
        PADDING_BOTTOM_SCROLL = (int) ManagerApplication.getInstance().getResources().getDimension(R.dimen.padding_bottom_scroll);
    }

    public FragmentDay(Context context) {
        PADDING_BOTTOM_SCROLL = (int) context.getResources().getDimension(R.dimen.padding_bottom_scroll);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDateByIndex();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getRootContainer() == null) {
            setRootContainer((ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.fragment_day, null));
        }

        initViews(); // Ищем въюхи в контейнерах
        init();
        return getRootContainer();
    }

    private void init() {
        initDateByIndex(); // Обновляем текущую дату
        updateData(); // Выгружаем данные из базы
        startAppeandLines(); // Добавляем линии на контейнер
    }

    @Override
    public void onResume() {
        super.onResume();
        relativeLayoutCalendarDeadlineDates.updateLineCurrentTime();
        relativeLayoutCalendarDeadlineDates.endShowMinutes();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    //==========================================
    // Init data
    //==========================================
    private void initDateByIndex() {
        int index = realPosition - FragmentCalendar.COUNT_ITEMS / 2;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, 60 * 24 * index);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        setDateCurrentDay(calendar);
    }

    public void updateData() {
        long startTime = dateCurrentDay.getTimeInMillis();
        long endTime = dateCurrentDay.getTimeInMillis() + (24 * 60 * 60 * 1000);
        if (interfaceFragmentCalendar != null) {
            arrLinesNotExsistEndDeadlineDate = Line.getPlannerLinesForDayNotDeadlineByFilter(startTime, endTime, interfaceFragmentCalendar.getFilter());
            relativeLayoutCalendarDeadlineDates.setArrayLines(Line.getPlannerLinesForDeadlineDateByFilter(startTime, endTime, interfaceFragmentCalendar.getFilter()));
        }
    }

    // Инициализация контейнеров для отображения, обновления задач
    private void initViews() {
        scrollViewDeadlineDates = rootContainer.findViewById(R.id.scroll_view_deadlines_date);
        scrollViewMaxHeight = rootContainer.findViewById(R.id.scroll_view_max_height);
        outlineLineUIHelper = new OutlineLineUIHelper();
        containerNotDeadlineDate = rootContainer.findViewById(R.id.container_lines_not_deadline_date);
        lineDividerVertical = rootContainer.findViewById(R.id.line_divider_vertical);
        relativeLayoutCalendarDeadlineDates = rootContainer.findViewById(R.id.container_task);
        containerNotDeadlineDate.setOnDragListener(onDragListenerContainerDeadlinesDate);
        relativeLayoutCalendarDeadlineDates.setInterfaceFragmentDay(this);
        relativeLayoutCalendarDeadlineDates.setOnDragListenerItems(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                return onDragListenerContainerDeadlinesDate.onDrag(v, event);
            }
        });

        // Скроллинг к текущему времени при старте
        getRootContainer().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    getRootContainer().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    getRootContainer().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }

                if (!ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.IS_FIRST_OPEN_CALENDAR_SESSION)) {
                    Calendar cal = Calendar.getInstance();
                    if ((getDateCurrentDay().get(Calendar.DAY_OF_YEAR) == cal.get(Calendar.DAY_OF_YEAR))
                            && (getDateCurrentDay().get(Calendar.YEAR) == cal.get(Calendar.YEAR))) {
                        ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.IS_FIRST_OPEN_CALENDAR_SESSION, true);
                        relativeLayoutCalendarDeadlineDates.getContainerCurrentHour().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @Override
                            public void onGlobalLayout() {
                                if (Build.VERSION.SDK_INT < 16) {
                                    relativeLayoutCalendarDeadlineDates.getContainerCurrentHour().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                } else {
                                    relativeLayoutCalendarDeadlineDates.getContainerCurrentHour().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                }

                                scrollViewDeadlineDates.smoothScrollBy(0, (int)relativeLayoutCalendarDeadlineDates.getContainerCurrentHour().getY()-200);
                            }
                        });
                    }
                }
            }
        });
    }

    // Начало добавление задач с датами и без дат
    private void startAppeandLines() {
        addLinesNotDeadLine();
        relativeLayoutCalendarDeadlineDates.updateDrawableItems();
    }

    // TODO: Вынести в контейнер одним методом название update
    // Добавление задач без дат
    private void addLinesNotDeadLine() {
        containerNotDeadlineDate.removeAllViews();
        if (getArrLinesNotExsistEndDeadlineDate().size() == 0) {
            scrollViewMaxHeight.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) scrollViewDeadlineDates.getLayoutParams();
            lp.topMargin = (int) ManagerApplication.convertDpToPixel(10);
            scrollViewDeadlineDates.setLayoutParams(lp);
        } else {
            scrollViewMaxHeight.setVisibility(View.VISIBLE);
            for (int i = 0; i < arrLinesNotExsistEndDeadlineDate.size(); i++) {
                containerNotDeadlineDate.addView(createViewItemLine(arrLinesNotExsistEndDeadlineDate.get(i), i ) );
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg == null) {
            init();
        } else if (arg instanceof Integer) {
            if (getDateCurrentDay() != null) {
                int week = getDateCurrentDay().get(Calendar.WEEK_OF_YEAR);
                int updateweek = (int)arg;
                if (week == (updateweek+1) || week == updateweek || week == (updateweek-1)) {
                    init();
                }
            }
        } else if ((int) arg == NOTIFY_CURRENT_HOUR) {
            relativeLayoutCalendarDeadlineDates.updateLineCurrentTime();
        } else if ((int) arg == NOTIFY_HIDE_MINUTES_TITLE) {
            relativeLayoutCalendarDeadlineDates.endShowMinutes();
        }
    }

    private CheckBox.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (buttonView.getTag() != null) {
                long id = Long.parseLong(buttonView.getTag().toString());
                Line l = Line.findByServerId(Line.class, id);
                if (l != null) {
                    l.setIsCompleted(isChecked ? "1" : "0");
                    l.save();
                    TextView title = ((ViewGroup) (buttonView.getParent()).getParent()).findViewById(R.id.title);
                    outlineLineUIHelper.checkTitle(l, title);
                }
                try {
                    if (l.isImportant() && !isChecked) {
                        ((ViewGroup) buttonView.getParent()).findViewById(R.id.img_important).setVisibility(View.VISIBLE);
                    } else {
                        ((ViewGroup) buttonView.getParent()).findViewById(R.id.img_important).setVisibility(View.GONE);
                    }
                    resposneUpdateCheckBox(l);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private View.OnClickListener onClickItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag() != null) {
                if (interfaceFragmentCalendar != null) {
                    interfaceFragmentCalendar.openTask(Long.parseLong(v.getTag().toString()));
                }
            }
        }
    };

    private View.OnClickListener onClickOpenOutline = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getInterfaceFragmentCalendar() != null && v.getTag() != null) {
                getInterfaceFragmentCalendar().openOutline(Long.parseLong(v.getTag().toString()));
            }
        }
    };

    //==========================================
    // Drag and drop
    //==========================================
    public View.OnDragListener onDragListenerContainerDeadlinesDate = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent dragEvent) {
            int action = dragEvent.getAction();
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    stopAnimationScroll();
                    onEndDrag(v, dragEvent);
                    relativeLayoutCalendarDeadlineDates.endShowMinutes();
                    interfaceFragmentCalendar.finishDragItem();
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    ((View) dragEvent.getLocalState()).setVisibility(View.VISIBLE);
                    relativeLayoutCalendarDeadlineDates.endShowMinutes();
                    stopAnimationScroll();
                    break;
                case DragEvent.ACTION_DRAG_LOCATION:
                    scrollBydragAndDrop(v, dragEvent); // Расчет того надо ли вклюючать анимацию сролла
                    break;
                default:
                    break;
            }
            return true;
        }
    };

    // Получаем точные координаты где отпустили элемент
    private Point getTouchPositionFromDragEvent(View item, DragEvent event) {
        Rect rItem = new Rect();
        item.getGlobalVisibleRect(rItem);
        return new Point(rItem.left + Math.round(event.getX()), rItem.top + Math.round(event.getY()));
    }

    /**
     * Закончили перемещение задачи - в методе - развилка на обработку действия перетаскивания задач с датой и без даты
     */
    private void onEndDrag(View v, DragEvent dragEvent) {
        // Делать проверку если переместили в тот же самый контейнер то возвращать на старое место
        ItemLine itemLine = (ItemLine) dragEvent.getLocalState();
        if (itemLine.getParent() != null) {
            ((ViewGroup) itemLine.getParent()).removeView(itemLine);
        }

        if (v instanceof RelativeLayout) { // Перемещение в контейнере с датами
            setNewDateLine(itemLine);
        } else if (v instanceof InfiniteViewPager) { // Вызов на ViewPager при сроллинге на другой день

            Point pointView = getTouchPositionFromDragEvent(v, dragEvent);
            int yOffsetScreenTop = pointView.y; // Точное местоположение нажатия на экране
            int[] pointScrollForScreen = new int[2];
            scrollViewDeadlineDates.getLocationOnScreen(pointScrollForScreen);
            // Отступ после которого сролиим кверху
            int positionScrollY = yOffsetScreenTop - pointScrollForScreen[1];
            if (positionScrollY > 0) {
                Calendar calendarStart = Calendar.getInstance();
                calendarStart.setTimeInMillis(getDateCurrentDay().getTimeInMillis());
                calendarStart.set(Calendar.HOUR_OF_DAY, tempCalendarEndDragtime.get(Calendar.HOUR_OF_DAY));
                calendarStart.set(Calendar.MINUTE, tempCalendarEndDragtime.get(Calendar.MINUTE));
                itemLine.getLine().setDeadlineDateStart(calendarStart);
                if (itemLine.getLine().getDuration() == 0) {
                    itemLine.getLine().setDuration(60);
                }
                itemLine.getLine().updateFinishDateForCurrentDuration();
            } else if (positionScrollY < 0) {
                itemLine.getLine().getDeadlineDateStart().setTimeInMillis(getDateCurrentDay().getTimeInMillis());
                itemLine.getLine().removeDeadlineDates();
            }

        } else if (v instanceof LinearLayout) { // Контенер с задачами на весь день
            itemLine.getLine().setDeadlineDateStart(getDateCurrentDay());
            itemLine.getLine().removeDeadlineDates();
        }
        itemLine.getLine().updateDatesTextByCalendar();
        itemLine.getLine().save();

        responseUpdateDateEndDrag(itemLine);
    }

    private void setNewDateLine(ItemLine itemLine) {
        if (itemLine.getLine().getDeadlineDateEnd() == null && tempCalendarEndDragtime != null) {
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTimeInMillis(getDateCurrentDay().getTimeInMillis());
            calendarStart.set(Calendar.HOUR_OF_DAY, tempCalendarEndDragtime.get(Calendar.HOUR_OF_DAY));
            calendarStart.set(Calendar.MINUTE, tempCalendarEndDragtime.get(Calendar.MINUTE));
            itemLine.getLine().setDeadlineDateStart(calendarStart);
            itemLine.getLine().setDuration(60);
        } else if(itemLine.getLine().getDeadlineDateStart() != null && tempCalendarEndDragtime != null){
            Calendar calendarStart = Calendar.getInstance();
            calendarStart.setTimeInMillis(getDateCurrentDay().getTimeInMillis());
            calendarStart.set(Calendar.HOUR_OF_DAY, tempCalendarEndDragtime.get(Calendar.HOUR_OF_DAY));
            calendarStart.set(Calendar.MINUTE, tempCalendarEndDragtime.get(Calendar.MINUTE));
            itemLine.getLine().setDeadlineDateStart(calendarStart);
        }
    }

    /**
     * Расчитывает точное время куда отпустили задачу
     *
     * @return точное время с округлением куда переместили задачу с округлением до 10 минут
     */
    public Calendar calcualteTimeMove(int y) {
        if (tempCalendarEndDragtime == null) {
            tempCalendarEndDragtime = Calendar.getInstance();
        }

        int minutes = 0;
        int hour = 0;
        if (y > 0) {
            hour = (y / ManagerPlanner.getHeightSteepMinute()) / 60;
            minutes = (y / ManagerPlanner.getHeightSteepMinute()) % 60;
        }

        if (hour == 24) {
            hour--;
        }

        tempCalendarEndDragtime.setTimeInMillis(getDateCurrentDay().getTimeInMillis());
        tempCalendarEndDragtime.set(Calendar.HOUR_OF_DAY, hour);
        tempCalendarEndDragtime.set(Calendar.MINUTE, minutes);
        tempCalendarEndDragtime.set(Calendar.MINUTE, ManagerCalendar.roundMinutes10(tempCalendarEndDragtime)); // Округляем до 10
        relativeLayoutCalendarDeadlineDates.updatePositionTitleMinutes(y, ManagerCalendar.convertCalendarToString(tempCalendarEndDragtime, "HH:mm"),
                heightTextViewMinutes, scrollViewDeadlineDates, PADDING_BOTTOM_SCROLL);
        return tempCalendarEndDragtime;
    }


    @Override
    public ItemLine createViewItemLine(Line line, int positionNotDeadlineDates) {
        ItemLine item;
        TextView title;
        // Есть стартовое время задачи
        if (line.getDeadlineDateEnd() != null) {
            item = (ItemLine) inflater.inflate(R.layout.item_outline_task_calendar_deadline_dates, null);
            item.setLine(line);
            title = item.findViewById(R.id.title);
            item.updateLayoutParamsFromDeadlineDates(ManagerPlanner.getHeightItemLine());
            if (item.getLine().getDuration() == 15) {
                item.getTitleAssign().setVisibility(View.GONE);
                title.setPadding(0,0,0,0);
            } else {
                item.getTitleAssign().setVisibility(View.VISIBLE);
                updateTextViewInfoTitle(item, true);
            }
        } else {
            item = (ItemLine) inflater.inflate(R.layout.item_outline_task_calendar, null);
            item.setBackgroundResource(R.drawable.shadow_all);
            item.setLine(line);
            if (positionNotDeadlineDates == 0) {
                View viewRootLayuoutItem = item.findViewById(R.id.outline_root_layout);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) viewRootLayuoutItem.getLayoutParams();
                lp.topMargin = 0;
                viewRootLayuoutItem.setLayoutParams(lp);
            }
            title = item.findViewById(R.id.title);
            updateTextViewInfoTitle(item, false);
        }

        item.setOnLongClickListener(onLongClickListener);
        title.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ViewGroup parent = (ViewGroup) v.getParent();
                if (parent != null) {
                    ViewGroup itemLine = (ViewGroup) parent.getParent().getParent();
                    if (itemLine != null) {
                        itemLine.performLongClick();
                    }
                }
                return false;
            }

        });

        title.setEllipsize(TextUtils.TruncateAt.END);
        title.setTag(line.getServerId());
        title.setOnClickListener(onClickItem);
        title.setHorizontallyScrolling(false);

        View btnOpenOutlineTask = item.findViewById(R.id.btn_open_outline);
        btnOpenOutlineTask.setTag(line.getServerId());
        btnOpenOutlineTask.setOnClickListener(onClickOpenOutline);

        AppCompatCheckBox checkBox = item.findViewById(R.id.check_box_check_line);
        checkBox.setSaveEnabled(false);
        checkBox.setOnCheckedChangeListener(onCheckedChangeListener);
        checkBox.setChecked(line.isComplete());
        checkBox.setTag(line.getServerId());

        outlineLineUIHelper.checkTitle(line, title);
        outlineLineUIHelper.checkDeadlineProsecution(checkBox, line);
        View rootContainer = item.findViewById(R.id.outline_root_layout);
        if (rootContainer != null) {
            rootContainer.setBackgroundResource(outlineLineUIHelper.getColorRoundAll(line.getColor()));
        }

        item.setOnClickListener(onClickItem);

        if (line.isImportant() && !checkBox.isChecked()) {
            View imgImportant = item.findViewById(R.id.img_important);
            imgImportant.setVisibility(View.VISIBLE);
            if (line.getDuration() == 15) {
                imgImportant.setPadding(0,0,0,0);
            } else {
                int padding = (int) ManagerApplication.convertDpToPixel(7);
                imgImportant.setPadding(padding,padding,padding,padding);
            }
        } else {
            item.findViewById(R.id.img_important).setVisibility(View.GONE);
        }

        return item;
    }

    /**
     * Обновление информации назначеный человек, дата, время, на TexView из объекта Line
     *
     * @param item          - View с задачей
     * @param isAppeandtime - надо ли добавлять время для задач без даты дедлайна
     */
    private void updateTextViewInfoTitle(ItemLine item, boolean isAppeandtime) {
        StringBuilder builder = new StringBuilder();
        builder.append(outlineLineUIHelper.getAssignName(item.getLine()));
        if (builder.length() > 0) {
            builder.append(" ");
        }

        if (isAppeandtime) {
            if (!TextUtils.isEmpty(item.getLine().getDeadlineDateStartTimeStr())) {
                String[] arr = item.getLine().getDeadlineDateStartTimeStr().split(":");
                try {
                    builder.append(arr[0]).append(":").append(arr[1]);
                } catch (Exception e) {
                    builder.append(item.getLine().getDeadlineDateStartTimeStr());
                }
            }
        }

        item.getTitleAssign().setText(builder);
    }

    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        private ItemLine itemLine = null;

        @Override
        public boolean onLongClick(View v) {
            itemLine = (ItemLine) v;
            itemLine.post(new Runnable() {
                @Override
                public void run() {
                    if (Build.VERSION.SDK_INT >= 26) {
                        ((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        ((Vibrator)getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(50);
                    }

                    ClipData data = ClipData.newPlainText("", "");
                    shadowBuilder = new DragShadowBuilderPositionItemCenter(itemLine);
                    Point point = new Point();
                    point.x = 0;
                    point.y = 0;
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
                        itemLine.startDragAndDrop(data, shadowBuilder, itemLine, 0);
                    } else {
                        itemLine.startDrag(data, shadowBuilder, itemLine, 0);
                    }
                    itemLine.setVisibility(View.INVISIBLE);
                    itemLine.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            return false;
                        }
                    });
                }
            });

            return false;
        }
    };


    // Анимация скрола когда подносят элемент к низу или верху
    private ObjectAnimator objectAnimatorDown, objectAnimatorUp;

    public void scrollBydragAndDrop(View v, DragEvent dragEvent) {
        // Кусок кода с магией расчета координат
        int yOffsetScreenTop = 0; // Точное местоположение нажатия на экране
        int xOffsetLeft = Integer.MAX_VALUE;
        int[] pointScrollForScreen = new int[2];
        scrollViewDeadlineDates.getLocationOnScreen(pointScrollForScreen);
        // Отступ после которого сролиим кверху
        int top = pointScrollForScreen[1] + ManagerPlanner.getHeightFloatingActionBar();
        Point pointView = getTouchPositionFromDragEvent(v, dragEvent);
        if (v instanceof RelativeLayout) { // Перемещение в контейнере с датами
            xOffsetLeft = Math.round(v.getX()) + Math.round(dragEvent.getX());
            int positionInScroll = pointView.y - pointScrollForScreen[1];
            int newY = positionInScroll - scrollViewDeadlineDates.getScrollY();
            yOffsetScreenTop = pointScrollForScreen[1] + newY;
            calcualteTimeMove(positionInScroll);
        } else if (v instanceof InfiniteViewPager) { // Вызов на ViewPager при сроллинге на другой день
            yOffsetScreenTop = pointView.y;
            xOffsetLeft = pointView.x;
            int positionScrollY = yOffsetScreenTop - pointScrollForScreen[1];
            if (positionScrollY > 0) {
                int yScroll = scrollViewDeadlineDates.getScrollY()+positionScrollY;
                calcualteTimeMove(yScroll);
            } else if (positionScrollY < 0) {
                relativeLayoutCalendarDeadlineDates.endShowMinutes();
            }
        } else if (v instanceof LinearLayout) { // Контенер с задачами на весь день
            xOffsetLeft = (int) dragEvent.getX();
            yOffsetScreenTop += scrollViewMaxHeight.getHeight();
            top -= scrollViewMaxHeight.getHeight();
        }

        // Расчет того на до ли вызывать автоматический скролл вверх/вниз
        if (yOffsetScreenTop < top) {
            if (objectAnimatorUp == null) {
                objectAnimatorUp = ObjectAnimator.ofInt(scrollViewDeadlineDates, NAME_SCROLLY_ANIMATION, 0).setDuration(DURATION_ANIMATION_SCROLL_BOTTOM);
                objectAnimatorUp.start();
            }
        } else if (yOffsetScreenTop > getBottomScroll()) {
            try {
                if (objectAnimatorDown == null) {
                    objectAnimatorDown = ObjectAnimator.ofInt(scrollViewDeadlineDates, NAME_SCROLLY_ANIMATION, lineDividerVertical.getHeight()).setDuration(DURATION_ANIMATION_SCROLL_BOTTOM);
                    objectAnimatorDown.start();
                }
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        } else {
            stopAnimationScroll();
        }

        // Расчет того надо ли вызывать автоматический скролл влево или право
        if (xOffsetLeft != Integer.MAX_VALUE) {
            if (xOffsetLeft <= ManagerPlanner.getHeightFloatingActionBar()) {
                long time = Calendar.getInstance().getTimeInMillis();
                if (time - tempBeetwenResponseScroll > DELAY_MILLISECOND_BEETWEN_RESPONSE_SCROLL) {
                    interfaceFragmentCalendar.scrollPrevDay();
                    tempBeetwenResponseScroll = time;
                }
            } else if (xOffsetLeft >= ManagerApplication.getInstance().getScreenWidth() - ManagerPlanner.getHeightFloatingActionBar()) {
                long time = Calendar.getInstance().getTimeInMillis();
                if (time - tempBeetwenResponseScroll > DELAY_MILLISECOND_BEETWEN_RESPONSE_SCROLL) {
                    interfaceFragmentCalendar.scrollNextDay();
                    tempBeetwenResponseScroll = time;
                }
            }
        }
    }

    private void stopAnimationScroll() {
        if (objectAnimatorDown != null) {
            objectAnimatorDown.cancel();
            objectAnimatorDown = null;
        }

        if (objectAnimatorUp != null) {
            objectAnimatorUp.cancel();
            objectAnimatorUp = null;
        }
    }

    //==========================================
    // Response
    //==========================================
    // Запрос на обновление даты после пееретаскивания.
    private void responseUpdateDateEndDrag(ItemLine itemLine) {
        itemLine.getLine().updateFinishDateForCurrentDuration();
        itemLine.getLine().updateDatesTextByCalendar();
        ReciverTimeNotifications.addNotification(itemLine.getLine());
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            itemLine.getLine().save();
            ManagerOutline.responseSetDeadlineDate(
                    itemLine.getLine().getServerId(),
                    itemLine.getLine().getDeadlineDateStartStr(),
                    itemLine.getLine().getDeadlineDateStartTimeStr(),
                    itemLine.getLine().getDuration(),
                    new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (msg.what == HttpParam.RESPONSE_OK) {
                                // Все параметры сохранены на сервер
                                if (interfaceFragmentCalendar != null) {
                                    interfaceFragmentCalendar.notifyUpdateDays();
                                }
                            }
                        }
                    });
        } else {
            itemLine.getLine().setOfflineStatus(Line.StatusOffline.UPDATE);
            itemLine.getLine().setModeEdit(ActivityEditLineOutline.Mode.UPDATE_LINE_CALENDAR);
            itemLine.getLine().save();
            if (interfaceFragmentCalendar != null) {
                interfaceFragmentCalendar.notifyUpdateDays();
            }
        }
    }

    private void resposneUpdateCheckBox(Line line) {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ManagerOutline.responseSetCompletedLine(line, line.getIsCompleted(), null);
        } else {
            line.setModeEdit(ActivityEditLineOutline.Mode.UPDATE_LINE_CALENDAR);
            line.setOfflineStatus(Line.StatusOffline.UPDATE);
            line.save();
        }
    }

    //==========================================
    // Getter setter
    //==========================================
    @Override
    public Calendar getDateCurrentDay() {
        return dateCurrentDay;
    }

    public void setDateCurrentDay(Calendar dateCurrentDay) {
        this.dateCurrentDay = dateCurrentDay;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setRootContainer(ViewGroup c) {
        this.rootContainer = c;
        inflater = (LayoutInflater) rootContainer.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        heightTextViewMinutes = (int) getResources().getDimension(R.dimen.height_title_time_calendar);
    }

    @Override
    public String toString() {
        return "[toString] dateCurrentDay = " + ManagerCalendar.convertCalendarToString(dateCurrentDay, true);
    }

    public void setRealPosition(int realPosition) {
        this.realPosition = realPosition;
    }

    public InterfaceFragmentCalendar getInterfaceFragmentCalendar() {
        return interfaceFragmentCalendar;
    }

    public void setInterfaceFragmentCalendar(InterfaceFragmentCalendar interfaceFragmentCalendar) {
        this.interfaceFragmentCalendar = interfaceFragmentCalendar;
    }

    public List<Line> getArrLinesNotExsistEndDeadlineDate() {
        if (arrLinesNotExsistEndDeadlineDate == null) {
            arrLinesNotExsistEndDeadlineDate = new ArrayList<>();
        }
        return arrLinesNotExsistEndDeadlineDate;
    }

    public ViewGroup getRootContainer() {
        return rootContainer;
    }

    public int getBottomScroll() {
        if (bottomScroll == 0) {
            bottomScroll = ManagerApplication.getInstance().getScreenHeight() - ManagerPlanner.getHeightFloatingActionBar();
        }
        return bottomScroll;
    }
}