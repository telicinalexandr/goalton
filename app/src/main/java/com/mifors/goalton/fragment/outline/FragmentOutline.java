package com.mifors.goalton.fragment.outline;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivityEditLineOutline;
import com.mifors.goalton.activity.ActivityMain;
import com.mifors.goalton.adapters.AdapterOutline;
import com.mifors.goalton.adapters.MultiLevelExpIndListAdapter;
import com.mifors.goalton.interfaces.InterfaceCallbackActivityMain;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.utils.ReciverCoonnectNetwork;
import com.mifors.goalton.utils.ServiceSyncTasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import static com.mifors.goalton.model.outline.Line.getChildrenParentLine;
import static com.mifors.goalton.model.outline.Line.getChildrenParentLineAndProjectID;

public class FragmentOutline extends Fragment implements AdapterView.OnItemClickListener, Observer {
    private static final String TAG = "Goalton [" + FragmentOutline.class.getSimpleName() + "]";
    public static final String TAG_FRAGMENT = "FragmentOutline";
    private long projectId;
    private Project project;
    private View rootView;
    private RecyclerView recyclerView;
    private AdapterOutline adapterOutlline;
    private RelativeLayout containerBreadCrums;
    private TextView titleBreadCrums;
    private List<Line> list = new ArrayList<>();
    private Line parent = null;
    private long tempParentId; // Сохраняет выбранного родителя
    private int showCompleted = 0;
    private SwipeRefreshLayout swipeToRefreshlayout;
    private Line lineStart;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ReciverCoonnectNetwork.NetworkObservable.getInstance().addObserver(this);
        if (getActivity() != null) {
            IntentFilter intent = new IntentFilter();
            intent.addAction(ServiceSyncTasks.ACTION_SYNC);
            getActivity().registerReceiver(broadcastReceiverSendComplete, intent);
        }


        if (project != null) {
            ((ActivityMain) getActivity()).setActionBarTitle(project.getName(), true);
        } else {
            if (getActivity() != null) {
                ManagerApplication.getInstance().showToast(R.string.error_open_project);
                ((InterfaceCallbackActivityMain) getActivity()).replaceFragmentCalendar();
            }
        }
    }

    private BroadcastReceiver broadcastReceiverSendComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(ServiceSyncTasks.EXTRA_KEY_IS_FINISH, false)) {
                ManagerOutline.getOutlineProject(null, projectId, new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        initRecyclerView(parent);
                        super.handleMessage(msg);
                    }
                });
            }
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        ReciverCoonnectNetwork.NetworkObservable.getInstance().deleteObserver(this);
        if (getActivity() != null) {
            try{
                getActivity().unregisterReceiver(broadcastReceiverSendComplete);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ActivityEditLineOutline.REQUEST_CODE_LINE_EDIT) {
            if (data != null && data.getExtras() != null) {
                long lineId = data.getExtras().getLong(ActivityEditLineOutline.EXTRA_KEY_LINE_ID);
                switch (resultCode) {
                    case ActivityEditLineOutline.RESULT_CODE_LINE_UPDATE:
                        updateLinetoData(lineId);
                        break;
                    case ActivityEditLineOutline.RESULT_CODE_LINE_DELETE_OUTLINE:
                        deleteLine(lineId);
                        break;
                    case ActivityEditLineOutline.RESULT_CODE_LINE_CREATE:
                        completeCreateLine(Line.findByServerId(Line.class, lineId));
                        break;
                    case ActivityEditLineOutline.RESULT_CODE_UPDATE_OUTLINE:
                        init();
                        break;
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_outline, container, false);
        if (rootView != null) {
            rootView.setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(getContext()));
        }
        project = Project.findByServerId(Project.class, projectId);
        ManagerProjects.saveSelectedProjects(projectId);
        init();
        return rootView;
    }

    public void init() {
        initView();
        recyclerView = rootView.findViewById(R.id.recyclerView_outline);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallbackLeft);
        itemTouchHelper.attachToRecyclerView(recyclerView);
        initRecyclerView(lineStart);
    }

    private void initView() {
        containerBreadCrums = rootView.findViewById(R.id.container_back_level);
        titleBreadCrums = rootView.findViewById(R.id.title_bread_crums);

        FloatingActionButton btnAddItem = rootView.findViewById(R.id.fab_add_new_outline_item);
        btnAddItem.setOnClickListener(onClickAdd);

        titleBreadCrums.setOnClickListener(onClickBreadCrumbs);
        containerBreadCrums.setOnClickListener(onClickBreadCrumbs);
        swipeToRefreshlayout = rootView.findViewById(R.id.outline_swipe_layout);
        swipeToRefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                    Line.deleteAllByProjectId(projectId);
                    responseUpdateProject();
                } else {
                    swipeToRefreshlayout.setRefreshing(false);
                }
            }
        });
    }

    private void responseUpdateProject() {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ManagerOutline.getOutlineProject(null, projectId, new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    initRecyclerView(parent);
                    swipeToRefreshlayout.setRefreshing(false);
                }
            });
        }
    }

    private void initRecyclerView(Line line) {
        parent = line;
        list.clear();
        if (line == null) { // Рутовый элемент
            tempParentId = 0;
        } else {
            if (adapterOutlline != null) {
                adapterOutlline.notifyDataSetChanged();
            } else {
                initAdapterByCurrentLines(line);
            }

            tempParentId = line.getServerId();
        }

        createListChilds();
    }

    private void initAdapterByCurrentLines(Line line) {
        int level = 0;
        if (line != null) {
            level = line.getLevel();
            showBreadCrumbs();
            if (!TextUtils.isEmpty(line.getTitle())) {
                titleBreadCrums.setText(Html.fromHtml(line.getTitle().replace("<br />", "")));
            } else {
                titleBreadCrums.setText(line.getTitle());
            }
            if (parent != null && TextUtils.isEmpty(parent.getTitle())) {
                hideBreadCrumbs();
            }
        } else {
            hideBreadCrumbs();
        }

        if (project != null) {
            adapterOutlline = new AdapterOutline(getContext(), list, ++level, this, project.isShowCompletedLines());
            adapterOutlline.addAll(list);
            recyclerView.setAdapter(adapterOutlline);
            // Нотификация вызывается в этом методе
            adapterOutlline.setShowComplete(project.isShowCompletedLines());
            parent = line;
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    ManagerProgressDialog.dismiss();
                }
            });
        }
    }

    private void hideBreadCrumbs() {
        containerBreadCrums.setVisibility(View.GONE);
    }

    private void showBreadCrumbs() {
        containerBreadCrums.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.context_menu_outline_list, menu);
        MenuItem item = menu.findItem(R.id.nav_menu_outline_list_hide_all);
        item.setChecked(project.isShowCompletedLines());
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_menu_outline_list_hide_all:
                if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                    responseShowChecked(item);
                } else {
                    setShowCompleteTask(item);
                    initRecyclerView(parent);
                }
                break;
            case R.id.nav_menu_outline_list_expande_all:
                if (adapterOutlline.mData != null && adapterOutlline.mData.size() > 0) {
                    adapterOutlline.expandAll();
                }
                break;
            case R.id.nav_menu_outline_list_colapse_all:
                if (adapterOutlline.mData != null && adapterOutlline.mData.size() > 0) {
                    adapterOutlline.collapseAll();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public void onBackPressed() {
        onClickBreadCrumbs.onClick(null);
    }

    public int getRootLevel() {
        return adapterOutlline.getRootLevel();
    }

    //==========================================
    // Create list childs
    //==========================================
    // Вызывается после построения списка
    private Handler callbackCompleteRecursive = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            initAdapterByCurrentLines(parent);
        }
    };

    // Запуск потока на создание списка
    private void createListChilds() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                CreateListChildren c = new CreateListChildren();
                if (parent == null) {
                    c.addChildrenRecursively(list, 0);
                } else {
                    c.addChildrenRecursively(list, parent.getServerId());
                }
            }
        }).start();
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            if (arg != null && true == (boolean) arg && (project.getCountLinesDb() == 0 || project.getCountLinesDb() != Integer.parseInt(project.getCountItems()))) {
                responseUpdateProject();
            } else {
                initRecyclerView(parent);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private class CreateListChildren {
        private void addChildrenRecursively(List<Line> list, long parentId) {
            Line parent = Line.findByServerId(Line.class, parentId);
            List<Line> children;
            if (parent != null) {
                children = getChildrenParentLineAndProjectID(parentId, projectId, parent.getLevel());
            } else {
                children = getChildrenParentLineAndProjectID(parentId, projectId, 1);
            }

            for (int i = 0; i < children.size(); i++) {
                Line l = children.get(i);
                if (project.isShowCompletedLines()) { // Показываем выполненые линии
                    list.add(l);
                    if (l.getCountChildren() > 0) { // Если есть дети вызываем еще раз метод
                        addChildrenRecursively(list, l.getServerId());
                    }
                } else {
                    if (!l.isComplete()) {
                        list.add(l);
                        if (l.getCountChild() > 0) {
                            addChildrenRecursively(list, l.getServerId());
                        }
                    }
                }
            }

            if (parentId == tempParentId) {
                if (callbackCompleteRecursive != null) {
                    callbackCompleteRecursive.sendEmptyMessage(0);
                }
            }
        }
    }

    public void deleteLine(long serverId) {
        if (adapterOutlline != null) {
            Line l = Line.findByServerId(Line.class, serverId);
            ActiveAndroid.beginTransaction();
            try {
                if (l != null) {
                    adapterOutlline.deleteLineNotNotify(l);
                    recursiveRemoveAllChildrensLine(serverId);
                }
                if (l != null) {
                    l.delete();
                }
                adapterOutlline.notifyDataSetChanged();
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
                responseUpdateProject();
            }
        } else {
            Line l = Line.findByServerId(Line.class, serverId);
            if (l != null) {
                recursiveRemoveAllChildrensLine(serverId);
                l.delete();
            }
        }
    }

    // Рекурсивное удаление детей при удаление родителя.
    private void recursiveRemoveAllChildrensLine(long serverId) {
        List<Line> list = Line.getChildrenParentLine(serverId);
        for (Line line : list) {
            if (line.getCountChild() > 0) {
                recursiveRemoveAllChildrensLine(line.getServerId());
            }
            if (adapterOutlline != null) {
                adapterOutlline.deleteLineNotNotify(line);
            }

            line.delete();
        }
    }

    public void updateLinetoData(long serverId) {
        Line line = Line.findByServerId(Line.class, serverId);
        if (line != null) {
            try {
                if (getProject() != null) {
                    adapterOutlline.updateLineToList(line);
                    adapterOutlline.setShowComplete(getProject().isShowCompletedLines());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //==========================================
    // Add new item
    //==========================================
    private View.OnClickListener onClickAdd = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openActivityEditLine(0);
        }
    };

    //==========================================
    // Response
    //==========================================

    /**
     * @param list - список с задачами у которых поменялся индекс сортировки
     */
    private void responseUpdateSort(List<Line> list) {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ManagerOutline.responseUpdateSort(list, null);
        } else {
            // Нет интернета выставляем офлайн статус на сортировку
            // Офлайн статус сбрасывается при отправке
            ActiveAndroid.beginTransaction();
            try {
                for (int i = 0; i < list.size(); i++) {
                    Line line = list.get(i);
                    line.setOfflineStatusSort(Line.StatusOffline.UPDATE_SORT);
                    line.save();
                }
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }
            Log.e(TAG, "[responseUpdateSort] error not network");
            ManagerProgressDialog.dismiss();
        }
    }

    private void completeCreateLine(Line newLine) {
        if (adapterOutlline != null) {
            int sort = 0;
            int parentLevel = 1;
            long parentId = 0;
            if (parent != null) {
                parentLevel = parent.getLevel() + 1;
                parentId = parent.getServerId();
            }

            for (MultiLevelExpIndListAdapter.ExpIndData e : adapterOutlline.mData) {
                if (((Line) e).getLevel() == parentLevel && ((Line) e).getSort() > sort) {
                    sort = (int) ((Line) e).getSort();
                }
            }

            newLine.setParentItemId(parentId);
            newLine.setLevel(parentLevel);
            newLine.setSort(sort + 1);
            newLine.save();

            if (project.isShowCompletedLines()) {
                adapterOutlline.mData.add(adapterOutlline.mData.size(), newLine);
            } else {
                if (!newLine.isComplete()) {
                    adapterOutlline.mData.add(adapterOutlline.mData.size(), newLine);
                }
            }

            adapterOutlline.getList().add(adapterOutlline.getList().size(), newLine);
            adapterOutlline.notifyDataSetChanged();
            if (parent != null) {
                responseUpdateSort(Line.getChildrenParentLine(parent.getServerId()));
            } else {
                responseUpdateSort(Line.getChildrenParentLine(0));
            }
        } else {
            responseUpdateSort(Line.getChildrenParentLine(0));
        }
    }

    private void responseShowChecked(final MenuItem item) {
        ManagerProgressDialog.showProgressBar(getActivity(), R.string.update_interface, false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setShowCompleteTask(item);
                responseChangeShowTask();
            }
        }, 10);
    }

    // Установка показывать/скрыть выполненые задачи
    private void setShowCompleteTask(MenuItem item) {
        item.setChecked(!item.isChecked());
        if (item.isChecked()) {
            showCompleted = 1;
            adapterOutlline.setShowComplete(true);
            project.setShowCompleted(1);
        } else {
            showCompleted = 0;
            adapterOutlline.setShowComplete(false);
            project.setShowCompleted(0);
        }

        project.save();
    }

    // Показать скрыть выполненые
    private void responseChangeShowTask() {
        ManagerOutline.responseToogleTaskComplete(project.getServerId(), showCompleted, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                initRecyclerView(parent);
            }
        });
    }

    //=========================================
    // ONCLICK METHODS
    //=========================================
    private View.OnClickListener onClickBreadCrumbs = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (parent != null) {
                Line line = Line.findByServerId(Line.class, parent.getParentItemId());
                initRecyclerView(line);
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.btn_expande:
                adapterOutlline.toggleGroup(position);
                break;
            case R.id.btn_next:
                initRecyclerView((Line) adapterOutlline.getItemAt(position));
                break;
            case R.id.title:
                openActivityEditLine(Long.parseLong(view.getTag().toString()));
                break;
            case R.id.line_info:
                openActivityEditLine(Long.parseLong(view.getTag().toString()));
                break;
        }
    }

    public void showProgress() {
        ManagerProgressDialog.showProgressBar(getActivity(), R.string.saves_changes, false);
    }

    private void openActivityEditLine(long serverId) {
        Line l = Line.findByServerId(Line.class, serverId);
        Intent intent = new Intent(getActivity(), ActivityEditLineOutline.class);
        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_LINE_ID, serverId);
        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_PROJECT_ID, projectId);
        if (l == null) { // Открываем для создания задачи
            long parentId = 0;
            if (parent != null) {
                parentId = parent.getServerId();
            }
            intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_PARENT_ID, parentId);
            intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_MODE, ActivityEditLineOutline.Mode.CREATE_LINE_OUTLINE);
        } else { // Открываем существующию линию
            intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_PARENT_ID, l.getParentItemId());
            intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_MODE, ActivityEditLineOutline.Mode.UPDATE_LINE_OUTLINE);
        }

        startActivityForResult(intent, ActivityEditLineOutline.REQUEST_CODE_LINE_EDIT);
    }

    //==========================================
    // Adapter animation
    //==========================================
    private ItemTouchHelper.SimpleCallback simpleItemTouchCallbackLeft = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        int dragFrom = -1;
        int dragTo = -1;
        private AdapterOutline.ViewHolder tempDragViewHolder;
        private AdapterOutline.ViewHolder tempTargetHolder;

        // Флаг для сохранения позиции по Y у перемещаемого элемента способом DragAndDrop
        private boolean isEndDrag;
        // Начало перетаскивания элемента способом DragAndDrop
        private boolean isStartDrag;
        // Был ли перемещен элемент так что заменил другой
        private boolean isMoveItem = false;
        //==========================================
        // Drag and drop methods
        //==========================================
        float lastdY;

        // Вызывается при изменине позиции элемента по Y
        @Override
        public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            if (lastdY != dY) {
                boolean isMoveFromTop = toPosTemp > fromPosTemp;
                if (isMoveFromTop) {  // Перемещение сверху
                    if (dY < 0) { // Выше середины итема
                        // Работаем с таргетом который взяли в методе onMoved
                        if (tempTargetHolder != null) {
                            Line line = Line.findByServerId(Line.class, tempTargetHolder.getServerId());
                            if (line.getCountChild() > 0 && !line.isGroup()) { // Есть дети, группа открыта
                                adapterOutlline.drawableLineLevelsMoving((AdapterOutline.ViewHolder) viewHolder, line.getLevel() + 1);
                            } else { // Нет детей
                                setBGViewHolderCenter(viewHolder, line);
                            }
                        }
                    } else if (dY > 0) { // Ниже середины итема
                        // Здесь нужно найти линию которая находится ниже перетаскиваемого итема
                        Line line = getBottomLineMoveItem(viewHolder);
                        if (line != null) {
                            if (line.getLevel() == adapterOutlline.getRootLevel()) { // Линия на верхнем уровне
                                setBGViewHolderRoundAll(viewHolder);
                            } else {
                                setBGViewHolderCenter(viewHolder, line);
                            }
                        } else {
                            setBGViewHolderRoundAll(viewHolder);
                        }
                    }
                } else { // Перемещение снизу
                    if (dY < 0) { // Выше середины итема
                        Line line = getTopLine(viewHolder, recyclerView);
                        if (line != null) {
                            if (line.getCountChild() > 0 && !line.isGroup()) { // Есть дети и группа открыта
                                ((AdapterOutline.ViewHolder) viewHolder).setBackgroundCenter();
                                adapterOutlline.drawableLineLevelsMoving((AdapterOutline.ViewHolder) viewHolder, line.getLevel() + 1);
                            } else {
                                if (line.getLevel() == adapterOutlline.getRootLevel()) { // Линия находится на уровне задачи в фокусе
                                    setBGViewHolderRoundAll(viewHolder);
                                } else {
                                    setBGViewHolderCenter(viewHolder, line);
                                }
                            }
                        } else { // Переместили на самый верх
                            setBGViewHolderRoundAll(viewHolder);
                        }
                    } else if (dY > 0) { // Ниже середины итема
                        // Работаем с таргетом который взяли в методе onMoved
                        Line line = getBottomLineMoveItem(viewHolder);
                        if (line != null) {
                            if (line.getLevel() == adapterOutlline.getRootLevel()) { // Линия на верхнем уровне
                                setBGViewHolderRoundAll(viewHolder);
                            } else {
                                setBGViewHolderCenter(viewHolder, line);
                            }
                        } else { // Нету линии снизу - Переместили в конец списка
                            setBGViewHolderRoundAll(viewHolder);
                        }
                    }
                }

                if (dX == 0 && dY == 0) { // Отпустили элемент
                    if (!isStartDrag) { // Закончили перетаскивание
                        ((AdapterOutline.ViewHolder) viewHolder).endSelectedDrag(); // Меняем фон на обычный
                        tempTargetHolder = null; // Сбрасываем переменные сохраненые из метода onMove
                        toPosTemp = -1;
                        fromPosTemp = -1;
                        if (!isMoveItem) { // Если элемент не перемещали между итемами
                            // Никуда не смещали только в пределах одного элемента вызываем метод перерисовки така как сам он не вызывается
                            reallyMoved(viewHolder.getAdapterPosition(), viewHolder.getAdapterPosition());
                        }
                        setMoveItem(false);
                    }
                }

                if (isEndDrag) {
                    isEndDrag = false;
                    ((AdapterOutline.ViewHolder) viewHolder).setdY(dY); // Сохраняем позицию для расчета к какому элементу добавить нижний/верхний
                }
            }

            lastdY = dY;
        }

        // Устанавливает фон ViewHolder без скругленей
        private void setBGViewHolderCenter(RecyclerView.ViewHolder viewHolder, Line line) {
            ((AdapterOutline.ViewHolder) viewHolder).setBackgroundCenter();
            adapterOutlline.drawableLineLevelsMoving(((AdapterOutline.ViewHolder) viewHolder), line.getLevel());
        }

        // Устанавливает фон ViewHolder с скруглениями
        private void setBGViewHolderRoundAll(RecyclerView.ViewHolder viewHolder) {
            ((AdapterOutline.ViewHolder) viewHolder).setBackgroundRoundAll();
            adapterOutlline.drawableLineLevelsMoving((AdapterOutline.ViewHolder) viewHolder, 0);
        }

        // Возвращает линию которая находится под перемещаемой
        private Line getBottomLineMoveItem(RecyclerView.ViewHolder viewHolder) {
            int positionAdapter = viewHolder.getAdapterPosition();
            positionAdapter++;
            if (positionAdapter < adapterOutlline.mData.size()) {
                AdapterOutline.ViewHolder h = (AdapterOutline.ViewHolder) recyclerView.findViewHolderForAdapterPosition(positionAdapter);
                if (h != null) {
                    return Line.findByServerId(Line.class, h.getServerId());
                }
            }
            return null;
        }

        // Возвращает линию которая находится выше перемещаемой
        private Line getTopLine(RecyclerView.ViewHolder viewHolder, RecyclerView recyclerView) {
            int positionAdapter = viewHolder.getAdapterPosition();
            positionAdapter--;
            if (positionAdapter > -1) {
                AdapterOutline.ViewHolder h = (AdapterOutline.ViewHolder) recyclerView.findViewHolderForAdapterPosition(positionAdapter);
                if (h != null) {
                    return Line.findByServerId(Line.class, h.getServerId());
                }
            }
            return null;
        }

        @Override
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            super.clearView(recyclerView, viewHolder);
            if (dragFrom != -1 && dragTo != -1) {
                tempDragViewHolder = (AdapterOutline.ViewHolder) viewHolder;
                reallyMoved(dragFrom, dragTo);
            }

            dragFrom = dragTo = -1;
        }

        private int fromPosTemp = -1, toPosTemp = -1;

        @Override
        public void onMoved(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int fromPos, RecyclerView.ViewHolder target, int toPos, int x, int y) {
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y);
            setMoveItem(true);
            fromPosTemp = fromPos;
            toPosTemp = toPos;
            tempTargetHolder = (AdapterOutline.ViewHolder) target;
        }

        @Override
        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return 0;
        }

        // Перетаскивание начало
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            int fromPosition = viewHolder.getAdapterPosition();
            int toPosition = target.getAdapterPosition();
            if (dragFrom == -1) {
                dragFrom = fromPosition;
            }

            dragTo = toPosition;
            adapterOutlline.onItemMove(fromPosition, toPosition);
            return true;
        }

        // Перетаскивание закончено
        private void reallyMoved(int fromPosition, int toPosition) {
            showProgress();
            ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.POSITION_SWIPE_FROM, fromPosition);
            ManagerApplication.getInstance().getSharedManager().putKeyInteger(SharedKeys.POSITION_SWIPE_TO, toPosition);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    int toPosition = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.POSITION_SWIPE_TO);
                    int fromPosition = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.POSITION_SWIPE_FROM);
                    Line lineFrom = (Line) adapterOutlline.getItemAt(fromPosition);
                    dragAndDropLine(lineFrom, toPosition, fromPosition);
                    adapterOutlline.notifyDataSetChanged();
                    ManagerProgressDialog.dismiss();
                }
            }, 10);
        }

        // Логика перетаскивания
        private void dragAndDropLine(Line lineFrom, int toPosition, int fromPosition) {
            Line lineBottom = null;
            Line lineTop = null;
            adapterOutlline.mData.add(toPosition, adapterOutlline.mData.remove(fromPosition));
            int tempBottomToPosition = toPosition + 1;
            int tempTopPosition = toPosition - 1;
            if (tempBottomToPosition < adapterOutlline.mData.size()) {
                lineBottom = (Line) adapterOutlline.mData.get(tempBottomToPosition);
            }

            if (tempTopPosition >= 0) {
                lineTop = (Line) adapterOutlline.mData.get(tempTopPosition);
            }

            if (tempDragViewHolder != null) {
                if (tempDragViewHolder.getdY() > 0) { // Берем параметры у нижнего элемента
                    if (lineBottom != null) { // Есть линия снизу
                        lineFrom.setParentItemId(lineBottom.getParentItemId());
                        lineFrom.setSort(lineBottom.getSort() - 0.1);
                        lineFrom.setLevel(lineBottom.getLevel());
                    } else {  // Линии снизу нету переместили в конец списка
                        lineFrom.setLevel(adapterOutlline.getRootLevel()); // Уровень задачи той что в фокусе
                        lineFrom.setSort(adapterOutlline.mData.size()); // По сортировке самый последний индекс
                        if (lineFrom.getLevel() == 1) { // Рутовый уровень
                            lineFrom.setParentItemId(0);
                        } else { // Уровень задачи в фокусе
                            lineFrom.setParentItemId(parent.getServerId());
                        }
                    }
                } else if (tempDragViewHolder.getdY() < 0) { // Берем параметры у верхнего элемента
                    if (lineTop != null) {
                        if (lineTop.getCountChild() > 0 && !lineTop.isGroup()) { // Перемещаем под header группы
                            lineFrom.setParentItemId(lineTop.getServerId());
                            lineFrom.setSort(0.1);
                            lineFrom.setLevel(lineTop.getLevel() + 1);
                        } else {
                            lineFrom.setParentItemId(lineTop.getParentItemId());
                            lineFrom.setSort(lineTop.getSort() + 0.1);
                            lineFrom.setLevel(lineTop.getLevel());
                        }
                    } else {
                        // Переместили на самый верх
                        lineFrom.setLevel(adapterOutlline.getRootLevel());
                        lineFrom.setSort(-0.1);
                        if (parent != null) {
                            lineFrom.setParentItemId(parent.getServerId());
                        } else {
                            lineFrom.setParentItemId(0);
                        }
                    }
                }
            }

            lineFrom.save();

            list.remove(lineFrom);

            List<Line> childs = new ArrayList<>();
            lineFrom.recursiveUpdateChildsLevel(lineFrom, childs); // Обновления уровня при перемещение группы элементов
            list.removeAll(childs);
            childs.add(0, lineFrom);
            list.addAll(toPosition, childs);
            List<Line> listOfChildsNewParent = getChildrenParentLineAndProjectID(lineFrom.getParentItemId(), projectId, lineFrom.getLevel());
            updateItemsSortIndex(listOfChildsNewParent);
//            if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            responseUpdateSort(listOfChildsNewParent);
//            } else {
//
//            }
            adapterOutlline.updateItemsSortIndex(lineFrom);// Обновление индекса после смены сортировки
            adapterOutlline.updateItemToMData(lineFrom);
            adapterOutlline.updateLineToList(lineTop);
            adapterOutlline.updateLineToList(lineBottom);
            if (lineFrom.getCountChild() > 0) {
                adapterOutlline.updateChildsLevelParentLineToMGroups(lineFrom);
            }
        }

        // Обновление индеска сортировки у элементов списка нового родителя
        private void updateItemsSortIndex(List<Line> list) {
            ActiveAndroid.beginTransaction();
            try {
                for (int i = 0; i < list.size(); i++) {
                    Line l = list.get(i);
                    l.setSort(i);
                    l.save();
                }
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
            }
        }

        //==========================================
        // Swipe left right
        //==========================================
        // Перемещение уровней задач свайпом
        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            int positionSwipeLine = viewHolder.getAdapterPosition();
            // итем который свайпается
            Line swipeLine = (Line) adapterOutlline.mData.get(positionSwipeLine);
            // перетаскивание влево (на уровень вверх)
            if (swipeDir == 4) {
                Line lastParent = Line.getParent(swipeLine);
                swipeLineLevelUp(callbackStartResponseUpdate, swipeLine, positionSwipeLine, lastParent);
                // перетаскивание вправо (на уровень вниз)
            } else if (swipeDir == 8) {
                Line newParent = null;
                // Ищем первый элемент выше на этом же уровне - он будет новым родителем, если попадается уровнем меньше,
                // Значит текущий элемент дочерний и выше только родитель
                for (int i = positionSwipeLine - 1; i >= 0; i--) {
                    if (((Line) adapterOutlline.mData.get(i)).getLevel() < swipeLine.getLevel()) {
                        break;
                    }
                    if (((Line) adapterOutlline.mData.get(i)).getLevel() == swipeLine.getLevel()) {
                        newParent = (Line) adapterOutlline.mData.get(i);
                        break;
                    }
                }
                swipeLineLevelDown(callbackStartResponseUpdate, swipeLine, positionSwipeLine, newParent);
            }
        }

        // Вызывается после обновления интерфейса повышение/понижение уровня
        private Handler callbackStartResponseUpdate = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                //чудо метод отправки всего на сервер
                adapterOutlline.notifyDataSetChanged();
            }
        };

        private Line swipeLine, newParent;
        private int positionSwipeLine;
        private Handler callbacResponseUpdate;

        // Смахивание вправо
        private void swipeLineLevelDown(Handler callbackStartResponseUpdate, final Line swipeLine, int positionSwipeLine, final Line newParent) {
            showProgress();
            this.swipeLine = swipeLine;
            this.newParent = newParent;
            this.positionSwipeLine = positionSwipeLine;
            this.callbacResponseUpdate = callbackStartResponseUpdate;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    startLineDownCalculate();
                    if (newParent != null) {
                        responseUpdateSort(Line.getChildrenParentLine(newParent.getServerId()));
                    }

                    if (callbacResponseUpdate != null) {
                        callbacResponseUpdate.sendEmptyMessage(0);
                    }
                }
            }).start();
        }

        // Начало смещения линии на уровень вверх
        @SuppressWarnings("unchecked")
        private void startLineDownCalculate() {
            // Ставим нового родителя, инкрементируем уровень и ставим в конец
            if (newParent != null) {
                // Список детей у перемещаемой линии
                List<Line> listChildrens = adapterOutlline.getListVisibilityLines(swipeLine);

                if (newParent.isGroup()) { // Родитель закрыт
                    adapterOutlline.mData.removeAll(listChildrens);
                    if (adapterOutlline.mGroups.containsKey(newParent)) {
                        List<MultiLevelExpIndListAdapter.ExpIndData> listChildParentGroup = (List<MultiLevelExpIndListAdapter.ExpIndData>) adapterOutlline.mGroups.get(newParent); // Получаем список с детьми к кому перемещаем элемент
                        try {
                            listChildParentGroup.addAll(newParent.getCountChild(), listChildrens); // Перемещаем элемент к нужному родителю в массиве
                        } catch (Exception e) {
                            listChildParentGroup.addAll(listChildParentGroup.size(), listChildrens); // Перемещаем элемент к нужному родителю в массиве
                        }
                    } else {
                        // Нету родителя которому перемещаем детей в mGroups
                        adapterOutlline.mGroups.put(newParent, listChildrens);
                    }
                }

                swipeLine.setParentItemId(newParent.getServerId());
                swipeLine.setLevel(newParent.getLevel() + 1);
                swipeLine.setSort(newParent.getCountChild() + 1);

                swipeLine.save();

                // После всех дел обновляем уровень всех детей в родителе
                ActiveAndroid.beginTransaction();
                try {
                    newParent.recursiveUpdateChildsLevel(newParent, null);
                    adapterOutlline.updateLineToList(newParent);
                    ActiveAndroid.setTransactionSuccessful();
                } finally {
                    ActiveAndroid.endTransaction();
                }

                if (swipeLine.getCountChild() > 0) { // Есть дети обновляем их уровень в mgroups
                    if (swipeLine.isGroup()) { // Обновление уровня в массиве mgroups
                        adapterOutlline.updateChildsLevelParentLineToMGroups(swipeLine);
                    } else { // Обновление группы в mData
                        checkLevelsGrup(listChildrens);
                    }
                }
            } else {
                ManagerProgressDialog.dismiss();
            }
        }

        private void checkLevelsGrup(List<Line> childs) {
            for (Line child : childs) {
                if (child.getCountChild() > 0) {
                    ArrayList<Line> newChilds = getChildsFromList(child, childs);
                    for (Line newChild : newChilds) {
                        newChild.setLevel(child.getLevel() + 1);
                        newChild.save();
                    }
                }
            }
        }

        private ArrayList<Line> getChildsFromList(Line parent, List<Line> list) {
            ArrayList<Line> newChilds = new ArrayList<>();
            for (Line child : list) {
                if (child.getParentItemId() == parent.getServerId()) {
                    newChilds.add(child);
                }
            }
            return newChilds;
        }

        // Смахивание влево
        private void swipeLineLevelUp(final Handler callbackStartResponseUpdate, final Line swipeLine, int positionSwipeLine, final Line lastParent) {
            showProgress();
            this.positionSwipeLine = positionSwipeLine;
            this.callbacResponseUpdate = callbackStartResponseUpdate;
            this.swipeLine = swipeLine;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    startLineUpLevel(lastParent, swipeLine);
                    if (callbackStartResponseUpdate != null) {
                        callbackStartResponseUpdate.sendEmptyMessage(0);
                    }
                }
            }).start();
        }

        // Начало смещение линиии на уровень вниз
        @SuppressWarnings("unchecked")
        private boolean startLineUpLevel(Line lastParent, final Line swipeLine) {
            // Если элемент не корневой - ставим ему нового родителя, декрементируем уровень вложенности,
            // Ставим сортировку после родителя
            if (lastParent != null) {
                // Перемещение элемента или его детей в массиве с данными
                adapterOutlline.moveLineToNewPosition(swipeLine, lastParent, positionSwipeLine);
                // Установка данных нового места
                swipeLine.setParentItemId(lastParent.getParentItemId());
                swipeLine.setLevel(swipeLine.getLevel() - 1);
                swipeLine.setSort(lastParent.getSort() + 0.1);
                swipeLine.save();
                // Обновляем сортировку
                Line newParent = Line.findByServerId(Line.class, swipeLine.getParentItemId());
                if (newParent != null) {
                    updateItemsSortIndex((List<Line>) newParent.getChildren());
                } else {
                    updateItemsSortIndex(getChildrenParentLine(0));
                }

                // Обновление уровня всех детей
                ActiveAndroid.beginTransaction();
                try {
                    swipeLine.recursiveUpdateChildsLevel(swipeLine, null);
                    adapterOutlline.updateMdataLevel(swipeLine);
                    adapterOutlline.updateLineToList(swipeLine);
                    ActiveAndroid.setTransactionSuccessful();
                } finally {
                    ActiveAndroid.endTransaction();
                }

                responseUpdateSort(Line.getChildrenParentLine(lastParent.getParentItemId()));
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        adapterOutlline.notifyDataSetChanged();
                        ManagerProgressDialog.dismiss();
                    }
                });
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        ManagerApplication.getInstance().showToast(R.string.error_level_root);
                        ManagerProgressDialog.dismiss();
                    }
                });
                return false;
            }
            return true;
        }

        //==========================================
        //  Another methods
        //==========================================
        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
                final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                final int swipeFlags = 0;
                return makeMovementFlags(dragFlags, swipeFlags);
            } else {
                final int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
                int swipeFlags = ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;

                // Отключаем скрол вправо у первой задачи в корневом списке
                if (viewHolder.getAdapterPosition() < 1 && adapterOutlline.getRootLevel() == 1) {
                    swipeFlags = 0;
                }

                return makeMovementFlags(dragFlags, swipeFlags);
            }
        }

        // DragAndDrop - event начало конец
        @Override
        public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
            super.onSelectedChanged(viewHolder, actionState);
            switch (actionState) {
                case ItemTouchHelper.ACTION_STATE_DRAG: // Start
                    if (Build.VERSION.SDK_INT >= 26) {
                        ((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        ((Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(50);
                    }

                    isStartDrag = true;
                    tempTargetHolder = (AdapterOutline.ViewHolder) viewHolder;
                    tempDragViewHolder = (AdapterOutline.ViewHolder) viewHolder;
                    ((AdapterOutline.ViewHolder) viewHolder).startDrag();
                    ((AdapterOutline.ViewHolder) viewHolder).checkColapseGroupStartDrag();
                    break;
                case ItemTouchHelper.ACTION_STATE_IDLE: // End
                    isEndDrag = true;
                    isStartDrag = false;
                    if (tempDragViewHolder != null) {
                        tempDragViewHolder.endSelectedDrag();
                    }
                    break;
                case ItemTouchHelper.ACTION_STATE_SWIPE:
                    break;
            }
        }

        public void setMoveItem(boolean moveItem) {
            isMoveItem = moveItem;
        }
    };

    public Project getProject() {
        if (project == null) {
            project = Project.findByServerId(Project.class, projectId);
        }
        return project;
    }

    public void setLineStart(Line lineStart) {
        this.lineStart = lineStart;
    }
}