package com.mifors.goalton.fragment.projects;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.mifors.goalton.R;
import com.mifors.goalton.activity.ActivitySettingProject;
import com.mifors.goalton.adapters.AdapterProfileSettingsUsers;
import com.mifors.goalton.adapters.AdapterSpinner;
import com.mifors.goalton.dialog.DialogEditProjectProfile;
import com.mifors.goalton.interfaces.InterfaceFragmentProjectSettings;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectRight;
import com.mifors.goalton.model.project.ProjectRightUser;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MultipartBody;

/**
 * Created by gly8 on 10.04.17.
 */

@SuppressWarnings("ALL")
public class FragmentProjectAccess extends Fragment implements Observer, InterfaceFragmentProjectSettings {
    private View rootView;
    private Spinner spinnerTeams;
    private AdapterSpinner adapterSpinner;
    private RecyclerView listUsers;
    private AdapterProfileSettingsUsers adapterProfileSettingsUsers;
    private long projectId;
    private long teamSelectedId;
    private long ownerUserId;
    private ArrayList<InterfaceSpinnerItem> listProjectRights;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_project_access, container, false);
        spinnerTeams = rootView.findViewById(R.id.spinner_teams);
        return rootView;
    }

    // Вызывается после завершения запроса на обновления данных
    @Override
    public void update(Observable o, Object arg) {
        initData();
        updateUserList();
    }

    private void initData() {
        projectId = getArguments().getLong(ActivitySettingProject.PROJECT_ID_KEY);
        teamSelectedId = getArguments().getLong(ActivitySettingProject.TEAM_ID);
        ownerUserId = getArguments().getLong(ActivitySettingProject.OWNER_USER_ID);
        listProjectRights = new ArrayList<>();
        listProjectRights.addAll(ProjectRight.findByParentProjectId(projectId));

        if (getActivity() != null) {
            adapterSpinner = new AdapterSpinner(getActivity(), R.layout.item_spinner_accounts_teams, listProjectRights, spinnerTeams, true, true);
        } else {
            return;
        }

        if (projectId == 0) { // Новый проект
            // Устанаваливаем первой выбранную команду нашу
            long myId = ManagerAccount.getMyProfile().getServerId();
            for (int i = 0; i < listProjectRights.size(); i++) {
                try {
                    ProjectRight pr = (ProjectRight) listProjectRights.get(i);
                    if (pr.getOwnerUserId() == myId) {
                        adapterSpinner.setSelectedPosition(i);
                        break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            adapterSpinner.setSelectedPositionByValue(teamSelectedId);
        }


        spinnerTeams.setAdapter(adapterSpinner);
        spinnerTeams.setSelection(adapterSpinner.getSelectedPosition());
        spinnerTeams.setOnItemSelectedListener(listenerSpiner);
    }

    private void initListUsers(ProjectRight projectRight) {
        try{
            ArrayList<ProjectRightUser> list = (ArrayList<ProjectRightUser>) ProjectRightUser.getAllRightsByProjectRight(projectRight);
            adapterProfileSettingsUsers = new AdapterProfileSettingsUsers(list, onClickItem);
            if (getListUsers() != null) {
                getListUsers().setAdapter(adapterProfileSettingsUsers);
            }

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private AdapterView.OnItemSelectedListener listenerSpiner = new AdapterView.OnItemSelectedListener() {
        boolean isInitCalling = true;

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (!isInitCalling) {
                adapterSpinner.setSelectedPosition(position);
                adapterSpinner.notifyDataSetChanged();
                String v = adapterSpinner.getValueByPosition(position);
                teamSelectedId = Long.parseLong(v);
                updateUserList();
            }
            isInitCalling = false;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /*
    * Возвращает выбранную группу из списка
    * **/
    private ProjectRight getProjectRightFromSpinnerAdapter() {
        try{
            long value = Long.parseLong(adapterSpinner.getItem(adapterSpinner.getSelectedPosition()).getValue());
            ProjectRight projectRight;
            if (ownerUserId == 0) {
                projectRight = ProjectRight.findByTeamId(value);
            } else {
                projectRight = ProjectRight.findByTeamIdAndOwnerUser(value, ownerUserId);
            }

            return projectRight;
        } catch (Exception e) {
            e.printStackTrace();
            if (listProjectRights != null && listProjectRights.size() > 0) {
                try{
                    return ProjectRight.findByTeamId(Long.parseLong(listProjectRights.get(0).getValue())) ;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        return null;
    }


    private void updateUserList() {
        ProjectRight projectRight;

        // Новый проект
        if (ownerUserId == 0) {
            projectRight = ProjectRight.findByTeamId(teamSelectedId);
        } else {
            projectRight = ProjectRight.findByTeamIdAndOwnerUser(teamSelectedId, ownerUserId);
        }

        if (projectRight != null) {
            initListUsers(projectRight);
        } else {
            long myId = ManagerAccount.getMyProfile().getServerId();
            // Нет выбранной команды. Ставим по умолчанию первую.
            List<ProjectRight> list = ProjectRight.findByParentProjectId(projectId);
            if (!list.isEmpty()) {
                for (int i = 0; i < list.size(); i++) {
                    ProjectRight pr = list.get(i);
                    if (pr.getOwnerUserId() == myId) {
                        // Сохраняем то что команда выбрана
                        teamSelectedId = pr.getTeamId();
                        initListUsers(pr);
                        break;
                    }
                }
            }
        }
    }

    private View.OnClickListener onClickItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Project project = Project.findByServerId(Project.class, projectId);
            DialogEditProjectProfile dialog = new DialogEditProjectProfile();
            dialog.setProject(project);
            ProjectRight projectRight = getProjectRightFromSpinnerAdapter();
            if (projectRight != null) {
                dialog.setProjectRightUser(ProjectRightUser.findByProjectRight(teamSelectedId, Long.parseLong(v.getTag().toString()), projectRight));
                dialog.show(getActivity().getSupportFragmentManager(), "TAG");
            }
        }
    };

    // Конвертирование данных к телу запросу
    @Override
    public MultipartBody.Builder getSettings(MultipartBody.Builder buidler) {
        try {
            ProjectRight projectRight = getProjectRightFromSpinnerAdapter();
            buidler.addFormDataPart("Team[]", String.valueOf(projectRight.getTeamId()));
            List<ProjectRightUser> list = ProjectRightUser.getAllRightsByProjectRight(projectRight);
            for (ProjectRightUser projectRightUser : list) {
                String name = "User[" + projectRightUser.getUserId() + "]";
                buidler.addFormDataPart(name, String.valueOf(projectRightUser.getUserId()));
                if ("1".equals(projectRightUser.getRightToEdit())) {
                    String nameParams = "User[" + projectRightUser.getUserId() + "][right_to_edit]";
                    buidler.addFormDataPart(nameParams, projectRightUser.getRightToEdit());
                }

                if ("1".equals(projectRightUser.getRightToEditProject())) {
                    String nameParams = "User[" + projectRightUser.getUserId() + "][right_to_edit_project]";
                    buidler.addFormDataPart(nameParams, projectRightUser.getRightToEditProject());
                }

                if ("1".equals(projectRightUser.getRightToInvite())) {
                    String nameParams = "User[" + projectRightUser.getUserId() + "][right_to_invite]";
                    buidler.addFormDataPart(nameParams, projectRightUser.getRightToInvite());
                }

                if ("1".equals(projectRightUser.getRightToView())) {
                    String nameParams = "User[" + projectRightUser.getUserId() + "][right_to_view]";
                    buidler.addFormDataPart(nameParams, projectRightUser.getRightToView());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return buidler;
    }

    @Override
    public void updateInterface() {
    }

    public RecyclerView getListUsers() {
        if (listUsers == null && rootView != null) {
            listUsers = rootView.findViewById(R.id.edit_team_list);
            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
            listUsers.setLayoutManager(mLayoutManager);
        }
        return listUsers;
    }
}