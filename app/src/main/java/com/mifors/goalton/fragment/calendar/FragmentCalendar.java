package com.mifors.goalton.fragment.calendar;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.activity.ActivityEditLineOutline;
import com.mifors.goalton.adapters.AdapterViewPagerDays;
import com.mifors.goalton.interfaces.InterfaceCallbackActivityMain;
import com.mifors.goalton.interfaces.InterfaceFragmentCalendar;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerPlanner;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.ui.InfiniteViewPager;
import com.mifors.goalton.ui.compactcalendarview.CompactCalendarView;
import com.mifors.goalton.utils.calendar.ObservableFragmentDays;
import com.mifors.goalton.utils.calendar.PlannerFilter;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FragmentCalendar extends Fragment implements InterfaceFragmentCalendar {
    private static final String TAG = "Goalton [" + FragmentCalendar.class.getSimpleName() + "]";
    private View rootView;
    private InterfaceCallbackActivityMain interfaceCallbackActivity;
    private TextView titleDay;
    private TextView titleMonth;
    private Calendar selectedCalendar; // Дата дня выбраного на viewPager
    private InfiniteViewPager viewPagerDays;
    private FragmentPagerAdapter adapterViewPager;
    public static final int COUNT_ITEMS = 400000;
    private ObservableFragmentDays observableResponse;
    private ActionBar actionBar;
    private RelativeLayout appBarView;
    // Обновлять ли данные после того как закончили перемещение
    // Нужно для того чтобы после запроса данных не терялся родеитель перетаскиваемого элемента
    private boolean isUpdateFragmentsEndDrag = false;
    private FragmentDay currentFragment;
    private View btnFloating;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        registerBroadcatReciverChangeTime();
        super.onCreate(savedInstanceState);
        responseGetPlanner(ManagerCalendar.getInstance());
    }

    @Override
    public void onDestroy() {
        destroyBroadcastReciverTimer();
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setSelectedCalendar(ManagerCalendar.getInstance());
        observableResponse = new ObservableFragmentDays();
        interfaceCallbackActivity = (InterfaceCallbackActivityMain) getActivity();
        rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        viewPagerDays = rootView.findViewById(R.id.view_pager_days);

        if (rootView != null) {
            rootView.setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(getContext()));
        }


        setCustomActionBar();
        initViewPager();
        updateActionBarDate();
        viewPagerDays.setOnDragListener(onDragListinerViewPger);
        btnFloating = rootView.findViewById(R.id.fab_add_new_outline_item);
        btnFloating.setOnClickListener(onClickAddItem);
        btnFloating.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    btnFloating.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    btnFloating.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                ManagerPlanner.saveHeightFloatingActionBar(btnFloating.getHeight());
            }
        });
        Log.v(TAG, "[onCreate] listLines = " + Line.getOfflineLines());
        return rootView;
    }

    private View.OnDragListener onDragListinerViewPger = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent dragEvent) {
            int action = dragEvent.getAction();
            switch (action) {
                case DragEvent.ACTION_DRAG_STARTED:
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    try {
                        FragmentDay fragmentDay = (FragmentDay) adapterViewPager.instantiateItem(viewPagerDays, viewPagerDays.getCurrentItem());
                        fragmentDay.onDragListenerContainerDeadlinesDate.onDrag(v, dragEvent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    ((View) dragEvent.getLocalState()).setVisibility(View.VISIBLE);
                    break;
                case DragEvent.ACTION_DRAG_LOCATION:
                    if (currentFragment != null) {
                        currentFragment.scrollBydragAndDrop(v, dragEvent);
                    }
                    break;
                default:
                    break;
            }

            return false;
        }
    };

    public void setCustomActionBar() {
        interfaceCallbackActivity.hideSpinnerSort();
        if (actionBar == null) {
            actionBar = interfaceCallbackActivity.getSupportActionBar();
            actionBar.setTitle("");
            LayoutInflater inflater = (LayoutInflater) rootView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            appBarView = (RelativeLayout) inflater.inflate(R.layout.app_bar_calendar, null);
            titleMonth = appBarView.findViewById(R.id.title_month_calendar);
            titleDay = appBarView.findViewById(R.id.title_day);
            appBarView.findViewById(R.id.container_action_bar_month).setOnClickListener(onClickCalendar);
            appBarView.findViewById(R.id.btn_refresh_day).setOnClickListener(onClickRefreshDay);
            ImageView btnMenu = appBarView.findViewById(R.id.item_menu_calendar);
            btnMenu.setOnClickListener(onClickMenu);
            ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
            actionBar.setCustomView(appBarView, params);
        }

        actionBar.setDisplayShowCustomEnabled(true);
        appBarView.setVisibility(View.VISIBLE);
    }

    private void showPopupMenu(View view) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.context_menu_calendar, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_item_calendar_filter:
                        openDialogFilter();
                        break;
                    case R.id.menu_item_calendar_tomato:
                        observableResponse.sendNotifyUpdateCurrentHour();
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    private void updateActionBarDate() {
        String month = ManagerCalendar.getMonthByNumber(getContext(), getSelectedCalendar().get(Calendar.MONTH));
        String day = ManagerCalendar.convertCalendarToString(getSelectedCalendar(), "EE");
        String titleCurrentDay = month + ", " + day;
        titleMonth.setText(titleCurrentDay);
        titleDay.setText(String.valueOf(getSelectedCalendar().get(Calendar.DAY_OF_MONTH)));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        responseGetPlanner(getSelectedCalendar());
        super.onActivityResult(requestCode, resultCode, data);
    }

    //==========================================
    // View pager methods
    //==========================================
    private void initViewPager() {
        adapterViewPager = new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public int getCount() {
                return COUNT_ITEMS;
            }

            @Override
            public Fragment getItem(int position) {
                FragmentDay fragment = new FragmentDay(getContext());
                fragment.setInterfaceFragmentCalendar(FragmentCalendar.this);
                return fragment;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                FragmentDay fragmentDay = (FragmentDay) super.instantiateItem(container, position);
                observableResponse.addObserver(fragmentDay);
                return fragmentDay;
            }
        };

        AdapterViewPagerDays wrappedAdapter = new AdapterViewPagerDays(adapterViewPager);
        viewPagerDays.setAdapter(wrappedAdapter);
        viewPagerDays.addOnPageChangeListener(onPageChangeListener);
        viewPagerDays.setCurrentItem(200000);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int p, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            currentFragment = (FragmentDay) adapterViewPager.instantiateItem(viewPagerDays, viewPagerDays.getCurrentItem());
            ManagerProgressDialog.dismiss();
            int index = position - FragmentCalendar.COUNT_ITEMS / 2;
            Calendar calendar = ManagerCalendar.getInstance();
            calendar.add(Calendar.MINUTE, 60 * 24 * index);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            updateSelectedDate(calendar);
            isScrollRight = false;
            isScrollLeft = false;
        }

        @Override
        public void onPageScrollStateChanged(int scrollState) {
            if (scrollState != ViewPager.SCROLL_STATE_IDLE) {
                final int childCount = viewPagerDays.getChildCount();
                for (int i = 0; i < childCount; i++) {
                    viewPagerDays.getChildAt(i).setLayerType(View.LAYER_TYPE_NONE, null);
                }
            }
        }
    };

    private Calendar tempCalendarScroll;

    private void scrollToDate(Calendar calendar, int resMessage) {
        if ((calendar.get(Calendar.DAY_OF_YEAR) == getSelectedCalendar().get(Calendar.DAY_OF_YEAR)) &&
                (calendar.get(Calendar.YEAR) == getSelectedCalendar().get(Calendar.YEAR))) {
            ManagerProgressDialog.dismiss();
        } else {
            startScrollDate(calendar, true, resMessage);
        }
    }

    private void startScrollDate(Calendar calendar, final boolean isShowProgressBar, int resMessage) {
        tempCalendarScroll = calendar;
        if (isShowProgressBar) {
            ManagerProgressDialog.showProgressBar(getActivity(), resMessage, false);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Calendar c = ManagerCalendar.getInstance();
                long beetwenDays = ManagerCalendar.getBeetwenDays(c, tempCalendarScroll);
                if (beetwenDays < 0) {
                    beetwenDays *= -1;
                } else {
                    beetwenDays = 0 - beetwenDays;
                }
                viewPagerDays.setCurrentItem((int) beetwenDays, true);
            }
        }, 500);
    }

    //==========================================
    // Broadcast reciver 
    //==========================================
    private final BroadcastReceiver broadcastReciverChangeTime = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            observableResponse.sendNotifyUpdateCurrentHour();
        }
    };

    private void registerBroadcatReciverChangeTime() {
        if (getActivity() != null) {
            IntentFilter intent = new IntentFilter();
            intent.addAction(Intent.ACTION_TIME_TICK);
            getActivity().registerReceiver(broadcastReciverChangeTime, intent);
        }
    }

    private void destroyBroadcastReciverTimer() {
        if (getActivity() != null) {
            getActivity().unregisterReceiver(broadcastReciverChangeTime);
        }
    }

    //==========================================
    // On click
    //==========================================
    private View.OnClickListener onClickMenu = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showPopupMenu(view);
        }
    };

    private View.OnClickListener onClickCalendar = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showDialogCalendar();
        }
    };

    private View.OnClickListener onClickAddItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            openCreateTask();
        }
    };

    private View.OnClickListener onClickRefreshDay = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                if (!isResponse) {
                    ManagerProgressDialog.showProgressBar(getContext(), R.string.update, false);
                    responseGetPlanner(getSelectedCalendar());
                } else {
                    ManagerApplication.getInstance().showToast(R.string.response_start_update);
                }
            }
        }
    };

    @Override
    public void updateSelectedDate(Calendar date) {
        if (isScrollLeft || isScrollRight) { // Если смена дня была произведена при перетаскивание задач между днями
            Calendar calendar = ManagerCalendar.getInstance();
            calendar.setTimeInMillis(getSelectedCalendar().getTimeInMillis());
            if (isScrollLeft) {
                calendar.add(Calendar.MINUTE, 1440);
            } else if (isScrollRight) {
                calendar.add(Calendar.MINUTE, -1440);
            }
            if (calendar.get(Calendar.WEEK_OF_YEAR) != date.get(Calendar.WEEK_OF_YEAR)) {
                isUpdateFragmentsEndDrag = true;
            }
        } else {
            if (getSelectedCalendar().get(Calendar.WEEK_OF_YEAR) != date.get(Calendar.WEEK_OF_YEAR)) {
                responseGetPlanner(date);
            }
        }

        setSelectedCalendar(date);
        updateActionBarDate();
    }

    @Override
    public void openTask(long serverId) {
        Intent intent = new Intent(getActivity(), ActivityEditLineOutline.class);
        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_LINE_ID, serverId);
        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_IS_OPEN_FROM_CALENDAR, true);
        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_MODE, ActivityEditLineOutline.Mode.UPDATE_LINE_CALENDAR);
        getActivity().startActivityForResult(intent, ActivityEditLineOutline.REQUEST_CODE_LINE_EDIT);
    }

    @Override
    public void openOutline(long lineId) {
        if (interfaceCallbackActivity != null) {
            interfaceCallbackActivity.replaceFragmentOutline(0, lineId);
        }
    }

    @Override
    public void openCreateTask() {
        Intent intent = new Intent(getActivity(), ActivityEditLineOutline.class);
        Calendar currentDate = ManagerCalendar.getInstance();

        // Если открываем сегодняшний день
        if (currentDate.get(Calendar.YEAR) == getSelectedCalendar().get(Calendar.YEAR) &&
                currentDate.get(Calendar.DAY_OF_YEAR) == getSelectedCalendar().get(Calendar.DAY_OF_YEAR)) {
            intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_DATE_CREATED, currentDate.getTimeInMillis());
        } else {
            intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_DATE_CREATED, getSelectedCalendar().getTimeInMillis());
        }

        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_IS_OPEN_FROM_CALENDAR, true);
        intent.putExtra(ActivityEditLineOutline.EXTRA_KEY_MODE, ActivityEditLineOutline.Mode.CREATE_LINE_CALENDAR);
        getActivity().startActivityForResult(intent, ActivityEditLineOutline.REQUEST_CODE_LINE_EDIT);
    }

    private boolean isScrollRight = false, isScrollLeft = false;

    @Override
    public void scrollNextDay() {
        if (!isScrollRight) {
            isScrollRight = true;
            getSelectedCalendar().add(Calendar.MINUTE, 1440);
            startScrollDate(getSelectedCalendar(), false, R.string.opening_day);
        }
    }

    @Override
    public void scrollPrevDay() {
        if (!isScrollLeft) {
            isScrollLeft = true;
            getSelectedCalendar().add(Calendar.MINUTE, -1440);
            startScrollDate(getSelectedCalendar(), false, R.string.opening_day);
        }
    }

    @Override
    public void responseGetPlannerSelectedDate() {
        if (getSelectedCalendar() != null) {
            responseGetPlanner(getSelectedCalendar());
        }
    }

    @Override
    public void notifyUpdateDays() {
        observableResponse.sendNotify();
    }

    @Override
    public void finishDragItem() {
        observableResponse.sendNotifyHideMinutes();
    }

    @Override
    public PlannerFilter getFilter() {
        return ManagerPlanner.getFilter();
    }


    //==========================================
    // Dialog calendar
    //==========================================
    private View dialogView;
    private AlertDialog alertDialogDate;
    private Calendar selectedDateDialogCalendar;
    private LinearLayout containerProjects;
    private TextView calendarTextViewMonth;
    private CheckBox checkBoxAllProjects;

    private void showDialogCalendar() {
        dialogView = View.inflate(getContext(), R.layout.dialog_calendar_selected_date, null);
        CompactCalendarView calendarViewDialogCalendar = dialogView.findViewById(R.id.calendar_view);
        calendarTextViewMonth = dialogView.findViewById(R.id.title_month_calendar);
        Date date = new Date();
        date.setTime(getSelectedCalendar().getTimeInMillis());
        updateTitleDialogMonth(date);
        calendarViewDialogCalendar.setCurrentDate(date);

        calendarViewDialogCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(java.util.Date date) {
                selectedDateDialogCalendar = ManagerCalendar.getInstance();
                selectedDateDialogCalendar.setTime(date);
                selectedDateDialogCalendar.set(Calendar.HOUR_OF_DAY, 2);
            }

            @Override
            public void onMonthScroll(java.util.Date firstDayOfNewMonth) {
                updateTitleDialogMonth(firstDayOfNewMonth);
            }
        });

        dialogView.findViewById(R.id.date_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogDate != null && alertDialogDate.isShowing()) {
                    alertDialogDate.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            finishSetDialogCalendar();
                        }
                    });

                    alertDialogDate.dismiss();
                }
            }
        });

        dialogView.findViewById(R.id.date_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alertDialogDate != null && alertDialogDate.isShowing()) {
                    alertDialogDate.dismiss();
                }
            }
        });

        alertDialogDate = new AlertDialog.Builder(getContext()).create();
        alertDialogDate.setView(dialogView);
        alertDialogDate.show();
    }

    private void updateTitleDialogMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        calendarTextViewMonth.setText(ManagerCalendar.getMonthByNumber(getContext(), calendar.get(Calendar.MONTH)) + "  " + calendar.get(Calendar.YEAR));
    }

    private void finishSetDialogCalendar() {
        if (selectedDateDialogCalendar != null) {
            scrollToDate(selectedDateDialogCalendar, R.string.opening_day);
            updateSelectedDate(selectedDateDialogCalendar);
            selectedDateDialogCalendar = null;
        }
    }

    //==========================================
    // Dialog filter
    //==========================================
    private void openDialogFilter() {
        try {
            dialogView = View.inflate(getContext(), R.layout.dialog_calendar_filter, null);
            RadioGroup radioGrouptypeProjects = dialogView.findViewById(R.id.radio_grou_types_projects);
            dialogView.findViewById(R.id.btn_ok).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ManagerPlanner.getFilter().getProjectsChecked().size() == 0) {
                        ManagerApplication.getInstance().showToast(R.string.error_empty_selected_project);
                    } else {
                        if (alertDialogDate != null) {
                            alertDialogDate.dismiss();
                        }
                        for (int i = 0; i < containerProjects.getChildCount(); i++) {
                            CheckBox checkBox = (CheckBox) containerProjects.getChildAt(i);
                            saveCheckedProject(checkBox, checkBox.isChecked());
                        }

                        ManagerPlanner.getFilter().save();
                        observableResponse.sendNotify();
                    }
                }
            });

            dialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (alertDialogDate != null) {
                        alertDialogDate.dismiss();
                    }
                    ManagerPlanner.getFilter().restore();
                }
            });
            radioGrouptypeProjects.check(ManagerPlanner.getFilter().getTypeShowRadioButon());
            radioGrouptypeProjects.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                    ManagerPlanner.getFilter().setTypeShowRadioButon(checkedId);
                }
            });

            List<Project> listProjects = Project.getAll(Project.class);
            containerProjects = dialogView.findViewById(R.id.container_projects);
            checkBoxAllProjects = dialogView.findViewById(R.id.checkbox_all_projects);
            checkBoxAllProjects.setOnCheckedChangeListener(onCheckedChangeListenerCheckBoxAll);

            for (Project project : listProjects) {
                CheckBox checkBox = new CheckBox(dialogView.getContext());
                checkBox.setTag(project.getServerId());
                checkBox.setText(project.getName());
                checkBox.setChecked(ManagerPlanner.getFilter().containsProjectId(new Long(project.getServerId())));
                checkBox.setOnCheckedChangeListener(onCheckedChangeListenerProjectDialogFilter);
                containerProjects.addView(checkBox);
            }

            updateCheckDialogFilter();
            alertDialogDate = new AlertDialog.Builder(getContext()).create();
            alertDialogDate.setView(dialogView);
            alertDialogDate.show();


            if (ManagerPlanner.getFilter().getProjectsChecked().size() == 1 && ManagerPlanner.getFilter().getProjectsChecked().get(0) == 0) {
                checkBoxAllProjects.setOnCheckedChangeListener(null);
                checkBoxAllProjects.setChecked(true);
                checkBoxAllProjects.setOnCheckedChangeListener(onCheckedChangeListenerCheckBoxAll);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }

    private void updateCheckDialogFilter() {
        int countCheck = 0;
        for (int i = 0; i < containerProjects.getChildCount(); i++) {
            CheckBox checkBox = (CheckBox) containerProjects.getChildAt(i);
            if (checkBox.isChecked()) {
                countCheck++;
            }
        }

        if (countCheck == containerProjects.getChildCount()) {
            checkBoxAllProjects.setOnCheckedChangeListener(null);
            checkBoxAllProjects.setChecked(true);
            checkBoxAllProjects.setOnCheckedChangeListener(onCheckedChangeListenerCheckBoxAll);
        } else {
            checkBoxAllProjects.setOnCheckedChangeListener(null);
            checkBoxAllProjects.setChecked(false);
            checkBoxAllProjects.setOnCheckedChangeListener(onCheckedChangeListenerCheckBoxAll);
        }
    }

    private CheckBox.OnCheckedChangeListener onCheckedChangeListenerCheckBoxAll = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (containerProjects != null) {
                for (int i = 0; i < containerProjects.getChildCount(); i++) {
                    CheckBox checkBox = (CheckBox) containerProjects.getChildAt(i);
                    checkBox.setChecked(isChecked);
                }
            }
            saveCheckedProject(buttonView, isChecked);
        }
    };

    private CheckBox.OnCheckedChangeListener onCheckedChangeListenerProjectDialogFilter = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            updateCheckDialogFilter();
            saveCheckedProject(buttonView, isChecked);
        }
    };

    private void saveCheckedProject(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getTag() != null) {
            long projectId = Long.parseLong(buttonView.getTag().toString());
            if (isChecked) {
                ManagerPlanner.getFilter().addIdProject(new Long(projectId));
            } else {
                ManagerPlanner.getFilter().removeIdProject(new Long(projectId));
            }
        }
    }

    //==========================================
    // Response methods
    //==========================================
    private boolean isResponse = false;

    private void responseGetPlanner(Calendar calendar) {
        if (!isResponse) {
            isResponse = true;
            if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                ManagerPlanner.responseGetPlannerData(calendar.get(Calendar.WEEK_OF_YEAR), calendar.get(Calendar.YEAR), callbackGetResponse);
            } else {
                isResponse = false;
                if (observableResponse != null) {
                    observableResponse.sendNotify();
                }
            }
        } else {
            ManagerProgressDialog.dismiss();
        }
    }

    private Handler callbackGetResponse = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            ManagerProgressDialog.dismiss();
            isResponse = false;
            if (msg.obj != null) {
                int week = (int) msg.obj;
                observableResponse.sendNotifyWeekDays(week);
            }
            super.handleMessage(msg);
        }
    };

    public Calendar getSelectedCalendar() {
        return selectedCalendar;
    }

    public void setSelectedCalendar(Calendar selectedCalendar) {
        this.selectedCalendar = selectedCalendar;
    }
}