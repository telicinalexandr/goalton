package com.mifors.goalton.fragment.profile_settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mifors.goalton.R;
import com.mifors.goalton.adapters.AdapterMemberships;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerInvite;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.myteams.MyTeam;
import com.mifors.goalton.model.myteams.MyTeamProject;

import java.util.Observable;
import java.util.Observer;

public class FragmentMemberships extends Fragment implements View.OnClickListener, Observer {
    private static final String TAG = "Goalton [" + FragmentMemberships.class.getSimpleName() + "]";

    private RecyclerView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        ManagerInvite.getInstance().addObserver(this);
        startResponseGetMemberShips();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_membership, container, false);
        listView = rootView.findViewById(R.id.recycleview_teams);
        swipeRefreshLayout = rootView.findViewById(R.id.refresh_layout_recycler);

        initView();
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ManagerInvite.getInstance().deleteObserver(this);
    }

    private void initView() {
        if (listView != null && getActivity() != null) {
            listView.setLayoutManager(new LinearLayoutManager(getActivity()));
            AdapterMemberships adapter = new AdapterMemberships(MyTeam.getAll(MyTeam.class), this, getActivity());
            listView.setAdapter(adapter);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                        responseGetTeamsData();
                    } else {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // Удалить меня из команды
            case R.id.btn_delete_me_from_teams:
                showDialogIsExitFromTeam(Long.parseLong(v.getTag().toString()));
                break;

            // Удалить меня из проекта
            case R.id.btn_delete_me_from_team_project:
                showDialogIsExitFromProject(Long.parseLong(v.getTag().toString()));
                break;
        }
    }

    //==========================================
    // Show dialog
    //==========================================
    private void showDialogIsExitFromTeam(final long teamProjectId) {
        MyTeam myTeam = MyTeam.findByServerId(MyTeam.class, teamProjectId);
        String message = String.format(getString(R.string.is_exit_from_team), myTeam.getName());
        ManagerAlertDialog.showDialog(getActivity(), message, R.string.yes, R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                responseUnLinkUserFromTeam(teamProjectId);
            }
        }, null);
    }

    private void showDialogIsExitFromProject(final long teamProjectId) {
        final MyTeamProject myTeamProject = MyTeamProject.findByServerId(MyTeamProject.class, teamProjectId);
        String message = String.format(getString(R.string.is_exit_from_project), myTeamProject.getName());
        ManagerAlertDialog.showDialog(getActivity(), message, R.string.yes, R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                responseUnlinkUserFromTeamProject(myTeamProject.getProjectId());
            }
        }, null);
    }
    //==========================================
    // Response
    //==========================================
    // Удаление из проекта
    private void responseUnlinkUserFromTeamProject(long temId) {
        ManagerProgressDialog.showProgressBar(getActivity(), R.string.delete_from_team_project, false);
        ManagerMyTeams.responseUserUnlinkFromTeamProject(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                responseGetTeamsData();
                responseUpdateProjectList();
            }
        }, temId);
    }

    // Удаление из команды
    private void responseUnLinkUserFromTeam(long teamProjectId) {
        ManagerProgressDialog.showProgressBar(getActivity(), R.string.delete_from_team, false);
        ManagerMyTeams.responseUserUnlinkFromTeam(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                responseGetTeamsData();
                responseUpdateProjectList();
            }
        }, teamProjectId);
    }

    // Загрузка данных команд
    public void responseGetTeamsData() {
        if (getActivity() != null) {
            ManagerProgressDialog.showProgressBar(getActivity(), R.string.update_data, false);
        }
        startResponseGetMemberShips();
    }

    private void responseUpdateProjectList() {
        ManagerProgressDialog.showProgressBar(getContext(), R.string.update_projects, false);
        ManagerProjects.responseGetProjects(new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
            }
        });
    }

    private void startResponseGetMemberShips(){
        ManagerMyTeams.responseGetMemberships(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                initView();
                if (swipeRefreshLayout != null) {
                    swipeRefreshLayout.setRefreshing(false);
                }

                ManagerProgressDialog.dismiss();
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        startResponseGetMemberShips();
    }
}