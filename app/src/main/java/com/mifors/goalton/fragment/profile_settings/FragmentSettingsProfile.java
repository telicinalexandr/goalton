package com.mifors.goalton.fragment.profile_settings;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.adapters.AdapterGridBgProfileSettings;
import com.mifors.goalton.adapters.AdapterSpinner;
import com.mifors.goalton.interfaces.InterfaceCallbackActivityMain;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerSpinnerGenerationData;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.user.Profile;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;
import com.mifors.goalton.ui.spinner.view.ItemSpinner;

import java.util.ArrayList;

public class FragmentSettingsProfile extends Fragment {

    private static final String TAG = "Goalton [" + FragmentSettingsProfile.class.getSimpleName() + "]";
    public static final int REQUEST_CODE = 52486;

    private Spinner spinnerCountrys, spinnerTimezone, spinnerCompanyCountry, spinnerLanguage;
    private AdapterSpinner adapterCountrys, adapterTimezone, adapterCompanySize, adapterLanguage;
    private CheckBox checkBoxGetToDo;
    private GridView gridBgSelected;
    private RelativeLayout btnOk;
    private View rootView;
    private AdapterGridBgProfileSettings adapterGridBg;
    private Profile myProfile;
    Profile tempProfile;
    private TextView titleSound;

    private CheckBox checkBoxVibroPlay, checkBoxSoundPlay;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myProfile = ManagerAccount.getMyProfile();
    }

    @Override
    public void onResume() {
        super.onResume();
        setGridSelectedBg();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings_profile, container, false);
        initViews();
        if (myProfile != null) {
            initDataSpinners();
            initDataGridBg();
            initDataProfile();
        }

        ManagerSpinnerGenerationData.initCounters(getActivity());
        ManagerSpinnerGenerationData.initTimezones(getActivity());
        ManagerSpinnerGenerationData.initCompanySize(getActivity());
        updateBg();
        ((InterfaceCallbackActivityMain) getActivity()).setActionBarTitle(R.string.settings_title, true);
        return rootView;
    }

    public void updateBg() {
        rootView.setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(getActivity()));
    }

    private void initViews() {
        spinnerCountrys = rootView.findViewById(R.id.spinner_country);
        spinnerTimezone = rootView.findViewById(R.id.spinner_timezone);
        spinnerLanguage = rootView.findViewById(R.id.spinner_language);
        spinnerCompanyCountry = rootView.findViewById(R.id.spinner_company_countrys);
        gridBgSelected = rootView.findViewById(R.id.gird_bg_select);
        checkBoxVibroPlay = rootView.findViewById(R.id.checkbox_vibro_notify);
        checkBoxSoundPlay = rootView.findViewById(R.id.checkbox_sound_notify);
        checkBoxGetToDo = rootView.findViewById(R.id.checkbox_get_todo);
        titleSound = rootView.findViewById(R.id.title_sound);
        checkBoxGetToDo.setOnClickListener(onGetToDoListener);


        btnOk = rootView.findViewById(R.id.settings_button_ok);
        btnOk.setOnClickListener(onBtnOkClickListener);


        checkBoxSoundPlay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.NOTIFICATIONS_SOUND, isChecked);
            }
        });

        checkBoxVibroPlay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.NOTIFICATIONS_VIBRO, isChecked);
            }
        });

        titleSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION);
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, getContext().getString(R.string.select_sound_dialog));
                intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);


                String uriStr = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.NOTIFICATIONS_SOUND_PATH);
                if (!TextUtils.isEmpty(uriStr)){
                    intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(uriStr));
                }

                ((Activity) getContext()).startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    private void initDataProfile() {
        if (myProfile != null) {
            ((EditText) rootView.findViewById(R.id.edit_login)).setText(myProfile.getUsername());
            ((EditText) rootView.findViewById(R.id.edit_email)).setText(myProfile.getEmail());
            ((EditText) rootView.findViewById(R.id.edit_firstname)).setText(myProfile.getFirstname());
            ((EditText) rootView.findViewById(R.id.edit_lastname)).setText(myProfile.getLastname());
            ((EditText) rootView.findViewById(R.id.edit_company_name)).setText(myProfile.getCompanyName());

            if (myProfile.getGetTodolist() == 0) {
                checkBoxGetToDo.setChecked(false);
            } else {
                checkBoxGetToDo.setChecked(true);
            }

            checkBoxSoundPlay.setChecked(ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.NOTIFICATIONS_SOUND));
            checkBoxVibroPlay.setChecked(ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.NOTIFICATIONS_VIBRO));
            updateTitleSoundSelected();
        }
    }

    public void updateTitleSoundSelected() {
        String nameringtone = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.NOTIFICATIONS_SOUND_NAME);
        if (!TextUtils.isEmpty(nameringtone)) {
            titleSound.setText(getString(R.string.sound_notifications) + "\r\n" + nameringtone);
        } else {
            titleSound.setText(getString(R.string.sound_notifications));
        }
    }

    private void initDataSpinners() {
        adapterCountrys = createSpinnerAdapter(spinnerCountrys,
                ManagerSpinnerGenerationData.getPositionFromArrayString(R.array.countrys_values, myProfile.getCountry()),
                createArray(getResources().getStringArray(R.array.countrys_values)));

        adapterTimezone = createSpinnerAdapter(spinnerTimezone,
                ManagerSpinnerGenerationData.getPositionFromArrayString(R.array.timezones_my_values, String.valueOf(myProfile.getTimezone()) + ".0"),
                createArray(getResources().getStringArray(R.array.timezones_my_values)));

        adapterCompanySize = createSpinnerAdapter(spinnerCompanyCountry,
                ManagerSpinnerGenerationData.getPositionFromArrayString(R.array.company_size_values, String.valueOf(myProfile.getCompanySize())),
                createArray(getResources().getStringArray(R.array.company_size_values)));

        String arrLang[] = getResources().getStringArray(R.array.languages);
        ArrayList<InterfaceSpinnerItem> listLangauges = new ArrayList<>();
        listLangauges.add(new ItemSpinner(arrLang[0], "ru", 1));
        listLangauges.add(new ItemSpinner(arrLang[1], "en", 2));
        adapterLanguage = createSpinnerAdapter(spinnerLanguage,
                ManagerSpinnerGenerationData.getPositionFromArrayStringContains(R.array.languages, ManagerApplication.getInstance().getCurrentLangiage().split("_")[0]),
                listLangauges);
    }


    /**
     * Создание выпадающего списка
     *
     * @param spinner          - Spinner для которого нужно создать список
     * @param selectedPosition - выбранная ранее позиция. Начинается с 0
     */
    private AdapterSpinner createSpinnerAdapter(Spinner spinner, int selectedPosition, ArrayList<InterfaceSpinnerItem> arrayItems) {
        AdapterSpinner adapter = new AdapterSpinner(getActivity(), R.layout.item_spinner_check, arrayItems);
        adapter.setSelectedPosition(selectedPosition);
        spinner.setAdapter(adapter);
        spinner.setSelection(selectedPosition);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            boolean isInitCalling = true;

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!isInitCalling) {
                    AdapterSpinner a = (AdapterSpinner) parent.getAdapter();
                    a.setSelectedPosition(position);
                    a.notifyDataSetChanged();
                }
                isInitCalling = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        return adapter;
    }

    private ArrayList<InterfaceSpinnerItem> createArray(String[] arr) {
        ArrayList<InterfaceSpinnerItem> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            list.add(createItem(arr[i], i));
        }
        return list;
    }

    private ItemSpinner createItem(String name, int id) {
        return new ItemSpinner(name, id);
    }

    private View.OnClickListener onBtnOkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                String login = ((EditText) rootView.findViewById(R.id.edit_login)).getText().toString();
                String email = ((EditText) rootView.findViewById(R.id.edit_email)).getText().toString();
                String newPass = ((EditText) rootView.findViewById(R.id.edit_new_pass)).getText().toString();
                String firstname = ((EditText) rootView.findViewById(R.id.edit_firstname)).getText().toString();
                String lastname = ((EditText) rootView.findViewById(R.id.edit_lastname)).getText().toString();
                String companyName = ((EditText) rootView.findViewById(R.id.edit_company_name)).getText().toString();
                String country = ManagerSpinnerGenerationData.getNameByPositionFromArray(R.array.countrys_values, adapterCountrys.getSelectedPosition());
                String timeZone = ManagerSpinnerGenerationData.getNameByPositionFromArray(R.array.timezones_my_values, adapterTimezone.getSelectedPosition());
                String companySize = ManagerSpinnerGenerationData.getNameByPositionFromArray(R.array.company_size_values, adapterCompanySize.getSelectedPosition());
                String bg = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.NAME_SELECTED_BG);

                tempProfile = new Profile();
                tempProfile.setUsername(login);
                tempProfile.setEmail(email);
                tempProfile.setPassword(newPass);
                tempProfile.setFirstname(firstname);
                tempProfile.setLastname(lastname);
                tempProfile.setCountry(country);
                tempProfile.setTimezone(timeZone);
                tempProfile.setCompanySize(companySize);
                tempProfile.setCompanyName(companyName);
                tempProfile.setBg(bg);
                ManagerProgressDialog.showProgressBar(getActivity(), R.string.update, false);
                ManagerAccount.responseUpdateProfile(tempProfile, callbackCompleteUpdate);
            }
        }
    };

    private Handler callbackCompleteUpdate = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ManagerProgressDialog.dismiss();
            switch (msg.what) {
                case HttpParam.RESPONSE_OK:
                    ManagerJSONParsing.saveMyProfile(myProfile, tempProfile, true);


                    if (adapterLanguage != null) {
                        String value = adapterLanguage.getValueByPosition(adapterLanguage.getSelectedPosition());
                        String lang = ManagerApplication.getInstance().getCurrentLangiage();
                        if (!value.toLowerCase().contains(lang)) {
                            showDailogRestartApplication();
                            ManagerApplication.getInstance().setLanguageApplication(value);
                        } else {
                            showDialog();
                        }
                        Log.v(TAG, "[] ");
                    } else {
                        showDialog();
                    }

                    if (getActivity() != null) {
                        ((InterfaceCallbackActivityMain) getActivity()).updateNavHeader();
                    }
                    break;
                case HttpParam.RESPONSE_ERROR:
                    ManagerApplication.getInstance().showToast(R.string.error_save_profile_setup);
                    break;
            }
        }
    };

    private void showDialog() {
        ManagerAlertDialog.showDialog(getActivity(), R.string.data_send_complete);
    }

    private void showDailogRestartApplication() {
        ManagerAlertDialog.showDialog(getContext(), R.string.language_change_is_restart_app, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ManagerApplication.getInstance().restartApplication();
            }
        });
    }

    private void initDataGridBg() {
        adapterGridBg = new AdapterGridBgProfileSettings(getActivity(), getResources().getStringArray(R.array.bg_names));
        gridBgSelected.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapterGridBg != null) {
                    ManagerAccount.saveSelectedBackgroundNameImage(adapterGridBg.getValue(position));
                    String nameBg = ManagerAccount.getSelectedNameBackground();
                    myProfile.setBg(nameBg);
                    myProfile.save();
                    updateBg();
                    ((FragmentControllerProfileSettings) getParentFragment()).updateSelectedRoot();
                    setGridSelectedBg();
                    ManagerAccount.responseChangeBackGround(nameBg);
                }
            }
        });
        gridBgSelected.setAdapter(adapterGridBg);
        setGridSelectedBg();
    }

    private void setGridSelectedBg() {
        if (adapterGridBg != null) {
            adapterGridBg.setSelectedPosition(ManagerSpinnerGenerationData.getPositionFromArrayString(R.array.bg_names, myProfile.getBg()));
            adapterGridBg.notifyDataSetChanged();
        }
    }

    private View.OnClickListener onGetToDoListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            CheckBox c = (CheckBox) v;
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                if (c.isChecked()) {
                    ManagerAccount.responseSubscriptNotify(1);
                } else {
                    ManagerAccount.responseSubscriptNotify(0);
                }
            } else {
                c.setChecked(!c.isChecked());
            }
        }
    };
}
