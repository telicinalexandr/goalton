package com.mifors.goalton.fragment.projects;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.mifors.goalton.DebugKeys;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivitySettingProject;
import com.mifors.goalton.adapters.AdapterSpinner;
import com.mifors.goalton.interfaces.InterfaceFragmentProjectSettings;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerTakePhoto;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectSubject;
import com.mifors.goalton.network.Api.ApiModule;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;
import com.mifors.goalton.utils.RoundedTransformation;
import com.mifors.goalton.utils.exceptions.ErrorCreateProjectSettings;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import okhttp3.MultipartBody;

import static android.R.attr.defaultValue;
import static android.app.Activity.RESULT_OK;
import static android.graphics.Bitmap.createBitmap;
import static com.mifors.goalton.managers.utils.ManagerTakePhoto.REQUEST_CODE_GALLERY;
import static com.mifors.goalton.managers.utils.ManagerTakePhoto.REQUEST_IMAGE_CAPTURE;

public class FragmentProjectSettings extends Fragment implements Observer, View.OnClickListener, InterfaceFragmentProjectSettings {
    private View rootView;
    private Project project;
    private ImageView imageProject;
    private View imageCover;
    private EditText editName;
    private Spinner spinnerSubject;
    private View containerSpinner;
    private CheckBox checkBoxPublic;
    private AdapterSpinner adapterSpinner;
    private View bigHead;
    private View containerCover;
    private RelativeLayout containerTouchCover;
    private Bitmap bitmapImage; // Содержит обложку при смене с камеры или галереи для отправки на сервак
    private boolean isDeleteCover = false;

    private ManagerTakePhoto managerTakePhoto;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initMy();
        return rootView;
    }

    private void initMy() {
        rootView = LayoutInflater.from(getContext()).inflate(R.layout.fragment_projects_settings, null, false);
        findViewById();
        init();
    }

    private void findViewById() {
        containerTouchCover = rootView.findViewById(R.id.container_touch_cover);
        imageProject = rootView.findViewById(R.id.project_cover);
        imageCover = rootView.findViewById(R.id.img_cover);
        bigHead = rootView.findViewById(R.id.project_big_head);
        checkBoxPublic = rootView.findViewById(R.id.checkbox_public);
        spinnerSubject = rootView.findViewById(R.id.spinner_subject);
        containerSpinner = rootView.findViewById(R.id.container_spinner);
        containerCover = rootView.findViewById(R.id.container_covers);
        checkBoxPublic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    spinnerSubjectEnabled();
                } else {
                    spinnerSubjectDisable();
                }
            }
        });

        rootView.findViewById(R.id.btn_delete_image_project).setOnClickListener(this);
        rootView.findViewById(R.id.btn_select_image_project).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (rootView == null) {
            initMy();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case (REQUEST_IMAGE_CAPTURE): {
                    bigHead.setVisibility(View.GONE);
                    bitmapImage = getManagerTakePhoto().getBitmapFromCameraData();
                    imageProject.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                    imageProject.setImageBitmap(bitmapImage);
                }
                break;

                case (REQUEST_CODE_GALLERY): {
                    if (bigHead == null) {
                        findViewById();
                    }

                    bigHead.setVisibility(View.GONE);
                    bitmapImage = getManagerTakePhoto().getBitmapFromGalleryData(data);
                    imageProject.measure(0, 0);
                    if (bitmapImage != null) {
                        imageProject.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                        imageProject.setImageBitmap(bitmapImage);
                    }
                }
                break;
            }

            if (bitmapImage != null) {
                isDeleteCover = false;
                imageProject.setEnabled(true);
                initImageProject();
            }
        }
    }

    private void init() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            long projectId = bundle.getLong(ActivitySettingProject.PROJECT_ID_KEY, defaultValue);
            project = Project.findByServerId(Project.class, projectId);
        }

        if (project.getSubjectId() > 0) {
            checkBoxPublic.setChecked(true);
        } else {
            checkBoxPublic.setChecked(false);
        }

        try {
            int width = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.WIDTH_COVER);
            int height = ManagerApplication.getInstance().getSharedManager().getValueInteger(SharedKeys.HEIGHT_COVER);

            if (width == 0 && height == 0) { // При первом запуске нет размеров
                width = (int) getResources().getDimension(R.dimen.width_cover);
                height = (int) getResources().getDimension(R.dimen.height_cover);
            }

            if (width > 0 && height > 0) {
                RelativeLayout.LayoutParams lpContainerCover = new RelativeLayout.LayoutParams(width, height);
                lpContainerCover.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                containerCover.setLayoutParams(lpContainerCover);
            }

            if (!TextUtils.isEmpty(project.getUrlImage())) {
                bigHead.setVisibility(View.GONE);
                Picasso.Builder builder = new Picasso.Builder(ManagerApplication.getInstance().getApplicationContext());
                builder.build()
                        .load(project.getUrlImage())
                        .transform(new RoundedTransformation(9, 0))
                        .into(imageProject);
            } else {
                bigHead.setVisibility(View.VISIBLE);
                Picasso.with(rootView.getContext())
                        .load(R.drawable.bg_default)
                        .transform(new RoundedTransformation(9, 0))
                        .into(imageProject);
            }

            imageProject.setEnabled(false);

        } catch (Exception e) {
            e.printStackTrace();
        }

        getEditName().setText(project.getName());

        ArrayList<InterfaceSpinnerItem> arrayList = new ArrayList<>();
        List<ProjectSubject> list = ProjectSubject.getSubjectByProjectId(project.getServerId());
        arrayList.addAll(list);
        adapterSpinner = new AdapterSpinner(getContext(), R.layout.item_spinner_check, arrayList, spinnerSubject, true, false);
        spinnerSubject.setAdapter(adapterSpinner);

        if (project.getSubjectId() == 0) {
            adapterSpinner.setSelectedPosition(0);
            spinnerSubjectDisable();
        } else {
            spinnerSubjectEnabled();
            int subjjectId = (int) project.getSubjectId();
            subjjectId--;
            adapterSpinner.setSelectedPosition(subjjectId);
            spinnerSubject.setSelection(subjjectId);
        }

        initImageProject();
    }

    // Вызывается после загрузки настроек проекта
    @Override
    public void update(Observable o, Object arg) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_delete_image_project:
                ManagerAlertDialog.showDialog(getContext(), R.string.is_delete_cover, R.string.yes, R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteImageCover();
                    }
                }, null);
                break;
            case R.id.btn_select_image_project:
                getManagerTakePhoto().takePhoto();
                break;
        }
    }

    private void spinnerSubjectDisable() {
        spinnerSubject.setEnabled(false);
        if (spinnerSubject.getSelectedView() != null) {
            spinnerSubject.getSelectedView().setEnabled(false);
        }
        containerSpinner.setAlpha(0f);
    }

    private void spinnerSubjectEnabled() {
        spinnerSubject.setEnabled(true);
        if (spinnerSubject.getSelectedView() != null) {
            spinnerSubject.getSelectedView().setEnabled(true);
        }
        containerSpinner.setAlpha(1f);
    }

    //==========================================
    // Image cover
    //==========================================
    // Инициализация обложки
    private void initImageProject() {
        imageProject.setOnTouchListener(ontouchImage);
        containerTouchCover.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ontouchImage.onTouch(v, event);
                return true;
            }
        });
    }

    private View.OnTouchListener ontouchImage = new View.OnTouchListener() {
        private int clickCount;
        private long startTime = 0, duration = 0;
        private Matrix matrix = new Matrix();
        private Matrix savedMatrix = new Matrix();
        private final int NONE = 0;
        private final int DRAG = 1;
        private final int ZOOM = 2;
        private int mode = NONE;

        private PointF start = new PointF();
        private PointF mid = new PointF();
        private float oldDist = 1f;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ImageView view = null;
            float scale;
            float x = event.getX();
            float y = event.getY();

            if (v instanceof ImageView) {
                view = (ImageView) v;
                view.setScaleType(ImageView.ScaleType.MATRIX);
            } else {
                if (imageProject.isEnabled()) {
                    view = imageProject;
                    view.setScaleType(ImageView.ScaleType.MATRIX);
                }
            }

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:   // first finger down only
                    savedMatrix.set(matrix);
                    start.set(x, y);
                    mode = DRAG;
                    startTime = System.currentTimeMillis();
                    clickCount++;
                    break;
                case MotionEvent.ACTION_UP: // first finger lifted
                    long time = System.currentTimeMillis() - startTime;
                    duration = duration + time;
                    if (clickCount == 2) {
                        if (duration <= 1000) {
                            matrix.postTranslate(0, 0);
                        }
                        clickCount = 0;
                        duration = 0;
                        break;
                    }
                    break;
                case MotionEvent.ACTION_POINTER_UP: // second finger lifted
                    mode = NONE;
                    break;
                case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down
                    oldDist = spacing(event);
                    if (oldDist > 5f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event);
                        mode = ZOOM;
                    }
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mode == DRAG) {
                        matrix.set(savedMatrix);
                        matrix.postTranslate((x - start.x), (y - start.y)); // create the transformation in the matrix  of points
                    } else if (mode == ZOOM) {
                        float newDist = spacing(event);
                        if (newDist > 5f) {
                            matrix.set(savedMatrix);
                            scale = newDist / oldDist;
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                    }
                    break;
            }
            if (view != null) {
                view.setImageMatrix(matrix);
                view.invalidate();
            }
            return true;
        }

        /**
         * Расчитывает растояние между двумя пальцами
         */
        private float spacing(MotionEvent event) {
            float x = event.getX(0) - event.getX(1);
            float y = event.getY(0) - event.getY(1);
            return (float) Math.sqrt(x * x + y * y);
        }

        /**
         * Расчитывает точку посередине между двумя пальцами
         */
        private void midPoint(PointF point, MotionEvent event) {
            float x = event.getX(0) + event.getX(1);
            float y = event.getY(0) + event.getY(1);
            point.set(x / 2, y / 2);
        }
    };

    // Удаление обложки проекта
    private void deleteImageCover() {
        isDeleteCover = true;
        bitmapImage = null;
        bigHead.setVisibility(View.VISIBLE);
        imageProject.setImageResource(R.drawable.bg_default);
        Picasso.with(rootView.getContext())
                .load(R.drawable.bg_default)
                .transform(new RoundedTransformation(9, 0))
                .into(imageProject);
    }

    // Добавление отступов для коректной отрисовки на сервере
    private Bitmap addSpacingBorder(Bitmap bmp) {
        int offsetYserverImage = 38;
        int offsetXServerImage = 119;
        Bitmap bmpWithBorder = createBitmap(bmp.getWidth() + offsetXServerImage, bmp.getHeight() + offsetYserverImage, bmp.getConfig());
        Canvas canvas = new Canvas(bmpWithBorder);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawBitmap(bmp, offsetXServerImage, offsetYserverImage, paint);
        return bmpWithBorder;
    }

    // Возвращает выбранную картинку в формате base64
    private String getCoverBase64() {
        if (bitmapImage != null && !isDeleteCover) {
            StringBuilder builder = new StringBuilder("data:image/jpeg;base64,");
            // Получить текущий scale
            // Получить размер картинки
            // Вырезать из картинки нужный размер
            // Вырезаный кусок преобразовать к нужному размеру
            // Добавить отступы
            int appeandWdth = 0;
            int appeandHeight = 0;
            int indexMnozh = 2;
            switch (getResources().getDisplayMetrics().densityDpi) {
                case DisplayMetrics.DENSITY_LOW:
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    break;
                case DisplayMetrics.DENSITY_420: // FACKING MAGIC NUMBER (
                    appeandWdth = -37;
                    appeandHeight = -5;
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    appeandWdth = -37;
                    appeandHeight = -5;
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH: // 640dpi
                    indexMnozh = 4;
                    break;
            }

            imageProject.setDrawingCacheEnabled(true);
            // Получаем изображение которое сделал юзер (уменьшил-переместил)
            Bitmap zoomedBitmap = Bitmap.createScaledBitmap(imageProject.getDrawingCache(true), imageProject.getWidth() + appeandWdth, imageProject.getHeight() + appeandHeight, true);
            imageProject.setDrawingCacheEnabled(false);

            // Вырезаем нужный фрагмент из картинки
            Bitmap source = null;
            try {
                source = Bitmap.createBitmap(zoomedBitmap, imageCover.getLeft(), imageCover.getTop(), 157 * indexMnozh, 224 * indexMnozh);
            } catch (IllegalArgumentException e) { // Если картинка не соответсвует размерам
            }

            // Преобразовываем к нужному размеру для отправки на сервер
            Bitmap scale;
            int heightImageServer = 224;
            int widthImageServer = 157;
            if (source == null) {
                scale = Bitmap.createScaledBitmap(zoomedBitmap, widthImageServer, heightImageServer, true);
            } else {
                scale = Bitmap.createScaledBitmap(source, widthImageServer, heightImageServer, true);
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            // Добавялем отступы
            addSpacingBorder(scale).compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
            builder.append(encodedImage);
            return builder.toString();
        }
        return "";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getManagerTakePhoto().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public MultipartBody.Builder getSettings(MultipartBody.Builder builder) throws ErrorCreateProjectSettings {
        if (builder == null) {
            return builder;
        }
        String name;
        project = Project.findByServerId(Project.class, ManagerApplication.getInstance().getSharedManager().getValueLong(SharedKeys.ID_PROJECT_SETTINGS));
        if (getEditName() != null) {
            name = getEditName().getText().toString();
            if (name != null) {
                String temp = name.trim();
                if (TextUtils.isEmpty(temp)) {
                    throw new ErrorCreateProjectSettings(ErrorCreateProjectSettings.EMPTY_NAME);
                } else {
                    name = name.replace("\\r", "").replace("\\n", "");
                }
            }

            getEditName().setText(name);
        } else {
            name = project.getName();
        }

        boolean isChecked;

        if (checkBoxPublic != null) {
            isChecked = checkBoxPublic.isChecked();
        } else {
            if (project.getSubjectId() > 0) {
                isChecked = true;
            } else {
                isChecked = false;
            }
        }

        String subjectId = "";
        String type = "";
        if (isChecked) {
            type = "public";
            try{
                if (adapterSpinner != null) {
                    subjectId = String.valueOf(adapterSpinner.getValueByPosition(adapterSpinner.getSelectedPosition()));
                } else {
                    subjectId = String.valueOf(project.getSubjectId());
                }
            } catch (Exception e) {
                e.printStackTrace();
                type ="";
                subjectId = "";
            }
        }

        builder.addFormDataPart("Project[type]", type);
        builder.addFormDataPart("Project[subject_id]", subjectId);
        builder.addFormDataPart("Project[delimg]", isDeleteCover ? "1" : "0");

        if (!TextUtils.isEmpty(name)) {
            builder.addFormDataPart("Project[name]", name);
        }

        String imageBase64 = getCoverBase64();
        if (!TextUtils.isEmpty(imageBase64)) {
            builder.addFormDataPart("Project[imgsrc]", imageBase64);
        }
        return builder;
    }

    public EditText getEditName() {
        if (editName == null && rootView != null) {
            editName = rootView.findViewById(R.id.edit_name);
        }
        return editName;
    }

    //==========================================
    // Implements methods
    //==========================================
    @Override
    public void updateInterface() {
        if (rootView != null) {
            String text = getEditName().getText().toString();
            if (!TextUtils.isEmpty(text)) {
                project.setName(text);
                project.setType(checkBoxPublic.isChecked() ? "public" : "");
                project.save();
                ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_RELOAD_IMAGE, project.getServerId());
            }

            Picasso.Builder builder = new Picasso.Builder(ManagerApplication.getInstance().getApplicationContext());
            builder.build().invalidate(project.getUrlImage());
        }
    }

    public ManagerTakePhoto getManagerTakePhoto() {
        if (managerTakePhoto == null) {
            managerTakePhoto = new ManagerTakePhoto(getActivity());
        }
        return managerTakePhoto;
    }
}