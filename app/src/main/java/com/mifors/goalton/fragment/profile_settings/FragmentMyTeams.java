package com.mifors.goalton.fragment.profile_settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.PopupMenu;
import android.widget.Spinner;

import com.mifors.goalton.R;
import com.mifors.goalton.activity.ActivityEditTeam;
import com.mifors.goalton.adapters.AdapterSettingsMyTeam;
import com.mifors.goalton.dialog.DialogAddTeamAccount;
import com.mifors.goalton.interfaces.InterfaceOnGroupMenuClickListener;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.team.TeamAccount;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.ui.spinner.adapters.AdapterSpinner;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.ArrayList;
import java.util.List;

public class FragmentMyTeams extends Fragment implements InterfaceOnGroupMenuClickListener {
    private static final String TAG = "Goalton [" + FragmentMyTeams.class.getSimpleName() + "]";
    private static final int DIALOG_ADD_REQUEST_CODE = 3525;
    private View rootView;
    private ExpandableListView listView;
    private List<TeamAccount> list;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_team, container, false);
        initView();
        updateListData();
        return rootView;
    }

    private void initView() {
        listView = rootView.findViewById(R.id.teams_expandable_list);
        listView.setOnChildClickListener(onChildClickListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.fragment_team, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.team_settings_menu_add:
                DialogAddTeamAccount dialog = new DialogAddTeamAccount();
                dialog.setTargetFragment(this, DIALOG_ADD_REQUEST_CODE);
                dialog.show(getFragmentManager(), "TAG");
                break;
            case R.id.team_settings_refresh:
                ManagerProgressDialog.showProgressBar(getContext(), R.string.sync_data, false);
                ManagerMyTeams.responseGetAccounts(callbackMyTeams);
                break;
        }
        return true;
    }

    //обновление данных
    private void updateListData() {
        list = TeamAccount.getAll(TeamAccount.class);
        AdapterSettingsMyTeam adapter = new AdapterSettingsMyTeam(getActivity(), list, this);
        listView.setAdapter(adapter);
    }

    //// callback от обновления данных с сервера
    private Handler callbackMyTeams = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ManagerProgressDialog.dismiss();
            if (msg.what == HttpParam.RESPONSE_OK) {
                updateListData();
            } else {
                ManagerApplication.getInstance().showToast(R.string.message_error_connect_to_server);
            }
        }
    };

    //// ответ от диалогового окна добавления аккаунтов\команд
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DIALOG_ADD_REQUEST_CODE) {
            if (resultCode == DialogAddTeamAccount.RESULT_CODE_UPDATE) {
                ManagerMyTeams.responseGetAccounts(callbackMyTeams);
            }
        }
    }

    private ExpandableListView.OnChildClickListener onChildClickListener = new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            Intent intent = new Intent(getActivity(), ActivityEditTeam.class);
            intent.putExtra(ActivityEditTeam.TEAM_ID, list.get(groupPosition).getTeams().get(childPosition).getJsonServerId());
            startActivity(intent);
            return true;
        }
    };

    //////////////////////////////////////////////////////////////////
    //// Показ контекстного меню

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.context_menu_team, popup.getMenu());
        popup.setOnMenuItemClickListener(new ItemMenuClickListener(position));
        popup.show();
    }

    @Override
    public void onClickMenuItem(View view, int position) {
        showPopupMenu(view, position);
    }

    private class ItemMenuClickListener implements PopupMenu.OnMenuItemClickListener {
        private int position;

        ItemMenuClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.item_menu_invite:
                    if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                        responseGetInviteProjects();
                    }
                    break;
            }
            return false;
        }
    }

    ////////////////////////////////////////////////////////////////////


    //==========================================
    // Invite dialog
    //==========================================
    private AdapterSpinner adapterSpinnerProjects;
    private EditText editTextEmailInvite;
    private AlertDialog alertDialogInvite;

    private void responseGetInviteProjects() {
        ManagerMyTeams.responseGetInviteList(new Handler() {
            @SuppressWarnings("unchecked")
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    if (msg.obj != null) {
                        showDialogInvite((ArrayList<InterfaceSpinnerItem>) msg.obj);
                    }
                }
            }
        });
    }

    private void showDialogInvite(ArrayList<InterfaceSpinnerItem> list) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.dialog_invite_user, null);
        editTextEmailInvite = dialoglayout.findViewById(R.id.edit_text_invite);

        Spinner spinner = dialoglayout.findViewById(R.id.spinner_projects);
        adapterSpinnerProjects = new AdapterSpinner(getActivity(), R.layout.item_spinner, list, spinner);
        adapterSpinnerProjects.setHideArrowRight(true);
        spinner.setAdapter(adapterSpinnerProjects);

        dialoglayout.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogInvite.dismiss();
            }
        });

        dialoglayout.findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextEmailInvite.getText().toString();
                if (TextUtils.isEmpty(email)) {
                    ManagerApplication.getInstance().showToast(R.string.empty_email);
                } else if (!ManagerApplication.isValidEmail(email)) {
                    ManagerApplication.getInstance().showToast(R.string.error_valid_email);
                } else {
                    responseSEndInvite(email);
                }

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.invite);
        builder.setView(dialoglayout);
        alertDialogInvite = builder.show();
    }

    private void responseSEndInvite(final String email) {
        ManagerMyTeams.responseSendInvite(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    ManagerApplication.getInstance().showToast(R.string.invite_send_complete);
                    if (alertDialogInvite != null) {
                        alertDialogInvite.dismiss();
                    }
                } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                    ManagerApplication.getInstance().showToast(R.string.invite_send_error);
                }
            }
        }, Long.parseLong(adapterSpinnerProjects.getSelectedValue()), email);
    }
}
