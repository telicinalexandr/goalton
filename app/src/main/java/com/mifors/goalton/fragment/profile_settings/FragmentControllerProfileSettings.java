package com.mifors.goalton.fragment.profile_settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mifors.goalton.R;
import com.mifors.goalton.adapters.AdapterViewPagerProjectSettings;
import com.mifors.goalton.interfaces.InterfaceCallbackActivityMain;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerInvite;

public class FragmentControllerProfileSettings extends Fragment {
    private View rootView;
    private ViewPager viewPager;
    private AdapterViewPagerProjectSettings adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        updateSelectedRoot();
        TabLayout tabLayout = rootView.findViewById(R.id.settings_tab_layout);
        viewPager = rootView.findViewById(R.id.settings_viewpager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().equals(getString(R.string.myteam_tab_title))) {
                    ManagerApplication.hideKeyboard(getActivity());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        ((InterfaceCallbackActivityMain) getActivity()).setActionBarTitle(R.string.settings_title, true);
        return rootView;
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new AdapterViewPagerProjectSettings(getChildFragmentManager());
        adapter.addFragment(new FragmentSettingsProfile(), getString(R.string.profile_tab_title));
        adapter.addFragment(new FragmentMyTeams(), getString(R.string.myteam_tab_title));
        adapter.addFragment(new FragmentMemberships(), getString(R.string.memberships_teams));

        viewPager.setAdapter(adapter);
    }

    public void updateSelectedSound() {
        if (viewPager != null) {
            try {
                FragmentSettingsProfile fragmentSettingsProfile = (FragmentSettingsProfile) adapter.getItem(0);
                if(fragmentSettingsProfile != null){
                    fragmentSettingsProfile.updateTitleSoundSelected();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void updateSelectedRoot() {
        rootView.setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(getActivity()));
    }

    @Override
    public void onStart() {
        super.onStart();
        ManagerInvite.getInstance().responseGetListMyInvites(null);
    }
}