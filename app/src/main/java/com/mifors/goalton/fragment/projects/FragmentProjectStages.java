package com.mifors.goalton.fragment.projects;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.PopupMenu;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.adapters.AdapterProjectStages;
import com.mifors.goalton.interfaces.InterfaceFragmentProjectSettings;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.model.project.ProjectStage;
import com.mifors.goalton.ui.ItemTouchHelper.OnStartDragListener;
import com.mifors.goalton.ui.ItemTouchHelper.SimpleItemTouchHelperCallback;
import com.mifors.goalton.utils.ComparatorProjectsStages;

import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import okhttp3.MultipartBody;


public class FragmentProjectStages extends Fragment implements OnStartDragListener, AdapterView.OnItemClickListener, Observer, InterfaceFragmentProjectSettings {
    private static final String TAG = "Goalton [" + FragmentProjectStages.class.getSimpleName() + "]";
    private View rootView;
    private RecyclerView stagesRecyclerView;
    private AdapterProjectStages adapterStages;
    private ItemTouchHelper mItemTouchHelper;
    private List<ProjectStage> projectStages;
    private EditText tempEditDialog; // Поле в диалоговом окне переименовать этап
    private long tempProjectStageSelected; // Хранит значение выбранного этапа
    private long projectId = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        projectId = ManagerApplication.getInstance().getSharedManager().getValueLong(SharedKeys.ID_PROJECT_SETTINGS);
        projectStages = ProjectStage.findByProjectIdAndNotDelete(projectId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_projects_stages, container, false);
        rootView.findViewById(R.id.fab_add_new_stage).setOnClickListener(onClickAddNewStage);
        initViews();
        updateStages();
        return rootView;
    }

    private void initViews() {
        stagesRecyclerView = rootView.findViewById(R.id.project_edit_stages);
        stagesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterStages = new AdapterProjectStages(projectStages, this);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapterStages);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(stagesRecyclerView);
    }

    private void updateStages() {
        if (adapterStages != null) {
            adapterStages.setList(projectStages);
            Collections.sort(projectStages, new ComparatorProjectsStages());
            stagesRecyclerView.setAdapter(adapterStages);
        }
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void update(Observable o, Object arg) {
        updateAdapterData();
    }

    private void updateAdapterData() {
        projectStages = ProjectStage.findByProjectIdAndNotDelete(projectId);
        updateStages();
    }

    //==========================================
    // On click
    //==========================================
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        tempProjectStageSelected = Long.parseLong(view.getTag().toString());
        showPopupMenu(view, position);
    }

    private View.OnClickListener onClickAddNewStage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            tempEditDialog = ManagerAlertDialog.showEditTextDialog(getActivity(), "", R.string.name_new_stage, R.string.ok, R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (tempEditDialog != null) {
                        String titleNewStage = tempEditDialog.getText().toString();
                        if (TextUtils.isEmpty(titleNewStage)) {
                            ManagerApplication.getInstance().showToast(R.string.error_empty_stage_name);
                            onClickAddNewStage.onClick(null);
                        } else {
                            createNewStage(titleNewStage);
                        }
                    }
                    hideKeyboard();
                    ManagerAlertDialog.dismiss();
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    hideKeyboard();
                }
            }, null);
        }
    };

    // Показ контекстного меню
    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.context_menu_edit_project, popup.getMenu());
        popup.setOnMenuItemClickListener(new ItemMenuClickListener(position));
        popup.show();
    }

    private class ItemMenuClickListener implements PopupMenu.OnMenuItemClickListener {
        private int position;

        ItemMenuClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.project_rename:
                    showDialogRenameStage(position);
                    break;
                case R.id.project_delete:
                    showDialogIsDelete(onClickdeleteStage);
                    break;
                case R.id.project_wip_days:
                    showDialogChangeWip(position);
                    break;
                case R.id.project_max_days:
                    showDialogChangeMax(position);
                    break;

            }
            return true;
        }

        private DialogInterface.OnClickListener onClickdeleteStage = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ProjectStage projectStage = projectStages.get(position);
                if (projectStage.getIsNewLocal()) { // Если этап был создан локально и не отправлялся на сервер
                    projectStage.delete();
                } else {
                    projectStage.setDelete(true);
                    projectStage.save();
                }

                projectStages.remove(position);
                adapterStages.notifyDataSetChanged();
            }
        };
    }

    /**
     * Получение данных из всплывающего окна и сохранение по типу данных в этап проекта
     * Сохраняет в базу
     * Делает обновление адаптера
     * Стирает ссылку на EditText полученный при показа окна
     *
     * @param type - что в данный момент ожидается от пользователя
     */
    private void updateDataProjectStage(ProjectStage.StageField type) {
        String value = tempEditDialog.getText().toString();
        ProjectStage projectStage = ProjectStage.findByServerId(ProjectStage.class, tempProjectStageSelected);
        if (projectStage != null) {
            if (type.getNumber() == ProjectStage.StageField.MAX_DAYS.getNumber()) {
                projectStage.setPeriod(value);
            } else if (type.getNumber() == ProjectStage.StageField.WIP_DAYS.getNumber()) {
                projectStage.setLimitItem(value);
            } else if (type.getNumber() == ProjectStage.StageField.RENAME.getNumber()) {
                projectStage.setName(value);
            }
            projectStage.save();
        }
        adapterStages.notifyDataSetChanged();
    }

    //==========================================
    // Dalogs
    //==========================================
    // Переименование этапа
    private void showDialogRenameStage(int position) {
        tempEditDialog = ManagerAlertDialog.showEditTextDialogText(
                getActivity(),
                projectStages.get(position).getName(),
                R.string.stage_new_name_stage_project,
                R.string.ok,
                R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateDataProjectStage(ProjectStage.StageField.RENAME);
                        hideKeyboard();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hideKeyboard();
                    }
                }
        );
    }

    // Удаление этапа
    private void showDialogIsDelete(DialogInterface.OnClickListener onClickdeleteStage) {
        ManagerAlertDialog.showDialog(getActivity(), R.string.message_delete_project_stage,
                R.string.yes, R.string.no,
                onClickdeleteStage, null);

    }

    // Смена WIP
    private void showDialogChangeWip(int position) {
        tempEditDialog = ManagerAlertDialog.showEditTextDialogNumber(
                getActivity(),
                projectStages.get(position).getLimitItem(),
                R.string.WIP_DAYS,
                R.string.ok,
                R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateDataProjectStage(ProjectStage.StageField.WIP_DAYS);
                        hideKeyboard();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hideKeyboard();
                    }
                }
        );
    }

    // Смена MAX
    private void showDialogChangeMax(int position) {
        tempEditDialog = ManagerAlertDialog.showEditTextDialogNumber(getActivity(),
                projectStages.get(position).getPeriod(),
                R.string.MAX_DAYS,
                R.string.ok,
                R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateDataProjectStage(ProjectStage.StageField.MAX_DAYS);
                        hideKeyboard();
                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hideKeyboard();
                    }
                }
        );
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        Activity activity = getActivity();
        if (activity == null)
            return;
        if (activity.getCurrentFocus() == null)
            return;
        if (activity.getCurrentFocus().getWindowToken() == null)
            return;
        if (tempEditDialog != null) {
            imm.hideSoftInputFromWindow(tempEditDialog.getWindowToken(), 0);
        }

        tempEditDialog = null;
    }

    //==========================================
    // Stage
    //==========================================
    private void createNewStage(String name) {
        // Поле чек новый не новый этап
        // Индекс сортировки - добавляется в конец (количество этапов у проекта)
        // При удачном запросе удаляются из базы только что созданные элементы. И Делается запрос на получение только что отправленных.

        long serverId = 1234567L + ((long) ((new Random()).nextDouble() * (23456789L - 1234567L)));
        ProjectStage projectStage = new ProjectStage();
        projectStage.setSort(String.valueOf(ProjectStage.countStagesProject(projectId)));
        projectStage.setProjectId(projectId);
        projectStage.setJsonServerid(Long.parseLong(projectStage.getSort()));
        projectStage.setName(name);
        projectStage.setServerId(serverId);
        projectStage.setNew(true);
        projectStage.save();
        updateAdapterData();
    }

    @Override
    public MultipartBody.Builder getSettings(MultipartBody.Builder builder) {
        int indexCounter = 1; //Счетчик для этапов которые в новом проекте
        projectId = ManagerApplication.getInstance().getSharedManager().getValueLong(SharedKeys.ID_PROJECT_SETTINGS);
        List<ProjectStage> st = ProjectStage.findByProjectId(projectId);

        for (ProjectStage projectStage : st) {
            String prefixField;

            // Fixme: костыль переписать на один метод
            // Проект новый
            if (projectId == 0) {
                prefixField = "ProjectStageNew";
                String name = prefixField + "['id_" + indexCounter + "']";
                String nameParams = prefixField + "_" + indexCounter;

                builder.addFormDataPart(name, String.valueOf(indexCounter));
                builder.addFormDataPart(nameParams + "[name]", projectStage.getName());
                builder.addFormDataPart(nameParams + "[sort]", projectStage.getSort());

                if (!TextUtils.isEmpty(projectStage.getPeriod())) {
                    builder.addFormDataPart(nameParams + "[period]", projectStage.getPeriod());
                } else {
                    builder.addFormDataPart(nameParams + "[period]", "");
                }

                if (!TextUtils.isEmpty(projectStage.getLimitItem())) {
                    builder.addFormDataPart(nameParams + "[limit_item]", projectStage.getLimitItem());
                } else {
                    builder.addFormDataPart(nameParams + "[limit_item]", "");
                }

                // Этап новый
            } else if (projectStage.getIsNewLocal()) {
                prefixField = "ProjectStageNew";
                String name = prefixField + "['id_" + projectStage.getJsonServerid() + "']";
                String nameParams = prefixField + "_" + projectStage.getJsonServerid();

                builder.addFormDataPart(name, String.valueOf(projectStage.getJsonServerid()));
                builder.addFormDataPart(nameParams + "[id]", String.valueOf(projectStage.getJsonServerid()));
                builder.addFormDataPart(nameParams + "[name]", projectStage.getName());
                builder.addFormDataPart(nameParams + "[sort]", projectStage.getSort());

                if (!TextUtils.isEmpty(projectStage.getPeriod())) {
                    builder.addFormDataPart(nameParams + "[period]", projectStage.getPeriod());
                } else {
                    builder.addFormDataPart(nameParams + "[period]", "");
                }

                if (!TextUtils.isEmpty(projectStage.getLimitItem())) {
                    builder.addFormDataPart(nameParams + "[limit_item]", projectStage.getLimitItem());
                } else {
                    builder.addFormDataPart(nameParams + "[limit_item]", "");
                }

                builder.addFormDataPart(nameParams + "[del]", projectStage.isDelete() ? "1" : "0"); // 0 - оставить (по умолчанию) / 1 - удалить этап

                // Проект существует, этап существует
            } else {
                prefixField = "ProjectStage";
                String name = prefixField + "['id_" + projectStage.getJsonServerid() + "']";
                String nameParams = prefixField + "_" + projectStage.getJsonServerid();

                builder.addFormDataPart(name, String.valueOf(projectStage.getJsonServerid()));
                builder.addFormDataPart(nameParams + "[id]", String.valueOf(projectStage.getJsonServerid()));
                builder.addFormDataPart(nameParams + "[name]", projectStage.getName());
                builder.addFormDataPart(nameParams + "[sort]", projectStage.getSort());

                if (!TextUtils.isEmpty(projectStage.getPeriod())) {
                    builder.addFormDataPart(nameParams + "[period]", projectStage.getPeriod());
                } else {
                    builder.addFormDataPart(nameParams + "[period]", "");
                }

                if (!TextUtils.isEmpty(projectStage.getLimitItem())) {
                    builder.addFormDataPart(nameParams + "[limit_item]", projectStage.getLimitItem());
                } else {
                    builder.addFormDataPart(nameParams + "[limit_item]", "");
                }

                builder.addFormDataPart(nameParams + "[del]", projectStage.isDelete() ? "1" : "0"); // 0 - оставить (по умолчанию) / 1 - удалить этап
            }

            indexCounter++;
        }

        return builder;
    }

    @Override
    public void updateInterface() {
        updateAdapterData();
    }
}