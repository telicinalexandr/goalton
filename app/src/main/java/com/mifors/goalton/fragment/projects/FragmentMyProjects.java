package com.mifors.goalton.fragment.projects;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.crashlytics.android.Crashlytics;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.activity.ActivityMain;
import com.mifors.goalton.activity.ActivitySettingProject;
import com.mifors.goalton.adapters.AdapterMyProjects;
import com.mifors.goalton.interfaces.InterfaceCallbackActivityMain;
import com.mifors.goalton.interfaces.InterfaceOnClickProjectPanelBtns;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.utils.ReciverCoonnectNetwork;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


public class FragmentMyProjects extends Fragment implements AdapterView.OnItemClickListener, InterfaceOnClickProjectPanelBtns, Observer {

    public enum TypeSort {
        NAME("name"),
        DATE("date"),
        OWNER("owner"),
        PUBLIC("public");

        private String type;

        TypeSort(String s) {
            type = s;
        }

        public String getType() {
            return type;
        }
    }

    private static final String TAG = "Goalton [" + FragmentMyProjects.class.getSimpleName() + "]";
    private View rootView;
    private RecyclerView recyclerView;
    private List<Project> list;
    private InterfaceCallbackActivityMain callbackActivity;
    private long tempProjectDeleteId; // Хранит id проекта который надо удалить
    private SwipeRefreshLayout swipeRefreshLayout;
    private AdapterMyProjects adapterMyProjects;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        callbackActivity = (InterfaceCallbackActivityMain) getActivity();
        rootView = inflater.inflate(R.layout.fragment_my_projects, container, false);
        callbackActivity.setActionBarTitle(R.string.my_projects, false);
        callbackActivity.showSpinnerSort();
        swipeRefreshLayout = rootView.findViewById(R.id.refresh_layout_recycler);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                    ManagerProjects.responseGetProjects(new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            swipeRefreshLayout.setRefreshing(false);
                            initRecycleViewProjectsNotUpdateSort();
                        }
                    });
                } else {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        initView();

        if (!ActivityMain.IS_LOAD_PROJECTS_RUN_SESSION) {
            ActivityMain.IS_LOAD_PROJECTS_RUN_SESSION = true;
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                callbackActivity.showProgressBar();
                responseGetProjects();
            } else {
                initRecycleViewProjectsNotUpdateSort();
            }
        } else {
            initRecycleViewProjectsNotUpdateSort();
        }

        return rootView;
    }

    public void responseGetProjects() {
        ManagerProjects.responseGetProjects(callbackGetProjects);
    }

    @Override
    public void onResume() {
        super.onResume();
        ReciverCoonnectNetwork.NetworkObservable.getInstance().addObserver(this); // Ставим наблюдатель на включение отключение интернета
        rootView.setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(getActivity()));
        if (ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.IS_UPDATE_PROJECTS_LIST)) {
            long idProject = ManagerApplication.getInstance().getSharedManager().getValueLong(SharedKeys.ID_PROJECT_RELOAD_TO_LIST);
            updateProjectList(idProject);
            ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.IS_UPDATE_PROJECTS_LIST, false);
            ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_RELOAD_TO_LIST, 0);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        ReciverCoonnectNetwork.NetworkObservable.getInstance().deleteObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapterMyProjects != null) {
            adapterMyProjects.revertCover();
        }
    }

    private void initView() {
        rootView.findViewById(R.id.fab_add_new_project).setOnClickListener(onClickNewProject);
        recyclerView = rootView.findViewById(R.id.fragment_project_recycler);
        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        int columnCount = Math.round((float) size.x / ((int) getResources().getDimension(R.dimen.width_cover)));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), columnCount));
        recyclerView.setItemViewCacheSize(900);
    }

    public void updateProjectList(long projectServerId) {
        if (adapterMyProjects != null) {
            adapterMyProjects.reloadDataProjectFromDb(projectServerId);
            adapterMyProjects.notifyDataSetChanged();
        }
    }

    public void initRecycleViewProjects() {
        list = Project.getAllAndNotNew();
        String typeSort = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.TYPE_PROJECT_SORT);
        if (TextUtils.isEmpty(typeSort)) {
            updateListBySort(TypeSort.NAME.getType(), false);
        } else {
            updateListBySort(typeSort, false);
        }
    }

    public void initRecycleViewProjectsNotUpdateSort() {
        list = Project.getAllAndNotNew();
        updateListSort();
    }

    private void initRecycleViewProjects(List<Project> listProjects) {
        if (listProjects != null) {
            adapterMyProjects = new AdapterMyProjects(listProjects, this, this);
            recyclerView.setAdapter(adapterMyProjects);
        }
    }

    private void startResponseGetSettings(final long serverId) {
        ManagerProgressDialog.showProgressBar(getActivity(), R.string.update_data_perfomed, true);
        ManagerProjects.responseGetSettingsProject(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                openActivityEditProject(serverId);
            }
        }, serverId);
    }

    private DialogInterface.OnClickListener onClickDeleteYes = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            responseDeleteProject();
        }
    };

    private Handler callbackGetProjects = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            callbackActivity.hideProgressBar();
            if (msg.what == HttpParam.RESPONSE_OK) {
                initRecycleViewProjects();
                ActivityMain.IS_LOAD_PROJECTS_RUN_SESSION = true;
            } else {
//                Crashlytics.logException(new Exception("Not load projects, start load projects from db"));
                initRecycleViewProjectsNotUpdateSort();
            }
        }
    };

    @Override
    public void update(Observable o, Object arg) {
        if (arg != null && true == (boolean)arg) {
            if (adapterMyProjects != null) {
                adapterMyProjects.notifyDataSetChanged();
            }
        }
    }

    //==========================================
    // On click
    //==========================================
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.item_project_edit:
                startResponseGetSettings(list.get(position).getServerId());
                break;
            case R.id.item_project_delete:
                try {
                    tempProjectDeleteId = Long.parseLong(view.getTag().toString());
                    showDialogIsDeleteProject();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private View.OnClickListener onClickNewProject = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                Project pr = Project.findByServerId(Project.class, 0);
                if (pr == null) {
                    // Проекта нету делаем запрос на получение настроек для нового проекта
                    ManagerProgressDialog.showProgressBar(getContext(), R.string.create_new_projects, true);
                    ManagerProjects.responseGetSettingsProject(new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            ManagerProgressDialog.dismiss();
                            if (msg.what == HttpParam.RESPONSE_OK) {
                                openActivityEditProject(0);
                            }
                        }
                    }, 0);
                } else {
                    // Настройки нового проекта были запрошены раньше
                    openActivityEditProject(0);
                }
            }
        }
    };

    @Override
    public void onClickOpenOutline(final long projectId) {
        if (callbackActivity != null) {
            if (adapterMyProjects != null) {
                adapterMyProjects.revertCover();
            }
            callbackActivity.replaceFragmentOutline(projectId, 0);
        }
    }

    //==========================================
    // Override methods
    //==========================================
    private void responseDeleteProject() {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ManagerProgressDialog.showProgressBar(getContext(), R.string.deleting, false);
            ManagerProjects.responseDeleteProject(new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    ManagerProgressDialog.dismiss();
                    if (msg.what == HttpParam.RESPONSE_OK) {
                        tempProjectDeleteId = 0;
                        initRecycleViewProjectsNotUpdateSort();
                        showDialogDeleteProjectComplete();
                    } else {
                        ManagerApplication.getInstance().showToast(R.string.project_delete_complete);
                    }
                }
            }, tempProjectDeleteId);
        }
    }

    //==========================================
    // Show dialog methods
    //==========================================
    private void showDialogIsDeleteProject() {
        ManagerAlertDialog.showDialog(getActivity(), R.string.message_delete_project, R.string.yes, R.string.no, onClickDeleteYes, null);
    }

    private void showDialogDeleteProjectComplete() {
        ManagerAlertDialog.showDialog(getContext(), R.string.delete_project_complete);
    }

    //==========================================
    // Open activity
    //==========================================
    private void openActivityEditProject(long serverId) {
        if (adapterMyProjects != null) {
            adapterMyProjects.revertCover();
        }
        try{
            Intent intent = new Intent(getActivity(), ActivitySettingProject.class);
            intent.putExtra(ActivitySettingProject.PROJECT_ID_KEY, serverId);
            startActivityForResult(intent, ActivityMain.REQUEST_CODE_RELOAD_PROJECTS);
        } catch (NullPointerException e) {
            try{
                Intent intent = new Intent(getContext(), ActivitySettingProject.class);
                intent.putExtra(ActivitySettingProject.PROJECT_ID_KEY, serverId);
                startActivityForResult(intent, ActivityMain.REQUEST_CODE_RELOAD_PROJECTS);
            } catch (NullPointerException e1) {
                e1.printStackTrace();
            }
        }
    }

    //==========================================
    // Sort list
    //==========================================
    public void updateListBySort(String type, boolean isReverseVariable) {
        String sharedType = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.TYPE_PROJECT_SORT);
        boolean isReverse = ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE);
        if (isReverseVariable) {
            isReverse = type.equals(sharedType) && !isReverse;
        }

        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.TYPE_PROJECT_SORT, type);
        ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE, isReverse);

        updateListSort();
    }

    private void updateListSort() {
        if (list != null && list.size() > 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    String typeSort = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.TYPE_PROJECT_SORT);
                    boolean isReverse = ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE);

                    if (TypeSort.NAME.getType().equals(typeSort)) {
                        Collections.sort(list, new ComparatorLineByName());
                    } else if (TypeSort.DATE.getType().equals(typeSort)) {
                        Collections.sort(list, new ComparatorLineDate());
                    } else if (TypeSort.OWNER.getType().equals(typeSort)) {
                        Collections.sort(list, new ComparatorLineOwner());
                    } else if (TypeSort.PUBLIC.getType().equals(typeSort)) {
                        Collections.sort(list, new ComparatorLinePublic());
                    }

                    if (isReverse) {
                        Collections.reverse(list);
                    }
                    initRecycleViewProjects(list);
                }
            }, 10);
        } else {
            initRecycleViewProjects(list);
        }
    }

    // Сортировка проектов по имени
    private class ComparatorLineByName implements Comparator<Project> {
        @Override
        public int compare(Project project1, Project project2) {
            try{
                return project1.getName().toUpperCase().compareTo(project2.getName().toUpperCase());
            } catch (Exception e) {
                e.printStackTrace();
                return project1.getName().compareTo(project2.getName());
            }
        }
    }

    // Сортировка проектов по владельцу
    private class ComparatorLineOwner implements Comparator<Project> {
        @Override
        public int compare(Project project1, Project project2) {
            Long x1 = project1.getOwnerUserId();
            Long x2 = project2.getOwnerUserId();
            int sComp = x2.compareTo(x1);

            if (sComp != 0) {
                return sComp;
            } else {
                return project2.getName().compareTo(project1.getName());
            }

        }
    }

    // Сортировка проектов по публичности
    private class ComparatorLinePublic implements Comparator<Project> {
        @Override
        public int compare(Project project1, Project project2) {
            int sComp = project2.getType().compareTo(project1.getType());
            if (sComp != 0) {
                return sComp;
            } else {
                return project2.getName().compareTo(project1.getName());
            }
        }
    }

    // Сортировка проектов по публичности
    private class ComparatorLineDate implements Comparator<Project> {
        @Override
        public int compare(Project project1, Project project2) {
            if (project1.getServerId() < project2.getServerId()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

}