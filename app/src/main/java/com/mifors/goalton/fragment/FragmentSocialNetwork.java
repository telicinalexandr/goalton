package com.mifors.goalton.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.gorbin.asne.core.AccessToken;
import com.github.gorbin.asne.core.SocialNetwork;
import com.github.gorbin.asne.core.SocialNetworkManager;
import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
import com.github.gorbin.asne.core.listener.OnRequestSocialPersonCompleteListener;
import com.github.gorbin.asne.core.persons.SocialPerson;
import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.mifors.goalton.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mifors.goalton.activity.ActivityIntro.SOCIAL_NETWORK_TAG;

//import com.github.gorbin.asne.vk.VKPerson;
//import com.github.gorbin.asne.vk.VkSocialNetwork;

//import com.github.gorbin.asne.core.SocialNetwork;
//import com.github.gorbin.asne.core.SocialNetworkManager;
//import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
//import com.github.gorbin.asne.facebook.FacebookSocialNetwork;
////import com.github.gorbin.asne.googleplus.GooglePlusSocialNetwork;
//import com.github.gorbin.asne.vk.VkSocialNetwork;
//import com.vk.sdk.VKScope;

/**
 * Created by kirpich on 05.12.2017.
 */

public class FragmentSocialNetwork extends Fragment
        implements SocialNetworkManager.OnInitializationCompleteListener, OnLoginCompleteListener, GoogleApiClient.OnConnectionFailedListener, OnRequestSocialPersonCompleteListener {

    private static final String TAG = "Goalton [" + FragmentSocialNetwork.class.getSimpleName() + "]";

    public static SocialNetworkManager mSocialNetworkManager;


    private GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;
    private AccessToken accestoken;

    private static final int RC_SIGN_IN = 9001;
    private View vk, facebook, google;

    private ICompleteLogin ICompleteLogin;

    public interface ICompleteLogin {
        void completeLogin(String typeSocial, String token, String userId, String email, String name, String firstName, String lastName);

        void loginVk();
    }

    public FragmentSocialNetwork() {
    }

    public FragmentSocialNetwork.ICompleteLogin getICompleteLogin() {
        return ICompleteLogin;
    }

    public void setICompleteLogin(FragmentSocialNetwork.ICompleteLogin ICompleteLogin) {
        this.ICompleteLogin = ICompleteLogin;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_btn_social, container, false);
        vk = rootView.findViewById(R.id.btn_vk);
        facebook = rootView.findViewById(R.id.btn_facebook);
        google = rootView.findViewById(R.id.btn_google);

        google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getContext());
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int networkId = FacebookSocialNetwork.ID;
                SocialNetwork socialNetwork = mSocialNetworkManager.getSocialNetwork(FacebookSocialNetwork.ID);
                if (socialNetwork != null) {
                    if (!socialNetwork.isConnected()) {
                        if (networkId != 0) {
                            socialNetwork.requestLogin(new OnLoginCompleteListener() {
                                @Override
                                public void onLoginSuccess(int socialNetworkID) {
                                    Log.v(TAG, "[onLoginSuccess] facebook");
                                }

                                @Override
                                public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
                                    Log.v(TAG, "[onError] requestID = "+requestID+"  errorMessage = "+errorMessage+"   data = "+data);
                                }
                            });
                        } else {
                            // TODO обработка ошибок
                        }
                    } else {
                        FacebookSocialNetwork fbsc = (FacebookSocialNetwork) mSocialNetworkManager.getSocialNetwork(socialNetwork.getID());
                        if (fbsc != null) {
                            fbsc.requestCurrentPerson();
                        }
                    }
                }
            }
        });

        vk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ICompleteLogin != null) {
                    ICompleteLogin.loginVk();
                }
            }
        });

        ArrayList<String> fbScope = new ArrayList<>();
        fbScope.addAll(Arrays.asList("email"));

        mSocialNetworkManager = (SocialNetworkManager) getFragmentManager().findFragmentByTag(SOCIAL_NETWORK_TAG);
        if (mSocialNetworkManager == null) {
            mSocialNetworkManager = new SocialNetworkManager();

            FacebookSocialNetwork fbNetwork = new FacebookSocialNetwork(this, fbScope);
            mSocialNetworkManager.addSocialNetwork(fbNetwork);

            getFragmentManager().beginTransaction().add(mSocialNetworkManager, SOCIAL_NETWORK_TAG).commit();
            mSocialNetworkManager.setOnInitializationCompleteListener(this);
        } else {
            if (!mSocialNetworkManager.getInitializedSocialNetworks().isEmpty()) {
                List<SocialNetwork> socialNetworks = mSocialNetworkManager.getInitializedSocialNetworks();
                for (SocialNetwork socialNetwork : socialNetworks) {
                    socialNetwork.setOnLoginCompleteListener(this);
                    initSocialNetwork(socialNetwork);

                }
            }
        }

        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(getString(R.string.server_client_id_google))
                .requestIdToken(getString(R.string.server_client_id_google))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.v(TAG, "[handleSignInResult] account = " + account);
            String userId = account.getId();
            String displayedUsername = account.getDisplayName();
            String userEmail = account.getEmail();

            Log.d(TAG, "account:" + account.toString() +
                    "  AuthCode = " + account.getServerAuthCode() +
                    "  userId = " + userId +
                    "  idToken = " + account.getIdToken() +
                    "   userName = " + displayedUsername +
                    "   userMail = " + userEmail);
            ICompleteLogin.completeLogin("google", account.getServerAuthCode(), account.getId(), account.getEmail(), account.getDisplayName(), account.getFamilyName(), account.getGivenName());
        } catch (ApiException e) {
            Log.v(TAG, "signInResult:failed code=" + e.getStatusCode());
        }
    }

    // Вызов после инициализации менеджера
    @Override
    public void onSocialNetworkManagerInitialized() {
        Log.i(TAG, "onSocialNetworkManagerInitialized");
        for (SocialNetwork socialNetwork : mSocialNetworkManager.getInitializedSocialNetworks()) {
            socialNetwork.setOnLoginCompleteListener(this);
            socialNetwork.setOnRequestCurrentPersonCompleteListener(this);
            initSocialNetwork(socialNetwork);
        }
    }

    private void initSocialNetwork(SocialNetwork socialNetwork) {
        if (socialNetwork.isConnected()) {
            accestoken = socialNetwork.getAccessToken();
            socialNetwork.requestCurrentPerson();
        }
    }

    // Калбек запроса пользователя
    @Override
    public void onRequestSocialPersonSuccess(int socialNetworkId, SocialPerson socialPerson) {
        Log.v(TAG, "[onRequestSocialPersonSuccess] socialNetworkId = " + socialNetworkId + "  socialPerson = " + socialPerson);
        if (ICompleteLogin != null) {
            switch (socialNetworkId) {
                case FacebookSocialNetwork.ID:
                    try {
                        String[] nameArr = socialPerson.name.split(" ");
                        String name = "", lastName = "", firstName = "";
                        for (int i = 0; i < nameArr.length; i++) {
                            switch (i) {
                                case 0:
                                    firstName = nameArr[i];
                                    break;
                                case 1:
                                    lastName = nameArr[i];
                                    break;
                                case 2:
                                    name = nameArr[i];
                                    break;
                            }
                        }

                        ICompleteLogin.completeLogin("facebook", accestoken.token, socialPerson.id, socialPerson.email, name, firstName, lastName);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    @Override
    public void onLoginSuccess(int socialNetworkID) {
        Log.i(TAG, "onLoginSuccess: " + socialNetworkID);
    }

    @Override
    public void onError(int socialNetworkID, String requestID, String errorMessage, Object data) {
        Log.i(TAG, "onError: " + socialNetworkID + "  requestID = " + requestID + "  errorMessage " + errorMessage + "  data " + data);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }
}



