package com.mifors.goalton.network.Api;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiOutline {
    /**
     * http://goalton.mifors.com/transfer/get-project
     * Получение списка дел отдельно взятого проекта
     *
     * @param projectId - id проекта  у котрого нужно получить список дел
     */
    @GET("mobile/outline/{project_id}")
    Call<ResponseBody> getOutlineProject(@Path("project_id") String projectId);

    /**
     * Возвращает список актуальных компаний
     * */
    @GET("mobile/outline/companies")
    Call<ResponseBody> getCompanies();

    /**
     * Установить дело завершенным
     *
     * @param itemId    - id дела
     * @param completed 0 - не выолнено, 1 - выполнено
     * @return
     */
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("tree-ajax/set-completed")
    Call<ResponseBody> setItemCompleted(@Header("X-CSRF-Token") String xCsrf,
                                        @Field("Item[id]") long itemId,
                                        @Field("Item[is_completed]") String completed);

    /**
     * Установить задачу как важную
     *
     * @param itemId    - id задачи
     * @param completed 0 - не выолнено, 1 - выполнено
     * @return
     */
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("tree-ajax/set-important")
    Call<ResponseBody> setImportant(@Header("X-CSRF-Token") String xCsrf,
                                    @Field("Item[id]") long itemId,
                                    @Field("Item[is_important]") String completed);


    /**
     * @param xCsrf         - token полученый при вызове метода init
     * @param projectId     - id проекта
     * @param showCompleted - 0 - скрывать / 1 - показывать завершенные
     * @return
     */
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("project/update-project-set-show-completed")
    Call<ResponseBody> setShowCompleted(@Header("X-CSRF-Token") String xCsrf,
                                        @Field("Project[project_id]") long projectId,
                                        @Field("Project[show_completed]") int showCompleted);


    /**
     * Добавление нового итема в outline
     *
     * @param xCsrf        - token полученый при вызове метода init
     * @param prevItemId   - хз я не понял что это
     * @param projectId    - id проекта
     * @param parentItemId - id родителя
     * @param itemName     - имя нового
     * @return
     */
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/create-item")
    Call<ResponseBody> addItemOutline(@Header("X-CSRF-Token") String xCsrf,
                                      @Field("Item[id]") String prevItemId,
                                      @Field("Item[project_id]") long projectId,
                                      @Field("Item[parent_item_id]") long parentItemId,
                                      @Field("Item[name]") String itemName);

    /**
     * @param map HashMap <Items[id итема][parent_item_id], id родителя, если нет то null> и
     *            <Items[id итема][sort], порядок сортировки>
     * @return
     */
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("tree-ajax/update-sort")
    Call<ResponseBody> updateSort(@Header("X-CSRF-Token") String xCsrf,
                                  @FieldMap HashMap<String, Object> map);

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/delete")
    Call<ResponseBody> delete(@Header("X-CSRF-Token") String xCsrf,
                              @Field("Item[id]") long lineId,
                              @Field("Item[project_id]") long projectId);

    // Назначить дату завершения задачи
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/get-image")
    Call<ResponseBody> getImageLine(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Item[id]") long lineId);

//    @Headers("X-Requested-With: XMLHttpRequest")
//    @GET("note/img/1151857/350")
//    Call<ResponseBody> getImageLine(
//            @Header("X-CSRF-Token") String xCsrf);

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/set-deadline")
    Call<ResponseBody> setDeadlinDate(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Task[item_id]") long lineId,
            @Field("Task[deadline_date]") String date,
            @Field("Task[deadline_time]") String time,
            @Field("Task[duration]") int duration);

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("tree-ajax/update-name")
    Call<ResponseBody> updateName(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Item[id]") long lineId,
            @Field("Item[name]") String name);

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/project/get-atr")
    Call<ResponseBody> getAttributesProject(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("project_id") long projectId);

    // Создание задачи из календаря
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/item/win-create-item-inbox")
    Call<ResponseBody> createPlannerItem(@Header("X-CSRF-Token") String xCsrf,
                                          @FieldMap HashMap<String, Object> map);
}
