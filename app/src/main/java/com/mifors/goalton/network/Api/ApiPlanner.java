package com.mifors.goalton.network.Api;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by zettig on 30.11.2016.
 */

@SuppressWarnings("ALL")
public interface ApiPlanner {

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/update-color")
    Call<ResponseBody> updateColor(@Header("X-CSRF-Token") String xCsrf,
                                   @Field("Item[id]") long lineId,
                                   @Field("Item[color]") String colorLine);

    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/img-delete")
    Call<ResponseBody> deleteImage(@Header("X-CSRF-Token") String xCsrf,
                                   @Field("Item[id]") long lineId);

    @Headers("X-Requested-With: XMLHttpRequest")
    @Multipart
    @POST("mobile/tree-ajax/update-image")
    Call<ResponseBody> updateImage(@Header("X-CSRF-Token") String xCsrf,
                                   @Part("Item[id]") RequestBody lineId,
                                   @Part MultipartBody.Part image);

    @FormUrlEncoded
    @Headers("X-Requested-With: XMLHttpRequest")
    @POST("mobile/planning/{week}/{year}")
    Call<ResponseBody> getDataPlanning(
            @Header("X-CSRF-Token") String xCsrf,
            @Path("week") int week,
            @Path("year") int year,
            @Field("filter[show_assign_item]") String assignItem,
            @Field("filter[project][]") String projects
    );

    // Данные компании назначеные люди
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/project/get-atr")
    Call<ResponseBody> getFilterDates(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("project_id") long projectId);

    // Назначить дату завершения задачи
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/tree-ajax/update-duration")
    Call<ResponseBody> setDuration(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Item[id]") long lineId,
            @Field("Item[duration]") int duration);

    // Назначить человека на задачу
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("tree-ajax/assign-update")
    Call<ResponseBody> setAssign(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Data[id]") long lineId,
            @Field("Data[assign_user_id]") long assignUserID);

    // Назначить человека на задачу
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("tree-ajax/set-company")
    Call<ResponseBody> setCompany(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Item[id]") long lineId,
            @Field("Item[company_id]") long companyId);

    // Удалить этап проекта
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("kanban/delete")
    Call<ResponseBody> deleteStageItem(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Data[id]") long lineId,
            @Field("Data[type]") String type);

    // Обновить этап проекта
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("kanban/update")
    Call<ResponseBody> updateStageItem(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Data[id]") long lineId,
            @Field("Data[type]") String type,
            @Field("Data[stage_id]") long stageId,
            @Field("Data[last]") String last, // Устанавливает значение выполнено не выполнено, значение поля выпонено не выполнено
            @Field("Data[receive]") String receive, // По умолчанию 1, если другое значение то задача не сохранится
            @Field("Data[status]") String status);

    // Создать задачу для календаря
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("item/win-create-item-inbox")
    Call<ResponseBody> createItemPlanner(
            @Header("X-CSRF-Token") String xCsrf,
            @Field("Data[id]") long lineId,
            @Field("Data[type]") String type,
            @Field("Data[stage_id]") long stageId,
            @Field("Data[last]") String last,
            @Field("Data[receive]") String receive,
            @Field("Data[status]") String status);
}