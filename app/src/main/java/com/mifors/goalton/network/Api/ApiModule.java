package com.mifors.goalton.network.Api;


import com.jakewharton.picasso.OkHttp3Downloader;
import com.mifors.goalton.BuildConfig;
import com.mifors.goalton.DebugKeys;
import com.mifors.goalton.network.CookiesRetrofit;
import com.mifors.goalton.network.HttpParam;

import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Created by zettig on 30.11.2016.
 */

@SuppressWarnings("ALL")
public class ApiModule {
    private static final String TAG = "Goalton [" + ApiModule.class.getSimpleName() + "]";

    public ApiModule() {
    }

    public static ApiInterface getApiInterface() {
        return getApiInterfaceByUrl(HttpParam.BASEURL);
    }

    private static ApiInterface getApiInterfaceByUrl(final String url) {
        return createBuilder(url).build().create(ApiInterface.class);
    }

    public static ApiTeams getApiInterfaceTeams() {
        return createBuilder(HttpParam.BASEURL).build().create(ApiTeams.class);
    }

    public static ApiPlanner getApiCalendar() {
        return createBuilder(HttpParam.BASEURL).build().create(ApiPlanner.class);
    }

    public static ApiOutline getApiOutline() {
        return createBuilder(HttpParam.BASEURL).build().create(ApiOutline.class);
    }

    public static ApiProjectSettings getApiProjectSettings() {
        return createBuilder(HttpParam.BASEURL).build().create(ApiProjectSettings.class);
    }

    private static Retrofit.Builder createBuilder(final String url) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if (BuildConfig.DEBUG) {
            if (DebugKeys.isShowLogsRetrofit) {
                if (DebugKeys.isShowLogsRetrofitHeaders) {
                    interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
                } else {
                    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                }

            } else {
                interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
            }
        }

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        OkHttpClient client = clientBuilder
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .cookieJar(new CookiesRetrofit())
                .build();

//        OkHttpClient client = getUnsafeOkHttpClient(interceptor);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .client(client);
        return builder;
    }


//    public static OkHttpClient getUnsafeOkHttpClient(HttpLoggingInterceptor interceptor) {
//        try {
//            // Create a trust manager that does not validate certificate chains
//            final TrustManager[] trustAllCerts = new TrustManager[]{
//                    new X509TrustManager() {
//                        @Override
//                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
//                        }
//
//                        @Override
//                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                            return new java.security.cert.X509Certificate[]{};
//                        }
//                    }
//            };
//
//            // Install the all-trusting trust manager
//            final SSLContext sslContext = SSLContext.getInstance("SSL");
//            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
//            // Create an ssl socket factory with our all-trusting manager
//            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
//
//            OkHttpClient.Builder builder = new OkHttpClient.Builder();
//            builder.sslSocketFactory(sslSocketFactory);
//            builder.hostnameVerifier(new HostnameVerifier() {
//                @Override
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            });
//
//            OkHttpClient okHttpClient;
//            if (interceptor != null) {
//                okHttpClient = builder
//                        .readTimeout(60, TimeUnit.SECONDS)
//                        .connectTimeout(30, TimeUnit.SECONDS)
//                        .addInterceptor(interceptor)
//                        .cookieJar(new CookiesRetrofit())
//                        .build();
//            } else {
//                okHttpClient = builder
//                        .readTimeout(60, TimeUnit.SECONDS)
//                        .connectTimeout(30, TimeUnit.SECONDS)
//                        .cookieJar(new CookiesRetrofit())
//                        .build();
//            }
//
//            return okHttpClient;
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    public static OkHttp3Downloader getPicasoTrustDownloadClient() {
//        return new OkHttp3Downloader(ApiModule.getUnsafeOkHttpClient(null));
//    }
}