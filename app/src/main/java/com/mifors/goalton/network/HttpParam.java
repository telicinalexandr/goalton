package com.mifors.goalton.network;

import android.text.TextUtils;

import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.model.project.Project;

/**
 * Created by zettig on 30.11.2016.
 */

@SuppressWarnings("ALL")
public class HttpParam {
    public static final String SCHEMA = "http";

    public static final String HOST = "goalton.mifors.com";
//    public static final String HOST = "dev.goalton.com";
//    public static final String HOST = "goalton.com";

    public static final String BASEURL = SCHEMA + "://" + HOST;
    public static final String PROTO_URL = "/mobile";
    public static final String PROTO_URL_FULL = BASEURL + "/mobile";
    public static final int RESPONSE_OK = 200;
    public static final int RESPONSE_ERROR = 400;
    public static final int RESPONSE_ACCESS_DENIED = 4000;

    //==========================================
    // COOKIES KEY
    //==========================================
    public static final String _CSRF = "_csrf";
    public static final String _CSRF_GENERATE_SERVER = "_csrf_shared";
    public static final String PHPSESSID = "PHPSESSID";
    public static final String CUR_LANGUAGE_CODE = "CurLanguageCode";

    public static final String param = "param";
    public static final String token = "token";
    //==========================================
    // HTTP MANAGER KEYS
    //==========================================
    public static final String KEY_CUSTOM_BODY = "custom_body"; // Отправка сосбтвенного multipart
    public static final String KEY_CUSTOM_URL = "custom_url"; // Отправка на произвольный url
    public static final String KEY_METHOD = "method";
    public static final String KEY_FORM_DATA_FILE_NAME = "file_name";
    public static final String KEY_RESPONSE_TYPE = "response_type";
    public static final String KEY_POST_FILE = "post_file";

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String POST_DATA_KEY = "data_post";
    public static final String TWO_LINES = "--";
    public static final String NEW_LINE = "\r\n";

    public static final String BOUNDARY_CONTENT_TYPE = "------WebKitFormBoundary1rge05aw4kQhEt93";
    public static final String BOUNDARY = "------WebKitFormBoundary1rge05aw4kQhEt93";
    public static final String BOUNDARY_LINE = TWO_LINES + BOUNDARY + NEW_LINE;
    public static final String BOUNDARY_LINE_END = TWO_LINES + BOUNDARY + TWO_LINES;

    public static String getCsrfToken() {
        return ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam._CSRF);
    }

    public static void saveCSRFTokent(String token) {
        ManagerApplication.getInstance().getSharedManager().putKeyString(HttpParam._CSRF, token);
    }


    public static String checkImageProject(Project project) {
        return checlImgUrlByType(project.getType(), project.getImg());
    }

    public static String checlImgUrlByType(String type, String imgName) {
        String url = "";
        if ("tutorial".equals(type)) {
            url = BASEURL + "/images/project/tutorial.jpg";
        } else {
            if (!TextUtils.isEmpty(imgName)) {
                url = BASEURL + "/images/project/cover/" + imgName;
            }
        }
        return url;
    }

    public static String getUrlProjectSettings(long idProject) {
        return BASEURL + PROTO_URL + "/project/settings/" + idProject;
    }

}
