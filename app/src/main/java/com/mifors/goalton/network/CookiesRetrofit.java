package com.mifors.goalton.network;

import android.text.TextUtils;
import android.util.Log;

import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.DebugKeys;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

import static com.mifors.goalton.network.HttpParam._CSRF;

/**
 * Created by gly8 on 29.03.17.
 */

@SuppressWarnings("ALL")
public class CookiesRetrofit implements CookieJar {
    private static final String TAG = "Goalton [" + CookiesRetrofit.class.getSimpleName() + "]";
    private List<Cookie> lastCookies;

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        this.lastCookies = cookies;
        if (DebugKeys.isShowCookiesRetrofit) {
            Log.v(TAG, "[saveFromResponse] cookies = " + cookies.toString());
        }

        for (Cookie cookie : cookies) {
            if (_CSRF.equals(cookie.name().toString())) {
                ManagerApplication.getInstance().getSharedManager().putKeyString(HttpParam._CSRF_GENERATE_SERVER, cookie.value());
                saveCookiesToSharedByName(HttpParam._CSRF_GENERATE_SERVER, cookie);
            } else if (HttpParam.PHPSESSID.equals(cookie.name())) {
                ManagerApplication.getInstance().getSharedManager().putKeyString(HttpParam.PHPSESSID, cookie.value());
                saveCookiesToSharedByName(HttpParam.PHPSESSID, cookie);
            } else if (HttpParam.CUR_LANGUAGE_CODE.equals(cookie.name())) {
                ManagerApplication.getInstance().getSharedManager().putKeyString(HttpParam.CUR_LANGUAGE_CODE, cookie.value());
                saveCookiesToSharedByName(HttpParam.CUR_LANGUAGE_CODE, cookie);
            }
        }
    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        List<Cookie> cookies = new ArrayList<>();
        String _CSRF = ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam._CSRF);
        String PHPSESSIONID = ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam.PHPSESSID);
        String CUR_LANGUAGE_CODE = ManagerApplication.getInstance().getSharedManager().getValueString(HttpParam.CUR_LANGUAGE_CODE);

        if (!TextUtils.isEmpty(_CSRF)) {
            Cookie c = getCookieFromShared(HttpParam._CSRF_GENERATE_SERVER, url.toString());
            if (c != null) {
                cookies.add(c);
            }
        }

        if (!TextUtils.isEmpty(PHPSESSIONID)) {
            Cookie c = getCookieFromShared(HttpParam.PHPSESSID, url.toString());
            if (c != null) {
                cookies.add(c);
            }
        }

        if (!TextUtils.isEmpty(CUR_LANGUAGE_CODE)) {
            Cookie c = getCookieFromShared(HttpParam.CUR_LANGUAGE_CODE, url.toString());
            if (c != null) {
                cookies.add(c);
            }
        }

        if (DebugKeys.isShowCookiesRetrofit) {
            Log.v(TAG, "[loadForRequest] cookies = " + cookies);
        }

        return cookies;
    }

    /**
     * Конвертирует параметр в JSONObject и сохраняет значение в шаред
     *
     * @param name   - имя параметра
     * @param cookie - кук который надо сохранить
     */
    private void saveCookiesToSharedByName(String name, Cookie cookie) {
        JSONObject obj = new JSONObject();
        try {
            obj.put(name, cookie.name());
            obj.put("domain", cookie.domain());
            obj.put("experias", cookie.expiresAt());
            obj.put("value", cookie.value());
            obj.put("httponly", cookie.httpOnly());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ManagerApplication.getInstance().getSharedManager().putKeyString(name, obj.toString());
    }

    /**
     * Ищет в шаред нужный кук по имени.
     * Если кук найден преобразовывает к JSONObject и создает из него валидный кук
     *
     * @param name - имя по которому записанно в шаредж
     * @return Cookie - может вернуть null;
     */
    private Cookie getCookieFromShared(String name, String url) {
        Cookie cookie;
        String str = ManagerApplication.getInstance().getSharedManager().getValueString(name);
        if (name.equals(HttpParam._CSRF_GENERATE_SERVER)) {
            name = _CSRF;
        }

        try {
            JSONObject obj = new JSONObject(str);
            long expirias = 1801816528441l; // Fri, 05 Feb 2027 08:35:28 GMT
            if (!obj.isNull("experias")) {
                expirias = obj.getLong("experias");
                if (expirias == 253402300799999l) {
                    expirias = 1801816528441l;
                }
            }

            // Если есть время жизни кука
            cookie = new Cookie.Builder().domain(
                    obj.getString("domain"))
                    .name(name)
                    .expiresAt(expirias)
                    .value(obj.getString("value"))
                    .build();

            if (obj.getBoolean("httponly")) {
                cookie.httpOnly();
            }
        } catch (JSONException e) {
            cookie = new Cookie.Builder()
                    .domain(HttpParam.HOST)
                    .name(name)
                    .value(str)
                    .build();
        }

        return cookie;
    }
}
