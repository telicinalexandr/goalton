package com.mifors.goalton.network.Api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by gly8 on 22.06.17.
 */

@SuppressWarnings("ALL")
public interface ApiTeams {

    //Получение списка аккаунтов\команд
    @FormUrlEncoded
    @POST("mobile/user/accounts")
    Call<ResponseBody> getAcccounts(@Field("_csrf") String scrfToken);

    /**
     * Создание нового аккаунта
     *
     * @param scrfToken   - token полученый при вызове метода init
     * @param userId      - id текущего пользователя
     * @param accountName - имя создоваемого аккаунта
     */
    @FormUrlEncoded
    @POST("mobile/user/create-account")
    Call<ResponseBody> createAccount(@Field("_csrf") String scrfToken,
                                     @Field("Account[owner_user_id]") long userId,
                                     @Field("Account[name]") String accountName);

    /**
     * Создание новой команды
     *
     * @param scrfToken - token полученый при вызове метода init
     * @param userId    - id текущего пользователя
     * @param accountId - id аккаунта
     * @param teamName  - имя создоваемой команды
     */
    @FormUrlEncoded
    @POST("mobile/user/create-team")
    Call<ResponseBody> createTeam(@Field("_csrf") String scrfToken,
                                  @Field("Team[owner_user_id]") long userId,
                                  @Field("Team[account_id]") long accountId,
                                  @Field("Team[name]") String teamName);

    /**
     * Изменение имени команды
     *
     * @param scrfToken - token полученый при вызове метода init
     * @param teamId    - id команды у которой меняем имя
     * @param teamName  - новое имя
     */
    @FormUrlEncoded
    @POST("mobile/user/change-team")
    Call<ResponseBody> changeNameTeam(@Field("_csrf") String scrfToken,
                                      @Field("Team[id]") long teamId,
                                      @Field("Team[name]") String teamName);

    /**
     * Удалить привязку пользователя к команде
     *
     * @param scrfToken  - token полученый при вызове метода init
     * @param userTeamId - id пользователя
     */
    @FormUrlEncoded
    @POST("mobile/user/unlink-team")
    Call<ResponseBody> unlinkUserFromTeam(@Field("_csrf") String scrfToken,
                                          @Field("user_team_id") long userTeamId);

    /**
     * Удалить привязку пользователя от проекта
     *
     * @param scrfToken - token полученый при вызове метода init
     * @param projectId - id проекта
     */
    @FormUrlEncoded
    @POST("mobile/user/unlink-project")
    Call<ResponseBody> unlinkUserFromTeamProject(@Field("_csrf") String scrfToken,
                                                 @Field("Project[id]") long projectId);

    /**
     * Изменить права для участника проекта
     *
     * @param scrfToken          - token полученый при вызове метода init
     * @param userId             - id пользователя
     * @param projectId          - id проекта для которого меняем права
     * @param rightToView        - можно ли просматривать 1 - включить 0 - выключить
     * @param rightToEdit        - можно ли править проект
     * @param rightToEditProject - можно ли редактировать проект
     * @param rightToInvite      - можно ли приглашать других участников в команду
     */
    @FormUrlEncoded
    @POST("mobile/user/change-right-for-progect")
    Call<ResponseBody> changePermissionTeamProfile(@Field("_csrf") String scrfToken,
                                                   @Field("user_id") long userId,
                                                   @Field("project_id") long projectId,
                                                   @Field("right_to_view") String rightToView,
                                                   @Field("right_to_edit") String rightToEdit,
                                                   @Field("right_to_edit_project") String rightToEditProject,
                                                   @Field("right_to_invite") String rightToInvite);

    /**
     * Получение всех групп куда приглашен пользователь
     */
    @GET("mobile/user/membership")
    Call<ResponseBody> getMemberships();

    /**
     * Получение всех групп куда приглашен пользователь
     */
    @GET("mobile/invite/win-edit-list")
    Call<ResponseBody> getListInvites();

    /**
     * Пригласить в команду
     * _csrf 		токен защиты
     * Invite[project_id] 	скрытое 	id проекта
     * Invite[email] 	email format 	email приглашаемого пользователя
     */
    @FormUrlEncoded
    @POST("mobile/invite/win-edit")
    Call<ResponseBody> sendInvite(@Field("_csrf") String csrf, @Field("Invite[project_id]") long projectId, @Field("Invite[email]") String email);

    /**
     * Получить список моих приглашений
     */
    @GET("mobile/invite/list")
    Call<ResponseBody> getListMyInvite();

    /**
     * Принять - отклонить приглашение
     * _csrf 		токен защиты
     * Invite[id] 	скрытое 	id приглашения
     * Invite[status] 	accepted / rejected 	accepted - принять, rejected - отказ
     */
    @FormUrlEncoded
    @POST("mobile/invite/change-status")
    Call<ResponseBody> changeStatusInvite(@Field("_csrf") String csrf, @Field("Invite[id]") long projectId, @Field("Invite[status]") String status);
}
