package com.mifors.goalton.network.Api;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by zettig on 30.11.2016.
 */

@SuppressWarnings("ALL")
public interface ApiInterface {

    //==========================================
    // Авторизация - регистрация
    //==========================================
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/auth/login")
    Call<ResponseBody> login(@Field("_csrf") String scrfToken,
                             @Field("LoginForm[username]") String username,
                             @Field("LoginForm[password]") String password,
                             @Field("LoginForm[rememberMe]") String rememberMy);


    /**
     * Автооризация на сервере с помощью социальных сетей
     *
     * @param scrfToken  - токен полученный через метод init
     * @param typeSocial - тип социальной сети через которую авторизовываемся
     * @param accestoken - токен полученный при авторизации в соцсти через SDK
     * @param userId     - id пользователя в соцсети
     * @param secretCode - дополнительный проверочный код MD5-хеш следующей строки:
     *                   csrfToken#typeSocial#userId
     *                   т. е. значения CsrfToken, typeSocial и userId с символом # между ними
     * @param email - полученный через социальную сеть
     * @param name  - полученный через социальную сеть
     * @param firstname - полученный через социальную сеть  - имя
     * @param lastname  - полученный через социальную сеть   -  фамилия
     */
    @Headers("X-Requested-With: XMLHttpRequest")
    @FormUrlEncoded
    @POST("mobile/auth/login-social")
    Call<ResponseBody> loginSocial(@Header("X-CSRF-Token") String scrfTokenHeader,
                                   @Field("_csrf") String scrfToken,
                                   @Field("LoginSocial[typeSocial]") String typeSocial,
                                   @Field("LoginSocial[userId]") String userId,
                                   @Field("LoginSocial[accestoken]") String accestoken,
                                   @Field("LoginSocial[code]") String secretCode,
                                   @Field("LoginSocial[email]") String email,
                                   @Field("LoginSocial[name]") String name,
                                   @Field("LoginSocial[firstname]") String firstname,
                                   @Field("LoginSocial[lastname]") String lastname);

    /**
     * Авторизация пользователя
     */
    @GET("mobile/auth/init")
    Call<ResponseBody> init();

    /**
     * Востановление пароля
     *
     * @param scrfToken - token полученый при вызове метода init
     * @param email     - email куда выслать письмо для востановления
     */
    @FormUrlEncoded
    @POST("mobile/auth/request-password-reset")
    Call<ResponseBody> remindPassword(@Field("_csrf") String scrfToken,
                                      @Field("PasswordResetRequestForm[email]")
                                              String email);

    /**
     * Авторизация пользователя
     *
     * @param scrfToken - token полученый при вызове метода init
     * @param username  - имя пользователя
     * @param email     - email пользователя
     * @param password  - пароль для входа
     */
    @FormUrlEncoded
    @POST("mobile/auth/signup")
    Call<ResponseBody> register(@Field("_csrf") String scrfToken,
                                @Field("SignupForm[username]") String username,
                                @Field("SignupForm[email]") String email,
                                @Field("SignupForm[password]") String password);

    //==========================================
    // Профиль пользователя
    //==========================================

    /**
     * Получение данных пользователя
     */
    @GET("mobile/user/profile")
    Call<ResponseBody> getUserData();

    /**
     * @param scrfToken - token полученый при вызове метода init
     * @param bgId      - id бэкграунда
     */
    @FormUrlEncoded
    @POST("mobile/user/set-background")
    Call<ResponseBody> changeBackground(@Field("_csrf") String scrfToken,
                                        @Field("fon_id") String bgId);

    @FormUrlEncoded
    @POST("mobile/user/update-user-property")
    Call<ResponseBody> subscriptNotify(@Field("_csrf") String scrfToken,
                                       @Field("field") String field,
                                       @Field("value") int value);

    @FormUrlEncoded
    @POST("mobile/user/update")
    Call<ResponseBody> updateDataUser(@Field("_csrf") String scrfToken,
                                      @Field("User[username]") String username,
                                      @Field("User[email]") String email,
                                      @Field("User[password]") String password,
                                      @Field("User[firstname]") String firstname,
                                      @Field("User[lastname]") String lastname,
                                      @Field("User[country]") String country,
                                      @Field("User[timezone]") String timezone,
                                      @Field("User[company_name]") String companyName,
                                      @Field("User[company_size]") String companySize);
}
