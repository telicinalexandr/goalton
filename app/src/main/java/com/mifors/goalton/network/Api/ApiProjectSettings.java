package com.mifors.goalton.network.Api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by gly8 on 27.04.17.
 */

@SuppressWarnings("ALL")
public interface ApiProjectSettings
{
    /**
     * Получение всех текущих проектов
     * */
    @GET("mobile/project/list")
    Call<ResponseBody> getProjects();

    @GET("mobile/project/settings/{project_id}")
    Call<ResponseBody> getProjectSettings(@Path("project_id") String projectId);

    @FormUrlEncoded
    @POST("mobile/project/delete/{project_id}")
    Call<ResponseBody> deleteProjectSettings(@Path("project_id") String projectId, @Field("_csrf") String csrf);

}
