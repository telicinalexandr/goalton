package com.mifors.goalton;

/**
 * Created by gly8 on 17.03.17.
 */

public class DebugKeys {
//    // Надо ли включать самоподписанный сертификат для https
//    public static final boolean isHttpClientTrus  = false; // default - false

    // Показывать ли логи при интернет запросах
    public static final boolean isShowLogsRetrofit  = true; // default - false

    // Показывать ли логи при интернет запросах
    public static final boolean isShowLogsRetrofitHeaders  = false; // default - false

    // Показывать ли логи при интернет запросах
    public static final boolean isShowCookiesRetrofit  = true; // default - false

    // Запускать ли таймер для получения списка приглашений
    public static final boolean isStartTimerResponseInvites = true; // default - true
}