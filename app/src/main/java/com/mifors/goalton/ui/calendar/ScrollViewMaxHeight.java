package com.mifors.goalton.ui.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

import com.mifors.goalton.managers.ManagerApplication;

/**
 * Created by gly8 on 11.08.17.
 */

@SuppressWarnings("ALL")
public class ScrollViewMaxHeight extends ScrollView {

    private static final String TAG = "Goalton [" + ScrollViewMaxHeight.class.getSimpleName() + "]";
    private int maxHeight;

    public ScrollViewMaxHeight(Context context) {
        super(context);
    }

    public ScrollViewMaxHeight(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            init();
        }
    }

    public ScrollViewMaxHeight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init();
        }
    }

    private void init() {
        maxHeight = ManagerApplication.getInstance().getScreenHeight50Percentage();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
