package com.mifors.goalton.ui.spinner.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.ArrayList;

/**
 * Created by gly8 on 23.03.17.
 */

@SuppressWarnings("ALL")
public class AdapterSpinner extends ArrayAdapter<InterfaceSpinnerItem> {
    private ArrayList<InterfaceSpinnerItem> data;
    private LayoutInflater inflater;
    private int resLayout;
    private int selectedPosition;
    private Spinner spinner;
    private boolean isHideArrowRight; // Надо ли скрывать правую стрелочку
    private boolean isSetBackgroundTranparante;
    private AdapterView.OnItemSelectedListener onItemSelectedListener;
    private boolean isReverse; // Обратная сортировка
    private boolean isShowArrowInHideLayout; // Показывать ли стрелоку в выбранном элементе
    private boolean isUpperCase;
    private int resColorTextHeader; // цвет текста выбраного элемента
    private String hexColorTextHeader;
    private int resHeader = 0;
    private View.OnClickListener onClickItem;
    private View.OnTouchListener onTouchListener;

    //==========================================
    // Constructors
    //==========================================
    public AdapterSpinner(Context context, int resource, ArrayList<InterfaceSpinnerItem> data, Spinner spinner) {
        super(context, resource, data);
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
        initSpinner(spinner);
    }

    public AdapterSpinner(Context context, int resource, ArrayList<InterfaceSpinnerItem> data, Spinner spinner, AdapterView.OnItemSelectedListener onItemSelectedListener) {
        super(context, resource, data);
        setVariable(resource, data, isHideArrowRight, isSetBackgroundTranparante, resHeader, isShowArrowInHideLayout, onItemSelectedListener);
        initSpinner(spinner);
    }

    public AdapterSpinner(Context context,
                          int resource,
                          ArrayList<InterfaceSpinnerItem> data,
                          Spinner spinner,
                          boolean isHideArrowRight,
                          boolean isSetBackgroundTranparante,
                          int resHeader,
                          boolean isShowArrowInHideLayout) {
        super(context, resource, data);
        setVariable(resource, data, isHideArrowRight, isSetBackgroundTranparante, resHeader, isShowArrowInHideLayout, null);
        initSpinner(spinner);
    }

    public AdapterSpinner(Context context,
                          int resource,
                          ArrayList<InterfaceSpinnerItem> data,
                          Spinner spinner,
                          boolean isHideArrowRight,
                          boolean isSetBackgroundTranparante,
                          int resHeader,
                          boolean isShowArrowInHideLayout,
                          boolean isUpperCase) {
        super(context, resource, data);
        this.isUpperCase = isUpperCase;
        setVariable(resource, data, isHideArrowRight, isSetBackgroundTranparante, resHeader, isShowArrowInHideLayout, null);
        initSpinner(spinner);
    }

    public AdapterSpinner(Context context,
                          int resource,
                          ArrayList<InterfaceSpinnerItem> data,
                          Spinner spinner,
                          boolean isHideArrowRight,
                          boolean isSetBackgroundTranparante,
                          int resHeader,
                          boolean isShowArrowInHideLayout,
                          AdapterView.OnItemSelectedListener onItemSelectedListener) {
        super(context, resource, data);
        setVariable(resource, data, isHideArrowRight, isSetBackgroundTranparante, resHeader, isShowArrowInHideLayout, onItemSelectedListener);
        initSpinner(spinner);
    }

    private void setVariable(int resource,
                             ArrayList<InterfaceSpinnerItem> data,
                             boolean isHideArrowRight,
                             boolean isSetBackgroundTranparante,
                             int resHeader,
                             boolean isShowArrowInHideLayout,
                             AdapterView.OnItemSelectedListener onItemSelectedListener) {
        this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.data = data;
        this.resLayout = resource;
        this.isHideArrowRight = isHideArrowRight;
        this.isSetBackgroundTranparante = isSetBackgroundTranparante;
        this.resHeader = resHeader;
        this.isShowArrowInHideLayout = isShowArrowInHideLayout;
        this.onItemSelectedListener = onItemSelectedListener;
    }

    private void initSpinner(Spinner spinner) {
        if (spinner != null) {
            this.spinner = spinner;
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                boolean isInitCalling = true;

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!isInitCalling) {
                        setSelectedPosition(position);
                        notifyDataSetChanged();
                        if (onItemSelectedListener != null) {
                            onItemSelectedListener.onItemSelected(parent, view, position, id);
                        }
                    } else {
                        if (onItemSelectedListener != null) {
                            onItemSelectedListener.onItemSelected(null, null, 0, 0);
                        }
                    }
                    isInitCalling = false;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    if (onItemSelectedListener != null) {
                        onItemSelectedListener.onNothingSelected(parent);
                    }
                }
            });
        }
    }

    //==========================================
    // View created
    //==========================================
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(parent, true, data.get(position), convertView, position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            return getCustomView(parent, false, data.get(position), convertView, position);
        } catch (IndexOutOfBoundsException e) {
            if (convertView == null || convertView != null) {
                return getCustomView(parent, false, data.get(0), convertView, position);
            }
        }
        return convertView;
    }

    public class ViewHolder {
        private boolean isDropDown;
        TextView title;
        ImageView imgCheck;
        ImageView imgArrow;
        View rootView;

        public ViewHolder(View holder) {
            this.title = holder.findViewById(R.id.title);
            this.imgCheck = holder.findViewById(R.id.img_check);
            this.imgArrow = holder.findViewById(R.id.img_arrow);
            this.rootView = holder.findViewById(R.id.root_view_item_spinner);
        }

        public boolean isDropDown() {
            return isDropDown;
        }

        public void setDropDown(boolean dropDown) {
            isDropDown = dropDown;
        }
    }

    public View getCustomView(ViewGroup parent, boolean isDropDown, InterfaceSpinnerItem option, View convertView, int position) {
        ViewHolder holder;
        isReverse = ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE);
        if (convertView == null) {
            if (!isDropDown && resHeader != 0) {
                convertView = inflater.inflate(resHeader, parent, false);
            } else {
                convertView = inflater.inflate(resLayout, parent, false);
            }
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (getOnTouchListener() != null) {
            holder.rootView.setOnTouchListener(getOnTouchListener());
        }

        holder.setDropDown(isDropDown);
        convertView.setPadding(0, convertView.getPaddingTop(), convertView.getPaddingRight(), convertView.getPaddingBottom());

        if (isDropDown) {
            if (selectedPosition == position) {
                holder.imgCheck.setAlpha(1f);
                holder.imgArrow.setVisibility(View.VISIBLE);
                if (isReverse) {
                    holder.imgArrow.setImageResource(R.drawable.ic_arrow_black_top);
                } else {
                    holder.imgArrow.setImageResource(R.drawable.ic_arrow_black_bottom);
                }
            } else {
                holder.imgCheck.setAlpha(0f);
                holder.imgArrow.setVisibility(View.GONE);
            }
        } else {
            if (holder.imgCheck != null) {
                holder.imgCheck.setVisibility(View.GONE);
            }
        }

        if (isHideArrowRight) {
            if (holder.imgArrow != null) {
                holder.imgArrow.setVisibility(View.GONE);
            }
        }

        if (!isDropDown) {
            if (isShowArrowInHideLayout) {
                holder.imgArrow.setVisibility(View.VISIBLE);
            } else {
                holder.imgArrow.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(hexColorTextHeader)) {
                holder.title.setTextColor(Color.parseColor(hexColorTextHeader));
            }
        }

        if (isSetBackgroundTranparante) {
            holder.rootView.setBackgroundColor(android.R.color.transparent);
        }

        if (isUpperCase) {
            holder.title.setText(option.getName().toUpperCase());
        } else {
            holder.title.setText(option.getName());
        }


        return convertView;
    }

    //==========================================
    // Help methods
    //==========================================
    public InterfaceSpinnerItem getItemPosition(int position) {
        return data.get(position);
    }

    public int getItemCount() {
        if (data != null) {
            return data.size();
        }
        return 0;
    }

    public void setSelectedPositionByValue(Object value) {
        boolean isSet = false;
        for (int i = 0; i < data.size(); i++) {
            try {
                if (data.get(i).getValue().equals(value.toString())) {
                    setSelectedPosition(i);
                    spinner.setSelection(i);
                    isSet = true;
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (!isSet) {
            setSelectedPosition(0);
        }
    }

    public String getSelectedValue() {
        return getValueByPosition(selectedPosition);
    }

    public Long getSelectedValueLong() {
        if (TextUtils.isEmpty(getValueByPosition(selectedPosition))) {
            return (long)0;
        }
        return Long.parseLong(getValueByPosition(selectedPosition));
    }

    public String getValueByPosition(int position) throws ArrayIndexOutOfBoundsException {
        return data.get(position).getValue();
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setIsReverse(boolean reverse) {
        isReverse = reverse;
    }

    public boolean isReverse() {
        return isReverse;
    }

    public void setHideArrowRight(boolean hideArrowRight) {
        isHideArrowRight = hideArrowRight;
    }

    public void setResColorTextHeader(int resColorTextHeader) {
        this.resColorTextHeader = resColorTextHeader;
    }

    public void setHexColorTextHeader(String hexColorTextHeader) {
        this.hexColorTextHeader = hexColorTextHeader;
    }

    public boolean isEmptySelectedValue(){
        return TextUtils.isEmpty(getSelectedValue());
    }

    public View.OnClickListener getOnClickItem() {
        return onClickItem;
    }

    public void setOnClickItem(View.OnClickListener onClickItem) {
        this.onClickItem = onClickItem;
    }

    public View.OnTouchListener getOnTouchListener() {
        return onTouchListener;
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }
}