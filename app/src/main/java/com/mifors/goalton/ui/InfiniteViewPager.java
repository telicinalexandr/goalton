package com.mifors.goalton.ui;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.mifors.goalton.adapters.AdapterViewPagerDays;
import com.mifors.goalton.fragment.calendar.FragmentCalendar;

/**
 * A {@link ViewPager} that allows pseudo-infinite paging with a wrap-around effect. Should be used with an {@link
 * AdapterViewPagerDays}.
 */
public class InfiniteViewPager extends ViewPager {
    public InfiniteViewPager(Context context) {
        super(context);
    }

    public InfiniteViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setAdapter(PagerAdapter adapter) {
        super.setAdapter(adapter);
        setCurrentItem(FragmentCalendar.COUNT_ITEMS/2);
    }

    @Override
    public void setCurrentItem(int item) {
        setCurrentItem(item, false);
    }

    @Override
    public void setCurrentItem(int item, boolean smoothScroll) {
        if (getAdapter().getCount() == 0) {
            super.setCurrentItem(item, smoothScroll);
            return;
        }
        item = (getOffsetAmount()/2) + (item % (getAdapter().getCount()/2));
        super.setCurrentItem(item, smoothScroll);
    }

    @Override
    public int getCurrentItem() {
        if (getAdapter().getCount() == 0) {
            return super.getCurrentItem();
        }

        int position = super.getCurrentItem();
        if (getAdapter() instanceof AdapterViewPagerDays) {
            AdapterViewPagerDays infAdapter = (AdapterViewPagerDays) getAdapter();
            int newPosition = (position % infAdapter.getRealCount());
            return newPosition;
        } else {
            return super.getCurrentItem();
        }
    }

    private int getOffsetAmount() {
        if (getAdapter().getCount() == 0) {
            return 0;
        }

        if (getAdapter() instanceof AdapterViewPagerDays) {
            return ((AdapterViewPagerDays) getAdapter()).getRealCount();
        } else {
            return 0;
        }
    }
}
