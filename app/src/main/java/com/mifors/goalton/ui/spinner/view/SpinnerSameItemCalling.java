package com.mifors.goalton.ui.spinner.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by gly8 on 01.06.17.
 *
 * Если надо чтобы при выборе того же самого элемента вызывался нотификатор
 * создаем спинер от этого класса
 */
public class SpinnerSameItemCalling extends Spinner {

    public SpinnerSameItemCalling(Context context) {
        super(context);
    }

    public SpinnerSameItemCalling(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerSameItemCalling(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setSelection(int position, boolean animate) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position, animate);
        if (sameSelected) {
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected) {
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }

}
