package com.mifors.goalton.ui.spinner.view;

import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItemColors;

/**
 * Created by Sergey on 12.05.17.
 */

@SuppressWarnings("ALL")
public class DefaultObjectColors implements InterfaceSpinnerItemColors {
    private int resColor;
    private String value;

    public DefaultObjectColors() {
    }

    @Override
    public int getResColor() {
        return resColor;
    }

    @Override
    public void setResColor(int resColor) {
        this.resColor = resColor;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}