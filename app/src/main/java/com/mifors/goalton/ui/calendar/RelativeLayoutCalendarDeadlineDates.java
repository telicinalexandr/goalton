package com.mifors.goalton.ui.calendar;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.interfaces.InterfaceFragmentDay;
import com.mifors.goalton.managers.core.ManagerPlanner;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.model.outline.Line;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by gly8 on 26.04.17.
 */
public class RelativeLayoutCalendarDeadlineDates extends RelativeLayout {
    private static final String TAG = "Goalton [" + RelativeLayoutCalendarDeadlineDates.class.getSimpleName() + "]";
    private RelativeLayout containerDays, containerControl;
    private OnDragListener onDragListenerItems;
    private LinearLayout containerCurrentHour;
    private ImageView lineHour;
    private String dayDate;
    private InterfaceFragmentDay interfaceFragmentDay;
    private TextView titleMinutes;
    private List<Line> arrayLines;
    private ArrayList<Line> tempListGroupHours = new ArrayList<>(); // Хранится результат при разделении задачпо времени
    private View line_00, line_01, line_02, line_03, line_04, line_05, line_06, line_07, line_08, line_09, line_10, line_11, line_12, line_13, line_14, line_15, line_16, line_17, line_18, line_19, line_20, line_21, line_22, line_23, line_24;

    public RelativeLayoutCalendarDeadlineDates(Context context) {
        super(context);
    }

    public RelativeLayoutCalendarDeadlineDates(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RelativeLayoutCalendarDeadlineDates(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void findItems() {
        containerCurrentHour = findViewById(R.id.container_line_current_hour);
        lineHour = findViewById(R.id.line_hour);
        titleMinutes = findViewById(R.id.title_minutes);
        containerDays = findViewById(R.id.container_days);
        containerControl = findViewById(R.id.container_control);
        containerControl.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    containerControl.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    containerControl.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                ManagerPlanner.saveWidthItemCalendar(containerControl.getWidth());
                ManagerPlanner.saveHeightItem(containerControl.getHeight());
                int minuteSteep = Math.round((((float) containerControl.getHeight()) / 60));
                ManagerPlanner.setHeightSteepMinute(minuteSteep);
                if (minuteSteep * 60 != containerControl.getHeight()) {
                    updateLinesTopMargin();
                }
                updateLineCurrentTime();
            }
        });

        this.post(new Runnable() {
            @Override
            public void run() {
                updateLineCurrentTime();
            }
        });
    }

    private void updateLinesTopMargin() {
        if (line_00 == null) {
            line_00 = findViewById(R.id.line_00);
            line_01 = findViewById(R.id.line_01);
            line_02 = findViewById(R.id.line_02);
            line_03 = findViewById(R.id.line_03);
            line_04 = findViewById(R.id.line_04);
            line_05 = findViewById(R.id.line_05);
            line_06 = findViewById(R.id.line_06);
            line_07 = findViewById(R.id.line_07);
            line_08 = findViewById(R.id.line_08);
            line_09 = findViewById(R.id.line_09);
            line_10 = findViewById(R.id.line_10);
            line_11 = findViewById(R.id.line_11);
            line_12 = findViewById(R.id.line_12);
            line_13 = findViewById(R.id.line_13);
            line_14 = findViewById(R.id.line_14);
            line_15 = findViewById(R.id.line_15);
            line_16 = findViewById(R.id.line_16);
            line_17 = findViewById(R.id.line_17);
            line_18 = findViewById(R.id.line_18);
            line_19 = findViewById(R.id.line_19);
            line_20 = findViewById(R.id.line_20);
            line_21 = findViewById(R.id.line_21);
            line_22 = findViewById(R.id.line_22);
            line_23 = findViewById(R.id.line_23);
            line_24 = findViewById(R.id.line_24);
        }

        updateLayoutLine(line_00);
        updateLayoutLine(line_01);
        updateLayoutLine(line_02);
        updateLayoutLine(line_03);
        updateLayoutLine(line_04);
        updateLayoutLine(line_05);
        updateLayoutLine(line_06);
        updateLayoutLine(line_07);
        updateLayoutLine(line_08);
        updateLayoutLine(line_09);
        updateLayoutLine(line_10);
        updateLayoutLine(line_11);
        updateLayoutLine(line_12);
        updateLayoutLine(line_13);
        updateLayoutLine(line_14);
        updateLayoutLine(line_15);
        updateLayoutLine(line_16);
        updateLayoutLine(line_17);
        updateLayoutLine(line_18);
        updateLayoutLine(line_19);
        updateLayoutLine(line_20);
        updateLayoutLine(line_21);
        updateLayoutLine(line_22);
        updateLayoutLine(line_23);
        updateLayoutLine(line_24);

    }

    private void updateLayoutLine(View line) {
        RelativeLayout.LayoutParams lp = (LayoutParams) line.getLayoutParams();
        lp.topMargin = ManagerPlanner.getHeightSteepMinute() * 60;
        line.setLayoutParams(lp);
    }

    public void setOnDragListenerItems(OnDragListener onDragListenerItem) {
        this.onDragListenerItems = onDragListenerItem;
        if (containerDays != null) {
            containerDays.setOnDragListener(onDragListenerItem);
        }
    }

    public void updateDateItems(String date) {
        dayDate = date;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findItems();
        updateDateItems(dayDate);
        setOnDragListenerItems(onDragListenerItems);
    }

    // Обновление позиции указывающей текущее время
    public void updateLineCurrentTime() {
        if (ManagerPlanner.getHeightItemLine() != Integer.MAX_VALUE) {
            Calendar cal = Calendar.getInstance();
            if ((interfaceFragmentDay.getDateCurrentDay().get(Calendar.DAY_OF_YEAR) == cal.get(Calendar.DAY_OF_YEAR))
                    && (interfaceFragmentDay.getDateCurrentDay().get(Calendar.YEAR) == cal.get(Calendar.YEAR))) {

                Calendar curCal = Calendar.getInstance();
                String strHoursCurrentLine = ManagerCalendar.bulBul(curCal.get(Calendar.HOUR_OF_DAY));
                int resourceId = getResourceTimeLineByName(strHoursCurrentLine);
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) containerCurrentHour.getLayoutParams();
                float minute = curCal.get(Calendar.MINUTE);
                float heightSteep = ((float) ManagerPlanner.getHeightItemLine()) / 60f;
                lp.topMargin = (int) ((minute * heightSteep) - (containerCurrentHour.getHeight() / 2) + 1);
                lp.addRule(RelativeLayout.BELOW, resourceId);
                containerCurrentHour.setLayoutParams(lp);
                LinearLayout.LayoutParams lpLine = (LinearLayout.LayoutParams) lineHour.getLayoutParams();
                if (ManagerPlanner.getWidthItemCalendar() > 0) {
                    containerCurrentHour.setVisibility(View.VISIBLE);
                } else {
                    containerCurrentHour.setVisibility(View.GONE);
                }
                lpLine.width = ManagerPlanner.getWidthItemCalendar();
                lineHour.setLayoutParams(lpLine);
            } else {
                containerCurrentHour.setVisibility(View.GONE);
            }
        }
    }

    private int getResourceTimeLineByName(String strHour) {
        return getContext().getResources().getIdentifier("line_" + strHour, "id", getContext().getPackageName());
    }

    public void setInterfaceFragmentDay(InterfaceFragmentDay interfaceFragmentDay) {
        this.interfaceFragmentDay = interfaceFragmentDay;
    }

    public void setArrayLines(List<Line> arrayLines) {
        this.arrayLines = arrayLines;
    }

    public LinearLayout getContainerCurrentHour() {
        return containerCurrentHour;
    }

    //==========================================
    // Add lines methods
    //==========================================
    // TODO: Rename to updateDrawableLines
    // Добавление задач с датами
    public void updateDrawableItems() {
        if (ManagerPlanner.getHeightItemLine() != Integer.MAX_VALUE) {
            containerDays.removeAllViews();
            if (arrayLines != null && arrayLines.size() > 0) {
                addRecursiveLinesDeadlineDate(arrayLines.get(0));
            }
        }
    }

    // Добавление линий с установленой датой дедлайна рекурсивным способом
    private void addRecursiveLinesDeadlineDate(Line tempLine) {
        if (tempLine == null) {
            return;
        }

        // Получаем группу элементов которые попадают в диапозон даты переданной линии
        ArrayList<Line> array = getGroupLines(tempLine);
        arrayLines.removeAll(array); // Удаляем из массива чтобы не было дублей
        if (!TextUtils.isEmpty(tempLine.getDeadlineDateStartTimeStr())) {
            // Перерасчет длины задачи и установка отступа слева и ширины задачи
            // Нельзя расчитать выше потому что неизвестно количество задач в группе
            int widthRootLayout = ManagerPlanner.getWidthItemCalendar() / array.size();
            tempListGroupHours.addAll(array);
            for (int i = 0; i < array.size(); i++) {
                ItemLine itemLine = interfaceFragmentDay.createViewItemLine(array.get(i), 0);
                itemLine.udpdateOffset(i, widthRootLayout);
                containerDays.addView(itemLine);
            }

            if (arrayLines.size() > 0) {
                addRecursiveLinesDeadlineDate(arrayLines.get(0));
            }
        }
    }
    // Возвращает группу задач которые попадают в промежуток времени
    private ArrayList<Line> getGroupLines(Line item) {
        ArrayList<Line> arr = new ArrayList<>();
        Calendar startCalendar = item.getDeadlineDateStart();
        Calendar endCalendar = item.getDeadlineDateEnd();
        for (Line line : arrayLines) {
            // попадает ли задача в промежуток времени
            if (line.getDeadlineDateStart().getTimeInMillis() >= startCalendar.getTimeInMillis()
                    && line.getDeadlineDateStart().getTimeInMillis() < endCalendar.getTimeInMillis()) {
                arr.add(line);
                // Если задача попадает в промежуток времени и дата окончания больше стартового
                if (line.getDeadlineDateEnd() != null &&
                    line.getDeadlineDateEnd().getTimeInMillis() > endCalendar.getTimeInMillis()) {
                    endCalendar = line.getDeadlineDateEnd();
                }
            }
        }
        return arr;
    }

    public void updatePositionTitleMinutes(int topMargin, String text, int heightTextView, ScrollView scrollView, int paddingbottomScroll) {
        if (titleMinutes != null) {
            titleMinutes.setVisibility(View.VISIBLE);
            titleMinutes.setText(text);
            int bottom = (scrollView.getChildAt(0).getHeight()-paddingbottomScroll);
            if (topMargin <= bottom) {
                RelativeLayout.LayoutParams lp = (LayoutParams) titleMinutes.getLayoutParams();
                if (heightTextView > 0) {
                    lp.topMargin = topMargin - heightTextView / 2;
                } else {
                    lp.topMargin = topMargin;
                }

                titleMinutes.setLayoutParams(lp);
            }
        }
    }

    public void endShowMinutes() {
        if (titleMinutes != null) {
            titleMinutes.setVisibility(View.GONE);
        }
    }
}