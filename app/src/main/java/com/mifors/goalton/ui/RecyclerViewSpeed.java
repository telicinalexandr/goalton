package com.mifors.goalton.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by gly8 on 16.05.17.
 */

@SuppressWarnings("ALL")
public class RecyclerViewSpeed extends RecyclerView {

    Context context;

    public RecyclerViewSpeed(Context context) {
        super(context);
        this.context = context;
    }

    public RecyclerViewSpeed(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecyclerViewSpeed(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    @Override
    public boolean fling(int velocityX, int velocityY) {
//        velocityY *= 15;
        return super.fling(velocityX, velocityY);
    }
}
