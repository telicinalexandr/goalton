package com.mifors.goalton.ui.calendar;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerPlanner;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.model.outline.Line;

import java.util.Calendar;

/**
 * Created by gly8 on 15.08.17.
 */

@SuppressWarnings("ALL")
public class ItemLine extends LinearLayout {
    private static final String TAG = "Goalton [" + ItemLine.class.getSimpleName() + "]";
    private int startTimeHour, endTimeHour;
    private Calendar date;
    private Line line;
    private LinearLayout.LayoutParams mLayoutParams = null;
    private TextView titleAssign;
    private int heightSteep;
    private final int OFFSET_4_DP = (int) ManagerApplication.convertDpToPixel(4);
    private final int OFFSET_1_DP = (int) ManagerApplication.convertDpToPixel(1);;
    public ItemLine(Context context) {
        super(context);
    }

    public ItemLine(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ItemLine(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void init() {
        titleAssign = findViewById(R.id.title_assign);
    }

    @Override
    public String toString() {
        String title = ((TextView) findViewById(R.id.title)).getText().toString();
        return "ItemLine {startTimeHour = " + startTimeHour + "  endTimeHour = " + endTimeHour + "  title = " + title + "}";
    }

    // Надо передавать количество детей для расчета ширины и порядковый индекс для расчета отступа слева
    public void updateLayoutParamsFromDeadlineDates(int heightSteepMinute) {
        int minuteBeetween = line.getDuration();
        if (line.getDeadlineDateStart().get(Calendar.DAY_OF_MONTH) != line.getDeadlineDateEnd().get(Calendar.DAY_OF_MONTH)) {
            // Если время дедлайна задачи выходит за рамки дня то уменьшать ее размер до самого конца
            Calendar calendar = ManagerCalendar.getInstance();
            calendar.setTimeInMillis(line.getDeadlineDateEnd().getTimeInMillis());
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 30);
            calendar.set(Calendar.SECOND, 0);
            minuteBeetween = (int) ManagerCalendar.getBeetwenMinutes(line.getDeadlineDateStart().getTimeInMillis(), calendar.getTimeInMillis());
        }

        int h = ManagerPlanner.getHeightSteepMinute();
        int newHeight = (int) (minuteBeetween * h);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, newHeight);
        this.setLayoutParams(lp);
    }

    public void udpdateOffset(int numberOffsetLeft, int newWidth){
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) getLayoutParams();
        int h = ManagerPlanner.getHeightSteepMinute();
        int roundMinutes10 = ManagerCalendar.roundMinutes10(line.getDeadlineDateStart());
        // Количество минут с начала дня
        int minuteFromStart = ((line.getDeadlineDateStart().get(Calendar.HOUR_OF_DAY) * 60)+roundMinutes10);
        int offsetTop =  (minuteFromStart * h);
        int newLeftMargin = (numberOffsetLeft * newWidth);
        int newHeight = lp.height-(OFFSET_1_DP*2);
        lp.topMargin = (int) offsetTop+OFFSET_1_DP;
        lp.height = newHeight;
        if (numberOffsetLeft == 0) {
            lp.leftMargin = newLeftMargin;
            lp.width = newWidth;
        } else {
            lp.leftMargin = newLeftMargin + OFFSET_4_DP;
            lp.width = newWidth-OFFSET_4_DP;
        }
        this.setLayoutParams(lp);
    }

    // Обновление параметров задачи по данным итема
    public void updateLineForCurentParams() {
        if (line != null) {
            if (line.getDeadlineDateStart() != null) {
                line.getDeadlineDateStart().set(Calendar.HOUR_OF_DAY, getStartTimeHour());
                Calendar dateEnd = Calendar.getInstance();
                dateEnd.setTimeInMillis(line.getDeadlineDateStart().getTimeInMillis());
                dateEnd.add(Calendar.MINUTE, line.getDuration());
                line.setDeadlineDateEnd(dateEnd);
            }

            line.updateDatesTextByCalendar();
        }
    }

    public void updateTimeByLine() {
        if (getLine() != null && getLine().getDeadlineDateStart() != null) {
            setStartTimeHour(getLine().getDeadlineDateStart().get(Calendar.HOUR_OF_DAY));
        }

        if (getLine() != null && getLine().getDeadlineDateEnd() != null) {
            setEndTimeHour(getLine().getDeadlineDateEnd().get(Calendar.HOUR_OF_DAY));
        }
    }

    //==========================================
    // Geter seter
    //==========================================
    @Override
    public Object getTag() {
        if (line != null) {
            return line.getServerId();
        }
        return super.getTag();
    }

    public int getStartTimeHour() {
        if (line != null && line.getDeadlineDateStart() != null) {
            return line.getDeadlineDateStart().get(Calendar.HOUR_OF_DAY);
        }
        return startTimeHour;
    }

    public void setStartTimeHour(int startTimeHour) {
        this.startTimeHour = startTimeHour;
    }

    public int getEndTimeHour() {
        if (line != null && line.getDeadlineDateEnd() != null) {
            return line.getDeadlineDateEnd().get(Calendar.HOUR_OF_DAY);
        }
        return endTimeHour;
    }

    public void setEndTimeHour(int endTimeHour) {
        this.endTimeHour = endTimeHour;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public LayoutParams getmLayoutParams() {
        return mLayoutParams;
    }

    public void setmLayoutParams(LayoutParams mLayoutParams) {
        this.mLayoutParams = mLayoutParams;
    }

    public TextView getTitleAssign() {
        if (titleAssign == null) {
            setTitleAssign((TextView) findViewById(R.id.title_assign));
        }
        return titleAssign;
    }

    public void setTitleAssign(TextView titleAssign) {
        this.titleAssign = titleAssign;
    }
}