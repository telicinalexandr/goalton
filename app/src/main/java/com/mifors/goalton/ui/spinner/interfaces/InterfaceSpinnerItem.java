package com.mifors.goalton.ui.spinner.interfaces;

/**
 * Created by gly8 on 23.03.17.
 */

@SuppressWarnings("ALL")
public interface InterfaceSpinnerItem {
    String getName();
    void setName(String name);
    String getValue(); // Значение по которому определяется уникальность итема
    void setValue(String value);
}
