package com.mifors.goalton.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.widget.EditText;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;

import java.util.ArrayList;

/**
 * Created by gly8 on 29.07.17.
 */

@SuppressWarnings("ALL")
public class EditTextOutline extends EditText {
    private static final String TAG = "Goalton [" + EditTextOutline.class.getSimpleName() + "]";
    private final String SPAN_START = "<span class";
    private final String SPAN_BG1 = "bg1"; // ffa49c
    private final String SPAN_BG2 = "bg2"; // f4eb83
    private final String SPAN_BG3 = "bg3"; // c6e479
    private final String SPAN_BOLD = "bold";
    private final String SPAN_END = "</span>";
    private ArrayList<Spann> listSpanables = new ArrayList<>();
    private ArrayList<Spann> tempList = new ArrayList<>();

    public EditTextOutline(Context context) {
        super(context);
    }

    public EditTextOutline(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextOutline(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        if (!TextUtils.isEmpty(text)) {
            // Объект у которого будем менять цвет
            String str = text.toString().replace("<br />", "");
            try {
                char[] arr = str.toCharArray();
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < arr.length; i++) {
                    char c = arr[i];
                    switch (c) {
                        case '<':
                            builder.append(c);
                            break;
                        case '>':
                            builder.append(c);
                            if (builder.toString().equals(SPAN_END)) {
                                updateLastSpan(i, builder.toString());
                            } else if (builder.toString().contains(SPAN_START)) {
                                // запоминаем позицию
                                // запоминаем собраную строку
                                Spann spann = new Spann(builder.toString(), i - (builder.length() - 1));
                                listSpanables.add(spann);
                            }

                            builder.setLength(0);
                            break;
                        case '/':
                            if (builder.length() == 1) {
                                builder.append(c);
                            }
                            break;
                        default:
                            if (builder.length() > 0) {
                                builder.append(c);
                            }
                            break;
                    }
                }

                String htmlString = fromHtml(str).toString();
                SpannableStringBuilder textSpanable = new SpannableStringBuilder(str);
//                SpannableStringBuilder textSpanable = new SpannableStringBuilder(htmlString);

//                for (Spann s : listSpanables) {
//                    tempList.add(new Spann(s.getId(), s.getStartText(), s.getStartPosition(), s.getEndText(), s.getEndPosition(), s.getLengthText()));
//                }
//
//                read(listSpanables);
//                read(tempList);
//
//                for (Spann listSpanable : listSpanables) {
//                    updateLength(listSpanable);
//                }

//                for (int i = 0; i < listSpanables.size(); i++) {
//                    Spann item = listSpanables.get(i);
//                    int childsLength = countItemsToChild(item);
//                    int leftLength = countItemsToLeft(item.getStartPosition());
//                    int rightLength = getLengthRight(item);
//
//                    if (i == 0) {
//                        item.setEndPosition(item.getEndPosition()-item.getLengthText());
//                    } else {
//                        item.setEndPosition(item.getEndPosition()-leftLength);
//                    }
//
//                    Log.v(TAG, "[setText] leftLength = "+leftLength+"  rightLength = "+rightLength+"  childsLegth = "+childsLength+"  item = "+item);
////                    Log.v(TAG, "[setText] item = " + item + "  childsLength = " + childsLength + "   leftLength = " + leftLength + "  deleteLength = " + deletLength+"  rightLength = "+rightLength);
////                    if (item.getStartPosition() > 0) {
////                        item.setStartPosition(item.getStartPosition() - deletLength);
////                    }
////                    item.setEndPosition(item.getEndPosition() - deletLength);
//                }

//                read(listSpanables);

                for (Spann item : tempList) {

                    if (item.getStartText().contains(SPAN_BG1)) {
                        textSpanable.setSpan(new BackgroundColorSpan(ContextCompat.getColor(getContext(), R.color.red_light)), item.getStartPosition(), item.getEndPosition(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (item.getStartText().contains(SPAN_BG2)) {
                        textSpanable.setSpan(new BackgroundColorSpan(ContextCompat.getColor(getContext(), R.color.yellow_light)), item.getStartPosition(), item.getEndPosition(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (item.getStartText().contains(SPAN_BG3)) {
                        textSpanable.setSpan(new BackgroundColorSpan(ContextCompat.getColor(getContext(), R.color.green_light)), item.getStartPosition(), item.getEndPosition(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (item.getStartText().contains(SPAN_BOLD)) {
                        textSpanable.setSpan(new StyleSpan(Typeface.BOLD), item.getStartPosition(), item.getEndPosition(), 0);
                    }
                }

//                Log.v(TAG, "[setText] listItems = " + listSpanables);
//                updateSpann(textSpanable);
//                Log.v(TAG, "[setText]******************************");
//                Log.v(TAG, "[setText] listItems = " + listSpanables);
                super.setText(textSpanable, type);
            } catch (Exception e) {
            }
        } else {
            super.setText(text, type);
        }
    }

    private void updateLength(Spann s) {
        for (Spann spann : tempList) {
            if (spann.getStartPosition() >= s.getStartPosition() && spann.getStartPosition() > 0) {
                spann.setStartPosition(spann.getStartPosition() - s.getLengthText());
            }

            spann.setEndPosition(spann.getEndPosition() - s.getLengthText());
        }
    }

    private void read(ArrayList<Spann> arr) {
    }

    private int getLengthRight(Spann item) {
        int indexOff = listSpanables.indexOf(item);
        int length = 0;
        for (int i = indexOff; i < listSpanables.size(); i++) {
            Spann spann = listSpanables.get(i);
            if (spann.getStartPosition() > item.getEndPosition()) {
                length += spann.getLengthText();
            }
        }

        return length;
    }

    private void updateSpann(SpannableStringBuilder spann) {
        if (listSpanables.size() > 0) {
            int counter = 0;
            do {
                Spann item = listSpanables.get(counter);
                counter++;
            } while (counter < listSpanables.size());

        }
    }

    private void removingLengthFromAnotherItems(Spann item) {
        for (Spann listSpanable : listSpanables) {
            if (listSpanable.getId() != item.getId()) {
                listSpanable.setStartPosition(listSpanable.getStartPosition() - item.getLengthText());
                listSpanable.setEndPosition(listSpanable.getEndPosition() - item.getLengthText());
            }
        }
    }

    private int countItemsToChild(Spann span) {
        int lengthChild = 0;
        for (Spann item : listSpanables) {
            if (item.getStartPosition() >= span.getStartPosition() && item.getEndPosition() <= span.getEndPosition()
                    && item.getId() != span.getId()) {
                lengthChild += item.getLengthText();
            }
        }
        return lengthChild;
    }

    // количество детей слева от передаваемой позиции
    private int countItemsToLeft(int start) {
        int lengthChild = 0;
        for (Spann listSpanable : listSpanables) {
            if (listSpanable.getStartPosition() < start) {
                lengthChild += listSpanable.getStartText().length();
                lengthChild += listSpanable.getEndText().length();
            }
        }

        return lengthChild;
    }

    // количество детей слева от передаваемой позиции c открытым и закрытым тегом
    private int countItemsToLeftFull(int start) {
        int lengthRemove = 0;
        for (Spann listSpanable : listSpanables) {
            if (listSpanable.getStartPosition() < start) {
                lengthRemove += listSpanable.getStartText().length();
            }

            if (listSpanable.getEndPosition() < start) {
                lengthRemove += listSpanable.getEndText().length();
            }
        }

        return lengthRemove;
    }

    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    private Spann updateLastSpan(int position, String text) {
        for (int i = listSpanables.size() - 1; i >= 0; i--) {
            if (TextUtils.isEmpty(listSpanables.get(i).getEndText())) {
                listSpanables.get(i).setEndPosition(position + 1);
                listSpanables.get(i).setEndText(text);
                break;
            }
        }

        return null;
    }


    class Spann {
        long id;
        private String startText;
        private int startPosition;
        private String endText;

        private int endPosition;
        private int lengthText;

        public Spann(String startText, int startPosition) {
            this.startText = startText;
            this.startPosition = startPosition;
            this.id = ManagerApplication.getRandomLong();
        }

        public Spann(long id, String startText, int startPosition, String endText, int endPosition, int lengthText) {
            this.id = id;
            this.startText = startText;
            this.startPosition = startPosition;
            this.endText = endText;
            this.endPosition = endPosition;
            this.lengthText = lengthText;
        }

        @Override
        public String toString() {
            return startText + "  " + startPosition + "   " + endText + "  " + endPosition + "   " + lengthText;
        }

        public String getEndText() {
            return endText;
        }

        public void setEndText(String endText) {
            this.endText = endText;
            lengthText = startText.length() + endText.length();
        }

        public int getEndPosition() {
            return endPosition;
        }

        public void setEndPosition(int endPosition) {
            this.endPosition = endPosition;
        }

        public String getStartText() {
            return startText;
        }

        public int getStartPosition() {
            return startPosition;
        }

        public int getLengthText() {
            return lengthText;
        }

        public void setStartPosition(int startPosition) {
            this.startPosition = startPosition;
        }

        private void changePosition(int lengthRemove) {
            if (startPosition > 0) {
                setStartPosition(startPosition - lengthRemove);
            }

            setEndPosition(endPosition - lengthRemove);
        }

        public long getId() {
            return id;
        }
    }

    public void editSelectedText(String tag) {
        int startSelection = getSelectionStart();
        int endSelection = getSelectionEnd();

        String selectedText = getText().toString().substring(startSelection, endSelection);

        switch (tag) {
            case "bg1":
                break;
            case "bg2":
                break;
            case "bg3":
                break;
            case "bold":
                break;
            case "clear":
                break;
        }
    }
}