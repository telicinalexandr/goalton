package com.mifors.goalton.ui.spinner.view;


import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

/**
 * Created by gly8 on 23.03.17.
 */
@SuppressWarnings("ALL")
public class ItemSpinnerColors implements InterfaceSpinnerItem {
    String name;
    String value;
    int id;

    public ItemSpinnerColors() {
    }

    public ItemSpinnerColors(String name, int id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
