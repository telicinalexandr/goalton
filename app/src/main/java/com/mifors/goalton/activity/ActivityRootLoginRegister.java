package com.mifors.goalton.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.interfaces.InterfaceCallbackResponseServer;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.network.HttpParam;

/**
 * Нужна для обработки ошибок запроса сервера
 * Показ ошибок под полями ввода
 * Валидация данных username, email, password.
 *
 * Created by gly8 on 24.03.17.
 */
public class ActivityRootLoginRegister extends AppCompatActivity implements InterfaceCallbackResponseServer {
    private static final String TAG = "Goalton [" + ActivityRootLoginRegister.class.getSimpleName() + "]";

    public boolean checkValidData(String email){
        if (TextUtils.isEmpty(email)) {
            ManagerApplication.getInstance().showToast(R.string.please_check_data);
            return false;
        }

        if (!ManagerApplication.isValidEmail(email)) {
            ManagerApplication.getInstance().showToast(R.string.error_valid_email);
            return false;
        }
        return true;
    }

    public boolean checkValidData(String userName, String password) {
        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password)) {
            ManagerApplication.getInstance().showToast(R.string.please_check_data);
            return false;
        }

        int MIN_LENGTH_USERNAME = 2;
        if (userName.length() < MIN_LENGTH_USERNAME) {
            ManagerApplication.getInstance().showToast(R.string.login_register_error_length_username);
            return false;
        }

        int MIN_LENGTH_PASSWORD = 6;
        if (password.length() < MIN_LENGTH_PASSWORD) {
            ManagerApplication.getInstance().showToast(R.string.login_register_error_length_password);
            return false;
        }
        return true;
    }

    public boolean checkValidData(String userName, String password, String email) {
        return checkValidData(userName, password) && checkValidData(email);
    }

    @Override
    public void completeResponse(String fieldError, String valueError) {
        ManagerProgressDialog.dismiss();
        switch (fieldError) {
            case InterfaceCallbackResponseServer.KEY_ERROR_EMAIL:
                showTitleError(R.id.title_error_email, valueError);
                break;
            case InterfaceCallbackResponseServer.KEY_ERROR_USERNAME:
                showTitleError(R.id.title_error_user_name, valueError);
                break;
            case InterfaceCallbackResponseServer.KEY_END_RESPONSE:
                openActivityMain();
                break;
            case InterfaceCallbackResponseServer.KEY_ERROR_PASSWORD:
                showTitleError(R.id.title_error_password, valueError);
                break;
            case InterfaceCallbackResponseServer.KEY_ERROR_RESPONSE:
                break;
            case InterfaceCallbackResponseServer.KEY_FINISH_ACTIVITY:
                finish();
                break;
        }
    }

    public void openActivityMain() {
        ManagerApplication.hideKeyboard(this);
        Intent intent = new Intent(this, ActivityMain.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /**
     * Показ ошибки под полем ввода
     *
     * @param idTitle - id лейбла ошибки
     * @param value   - описание ошибки
     */
    private void showTitleError(int idTitle, String value) {
        TextView titleErrorEmail = (TextView) findViewById(idTitle);
        titleErrorEmail.setVisibility(View.VISIBLE);
        titleErrorEmail.setText(value);
        ViewGroup viewGroup = (ViewGroup) titleErrorEmail.getParent();
        if (viewGroup != null) {
            View view = viewGroup.getChildAt(0);
            if (view instanceof EditText) {
                changeViewBg(view, R.drawable.shape_round_view_white_border_red);
            }
        }
    }

    private View.OnTouchListener onTouchEdit = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    changeViewBg(v, R.drawable.shape_round_view_white_border_gray);
                    break;
                case MotionEvent.ACTION_UP:
                    break;
            }
            return false;
        }
    };

    /**
     * Меняет фон Выбранной вьюхи
     *
     * @param view        - edittext у которого надо поменять фон
     * @param resDrawable - ресурс фона
     */
    private void changeViewBg(View view, int resDrawable) {
        view.setBackgroundResource(resDrawable);
        view.setOnTouchListener(onTouchEdit);
    }

    /**
     * Скрывает все ранее показанные ошибки (неправильный email, password и т.д)
     * Сбрасывает поля ввода текста до дефолтного цвета
     * Для того чтобы этот метод работал поля должны быть обернуты в контейнер с id container_fileds
     */
    public void hideAllTitlesError() {
        try {
            LinearLayout containerFields = (LinearLayout) findViewById(R.id.container_fileds);
            for (int i = 0; i < containerFields.getChildCount(); i++) {
                LinearLayout containerField = (LinearLayout) containerFields.getChildAt(i);
                changeViewBg(containerField.getChildAt(0), R.drawable.shape_round_view_white_border_gray); // Сбрасываем EditText
                containerField.getChildAt(1).setVisibility(View.GONE); // Прячем заголовок ошибки
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startLoginUser(String username, String password){
        hideAllTitlesError();
        ManagerProgressDialog.showProgressBar(this, R.string.authorized, false);
        ManagerAccount.login(username, password, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                if (msg.what == HttpParam.RESPONSE_OK) {
                    checkResponse(msg);
                    ManagerMyTeams.responseGetAccounts(null);
                } else {
                    ManagerApplication.getInstance().showToast(R.string.authorized_error);
                }
            }
        });
    }

    private void checkResponse(Message msg) {
        if (ManagerJSONParsing.isCheckStatusServerResponse(msg.obj.toString(), this)) {
            ManagerAccount.getDataUser(callbackGetUser);
            ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.IS_LOGIN_USER, true);
        }
    }

    private Handler callbackGetUser = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpParam.RESPONSE_OK){
            }
            openActivityMain();
        }
    };
}