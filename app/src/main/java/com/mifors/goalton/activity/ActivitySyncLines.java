package com.mifors.goalton.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.utils.ServiceSyncTasks;

import java.util.List;

/**
 * Created by gly8 on 07.11.17.
 */

public class ActivitySyncLines extends Activity {
    public static boolean isRunActivity = false;
    private static final String TAG = "Goalton [" + ActivitySyncLines.class.getSimpleName() + "]";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_task);
        TextView title = findViewById(R.id.title_sync_task);

        int countOfflineLines = Line.getOfflineLinesCount();
        int countOfflineChangeSort = Line.getOfflineLinesUpdateSortCount();

        String titleStr;
        if (countOfflineLines == 0) {
            titleStr = String.format(getString(R.string.message_alread_not_sync_lines), String.valueOf(countOfflineChangeSort));
        } else {
            titleStr = String.format(getString(R.string.message_alread_not_sync_lines), String.valueOf(countOfflineLines));
        }

        title.setText(titleStr);
        isRunActivity = true;
        IntentFilter intent = new IntentFilter();
        intent.addAction(ServiceSyncTasks.ACTION_SYNC);
        registerReceiver(broadcastReceiverSendComplete, intent);
    }

    private BroadcastReceiver broadcastReceiverSendComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(ServiceSyncTasks.EXTRA_KEY_IS_FINISH, false)) {
                ManagerProgressDialog.dismiss();
                ManagerApplication.getInstance().showToast(R.string.sync_task_compelte);
                closeActivity();
            } else if (intent.getBooleanExtra(ServiceSyncTasks.EXTRA_KEY_IS_UPDATE, false)) {
                changeTitleByIntent(intent);
            }
        }
    };

    public void onClickYes(View view) {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ManagerApplication.getInstance().startService(new Intent(this, ServiceSyncTasks.class));
            ManagerProgressDialog.showProgressBar(this, R.string.sync_task, false);
        }
    }

    public void onClickNo(View view) {
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            ManagerProgressDialog.showProgressBar(this, R.string.saves_changes, false);
            ActiveAndroid.beginTransaction();
            try {
                List<Line> listsort = Line.getOfflineLinesUpdateSort();
                for (Line line : listsort) {
                    line.setOfflineStatusSort(null);
                    line.save();
                }

                List<Line> listUpdate = Line.getOfflineLines();
                for (Line line : listUpdate) {
                    if (line.getOfflineStatus() == Line.StatusOffline.CREATE) {
                        line.delete();
                        line.save();
                    } else {
                        line.setOfflineId(0);
                        line.setOfflineStatus(null);
                        line.setOfflineStatusSort(null);
                        line.save();
                    }
                }
                ActiveAndroid.setTransactionSuccessful();
            } finally {
                ActiveAndroid.endTransaction();
                ManagerApplication.getInstance().startService(new Intent(this, ServiceSyncTasks.class));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isRunActivity = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunActivity = false;
        unregisterReceiver(broadcastReceiverSendComplete);
    }

    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        return super.bindService(service, conn, flags);
    }

    private void closeActivity() {
        isRunActivity = false;
        finish();
    }

    // Передается какая линия сейчас синхронизируется
    private void changeTitleByIntent(Intent intent) {
        String nameTask = intent.getStringExtra(ServiceSyncTasks.EXTRA_KEY_NAME_TASK_UPDATE);
        if (nameTask.length() > 15) {
            nameTask = nameTask.substring(0, 13) + "...";
        }
        String nameProject = "---";
        long projectId = intent.getLongExtra(ServiceSyncTasks.EXTRA_KEY_PROJECT_ID_UPDATE, 0);
        if (projectId != 0) {
            Project project = Project.findByServerId(Project.class, projectId);
            if (project != null) {
                nameProject = project.getName();
            }
        }

        String newTitle = String.format(getString(R.string.sync_task_status), nameTask, nameProject);
        if (!ManagerProgressDialog.changeTitleIsSet(newTitle)) {
            ManagerProgressDialog.showProgressBar(ActivitySyncLines.this, newTitle, false);
        }
    }
}
