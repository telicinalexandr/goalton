package com.mifors.goalton.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.mifors.goalton.R;
import com.mifors.goalton.interfaces.InterfaceCallbackResponseServer;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;

/**
 * Created by gly8 on 22.03.17.
 */

@SuppressWarnings("ALL")
public class ActivityRegister extends ActivityRootLoginRegister {

    private static final String TAG = "Goalton [" + ActivityRegister.class.getSimpleName() + "]";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void onClickRegister(View view) {
        String username = ((EditText) findViewById(R.id.edit_username)).getText().toString();
        String password = ((EditText) findViewById(R.id.edit_password)).getText().toString();
        String email = ((EditText) findViewById(R.id.edit_email)).getText().toString();

        if (checkValidData(username, password, email)) {
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                hideAllTitlesError();
                ManagerProgressDialog.showProgressBar(this, R.string.register, false);
                ManagerAccount.init(new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        startResponseRegister();
                    }
                });
            }
        }
    }

    public void onClickAuthFacebook(View view) {
    }

    private void startResponseRegister() {
        final String username = ((EditText) findViewById(R.id.edit_username)).getText().toString();
        final String password = ((EditText) findViewById(R.id.edit_password)).getText().toString();
        final String email = ((EditText) findViewById(R.id.edit_email)).getText().toString();

        ManagerAccount.register(username, email, password, new InterfaceCallbackResponseServer() {
            @Override
            public void completeResponse(String fieldError, String valueError) {
                switch (fieldError) {
                    case InterfaceCallbackResponseServer.KEY_END_RESPONSE:
                        startLoginUser(username, password);
                        break;
                    default:
                        callingInterfaceCompleteResponse(fieldError, valueError);
                        break;
                }
            }
        });
    }

    private void callingInterfaceCompleteResponse(String fieldError, String valueError) {
        completeResponse(fieldError, valueError);
    }

    public void onClickAuthGoogle(View view) {
    }

    public void onClickAuthVk(View view) {
    }
}