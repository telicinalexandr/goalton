package com.mifors.goalton.activity;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.adapters.AdapterViewPagerProjectSettings;
import com.mifors.goalton.fragment.projects.FragmentProjectSettings;
import com.mifors.goalton.fragment.projects.FragmentProjectAccess;
import com.mifors.goalton.fragment.projects.FragmentProjectStages;
import com.mifors.goalton.interfaces.InterfaceFragmentProjectSettings;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.managers.utils.ManagerTakePhoto;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.utils.ObservableCustom;
import com.mifors.goalton.utils.exceptions.ErrorCreateProjectSettings;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;

public class ActivitySettingProject extends ActivityRootSelectedBackground {
    private static final String TAG = "Goalton [" + ActivitySettingProject.class.getSimpleName() + "]";
    public static final String KEY_EXTRA_PROJECT_ID = "PROJECT_ID_KEY";
    public static final String KEY_EXTRA_IS_NOTIFYDATA_LIST_PROJECTS = "KEY_EXTRA_IS_NOTIFYDATA_LIST_PROJECTS"; // Надо ли полностью обновлять список

    public static final String PROJECT_ID_KEY = "PROJECT_ID_KEY";
    public static final String TEAM_ID = "TEAM_ID";
    public static final String OWNER_USER_ID = "OWNER_USER_ID";
    private boolean isUpdateData = false;
    private AdapterViewPagerProjectSettings adapter;
    private ObservableCustom observable;
    private FragmentProjectSettings fragmentProjectSettings;
    private Project project;

    private List<WeakReference<Fragment>> fragList = new ArrayList<>();

    @SuppressWarnings("unchecked")
    @Override
    public void onAttachFragment(Fragment fragment) {
        fragList.add(new WeakReference(fragment));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_project);
        long projectId = getIntent().getLongExtra(PROJECT_ID_KEY, 0);
        project = Project.findByServerId(Project.class, projectId);
        ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_SETTINGS, projectId);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_project_settings);
        setSupportActionBar(toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.projects_tab_layout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.projects_viewpager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getText().equals(getString(R.string.tab_project_settings))) {
                } else if (tab.getText().equals(getString(R.string.tab_project_stages))) {
                    ManagerApplication.hideKeyboard(ActivitySettingProject.this);
                } else if (tab.getText().equals(getString(R.string.teams))) {
                    ManagerApplication.hideKeyboard(ActivitySettingProject.this);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_project_settings);
        mActionBarToolbar.setTitle("");
        setSupportActionBar(mActionBarToolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ManagerTakePhoto.REQUEST_IMAGE_CAPTURE || requestCode == ManagerTakePhoto.REQUEST_CODE_GALLERY) {
                if (fragmentProjectSettings.getActivity() != null) {
                    fragmentProjectSettings.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (fragmentProjectSettings != null) {
            fragmentProjectSettings.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //==========================================
    // ONCLICK
    //==========================================
    @Override
    public void onBackPressed() {
        onClickBack(null);
    }

    public void onClickBack(View view) {
        if (isUpdateData) {
            closeActivityReloadProjectsList();
        } else {
            finish();
        }

    }

    public void onClickOk(View view) {
        // Проверка если проект новый то вызываем сначало создание
        if (ManagerApplication.getInstance().isConnectToInternet(true)) {
            responseUpdate();
        }
    }

    //==========================================
    // Response
    //==========================================
    private void responseUpdate() {
        MultipartBody.Builder builderMultipart = new MultipartBody.Builder();
        builderMultipart.setType(MultipartBody.FORM);
        builderMultipart.addFormDataPart("_csrf", HttpParam.getCsrfToken());
        ManagerProgressDialog.showProgressBar(this, R.string.update_data, true);
        boolean isError = false;
        try {
            // Получаем данные для отправки на сервер
            for (int i = 0; i < adapter.getCount(); i++) {
                ((InterfaceFragmentProjectSettings) adapter.getItem(i)).getSettings(builderMultipart);
            }

        } catch (ErrorCreateProjectSettings e) {
            ManagerProgressDialog.dismiss();
            isError = true;
            switch (e.getMessage()) {
                case ErrorCreateProjectSettings.EMPTY_NAME:
                    ManagerApplication.getInstance().showToast(R.string.error_empty_project_name);
                    break;
            }
        }

        if (!isError) {
            if (project.getServerId() == 0) {
                // Создание нового проекта
                responseCreate(builderMultipart);
            } else {
                // Обновление данных проекта
                ManagerProjects.responseUpdateSettings(builderMultipart, project.getServerId(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        for (int i = 0; i < adapter.getCount(); i++) {
                            ((InterfaceFragmentProjectSettings) adapter.getItem(i)).updateInterface();
                        }
                        ManagerApplication.getInstance().showToast(R.string.update_data_complete);
                        ManagerMyTeams.responseGetAccounts(null);
                        isUpdateData = true;
                    }
                });
            }
        }
    }

    private void responseCreate(MultipartBody.Builder builderMultipart) {
        ManagerProjects.responseCreateProject(builderMultipart, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                if (msg.what == HttpParam.RESPONSE_OK) {
                    ManagerProjects.deleteLastNewProject(); // Удаление данных созданого проекта
                    closeActivityReloadProjectsList();
                } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                    ManagerApplication.getInstance().showToast(R.string.error_create_new_project);
                }
            }
        });
    }

    // Закрытие окна и вызов перезагрузки списка проектов
    // для получения только что созданного с сервера
    private void closeActivityReloadProjectsList() {
        ManagerApplication.hideKeyboard(this);
        Intent intent = new Intent();
        intent.putExtra(KEY_EXTRA_IS_NOTIFYDATA_LIST_PROJECTS, true);
        setResult(ActivityMain.REQUEST_CODE_RELOAD_PROJECTS, intent);
        finish();
    }

    //==========================================
    // UI
    //==========================================
    private void setupViewPager(ViewPager viewPager) {
        adapter = new AdapterViewPagerProjectSettings(getSupportFragmentManager());
        try{
            Bundle bundle = new Bundle();
            bundle.putLong(PROJECT_ID_KEY, project.getServerId());
            bundle.putLong(TEAM_ID, project.getTeamId());
            bundle.putLong(OWNER_USER_ID, project.getOwnerUserId());

            observable = new ObservableCustom();
            fragmentProjectSettings = new FragmentProjectSettings();
            fragmentProjectSettings.setArguments(bundle);
            adapter.addFragment(fragmentProjectSettings, getString(R.string.tab_project_settings));

            FragmentProjectStages fragmentProjectStages = new FragmentProjectStages();
            fragmentProjectStages.setArguments(bundle);
            adapter.addFragment(fragmentProjectStages, getString(R.string.tab_project_stages));

            FragmentProjectAccess fragmentProjectAccess = new FragmentProjectAccess();
            fragmentProjectAccess.setArguments(bundle);
            adapter.addFragment(fragmentProjectAccess, getString(R.string.access));

            observable.addObserver(fragmentProjectSettings);
            observable.addObserver(fragmentProjectStages);
            observable.addObserver(fragmentProjectAccess);

            viewPager.setOffscreenPageLimit(3);
            viewPager.setAdapter(adapter);
            viewPager.post(new Runnable() {
                @Override
                public void run() {
                    observable.setChangeComplete();
                    observable.notifyObservers();
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }
}