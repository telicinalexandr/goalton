package com.mifors.goalton.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.core.ManagerAccount;

/**
 * Created by gly8 on 20.03.17.
 */

@SuppressWarnings("ALL")
public class ActivitySplash extends ActivityRootLoginRegister {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        startActivity(new Intent(this, ExampleActivity.class));
        Animation alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                openNextActivity();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        ImageView splash = (ImageView) findViewById(R.id.splash);
        splash.startAnimation(alphaAnimation);
    }

    private void openNextActivity() {
        if (ManagerAccount.isLogin()) {
            startActivity(new Intent(this, ActivityMain.class));
        } else {
            startActivity(new Intent(this, ActivityIntro.class));
        }
        finish();
    }
}
