package com.mifors.goalton.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.adapters.AdapterEditTeam;
import com.mifors.goalton.dialog.DialogEditTeamPermission;
import com.mifors.goalton.interfaces.InterfaceOnGroupMenuClickListener;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.model.team.Team;
import com.mifors.goalton.network.HttpParam;


public class ActivityEditTeam extends ActivityRootSelectedBackground implements InterfaceOnGroupMenuClickListener {
    private static final String TAG = "Goalton [" + ActivityEditTeam.class.getSimpleName() + "]";
    public static final String TEAM_ID = "TEAM_ID";

    Team team;
    EditText edName;
    ExpandableListView listView;
    ImageView btnEditName;
    TextView nameLabel;
    boolean editNameStatus = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_team);
        long id = 0;
        if (getIntent().hasExtra(TEAM_ID)) {
            id = getIntent().getLongExtra(TEAM_ID, 0);
        }
        if (id == 0) {
            finish();
        } else {
            team = Team.finByServerId(id);
        }
        initViews();
        if (team != null) {
            updateViews();
        }
    }

    private void initViews() {
        edName = (EditText) findViewById(R.id.edit_team_name);
        nameLabel = (TextView) findViewById(R.id.edit_team_name_label);
        listView = (ExpandableListView) findViewById(R.id.edit_team_list);
        listView.setOnChildClickListener(onChildClickListener);
        btnEditName = (ImageView) findViewById(R.id.edit_team_btn_edit);
        btnEditName.setOnClickListener(onEditNameClick);
        findViewById(R.id.btn_back).setOnClickListener(onBackClicked);
    }

    private void updateViews() {

        nameLabel.setText(team.getName());
        listView.setAdapter(new AdapterEditTeam(this, team.getProfiles(), this));
    }

    private ExpandableListView.OnChildClickListener onChildClickListener = new ExpandableListView.OnChildClickListener() {
        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            DialogEditTeamPermission dialog = new DialogEditTeamPermission();
            Bundle bundle = new Bundle();
            bundle.putLong(DialogEditTeamPermission.PROJECT_ID_KEY, team.getProfiles().get(groupPosition).getTeamProjects().get(childPosition).getProjectId());
            bundle.putLong(DialogEditTeamPermission.USER_TEAM_ID_KEY, team.getProfiles().get(groupPosition).getUsersTeamId());
            dialog.setArguments(bundle);
            dialog.show(getSupportFragmentManager(), "TAG");
            return true;
        }
    };

    private View.OnClickListener onBackClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    @Override
    public void onBackPressed() {
        if (editNameStatus) {
            btnEditName.callOnClick();
        } else {
            super.onBackPressed();
        }
    }

    private View.OnClickListener onEditNameClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!editNameStatus) {
                nameLabel.setVisibility(View.GONE);
                edName.setText(nameLabel.getText());
                edName.setVisibility(View.VISIBLE);
                edName.setSelection(edName.getText().length());
                edName.requestFocus();
                //show keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(edName, InputMethodManager.SHOW_IMPLICIT);

                btnEditName.setImageResource(R.drawable.ic_done);
                editNameStatus = true;
            } else {
                edName.setVisibility(View.GONE);
                nameLabel.setVisibility(View.VISIBLE);
                btnEditName.setImageResource(R.drawable.ic_edit);

                //hide keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edName.getWindowToken(), 0);
                if (!nameLabel.getText().toString().equals(edName.getText().toString())) {
                    saveTeamName();
                }
                editNameStatus = false;
            }
        }
    };

    private void saveTeamName() {
        ManagerMyTeams.responseChangeNameTheme(callbackChangeName, team.getJsonServerId(), edName.getText().toString().trim());
    }

    private Handler callbackChangeName = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpParam.RESPONSE_OK) {
                team.setName(edName.getText().toString().trim());
                ManagerApplication.getInstance().showToast(R.string.name_was_changed);
                nameLabel.setText(edName.getText().toString());
            }
        }
    };


    //////////////////////////////////////////////////////////////////
    //
    //// Показ контекстного меню

    @Override
    public void onClickMenuItem(View view, int position) {
        showPopupMenu(view, position);
    }

    private void showPopupMenu(View view, int position) {
        PopupMenu popup = new PopupMenu(view.getContext(), view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.context_menu_edit_team, popup.getMenu());
        if (team.getProfiles().get(position).getJsonServerId() == ManagerAccount.getMyProfile().getServerId()) {
            popup.getMenu().findItem(R.id.edit_team_delete_profile).setEnabled(false);
        }
        popup.setOnMenuItemClickListener(new ActivityEditTeam.ItemMenuClickListener(position));
        popup.show();
    }

    private class ItemMenuClickListener implements PopupMenu.OnMenuItemClickListener {
        private int position;

        ItemMenuClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getItemId() == R.id.edit_team_delete_profile) {
                ManagerMyTeams.responseUserUnlinkFromTeam(callbackUnlink, team.getProfiles().get(position).getUsersTeamId());
            }
            return true;
        }

        private Handler callbackUnlink = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
//                    TeamProfile.deleteByServerId(team.getProfiles().get(position).getJsonServerId());
                    team.getProfiles().remove(team.getProfiles().get(position));
                    updateViews();
                }
            }
        };
    }
    //
    //
    ///////////////////////////////////////////////////////////////////
}
