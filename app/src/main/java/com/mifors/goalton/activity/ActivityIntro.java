package com.mifors.goalton.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.fragment.FragmentSocialNetwork;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.network.HttpParam;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;

//import com.github.gorbin.asne.core.SocialNetwork;
//import com.github.gorbin.asne.core.SocialNetworkManager;
//import com.github.gorbin.asne.core.listener.OnLoginCompleteListener;
//import com.github.gorbin.asne.vk.VkSocialNetwork;
//import com.vk.sdk.VKScope;

/**
 * Created by Altair on 29.03.2017.
 */

@SuppressWarnings("ALL")
public class ActivityIntro extends AppCompatActivity implements FragmentSocialNetwork.ICompleteLogin {
    public static final String SOCIAL_NETWORK_TAG = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";

    public static final String TAG = "ActivityIntro";

    private String typeSocial, token, userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        intiView();
        FragmentSocialNetwork fragmentSocialNetwork = new FragmentSocialNetwork();
        fragmentSocialNetwork.setICompleteLogin(this);
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container_btns_social, fragmentSocialNetwork)
                .commit();
    }

    private void intiView() {
        TextView titleRemindPass = (TextView) findViewById(R.id.title_remind_pass);
        Spannable spannableRemindPass = new SpannableString(getString(R.string.remind_pass_title));

        if (ManagerApplication.getInstance().isEnglishLocale()) {
            spannableRemindPass.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_light)), 37, 46, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableRemindPass.setSpan(new UnderlineSpan(), 37, 46, 0);
        } else {
            spannableRemindPass.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_light)), 37, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableRemindPass.setSpan(new UnderlineSpan(), 37, 51, 0);
        }
        titleRemindPass.setText(spannableRemindPass);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                if (!TextUtils.isEmpty(res.email)) {
                    ManagerAccount.saveEmailVk(res.email);
                }

                if (!TextUtils.isEmpty(res.accessToken)) {
                    ManagerAccount.saveTokenVk(res.accessToken);
                }

                // Пользователь успешно авторизовался
                Log.v(TAG, "[onActivityResult] res.accesToken = " + res.accessToken + "   res = " + res);
                VKRequest request = VKApi.users().get();
                request.executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        //Do complete stuff
                        Log.v(TAG, "[onComplete] " + response.json + "  email = " + ManagerAccount.getEmailVk());
                        if (((VKList) response.parsedModel).get(0) != null) {
                            VKApiUserFull user = (VKApiUserFull) ((VKList) response.parsedModel).get(0);
                            completeLogin("vkontakte", ManagerAccount.getTokenVk(), String.valueOf(user.id), ManagerAccount.getEmailVk(), user.nickname, user.first_name, user.last_name);
                        }
                    }

                    @Override
                    public void onError(VKError error) {
                        Log.v(TAG, "[onError] error " + error);
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                        Log.v(TAG, "[attemptFailed] request " + request);
                    }
                });
            }

            @Override
            public void onError(VKError error) {
                // Произошла ошибка авторизации (например, пользователь запретил авторизацию)
                Log.v(TAG, "[onActivityResult] error = " + error);
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(SOCIAL_NETWORK_TAG);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        } else {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(SOCIAL_NETWORK_TAG);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    //====================================
    // On click
    //====================================

    public void onClickLogin(View view) {
        startActivity(new Intent(this, ActivityLogin.class));
    }

    public void onClickRegister(View view) {
        startActivity(new Intent(this, ActivityRegister.class));
    }

    public void onClickRemindPass(View view) {
        startActivity(new Intent(this, ActivityRemindPassword.class));
    }

    private void openMainActivity() {
        startActivity(new Intent(this, ActivityMain.class));
    }

    @Override
    public void completeLogin(String tSocial, String tok, String userID, String email, String name, String firstName, String lastName) {
        Log.v(TAG, "[completeLogin] typeSocial = " + typeSocial + "  token = " + token + "  userId = " + userID + "  email - " + email + "   name - " + name + "   firstName - " + firstName + "   lastName - " + lastName);
        ManagerAccount.responseLoginSocial(tSocial, tok, userID, email, name, firstName, lastName, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    ManagerMyTeams.responseGetAccounts(new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            if (msg.what == HttpParam.RESPONSE_OK) {
                                openMainActivity();
                            } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                                ManagerApplication.showToastStatic(R.string.error_auth_social);
                            }
                        }
                    });
                } else {
                    ManagerApplication.showToastStatic(R.string.error_auth_social);
                }
            }
        });
    }

    @Override
    public void loginVk() {
        String[] arr = new String[]{"email"};
        VKSdk.login(this, arr);
    }
}



