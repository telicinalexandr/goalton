package com.mifors.goalton.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.core.ManagerAccount;

/**
 * Created by gly8 on 24.03.17.
 */

@SuppressWarnings("ALL")
public class ActivityRemindPassword extends ActivityRootLoginRegister {
    private static final String TAG = "Goalton [" + ActivityRemindPassword.class.getSimpleName() + "]";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remind_password);
    }

    public void onClickSend(View view) {
        String email = ((EditText) findViewById(R.id.edit_email)).getText().toString();
        if (checkValidData(email)) {
            hideAllTitlesError();
            ManagerAccount.remindPassword(email, this);
        }
    }

}
