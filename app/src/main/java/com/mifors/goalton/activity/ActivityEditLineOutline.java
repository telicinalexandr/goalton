package com.mifors.goalton.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.TimePicker;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.interfaces.InterfaceManagerCreateLine;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerEditLine;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.core.ManagerSpinnerGenerationData;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.managers.utils.ManagerTakePhoto;
import com.mifors.goalton.model.Company;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectAssign;
import com.mifors.goalton.model.project.ProjectDeals;
import com.mifors.goalton.model.project.ProjectStage;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.ui.EditTextOutline;
import com.mifors.goalton.ui.spinner.adapters.AdapterSpinner;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItemColors;
import com.mifors.goalton.utils.OutlineLineUIHelper;
import com.mifors.goalton.utils.notifications.ReciverTimeNotifications;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import static com.mifors.goalton.activity.ActivityEditLineOutlineImage.RESULT_CODE_DELETE_IMAGE;
import static com.mifors.goalton.activity.ActivityEditLineOutlineImage.RESULT_CODE_UPDATE_IMAGE;

/**
 * Created by gly8 on 17.04.17.
 */

@SuppressWarnings("ALL")
public class ActivityEditLineOutline extends AppCompatActivity implements InterfaceManagerCreateLine
{
    private static final String TAG = "Goalton [" + ActivityEditLineOutline.class.getSimpleName() + "]";

    public static final String EXTRA_KEY_LINE_ID = "EXTRA_KEY_LINE_ID";
    public static final String EXTRA_KEY_PARENT_ID = "EXTRA_KEY_PARENT_ID";
    public static final String EXTRA_KEY_PROJECT_ID = "EXTRA_KEY_PROJECT_ID";
    public static final String EXTRA_KEY_IMAGE_BASE_64 = "EXTRA_KEY_IMAGE_BASE_64";
    public static final String EXTRA_KEY_DATE_CREATED = "EXTRA_KEY_DATE_CREATED";
    public static final String EXTRA_KEY_IS_OPEN_FROM_CALENDAR = "EXTRA_KEY_IS_OPEN_FROM_CALENDAR";
    public static final String EXTRA_KEY_MODE = "EXTRA_KEY_MODE";

    private static final String TAG_CHECK_TRUE = "1";
    private static final String TAG_CHECK_FALSE = "0";

    public static final int REQUEST_CODE_LINE_EDIT = 5000;
    public static final int RESULT_CODE_LINE_DELETE_OUTLINE = 100;
    public static final int RESULT_CODE_LINE_DELETE_CALENDAR = 101;
    public static final int RESULT_CODE_LINE_UPDATE = 102;
    public static final int RESULT_CODE_LINE_CREATE = 103;
    public static final int RESULT_CODE_UPDATE_OUTLINE = 104;
    private int RESULT_CODE;

    public static enum Mode {
        CREATE_LINE_OUTLINE,
        UPDATE_LINE_OUTLINE,
        CREATE_LINE_CALENDAR,
        UPDATE_LINE_CALENDAR;

        public static Mode convertStringToEnum(String myEnumString) {
            try {
                return valueOf(myEnumString);
            } catch (Exception ex) {
                return CREATE_LINE_OUTLINE;
            }
        }
    }

    private Line currentLine;
    private Spinner spinnerStages, spinnerAssigns, spinnerContact, spinnerDuration, spinnerProjects, spinnerDeals;
    private ImageView imageAtach;
    private RelativeLayout containerImage;
    private TextView titleDeadlineDate;
    private EditTextOutline editTextOutline;
    private ImageView btnImportant, btnCompleted;
    private LinearLayout containerSpinnerProjects, containerSpinnerDeals;
    private View containerColor, containerDateSet;
    private ViewGroup containerSelectedColor;
    private Calendar deadlineDate = null;
    private AdapterSpinner adapterSpinnerAssign, adapterSpinnerStage, adapterSpinnerContacts, adapterSpinnerDuration, adapterSpinnerProjects, adapterSpinnerDeals;
    private ManagerTakePhoto managerTakePhoto;
    private ScrollView scrollView;

    private int color = Integer.MIN_VALUE;
    private long projectId = Long.MIN_VALUE;
    private long parentId = 0; // Открытие задачи из списка проекта, какая задача в фокусе
    private long timeDateCreated;

    private OutlineLineUIHelper outlineLineUIHelper;
    private Mode mode;
    private Intent intentResult;

    private InterfaceSpinnerItemColors emptyColor = new InterfaceSpinnerItemColors() {
        @Override
        public int getResColor() {
            return 0;
        }

        @Override
        public void setResColor(int name) {

        }

        @Override
        public String getValue() {
            return null;
        }

        @Override
        public void setValue(String value) {

        }
    };

    private InterfaceSpinnerItem emptyItem = new InterfaceSpinnerItem() {
        @Override
        public String getName() {
            return "";
        }

        @Override
        public void setName(String name) {

        }

        @Override
        public String getValue() {
            return "";
        }

        @Override
        public void setValue(String value) {

        }
    };

    private InterfaceSpinnerItem durationItem = new InterfaceSpinnerItem() {
        String title = "";

        @Override
        public String getName() {
            if (TextUtils.isEmpty(title)) {
                title = getString(R.string.full_day);
            }
            return title;
        }

        @Override
        public void setName(String name) {

        }

        @Override
        public String getValue() {
            return "";
        }

        @Override
        public void setValue(String value) {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_outline_line);
        findViewById(R.id.root_layout).setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(this));
        findViewByIdMy(); // Поиск вьюх
        outlineLineUIHelper = new OutlineLineUIHelper();
        mode = (Mode) getIntent().getSerializableExtra(EXTRA_KEY_MODE);

        intentResult = new Intent();
        projectId = getIntent().getLongExtra(EXTRA_KEY_PROJECT_ID, 0);
        parentId = getIntent().getLongExtra(EXTRA_KEY_PARENT_ID, 0);
        timeDateCreated = getIntent().getLongExtra(EXTRA_KEY_DATE_CREATED, 0);

        long lineId = getIntent().getLongExtra(EXTRA_KEY_LINE_ID, 0);
        if (lineId != 0) { // Открыли задачу для редактирования востанавливаем параметры
            currentLine = Line.findByServerId(Line.class, lineId);
            if (currentLine != null) {
                color = currentLine.getColor();
                editTextOutline.setText(currentLine.getTitleReplacingTags());
                currentLine.updateDatesTextByCalendar();
                if (projectId == 0) {
                    projectId = currentLine.getProjectId();
                }
            }
        } else {
            // Открыли для создания новой задачи
            // Ставим временный id для сохранения картинки в файлы
            currentLine = Line.getLineNewForProject(projectId);
            if (currentLine == null) {
                currentLine = new Line();
                currentLine.setParentItemId(parentId);
                currentLine.setProjectId(projectId);
                currentLine.setOfflineId(System.currentTimeMillis());
                currentLine.setOfflineStatus(Line.StatusOffline.CREATE);
                currentLine.setServerId(currentLine.getOfflineId());
                currentLine.setNew(true);
                currentLine.save();
            }
        }

        if (currentLine == null) {
            finish();
            return;
        }

        currentLine.setModeEdit(mode);
        currentLine.save();
        intentResult.putExtra(EXTRA_KEY_LINE_ID, currentLine.getServerId());
        ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.EDIT_LINE_MODE, mode.toString());
        if (timeDateCreated == 0) {
            if (currentLine.getDeadlineDateStart() != null) {
                setDeadlineDate(currentLine.getDeadlineDateStart());
            }
        } else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timeDateCreated);
            calendar.set(Calendar.HOUR_OF_DAY, Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, Calendar.getInstance().get(Calendar.MINUTE));
            setDeadlineDate(calendar);
        }

        checkBtnImportant();
        checkBtnComplete();
        initContainerSelectedColor(); // Инициализация контейнера в котором выбирается цвет карточки
        initSpinners(); // Инициализация выпадающих списков
        updateColorSelected(); // Обновление фона
        updateDateDeadlineTitleCheckUI(); // Обновление времени начала задачи
        responseGetImage(); // Загрузка картинки если она есть
        initScroolView(); // Инициализация скрола, чтобы при скролинге скрывалась плашка с выбором цвета
    }

    @Override
    protected void onResume() {
        super.onResume();
        String m = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.EDIT_LINE_MODE);
        if (!TextUtils.isEmpty(m)) {
            mode = Mode.convertStringToEnum(m);
        }
    }

    private void findViewByIdMy() {
        containerDateSet = findViewById(R.id.scrollview_task_fields);
        containerColor = findViewById(R.id.layout_color);
        editTextOutline = (EditTextOutline) findViewById(R.id.edit_text_outline);
        imageAtach = (ImageView) findViewById(R.id.attach_image);
        containerImage = (RelativeLayout) findViewById(R.id.container_image);
        containerSpinnerProjects = (LinearLayout) findViewById(R.id.container_spinner_projects);
        containerSpinnerDeals = (LinearLayout) findViewById(R.id.container_spinner_deals);
        spinnerDeals = (Spinner) findViewById(R.id.spinner_deals);
        spinnerProjects = (Spinner) findViewById(R.id.spinner_projects);
        spinnerStages = (Spinner) findViewById(R.id.spinner_stage);
        spinnerAssigns = (Spinner) findViewById(R.id.spinner_assign_to);
        spinnerContact = (Spinner) findViewById(R.id.spinner_contact);
        spinnerDuration = (Spinner) findViewById(R.id.spinner_duration);
        containerSelectedColor = (ViewGroup) findViewById(R.id.container_selected_colors);
        titleDeadlineDate = (TextView) findViewById(R.id.deadline_date_format);
        btnImportant = (ImageView) findViewById(R.id.btn_important);
        btnCompleted = (ImageView) findViewById(R.id.btn_completed);
        scrollView = (ScrollView) findViewById(R.id.scroll_view_edit_line);
    }

    private void initScroolView(){
        scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (containerSelectedColor != null && containerSelectedColor.getVisibility() == View.VISIBLE) {
                    containerSelectedColor.setVisibility(View.GONE);
                }
            }
        });
    }

    private void updateDateDeadlineTitleCheckUI() {
        StringBuilder builder = new StringBuilder();

        if (timeDateCreated == 0) {
            if (getDeadlineDate() != null) {
                if (TextUtils.isEmpty(adapterSpinnerDuration.getSelectedValue())) {
                    builder.append(ManagerCalendar.convertCalendarToString(getDeadlineDate(), Line.DATE_FORMAT));
                } else {
                    builder.append(ManagerCalendar.convertCalendarToString(getDeadlineDate(), Line.DATE_FORMAT_TIME));
                }

            } else {
                if (!TextUtils.isEmpty(currentLine.getDeadlineDateFormat())) {
                    builder.append(currentLine.getDeadlineDateFormat());
                }

                if (!TextUtils.isEmpty(currentLine.getDeadlineDateFormatTime())) {
                    builder.append(" ").append(currentLine.getDeadlineDateFormatTime());
                }
            }
        } else {
            if (getDeadlineDate() != null) {
                builder.append(ManagerCalendar.convertCalendarToString(getDeadlineDate(), Line.DATE_FORMAT_TIME));
            } else {
                builder.append(ManagerCalendar.convertTimeStampToStringDate(timeDateCreated, Line.DATE_FORMAT_TIME));
            }

        }

        titleDeadlineDate.setText(builder);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = getManagerTakePhoto().getBitmapFromData(requestCode, data);
            if (bitmap != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                setImage(bitmap);
            } else {
                ManagerApplication.getInstance().showToast(R.string.error_add_photo);
            }
        } else if (resultCode == RESULT_CODE_DELETE_IMAGE) {
            deleteImage();
        } else if (resultCode == RESULT_CODE_UPDATE_IMAGE) {
            setImage(getManagerTakePhoto().getBitmapFromDirectory(currentLine.getServerId()));
        }
    }

    @Override
    public void showProgressBar(int resString) {
        ManagerProgressDialog.showProgressBar(this, resString, false);
    }

    // Обновление значений задачи по параметрам кнопки выполнена/важная
    @Override
    public void updateStatusImportantCompletion() {
        currentLine.setIsImportant(btnImportant.getTag().toString());
        currentLine.setIsCompleted(btnCompleted.getTag().toString());
    }

    @Override
    public void finishResponse(Line line) {
        ManagerProgressDialog.dismiss();

        if (projectId == 0) {
            ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_RELOAD_TO_LIST, Long.parseLong(adapterSpinnerProjects.getSelectedValue()));
        } else {
            ManagerApplication.getInstance().getSharedManager().putKeyLong(SharedKeys.ID_PROJECT_RELOAD_TO_LIST, projectId);
        }

        Intent intent = new Intent();
        if (line != null) {
            switch (mode) {
                case CREATE_LINE_OUTLINE:
                    if (parentId == 0) {
                        line.setLevel(1);
                        line.save();
                    } else {
                        Line parent = Line.findByServerId(Line.class, parentId);
                        if (parent != null) {
                            line.setLevel(parent.getLevel() + 1);
                            line.save();
                        }
                    }
                    intent.putExtra(EXTRA_KEY_LINE_ID, line.getServerId());
                    setResult(RESULT_CODE_LINE_CREATE, intent);
                    break;
                case CREATE_LINE_CALENDAR:
                    intent.putExtra(EXTRA_KEY_LINE_ID, line.getServerId());
                    Line parent = Line.getLineInbox(line.getProjectId());
                    if (parent != null) {
                        line.setParentItemId(parent.getServerId());
                    }

                    line.setLevel(2);
                    line.save();
                    setResult(RESULT_CODE_LINE_CREATE, intent);
                    break;
                case UPDATE_LINE_OUTLINE:
                case UPDATE_LINE_CALENDAR:
                    // Обновляем напоминание задачи по ID
                    intent.putExtra(EXTRA_KEY_LINE_ID, line.getServerId());
                    setResult(RESULT_CODE_LINE_UPDATE, intent);
                    break;
            }

            addAlarmManagerLine(line);
        }


        finish();
    }

    private void addAlarmManagerLine(Line line) {
        if (line.getDeadlineDateStart() != null) {
            ReciverTimeNotifications.addNotification(line);
        }
    }

    //==========================================
    // Image methods
    //==========================================
    // Удаление картинки
    private void deleteImage() {
        imageAtach.setImageBitmap(null);
        containerImage.setVisibility(View.GONE);
        getManagerTakePhoto().saveBitmapToFileDirectory(null, currentLine.getServerId());
    }

    // Установка картинки из строки
    private void setImagefromStringBase64(String image) {
        if (!TextUtils.isEmpty(image)) {
            try {
                byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
                setImage(BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Установка картинки из bitmapa
    private void setImage(Bitmap bmp) {
        if (bmp != null) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(bmp.getWidth(), RelativeLayout.LayoutParams.WRAP_CONTENT);
            lp.addRule(RelativeLayout.CENTER_IN_PARENT);
            imageAtach.setLayoutParams(lp);
            imageAtach.setAdjustViewBounds(true);
            imageAtach.setImageBitmap(bmp);
            containerImage.setVisibility(View.VISIBLE);
        }
        getManagerTakePhoto().saveBitmapToFileDirectory(bmp, currentLine.getServerId());
    }

    //==========================================
    // Spinners
    //==========================================
    private void initSpinners() {
        ArrayList<InterfaceSpinnerItem> arrCompanys = new ArrayList<>();


        arrCompanys.add(emptyItem);
        arrCompanys.addAll(Company.getAllByProjects());

        adapterSpinnerContacts = new AdapterSpinner(this, R.layout.item_spinner, arrCompanys, spinnerContact, true, false, R.layout.item_spinner_header_edit_line, true);
        spinnerContact.setAdapter(adapterSpinnerContacts);

        spinnerContact.post(new Runnable() {
            @Override
            public void run() {
                adapterSpinnerContacts.setSelectedPositionByValue(currentLine.getCompanyId());
            }
        });

        initSpinnerStage(projectId);
        initSpinnerAssigns(projectId);
        initSpinnerColor();
        initSpinnerDuration();

        if (mode == Mode.CREATE_LINE_CALENDAR || mode == Mode.UPDATE_LINE_CALENDAR) {
            initSpinnerDeals();
            initSpinnerProjects();
        }
        spinnerAssigns.setOnTouchListener(onTouchSpinner);
//        spinnerColor.setOnTouchListener(onTouchSpinner);
        spinnerContact.setOnTouchListener(onTouchSpinner);
        spinnerDeals.setOnTouchListener(onTouchSpinner);
        spinnerProjects.setOnTouchListener(onTouchSpinner);
        spinnerStages.setOnTouchListener(onTouchSpinner);
        titleDeadlineDate.setOnTouchListener(onTouchSpinner);
    }

    private void initSpinnerDuration() {
        ArrayList<InterfaceSpinnerItem> arrDuration = ManagerSpinnerGenerationData.getDataSpinnerDuration();
        adapterSpinnerDuration = new AdapterSpinner(this, R.layout.item_spinner, arrDuration, spinnerDuration, true, false,
                R.layout.item_spinner_header_edit_line, true, new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateDateDeadlineTitleCheckUI();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        arrDuration.add(0, durationItem);
        spinnerDuration.setAdapter(adapterSpinnerDuration);
        spinnerDuration.post(new Runnable() {
            @Override
            public void run() {
                if (getDeadlineDate() != null) {
                    spinnerDuration.setEnabled(true);
                } else {
                    spinnerDuration.setEnabled(false);
                }
                adapterSpinnerDuration.setSelectedPositionByValue(String.valueOf(currentLine.getDuration()));
            }
        });
        spinnerDuration.setOnTouchListener(onTouchSpinner);
    }

    private void initSpinnerColor() {
//        ArrayList<InterfaceSpinnerItemColors> arrayList = new ArrayList<>();
//        arrayList.add(emptyColor);
//        adapterSpinnerColor = new AdapterSpinnerColors(this, R.layout.item_spinner_colors, arrayList, spinnerColor, null, ManagerApplication.getInstance().getScreenWidth());
//        adapterSpinnerColor.setInterfaceAdapterSpinnerColor(this);
//        adapterSpinnerColor.setSelectedPosition(currentLine.getColor());
//        spinnerColor.setAdapter(adapterSpinnerColor);
    }

    public void selectedColor(int color) {
        this.color = color;
        currentLine.setColor(color);
        updateColorSelected();
    }

    private void hideSpinnerDropDown(Spinner spinner) {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(spinner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initSpinnerProjects() {
        ArrayList<InterfaceSpinnerItem> arrProjects = new ArrayList<>();
        arrProjects.add(emptyItem);
        arrProjects.addAll(Project.getAll(Project.class));
        containerSpinnerProjects.setVisibility(View.VISIBLE);
        adapterSpinnerProjects = new AdapterSpinner(this, R.layout.item_spinner, arrProjects, spinnerProjects, true, false, R.layout.item_spinner_header_edit_line, true,
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (!TextUtils.isEmpty(adapterSpinnerProjects.getSelectedValue())) {
                            responseGetAttributesProject(adapterSpinnerProjects.getSelectedValueLong());
                        } else {
                            ProjectDeals.clearTable(ProjectDeals.class);
                            initSpinnerDeals();
                            initSpinnerStage(adapterSpinnerProjects.getSelectedValueLong());
                            initSpinnerAssigns(adapterSpinnerProjects.getSelectedValueLong());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
        spinnerProjects.setAdapter(adapterSpinnerProjects);
        spinnerProjects.post(new Runnable() {
            @Override
            public void run() {
                if (currentLine.getProjectId() != 0) {
                    adapterSpinnerProjects.setSelectedPositionByValue(currentLine.getProjectId());
                } else {
                    long selectedProjectId = ManagerProjects.getSelectedProject();
                    if (selectedProjectId != 0) {
                        adapterSpinnerProjects.setSelectedPositionByValue(String.valueOf(selectedProjectId));
                    }
                }
            }
        });
    }

    private void initSpinnerDeals() {
        //TODO: Не удалять! Раскомитить если надо добавить поле сделки.
//        containerSpinnerDeals.setVisibility(View.VISIBLE);
        ArrayList<InterfaceSpinnerItem> arrDeals = new ArrayList<>();
        arrDeals.add(emptyItem);
        arrDeals.addAll(ProjectDeals.getAll(ProjectDeals.class));
        adapterSpinnerDeals = new AdapterSpinner(this, R.layout.item_spinner, arrDeals, spinnerDeals, true, false, R.layout.item_spinner_header_edit_line, true);
        spinnerDeals.setAdapter(adapterSpinnerDeals);
        spinnerDeals.post(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    private void initSpinnerAssigns(long projectId) {
        ArrayList<InterfaceSpinnerItem> arrAssigns = new ArrayList<>();
        arrAssigns.addAll(ProjectAssign.findByProjectID(projectId));
        if (arrAssigns.size() == 0) {
            arrAssigns.add(ManagerAccount.getProfileAssignMy(projectId));
        }

        adapterSpinnerAssign = new AdapterSpinner(this, R.layout.item_spinner, arrAssigns, spinnerAssigns, true, false, R.layout.item_spinner_header_edit_line, true);
        spinnerAssigns.setAdapter(adapterSpinnerAssign);
        spinnerAssigns.post(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(currentLine.getAssignUserId())) {
                    adapterSpinnerAssign.setSelectedPositionByValue(currentLine.getAssignUserId());
                }
            }
        });
    }

    private void initSpinnerStage(long projectId) {
        ArrayList<InterfaceSpinnerItem> arrStages = new ArrayList<>();
        arrStages.add(emptyItem);
        arrStages.addAll(ProjectStage.getAllByProjectId(projectId));
        adapterSpinnerStage = new AdapterSpinner(this, R.layout.item_spinner, arrStages, spinnerStages, true, false, R.layout.item_spinner_header_edit_line, true, true);
        spinnerStages.setAdapter(adapterSpinnerStage);
        spinnerStages.post(new Runnable() {
            @Override
            public void run() {
                adapterSpinnerStage.setSelectedPositionByValue(currentLine.getProjectStageId());
            }
        });
    }

    private View.OnTouchListener onTouchSpinner = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            try {
                InputMethodManager inputManager = (InputMethodManager) ActivityEditLineOutline.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(ActivityEditLineOutline.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return false;
        }
    };

    //==========================================
    // TAKE PHOTO
    //==========================================
    public ManagerTakePhoto getManagerTakePhoto() {
        if (managerTakePhoto == null) {
            managerTakePhoto = new ManagerTakePhoto(this);
        }
        return managerTakePhoto;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getManagerTakePhoto().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //==========================================
    //  On click
    //==========================================
    @Override
    public void onBackPressed() {
        if (RESULT_CODE != 0) {
            setResult(RESULT_CODE, intentResult);
        }

        finish();
    }

    public void onClickCompleted(View view) {
        if (currentLine.isComplete()) {
            btnCompleted.setTag(TAG_CHECK_FALSE);
            currentLine.setIsCompleted("0");
        } else {
            btnCompleted.setTag(TAG_CHECK_TRUE);
            currentLine.setIsCompleted("1");
        }

        checkBtnComplete();
    }

    public void onClickImportant(View view) {
        if (currentLine.isImportant()) {
            currentLine.setIsImportant("0");
            btnImportant.setTag(TAG_CHECK_FALSE);
        } else {
            currentLine.setIsImportant("1");
            btnImportant.setTag(TAG_CHECK_TRUE);
        }

        checkBtnImportant();
    }

    public void onClickAttachPhoto(View view) {
        getManagerTakePhoto().takePhoto(R.string.attach_file);
    }

    public void onClickDeleteImage(View view) {
        ManagerAlertDialog.showDialog(this, R.string.is_delete, R.string.yes, R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteImage();
            }
        }, null);
    }

    public void onClickDate(View view) {
        showDialogDeadLine();
    }

    public void onClickImage(View view) {
        Intent intent = new Intent(this, ActivityEditLineOutlineImage.class);
        intent.putExtra(ActivityEditLineOutlineImage.EXTRA_KEY_LINE_ID, currentLine.getServerId());
        startActivityForResult(intent, ActivityEditLineOutlineImage.EDIT_IMAGE);
    }

    public void onClickBack(View view) {
        onBackPressed();
    }

    public void onClickOk(View view) {
        if (updateAndSaveLineByCurrentFields()) {
            if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                if (mode == Mode.CREATE_LINE_CALENDAR || mode == Mode.UPDATE_LINE_CALENDAR) {
                    currentLine.setProjectId(adapterSpinnerProjects.getSelectedValueLong());
                    ManagerEditLine managerEditLine = new ManagerEditLine(this);
                    managerEditLine.setTempLine(currentLine);
                    managerEditLine.responseCreateItemPlanner();
                } else if (mode == Mode.UPDATE_LINE_OUTLINE) {
                    if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                        ManagerEditLine managerEditLine = new ManagerEditLine(this);
                        managerEditLine.setTempLine(currentLine);
                        managerEditLine.responseUpdateItem();
                    } else {
                        finishResponse(currentLine);
                    }
                } else if (mode == Mode.CREATE_LINE_OUTLINE) {
                    // Обновляем параметры текущей задачи по выставленым полям
                    ManagerEditLine managerEditLine = new ManagerEditLine(this, currentLine);
                    managerEditLine.responseCreateItemOutline();
                }
            } else {
                ManagerAlertDialog.showDialog(this, R.string.error_not_internet_sync_next_connect, R.string.yes, R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Нет интернета ставим статус задачи как офлайн
                        if (currentLine.getOfflineStatus() == null && (currentLine.getModeEdit() == Mode.UPDATE_LINE_OUTLINE || currentLine.getModeEdit() == Mode.UPDATE_LINE_CALENDAR)) {
                            currentLine.setOfflineStatus(Line.StatusOffline.UPDATE);
                        } else {
                            currentLine.setOfflineStatus(Line.StatusOffline.CREATE);
                        }

                        currentLine.save();
                        finishResponse(currentLine);
                    }
                }, null);
            }
        }
    }

    public void onClickSelectedColor(View view) {
        int[] arr = new int[2];
        view.getLocationOnScreen(arr);
        if (containerSelectedColor.getVisibility() == View.VISIBLE) {
            containerSelectedColor.setVisibility(View.GONE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) containerSelectedColor.getLayoutParams();
            lp.topMargin = 0;
            containerSelectedColor.setLayoutParams(lp);
        } else if (containerSelectedColor.getVisibility() == View.GONE) {
            containerSelectedColor.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) containerSelectedColor.getLayoutParams();
            lp.topMargin = arr[1] + view.getMeasuredHeight();
            containerSelectedColor.setLayoutParams(lp);
        }
    }

    /**
     * Обновление параметров задачи по выставленым полям
     * Показывает предупреждение о невалидности полей
     *
     * @return прошла ли валидация полей
     */
    private boolean updateAndSaveLineByCurrentFields() {
        // Если нету веденного текста
        if (TextUtils.isEmpty(editTextOutline.getText().toString())) {
            ManagerApplication.getInstance().showToast(R.string.empty_text);
            return false;
            //  Нету выбраного проекта для календаря
        } else if ((mode == Mode.CREATE_LINE_CALENDAR || mode == Mode.UPDATE_LINE_CALENDAR) && TextUtils.isEmpty(adapterSpinnerProjects.getSelectedValue())) {
            ManagerApplication.getInstance().showToast(R.string.error_not_selected_project);
            return false;
        } else {
            if (mode == Mode.CREATE_LINE_CALENDAR || mode == Mode.UPDATE_LINE_CALENDAR) {
                currentLine.setProjectId(Long.parseLong(adapterSpinnerProjects.getSelectedValue()));
            } else {
                currentLine.setProjectId(projectId);
            }

            currentLine.setParentItemId(parentId);
            currentLine.setTitle(editTextOutline.getText().toString());
            currentLine.updateReplaceTitleFromCurentTitle();
            currentLine.setCreatorUserId(ManagerAccount.getMyProfile().getServerId());

            if (parentId != 0) {
                Line parent = Line.findByServerId(Line.class, currentLine.getParentItemId());
                if (parent != null) {
                    currentLine.setLevel(parent.getLevel() + 1);
                }
            }

            // Время начала задачи
            if (getDeadlineDate() != null) {
                getDeadlineDate().set(Calendar.SECOND, 0);
                currentLine.setDeadlineDateStart(getDeadlineDate());
            }

            // Время выполнения
            String durationValue = adapterSpinnerDuration.getSelectedValue();
            if (!TextUtils.isEmpty(durationValue)) {
                currentLine.setDuration(Integer.parseInt(durationValue));
            } else {
                currentLine.setDuration(0);
            }

            currentLine.updateFinishDateForCurrentDuration();
            currentLine.updateDatesTextByCalendarNotSave();

            // Контакт
            String companyIdStr = adapterSpinnerContacts.getSelectedValue();
            if (!TextUtils.isEmpty(companyIdStr)) {
                currentLine.setCompanyId(Long.parseLong(companyIdStr));
                Company company = Company.findByServerId(Company.class, currentLine.getCompanyId());
                if (company != null) {
                    currentLine.setCompanyName(company.getName());
                }
            } else {
                currentLine.setCompanyId(0);
            }


            // Этап проекта
            if (adapterSpinnerStage.isEmptySelectedValue()) {
                currentLine.setProjectStageId(0);
            } else {
                currentLine.setProjectStageId(adapterSpinnerStage.getSelectedValueLong());
                ProjectStage projectStage = ProjectStage.findByProjectIdAndServerId(currentLine.getProjectId(), currentLine.getProjectStageId());
                if (projectStage != null) {
                    currentLine.setProjectStageName(projectStage.getName());
                }
            }

            // Назначеный человек
            String assignIdStr = adapterSpinnerAssign.getSelectedValue();
            if (!TextUtils.isEmpty(assignIdStr)) {
                currentLine.setAssignUserId(assignIdStr);
                ProjectAssign projectAssign = ProjectAssign.findByServerId(ProjectAssign.class, Long.parseLong(currentLine.getAssignUserId()));
                if (projectAssign != null) {
                    currentLine.setAssignUserUsername(projectAssign.getUserName());
                }
            } else {
                currentLine.setAssignUserId("");
            }

            // Цвет задачи
            if (color != Integer.MIN_VALUE) {
                currentLine.setColor(color);
            }

            // Выполнена/невыполнена
            currentLine.setIsCompleted(btnCompleted.getTag().toString());
            // Важная/неважная
            currentLine.setIsImportant(btnImportant.getTag().toString());
            // после того как заполнили все поля задача не является больше новой
            // и её еможно показывать на календаре и в списке задач
            currentLine.setNew(false);
            return true;
        }
    }

    public void onClickDeleteLine(View view) {
        if (currentLine != null && currentLine.getServerId() > 0) {
            ManagerAlertDialog.showDialog(this, R.string.is_delete_task, R.string.delete_en, R.string.cancel_en, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    responseDeleteLine();
                }
            }, null);
        }
    }

    private void checkBtnImportant() {
        if (currentLine.isImportant()) {
            btnImportant.setImageResource(R.drawable.ic_important);
            btnImportant.setTag(TAG_CHECK_TRUE);
        } else {
            btnImportant.setImageResource(R.drawable.ic_important_unselected);
            btnImportant.setTag(TAG_CHECK_FALSE);
        }
    }

    private void checkBtnComplete() {
        if (currentLine.isComplete()) {
            btnCompleted.setImageResource(R.drawable.ic_complete);
            btnCompleted.setTag(TAG_CHECK_TRUE);
        } else {
            btnCompleted.setImageResource(R.drawable.ic_complete_unselected);
            btnCompleted.setTag(TAG_CHECK_FALSE);
        }
    }

    private void updateColorSelected() {
        containerColor.setBackgroundResource(outlineLineUIHelper.getColorRoundAll(color));
        containerDateSet.setBackgroundResource(outlineLineUIHelper.getColorRoundAllDateSetContainer(color));
    }

    private void initContainerSelectedColor() {
        for (int i = 0; i < containerSelectedColor.getChildCount(); i++) {
            View view = containerSelectedColor.getChildAt(i);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getTag() != null) {
                        int color = Integer.parseInt(v.getTag().toString());
                        selectedColor(color);
                        initContainerSelectedColor();
                        containerSelectedColor.setVisibility(View.GONE);
                    }
                }
            });
            int tempX = i;
            tempX++;
            if (tempX == currentLine.getColor()) {
                (((ViewGroup) view).getChildAt(1)).setAlpha((float) 1);
            } else {
                (((ViewGroup) view).getChildAt(1)).setAlpha((float) 0);
            }
        }
    }

    //==========================================
    // Dialog
    //==========================================
    private View dialogView;
    private AlertDialog alertDialogDateDeadlin;
    private DatePicker datePicker;
    private TimePicker timePicker;

    private void showDialogDeadLine() {
        dialogView = View.inflate(this, R.layout.dialog_date_time_edit_line, null);
        datePicker = dialogView.findViewById(R.id.date_picker);
        timePicker = dialogView.findViewById(R.id.time_picker);
        timePicker.setIs24HourView(true);
        alertDialogDateDeadlin = new AlertDialog.Builder(this).create();
        dialogView.findViewById(R.id.date_time_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePicker datePicker = dialogView.findViewById(R.id.date_picker);
                TimePicker timePicker = dialogView.findViewById(R.id.time_picker);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
                calendar.set(Calendar.MONTH, datePicker.getMonth());
                calendar.set(Calendar.YEAR, datePicker.getYear());

                int hour, minute;
                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
                    hour = timePicker.getHour();
                    minute = timePicker.getMinute();
                } else {
                    hour = timePicker.getCurrentHour();
                    minute = timePicker.getCurrentMinute();
                }

                calendar.set(Calendar.HOUR_OF_DAY, hour);
                calendar.set(Calendar.MINUTE, minute);
                spinnerDuration.setEnabled(true);
                setDeadlineDate(calendar);
                currentLine.setDeadlineDateFormat(currentLine.convertCalendarToDateDeadLine(calendar));

                updateDateDeadlineTitleCheckUI();
                alertDialogDateDeadlin.dismiss();
            }
        });

        dialogView.findViewById(R.id.date_time_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogDateDeadlin.dismiss();
            }
        });

        dialogView.findViewById(R.id.date_time_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleDeadlineDate.setText("");
                adapterSpinnerDuration.setSelectedPositionByValue("");
                spinnerDuration.setEnabled(false);
                setDeadlineDate(null);
                alertDialogDateDeadlin.dismiss();
            }
        });

        TabHost tabHost = dialogView.findViewById(R.id.tabs_dates_edit_line);
        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");
        tabSpec.setContent(R.id.date_picker);
        tabSpec.setIndicator(getString(R.string.date));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.time_picker);
        tabSpec.setIndicator(getString(R.string.time));
        tabHost.addTab(tabSpec);

        tabHost.setCurrentTab(0);

        alertDialogDateDeadlin.setView(dialogView);
        alertDialogDateDeadlin.show();

        if (getDeadlineDate() == null) {
            setDeadlineDate(ManagerCalendar.getInstance());
        }
        // Установка ранее выбранных значений

        int year, month, day;
        year = getDeadlineDate().get(Calendar.YEAR);
        month = getDeadlineDate().get(Calendar.MONTH);
        day = getDeadlineDate().get(Calendar.DAY_OF_MONTH);
        datePicker.updateDate(year, month, day);
        timePicker.setCurrentHour(getDeadlineDate().get(Calendar.HOUR_OF_DAY));
        timePicker.setCurrentMinute(getDeadlineDate().get(Calendar.MINUTE));

    }

    //==========================================
    // Methods response updates
    //==========================================
    private void responseDeleteLine() {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ManagerOutline.responseDeleteLine(currentLine.getServerId(), projectId, new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    if (msg.what == HttpParam.RESPONSE_OK) {
                        currentLine.delete();
                        if (mode == Mode.CREATE_LINE_CALENDAR || mode == Mode.UPDATE_LINE_CALENDAR) {
                            RESULT_CODE = RESULT_CODE_LINE_DELETE_CALENDAR;
                        } else if (mode == Mode.CREATE_LINE_OUTLINE || mode == Mode.UPDATE_LINE_OUTLINE) {
                            RESULT_CODE = RESULT_CODE_LINE_DELETE_OUTLINE;
                        }

                        onBackPressed();
                        ReciverTimeNotifications.deleteNotification(currentLine);
                    } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                        ManagerApplication.getInstance().showToast(R.string.error_delete_task);
                    }
                }
            });
        } else {
            RESULT_CODE = RESULT_CODE_UPDATE_OUTLINE;
            if (currentLine.getOfflineStatus() == Line.StatusOffline.CREATE) {
                currentLine.delete();
            } else {
                currentLine.setOfflineStatus(Line.StatusOffline.REMOVE);
                currentLine.save();
            }

            ReciverTimeNotifications.deleteNotification(currentLine);
            onBackPressed();
        }
    }

    private void responseGetAttributesProject(long projectId) {
        ManagerOutline.responseGetAttributesProject(projectId, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == HttpParam.RESPONSE_OK) {
                    initSpinnerDeals();
                    initSpinnerAssigns(Long.parseLong(adapterSpinnerProjects.getSelectedValue()));
                    initSpinnerStage(Long.parseLong(adapterSpinnerProjects.getSelectedValue()));
                }
                super.handleMessage(msg);
            }
        });
    }

    private void responseGetImage() {
        if (currentLine.getServerId() != 0) {
            if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                ManagerOutline.responseGetImage(currentLine.getServerId(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.obj != null) {
                            setImage((Bitmap) msg.obj);
                        }
                        super.handleMessage(msg);
                    }
                });
            } else {
                setImage(getManagerTakePhoto().getBitmapFromDirectory(currentLine.getServerId()));
            }
        }
    }

    //==========================================
    // Getter setter
    //==========================================
    public Calendar getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(Calendar deadlineDate) {
        this.deadlineDate = deadlineDate;
    }
}