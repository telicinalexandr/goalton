package com.mifors.goalton.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerTakePhoto;

/**
 * Created by gly8 on 17.04.17.
 */
@SuppressWarnings("ALL")
public class ActivityEditLineOutlineImage extends AppCompatActivity {
    private static final String TAG = "Goalton [" + ActivityEditLineOutlineImage.class.getSimpleName() + "]";
    public static final String EXTRA_KEY_LINE_ID = "EXTRA_KEY_LINE_ID";
    public static final int EDIT_IMAGE = 4120;
    public static final int RESULT_CODE_DELETE_IMAGE = 4100;
    public static final int RESULT_CODE_UPDATE_IMAGE = 4000;
    private ManagerTakePhoto managerTakePhoto;

    private ImageView image;
    private Bitmap bitmap;
    private long serverIdLine;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_outline_line_image);
        image = (ImageView) findViewById(R.id.attach_image);

        try {
            serverIdLine = getIntent().getLongExtra(EXTRA_KEY_LINE_ID, 0);
            image.setImageBitmap(getManagerTakePhoto().getBitmapFromDirectory(serverIdLine));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            bitmap = getManagerTakePhoto().getBitmapFromData(requestCode, data);
            if (bitmap != null) {
                image.setImageBitmap(bitmap);
                getManagerTakePhoto().saveBitmapToFileDirectory(bitmap, serverIdLine);
            }
        }
    }

    public void onClickBack(View view) {
        onBackPressed();
    }

    public void onClickShare(View view) {

    }

    public void onClickChange(View view) {
        getManagerTakePhoto().takePhoto();
    }

    public void onClickDelete(View view) {
        ManagerAlertDialog.showDialog(this, R.string.is_delete, R.string.yes, R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getManagerTakePhoto().saveBitmapToFileDirectory(bitmap, serverIdLine);
                finishActivityDeleteImage();
            }
        }, null);
    }

    private void finishActivityDeleteImage() {
        setResult(RESULT_CODE_DELETE_IMAGE);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (bitmap != null) {
            setResult(RESULT_CODE_UPDATE_IMAGE);
        }
        finish();
    }

    //==========================================
    // TAKE PHOTO
    //==========================================
    public ManagerTakePhoto getManagerTakePhoto() {
        if (managerTakePhoto == null) {
            managerTakePhoto = new ManagerTakePhoto(this);
        }
        return managerTakePhoto;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        getManagerTakePhoto().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}