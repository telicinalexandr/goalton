package com.mifors.goalton.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.mifors.goalton.Constants;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.adapters.AdapterMyInvites;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerInvite;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerJSONParsing;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.myteams.Invite;
import com.mifors.goalton.network.HttpParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gly8 on 10.07.17.
 */

@SuppressWarnings("ALL")
public class ActivityMyInvites extends AppCompatActivity {
    private String tempStatusInvite; // Хранит статус отказался/принял приглашение
    private final String KEY_ACCEPTED = "accepted";
    private final String KEY_REJECTED = "rejected";
    public static boolean isRunActivity = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_invite);
        showDialogMyInvites();
        isRunActivity = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRunActivity = false;
    }

    public void showDialogMyInvites() {
        String invites = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.INVITES);
        if (!TextUtils.isEmpty(invites)) {
            ArrayList<Invite> list = null;
            try {
                list = ManagerJSONParsing.parseMyInviteList(new JSONObject(invites));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (list != null && list.size() == 0) {
                return;
            }
            RecyclerView recycleView = (RecyclerView) findViewById(R.id.recycle_view_invites);
            recycleView.setLayoutManager(new LinearLayoutManager(this));
            AdapterMyInvites adapter = new AdapterMyInvites(list, onClickItem, this);
            recycleView.setAdapter(adapter);
        }
    }

    private View.OnClickListener onClickItem = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                switch (v.getId()) {
                    case R.id.btn_accept:
                        responseChangeStatus(Long.parseLong(v.getTag().toString()), KEY_ACCEPTED);
                        break;
                    case R.id.btn_failure:
                        responseChangeStatus(Long.parseLong(v.getTag().toString()), KEY_REJECTED);
                        break;

                }
            }
        }
    };

    // Принять отклонить приглашение
    private void responseChangeStatus(long teamId, final String status) {
        tempStatusInvite = status;
        ManagerMyTeams.responseChangeStatusInvite(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    switch (tempStatusInvite) {
                        case KEY_ACCEPTED: // Принять приглашение
                            ManagerApplication.getInstance().showToast(R.string.invite_accepted);
                            // Загрузка списка комманд
                            break;
                        case KEY_REJECTED: // Отклонить приглашение
                            ManagerApplication.getInstance().showToast(R.string.invite_rejected);
                            break;
                    }

                    ManagerInvite.getInstance().responseGetListMyInvites(null);
                    ManagerInvite.saveCountInvites(ManagerInvite.getCountInvites() - 1);
                    responseGetProjects();
                } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                    ManagerApplication.getInstance().showToast(R.string.message_error_send_data_to_server);
                }
            }
        }, teamId, status);
    }

    private void responseGetProjects() {
        ManagerProgressDialog.showProgressBar(this, R.string.update_projects, false);
        ManagerProjects.responseGetProjects(new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                ManagerProgressDialog.dismiss();
                if (msg.what == HttpParam.RESPONSE_OK) {
                    ManagerInvite.getInstance().callingNotify(Constants.INVITE_ACCESS);
                }
                onClickCancel(null);
            }
        });
    }

    public void onClickCancel(View view) {
        finish();
    }
}
