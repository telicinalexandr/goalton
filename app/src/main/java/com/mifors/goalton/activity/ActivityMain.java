package com.mifors.goalton.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.mifors.goalton.Constants;
import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.fragment.calendar.FragmentCalendar;
import com.mifors.goalton.fragment.outline.FragmentOutline;
import com.mifors.goalton.fragment.profile_settings.FragmentControllerProfileSettings;
import com.mifors.goalton.fragment.projects.FragmentMyProjects;
import com.mifors.goalton.interfaces.InterfaceCallbackActivityMain;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerInvite;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.managers.core.ManagerProjects;
import com.mifors.goalton.managers.utils.ManagerAlertDialog;
import com.mifors.goalton.managers.utils.ManagerProgressDialog;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.user.Profile;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.ui.spinner.adapters.AdapterSpinner;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;
import com.mifors.goalton.ui.spinner.view.DefaultObject;
import com.mifors.goalton.utils.FragmentTags;
import com.mifors.goalton.utils.notifications.ReciverTimeNotifications;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.mifors.goalton.fragment.profile_settings.FragmentSettingsProfile.REQUEST_CODE;

public class ActivityMain extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, InterfaceCallbackActivityMain, Observer, ManagerOutline.IManagerOutline {
    // Обновление проектов после отправки нового на сервак и загрузки
    public static final int REQUEST_CODE_RELOAD_PROJECTS = 6328;
    private FragmentManager fragmentManager;
    private NavigationView navigationView;
    private ImageButton btnSort;
    private Spinner spinnerSort;
    private AdapterSpinner adapterSpinner;
    private View containerBtnSort;
    private Line tempLineStartOutline; // Хранит задачу переданную из иерархии выше
    public static boolean IS_LOAD_PROJECTS_RUN_SESSION = false; // Были ли загруженны проекты при старте фргмента мои проекты

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnSort = toolbar.findViewById(R.id.btn_projects_sort);
        btnSort.setOnClickListener(onClickBtnSort);

        containerBtnSort = toolbar.findViewById(R.id.container_sorts);
        spinnerSort = toolbar.findViewById(R.id.spinner_projects_sort);

        initSpinnerSort();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        replaceFragment(new FragmentMyProjects(), false, FragmentTags.FRAGMENT_MY_PROJECTS);
        ManagerInvite.getInstance().addObserver(this);
        toolbar.post(new Runnable() {
            @Override
            public void run() {
                if (getSupportActionBar() != null) {
                    ManagerApplication.getInstance().setHeightActionBar(getSupportActionBar().getHeight());
                }
                ManagerApplication.getInstance().setHeightStatusBar(getStatusBarHeight());
            }
        });
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ManagerApplication.getInstance().checkOfflineLines();
        }
        ManagerApplication.getInstance().registerReciver();
//        AlarmManager am = (AlarmManager) ManagerApplication.getInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE); // Получаем сервис
//        Intent intentTime = createIntent(ManagerApplication.getInstance().getApplicationContext(), String.valueOf(1151857), 1151857);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(ManagerApplication.getInstance().getApplicationContext(), 0, intentTime, PendingIntent.FLAG_UPDATE_CURRENT); // Регестрируем действие
//        Calendar c = Calendar.getInstance();
//        c.add(Calendar.SECOND, 5);
//        am.set(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), pendingIntent);
    }

    private static Intent createIntent(Context context, String action, long extra) {
        Intent intent = new Intent(context, ReciverTimeNotifications.class);
        intent.setAction(action);
        intent.putExtra(ReciverTimeNotifications.KEY_ID_LINE, extra);
        return intent;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateItemMenuCountInvites();
        updateNavHeader();
        if (ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.IS_UPDATE_CALENDAR_DATA_CHANGE_TIME_NOIFICATION)) {
            FragmentCalendar fragmentCalendar = (FragmentCalendar) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_CALENDAR);
            if (fragmentCalendar != null) {
                fragmentCalendar.notifyUpdateDays();
                ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.IS_UPDATE_CALENDAR_DATA_CHANGE_TIME_NOIFICATION, false);
            }
        }
    }

    private void initSpinnerSort() {
        ArrayList<InterfaceSpinnerItem> list = new ArrayList<>();
        list.add(0, new DefaultObject(getString(R.string.sort_by_name), FragmentMyProjects.TypeSort.NAME.getType()));
        list.add(1, new DefaultObject(getString(R.string.sort_by_date), FragmentMyProjects.TypeSort.DATE.getType()));
        list.add(2, new DefaultObject(getString(R.string.sort_by_owner), FragmentMyProjects.TypeSort.OWNER.getType()));
        list.add(3, new DefaultObject(getString(R.string.sort_by_public), FragmentMyProjects.TypeSort.PUBLIC.getType()));

        adapterSpinner = new AdapterSpinner(this, R.layout.item_spinner_projects_sort, list, spinnerSort, itemSelectedListenerSpinnerSort);
        spinnerSort.setAdapter(adapterSpinner);
        adapterSpinner.setIsReverse(ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE));
        spinnerSort.post(new Runnable() {
            @Override
            public void run() {
                String typeSort = ManagerApplication.getInstance().getSharedManager().getValueString(SharedKeys.TYPE_PROJECT_SORT);
                if (!TextUtils.isEmpty(typeSort)) {
                    adapterSpinner.setSelectedPositionByValue(typeSort);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == REQUEST_CODE_RELOAD_PROJECTS) {
            boolean isReloadList = data.getBooleanExtra(ActivitySettingProject.KEY_EXTRA_IS_NOTIFYDATA_LIST_PROJECTS, false);
            try {
                try {
                    FragmentMyProjects fr = (FragmentMyProjects) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_MY_PROJECTS);
                    if (isReloadList) {
                        fr.initRecycleViewProjectsNotUpdateSort();
                    }
                } catch (NullPointerException | IllegalStateException e) {
                    e.printStackTrace();
                }
            } catch (RuntimeException e1) {
                e1.printStackTrace();
            }
        } else if (resultCode == ActivityEditLineOutline.RESULT_CODE_LINE_CREATE || resultCode == ActivityEditLineOutline.RESULT_CODE_LINE_UPDATE) {
            FragmentCalendar fragmentCalendar = (FragmentCalendar) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_CALENDAR);
            if (fragmentCalendar != null && data != null && data.getExtras() != null) {
                fragmentCalendar.onActivityResult(requestCode, resultCode, data);
            }
        } else if (resultCode == ActivityEditLineOutline.RESULT_CODE_LINE_DELETE_OUTLINE || resultCode == ActivityEditLineOutline.RESULT_CODE_UPDATE_OUTLINE) {
            FragmentOutline fragmentOutline = (FragmentOutline) getSupportFragmentManager().findFragmentByTag(FragmentOutline.TAG_FRAGMENT);
            if (fragmentOutline != null) {
                fragmentOutline.onActivityResult(requestCode, resultCode, data);
            }
            FragmentCalendar fragmentCalendar = (FragmentCalendar) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_CALENDAR);
            if (fragmentCalendar != null && data != null && data.getExtras() != null) {
                fragmentCalendar.onActivityResult(requestCode, resultCode, data);
            }
        } else if (resultCode == ActivityEditLineOutline.RESULT_CODE_LINE_DELETE_CALENDAR) {
            FragmentCalendar fragmentCalendar = (FragmentCalendar) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_CALENDAR);
            if (fragmentCalendar != null && data != null && data.getExtras() != null) {
                fragmentCalendar.onActivityResult(requestCode, resultCode, data);
            }
        } else if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            Uri uri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

            if (uri != null) {
                Ringtone ringtone = RingtoneManager.getRingtone(this, uri);
                String title = ringtone.getTitle(this);
                ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.NOTIFICATIONS_SOUND_NAME, title);
                ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.NOTIFICATIONS_SOUND_PATH, uri.toString());
            } else {
                ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.NOTIFICATIONS_SOUND_NAME, "");
                ManagerApplication.getInstance().getSharedManager().putKeyString(SharedKeys.NOTIFICATIONS_SOUND_PATH, "");
            }

            FragmentControllerProfileSettings fragmentSettingsProfile = (FragmentControllerProfileSettings) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_SETTINGS);
            if (fragmentSettingsProfile != null) {
                fragmentSettingsProfile.updateSelectedSound();
            }
        }
    }

    // Боковое меню
    @Override
    public void updateNavHeader() {
        View view = navigationView.getHeaderView(0);
        Profile myProfile = ManagerAccount.getMyProfile();
        if (myProfile != null) {
            String displayedName = myProfile.getFio();
            if (!TextUtils.isEmpty(displayedName)) {
                TextView v = view.findViewById(R.id.nav_header_profile_name);
                v.setVisibility(View.VISIBLE);
                v.setText(displayedName);
            } else {
                view.findViewById(R.id.nav_header_profile_name).setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(myProfile.getEmail())) {
                TextView v = view.findViewById(R.id.nav_header_profile_email);
                v.setVisibility(View.VISIBLE);
                v.setText(myProfile.getEmail());
            } else {
                view.findViewById(R.id.nav_header_profile_email).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment myFragment = fragmentManager.findFragmentByTag(FragmentOutline.TAG_FRAGMENT);
            if (myFragment != null && myFragment.isVisible()) {
                if (((FragmentOutline) myFragment).getRootLevel() == 1) {
                    super.onBackPressed();
                } else {
                    ((FragmentOutline) myFragment).onBackPressed();
                }
            } else {
                try {
                    isExit();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_menu_profile: // Настройки
                if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                    ManagerAccount.responseGetDataUser(new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            super.handleMessage(msg);
                            replaceFragment(new FragmentControllerProfileSettings(), false, FragmentTags.FRAGMENT_SETTINGS);
                        }
                    });
                } else {
                    replaceFragment(new FragmentControllerProfileSettings(), false, FragmentTags.FRAGMENT_SETTINGS);
                }
                break;
            case R.id.nav_menu_projects: // Проекты
                replaceFragment(new FragmentMyProjects(), false, FragmentTags.FRAGMENT_MY_PROJECTS);
                getSupportActionBar().invalidateOptionsMenu();
                break;
            case R.id.nav_menu_exit: // Выход
                isExit();
                break;
            case R.id.nav_menu_outline: // Список задач
                tempLineStartOutline = null;
                if (ManagerProjects.getSelectedProject() == 0) {
                    ManagerApplication.getInstance().showToast(R.string.error_not_selected_project);
                } else {
                    ManagerProgressDialog.showProgressBar(this, R.string.opening_outline, true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            replaceFragmentOutline(ManagerProjects.getSelectedProject());
                        }
                    }, 500);
                }
                break;
            case R.id.nav_menu_calendar:
                replaceFragmentCalendar();
                getSupportActionBar().invalidateOptionsMenu();
                break;
//            case R.id.nav_menu_mail_support:
//                try {
//                    PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                    String version = pInfo.versionName;
//                    Intent intent = new Intent(Intent.ACTION_SENDTO);
//                    intent.setData(Uri.parse("mailto:"));
//                    intent.putExtra(Intent.EXTRA_EMAIL  , new String[] { "mail@mail.com" });
//                    intent.putExtra(Intent.EXTRA_TEXT, ("\r\n\r\n-------------------------\r\n"+getString(R.string.app_version)+" "+version));
//                    startActivity(Intent.createChooser(intent, version));
//                } catch (PackageManager.NameNotFoundException e) {
//                    e.printStackTrace();
//                }
//                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void isExit() {
        ManagerAlertDialog.showDialog(this, R.string.is_exit, R.string.yes, R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        }, null);
    }

    @Override
    public void setActionBarTitle(int resId) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setTitle(resId);
        }
    }

    @Override
    public void setActionBarTitle(int resId, boolean hideCustom) {
        if (hideCustom) {
            containerBtnSort.setVisibility(View.GONE);
        } else {
            containerBtnSort.setVisibility(View.VISIBLE);
        }
        setActionBarTitle(resId);
    }

    @Override
    public void replaceFragmentOutline(long projectId, long startParentLineServerId) {
        tempLineStartOutline = Line.findByServerId(Line.class, startParentLineServerId);
        if (tempLineStartOutline != null) {
            responseGetOutlineProject(tempLineStartOutline.getProjectId());
        } else {
            responseGetOutlineProject(projectId);
        }
    }

    private void responseGetOutlineProject(long projectId) {
        showProgressBar(R.string.load_outlines_project);
        if (Line.isLoadLines(projectId)) {
            if (ManagerApplication.getInstance().isConnectToInternet(false)) {
                startResponseGetOulineProject(projectId);
            } else {
                replaceFragmentOutline(projectId);
            }
        } else {
            if (tempLineStartOutline != null && tempLineStartOutline.isLoadParent()) { // Не подгружали раньше проект для этой линии
                startResponseGetOulineProject(projectId);
            } else {
                replaceFragmentOutline(projectId);
            }
        }
    }

    private void startResponseGetOulineProject(final long projectId) {
        ManagerOutline.getOutlineProject(this, projectId, new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == HttpParam.RESPONSE_OK) {
                    if (tempLineStartOutline != null) {
                        replaceFragmentOutline(projectId);
                    } else {
                        replaceFragmentOutline(projectId);
                    }
                } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                    ManagerProgressDialog.dismiss();
                    if (msg.obj != null) {
                        if ((Integer) msg.obj == HttpParam.RESPONSE_ACCESS_DENIED) {
                            ManagerAlertDialog.showDialog(ActivityMain.this, R.string.access_denied);
                        }
                    } else {
                        // Типа нет интернета
                        replaceFragmentOutline(projectId);
                    }
                } else {
                    ManagerProgressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void setActionBarTitle(String title, boolean hideCustom) {
        if (hideCustom) {
            containerBtnSort.setVisibility(View.GONE);
        } else {
            containerBtnSort.setVisibility(View.VISIBLE);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    private void logout() {
//        ManagerAccount.logout();
//        logoutActivity();
        ManagerApplication.getInstance().clearApplicationData();
    }

    private void logoutActivity() {
        finish();
        Intent intent = new Intent(this, ActivityLogin.class);
        intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void showProgressBar() {
        ManagerProgressDialog.showProgressBar(this, R.string.update_projects, false);
    }

    @Override
    public void showProgressBar(int resMessage) {
        ManagerProgressDialog.showProgressBar(this, resMessage, false);
    }

    @Override
    public void hideProgressBar() {
        ManagerProgressDialog.dismiss();
    }

    private View.OnClickListener onClickBtnSort = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            spinnerSort.performClick();
        }
    };

    private AdapterView.OnItemSelectedListener itemSelectedListenerSpinnerSort = new AdapterView.OnItemSelectedListener() {
        boolean isfirstRun = false; // Первый раз вызывается при созданиее
        boolean isDoubleCalling = false; // Второй раз вызывается при установке выбранных ранее значений

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (!isfirstRun) {
                isfirstRun = true;
            } else {
                if (isDoubleCalling) {
                    Fragment fr = fragmentManager.findFragmentByTag(FragmentTags.FRAGMENT_MY_PROJECTS);
                    if (fr instanceof FragmentMyProjects) {
                        String value = adapterSpinner.getValueByPosition(position);
                        ((FragmentMyProjects) fr).updateListBySort(value, true);
                        adapterSpinner.setIsReverse(ManagerApplication.getInstance().getSharedManager().getValueBoolean(SharedKeys.TYPE_PROJECT_SORT_REVERSE));
                        adapterSpinner.notifyDataSetChanged();
                    }
                } else {
                    isDoubleCalling = true;
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    //==========================================
    // Fragment methods
    //==========================================
    @Override
    public void replaceFragment(Fragment fragment, boolean addToBackStack, @Nullable String TAG) {
        hideKeyboard(this);
        getSupportActionBar().setDisplayShowCustomEnabled(false);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            transaction.replace(R.id.container, fragment, TAG);
        } else {
            Fragment fr = fragmentManager.findFragmentByTag(TAG);
            if (fr instanceof FragmentOutline) {
                if (fr.isVisible()) {
                    ManagerProgressDialog.dismiss();
                }
            } else if (fr instanceof FragmentCalendar) {
                ((FragmentCalendar) fr).setCustomActionBar();
            }

            transaction.replace(R.id.container, fr, TAG);
        }

        if (addToBackStack) {
            transaction.addToBackStack("backStack");
        }

        try {
            transaction.commitAllowingStateLoss();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void replaceFragmentOutline(long projectId) {
        ManagerProgressDialog.dismiss();
        ManagerProjects.saveSelectedProjects(projectId);

        Fragment fr = fragmentManager.findFragmentByTag(FragmentOutline.TAG_FRAGMENT);
        if (fr == null) {
            FragmentOutline outline = new FragmentOutline();
            outline.setProjectId(projectId);
            if (tempLineStartOutline != null) {
                outline.setLineStart(tempLineStartOutline);
            } else {
                outline.setLineStart(null);
            }

            replaceFragment(outline, true, FragmentOutline.TAG_FRAGMENT);
        } else {
            ((FragmentOutline) fr).setProjectId(projectId);
            if (tempLineStartOutline != null) {
                ((FragmentOutline) fr).setLineStart(tempLineStartOutline);
            } else {
                ((FragmentOutline) fr).setLineStart(null);
            }

            replaceFragment(fr, true, FragmentOutline.TAG_FRAGMENT);
        }
    }

    @Override
    public void replaceFragmentCalendar() {
        Fragment fr = fragmentManager.findFragmentByTag(FragmentTags.FRAGMENT_CALENDAR);
        if (fr == null) {
            replaceFragment(new FragmentCalendar(), false, FragmentTags.FRAGMENT_CALENDAR);
        } else {
            ((FragmentCalendar) fr).setCustomActionBar();
            replaceFragment(fr, false, FragmentTags.FRAGMENT_CALENDAR);
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void hideSpinnerSort() {
        containerBtnSort.setVisibility(View.GONE);
        btnSort.setVisibility(View.GONE);
        spinnerSort.setVisibility(View.GONE);
    }

    @Override
    public void showSpinnerSort() {
        if (spinnerSort != null) {
            containerBtnSort.setVisibility(View.VISIBLE);
            btnSort.setVisibility(View.VISIBLE);
            spinnerSort.setVisibility(View.VISIBLE);
            getSupportActionBar().setDisplayShowCustomEnabled(false);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        updateItemMenuCountInvites();
        if (arg != null && (int) arg == Constants.INVITE_ACCESS) {
            FragmentMyProjects fr = (FragmentMyProjects) getSupportFragmentManager().findFragmentByTag(FragmentTags.FRAGMENT_MY_PROJECTS);
            if (fr != null) {
                fr.initRecycleViewProjects();
            }
        }
    }

    //==========================================
    // Invites
    //==========================================

    // показать что пришли новые сообщения
    private void updateItemMenuCountInvites() {
        View iteminvite = navigationView.getMenu().findItem(R.id.nav_menu_profile).getActionView().findViewById(R.id.title_count_invite);
        if (iteminvite != null) {
            if (ManagerInvite.getCountInvites() > 0) {
                iteminvite.setVisibility(View.VISIBLE);
                ((TextView) iteminvite).setText(String.valueOf(ManagerInvite.getCountInvites()));
            } else {
                iteminvite.setVisibility(View.GONE);
            }
        }
    }

    private String titleUpdate;

    @Override
    public void updateTitleProgressParseOutline(String title) {
        this.titleUpdate = title;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ManagerProgressDialog.changeTitle(titleUpdate);
            }
        });
    }
}