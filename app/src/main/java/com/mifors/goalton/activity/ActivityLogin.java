package com.mifors.goalton.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;

/**
 * Created by gly8 on 20.03.17.
 */

@SuppressWarnings("ALL")
public class ActivityLogin extends ActivityRootLoginRegister {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                ManagerAccount.logoutFull();
            }
        }, 2000);
        intiView();
    }

    private void intiView() {
        TextView titleRemindPass = (TextView) findViewById(R.id.title_remind_pass);
        Spannable spannableRemindPass = new SpannableString(getString(R.string.remind_pass_title));
        if (ManagerApplication.getInstance().isEnglishLocale()) {
            spannableRemindPass.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_light)), 37, 46, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableRemindPass.setSpan(new UnderlineSpan(), 37, 46, 0);
        } else {
            spannableRemindPass.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.blue_light)), 37, 51, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableRemindPass.setSpan(new UnderlineSpan(), 37, 51, 0);
        }

        titleRemindPass.setText(spannableRemindPass);
    }

    //==========================================
    // ONCLICK METHODS
    //==========================================
    public void onClickRegister(View view) {
        startActivity(new Intent(this, ActivityRegister.class));
    }

    public void onClickRemindPass(View view) {
        startActivity(new Intent(this, ActivityRemindPassword.class));
    }

    public void onClickAuthFacebook(View view) {
    }

    public void onClickAuthGoogle(View view) {
    }

    public void onClickAuthVk(View view) {
    }

    public void onClickLogin(View view) {
        String username = ((EditText) findViewById(R.id.edit_username_login)).getText().toString();
        String password = ((EditText) findViewById(R.id.edit_password_login)).getText().toString();
        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)) {
            if (ManagerApplication.getInstance().isConnectToInternet(true)) {
                startLoginUser(username, password);
            }
        } else {
            ManagerApplication.getInstance().showToast(R.string.error_empty_name_or_pass);
        }
    }


}