package com.mifors.goalton.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.SharedKeys;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.managers.utils.ManagerTakePhoto;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.network.HttpParam;
import com.mifors.goalton.utils.notifications.ReciverTimeNotifications;
import com.squareup.picasso.Picasso;

import java.util.Calendar;

/**
 * Created by gly8 on 20.02.18.
 */

public class ActivityTimeNotification extends ActivityRootSelectedBackground {

    private static final String TAG = "Goalton [" + ActivityTimeNotification.class.getSimpleName() + "]";
    public static final String KEY_ID_LINE = "KEY_ID_LINE_ACTIVITY";
    private Line line;
    private ImageView imageAtach;
    private ManagerTakePhoto managerTakePhoto;
    private int selectedItemIndex = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_notification);

        imageAtach = (ImageView) findViewById(R.id.attach_image);

        long idLine = getIntent().getLongExtra(KEY_ID_LINE, 0);
        if (idLine == 0) {
            finish();
        } else {
            line = Line.findByServerId(Line.class, idLine);
            if (line != null) {
                Log.v(TAG, "[onCreate] line = " + line);
                ((TextView) findViewById(R.id.title)).setText("" + line.getTitleReplacingTags());
                responseGetImage();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (line != null) {
            Log.v(TAG, "[onResume] ");
        }
        // Проигрывать мелодию
        // Проигрывать вибрацию
    }

    public void onClickClose(View view) {
        Log.v("TAG", "[onClickClose] ");
        finish();
    }



    public void onClickDefer(View view) {
        Log.v("TAG", "[onClickDefer] ");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        String[] arrTimes = getResources().getStringArray(R.array.defer_time);
        arrayAdapter.addAll(arrTimes);


        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle(R.string.defer_for_time);
        builderSingle.setSingleChoiceItems(arrayAdapter, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                selectedItemIndex = which;
            }
        });

        builderSingle.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setPositiveButton(R.string.selected, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (selectedItemIndex != -1) {
                    String strName = arrayAdapter.getItem(selectedItemIndex);
                    Log.v(TAG, "[onClick] selectedItem - " + which + "  " + strName);
                    int minute = 0;
                    switch (selectedItemIndex) {
                        case 0:
                            minute = 5;
                            break;
                        case 1:
                            minute = 30;
                            break;
                        case 2:
                            minute = 60;
                            break;
                    }

                    if (minute > 0 && line != null && line.getDeadlineDateStart() != null) {
                        line.getDeadlineDateStart().add(Calendar.MINUTE, minute);
                        if (line.getDeadlineDateEnd() != null) {
                            line.getDeadlineDateEnd().add(Calendar.MINUTE, minute);
                        }
                        line.updateDatesTextByCalendar();
                        line.save();
                    }

                    ReciverTimeNotifications.addNotification(line);
                    ManagerApplication.showToastStatic(R.string.title_defer_complete);
                    responseUpdate();
                    ManagerApplication.getInstance().getSharedManager().putKeyBoolean(SharedKeys.IS_UPDATE_CALENDAR_DATA_CHANGE_TIME_NOIFICATION, true);
                }
            }
        });
        builderSingle.show();
    }

    private void responseUpdate() {
        if (ManagerApplication.getInstance().isConnectToInternet(false)) {
            ManagerOutline.responseSetDeadlineDate(
                    line.getServerId(),
                    line.getDeadlineDateStartStr(),
                    line.getDeadlineDateStartTimeStr(),
                    line.getDuration(),
                    new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            if (msg.what == HttpParam.RESPONSE_OK) {
                                // Вызываем обновление календаря
                            }
                            finish();
                        }
                    });
        } else {
            line.setOfflineStatus(Line.StatusOffline.UPDATE);
            line.setModeEdit(ActivityEditLineOutline.Mode.UPDATE_LINE_CALENDAR);
            line.save();
            // Вызываем обновление календаря
            finish();
        }
    }

    private void responseGetImage() {
        if (line.getServerId() != 0) {
            if (ManagerApplication.getInstance().isConnectToInternet(false)) {

//                String url = HttpParam.BASEURL+"/note/img/1151857/350";
//                Picasso.with(this).load(url).into(imageAtach);

                ManagerOutline.responseGetImage(line.getServerId(), new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.obj != null) {
                            setImage((Bitmap) msg.obj);
                        }
                        super.handleMessage(msg);
                    }
                });
            } else {
                setImage(getManagerTakePhoto().getBitmapFromDirectory(line.getServerId()));
            }
        }
    }

    // Установка картинки из bitmapa
    private void setImage(Bitmap bmp) {
        if (bmp != null) {
            imageAtach.setAdjustViewBounds(true);
            imageAtach.setImageBitmap(bmp);
        }
        getManagerTakePhoto().saveBitmapToFileDirectory(bmp, line.getServerId());
    }

    public ManagerTakePhoto getManagerTakePhoto() {
        if (managerTakePhoto == null) {
            managerTakePhoto = new ManagerTakePhoto(this);
        }
        return managerTakePhoto;
    }
}