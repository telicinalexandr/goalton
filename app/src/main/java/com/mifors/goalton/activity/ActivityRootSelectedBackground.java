package com.mifors.goalton.activity;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.core.ManagerAccount;

/**
 * Created by gly8 on 23.03.17.
 */

@SuppressWarnings("ALL")
public class ActivityRootSelectedBackground extends AppCompatActivity {

    public void updateSelectedRoot() {
        View rootView = findViewById(R.id.root_view);
        if (rootView != null) {
            rootView.setBackgroundResource(ManagerAccount.getResourceSelectedBackgroundImage(this));
        }
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        updateSelectedRoot();
    }
}
