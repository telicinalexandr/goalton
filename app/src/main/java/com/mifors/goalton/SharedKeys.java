package com.mifors.goalton;

/**
 * Created by grag on 02.12.2015.
 */
@SuppressWarnings("ALL")
public class SharedKeys
{
    public static final String IS_LOGIN_USER = "IS_LOGIN_USER"; // BOOLEAN
    public static final String NAME_SELECTED_BG = "NAME_SELECTED_BG"; // STRING
    public static final String TEMP_IMAGE = "TEMP_IMAGE"; // STRING
    public static final String PASSWORD = "password"; // STRING
    public static final String LOGIN = "login"; // STRING
    public static final String VK_EMAIL = "email_vk"; // STRING - email пользователя который получили из социальной сети
    public static final String VK_TOKEN = "vk_token"; // STRING - token пользователя который получили из социальной сети
    public static final String TYPE_PROJECT_SORT = "type_project_sort"; // STRING тип сортировки проектов на окне с списком проектов
    public static final String ID_PROJECT_SELECTED = "ID_PROJECT_SELECTED"; // LONG - id проекта выбранного в outline
    public static final String TYPE_PROJECT_SORT_REVERSE = "type_project_sort_reverse"; // BOOLEAN - в какую сторону сортировать проекты по убыванию/возрастанию
    public static final String COUNT_INVITES = "count_invites"; // INTEGER
    public static final String IS_UPDATE_PROJECTS_LIST = "IS_UPDATE_PROJECTS_LIST"; // BOOLEAN - надо ли обновлять список с проектами чтобы обновить количество задач.
    public static final String ID_PROJECT_RELOAD_TO_LIST = "ID_PROJECT_RELOAD_TO_LIST"; // LONG - id проекта который надо обновить в списке
    public static final String ID_PROJECT_RELOAD_IMAGE = "ID_PROJECT_RELOAD_IMAGE"; // LONG - id проекта у которого надо обновить картинку
    public static final String ID_PROJECT_SETTINGS = "ID_PROJECT_SETTINGS"; // LONG - id проекта который настраиваем

    public static final String POSITION_SWIPE_FROM = "from_position_swipe"; // INTEGER
    public static final String POSITION_SWIPE_TO = "to_position_swipe"; // INTEGER
    public static final String INVITES = "INVITES"; // STRING - jsonobject
    public static final String HEIGHT_ITEM_LINE = "HEIGHT_ITEM_LINE"; // STRING - высота задачи на календаре
    public static final String HEIGHT_ACTION_BAR = "HEIGHT_ACTION_BAR"; // INTEGER
    public static final String HEIGHT_STATUS_BAR = "HEIGHT_STATUS_BAR"; // INTEGER
    public static final String HEIGHT_STEEP_MINUTE = "HEIGHT_STEEP_MINUTE"; // INTEGER - ширина одной минуты на календаре для расчета позиции задачи на экране
    public static final String HEIGHT_FLOATING_ACTION_BUTTON = "HEIGHT_FLOATING_ACTION_BUTTON"; // INTEGER - рамзер кнопки для разных вычислений
    public static final String WIDTH_ITEM_CALENDAR = "WIDTH_ITEM_CALENDAR"; // INTEGER - длина элемента календаря
    public static final String IS_FIRST_OPEN_CALENDAR_SESSION = "IS_FIRST_OPEN_CALENDAR_SESSION"; // BOOLEAN - если в текущей сессии открыли календарь первый раз для скрола к текущему времени

    public static final String WIDTH_COVER = "WIDTH_COVER"; // INTEGER
    public static final String HEIGHT_COVER = "HEIGHT_COVER"; // INTEGER
    public static final String CALENDAR_FILTER_ARRAY_PROJECTS = "ARRAY_FILTER_PROJECTS"; // STRING-ARRAY - календарь - фильтры - массив выбранных проектов для отображения
    public static final String CALENDAR_FILTER_TYPE_PROJECTS = "CALENDAR_FILTER_TYPE_PROJECTS"; // INTEGER
    public static final String EDIT_LINE_MODE = "EDIT_LINE_MODE"; // STRIING - режим редактирования задачи (outline,calendar)
    public static final String CURRENT_LANGUAGE = "CURRENT_LANGUAGE"; // STRING - текущий язык приложения

    public static final String NOTIFICATIONS_VIBRO = "NOTIFICATIONS_VIBRO"; // BOOLEAN - проигрывать ли вибро при показе напоминания
    public static final String NOTIFICATIONS_SOUND = "NOTIFICATIONS_SOUND"; // BOOLEAN - проигрывать ли музыку при показе нотификации
    public static final String NOTIFICATIONS_SOUND_PATH = "NOTIFICATIONS_SOUND_PATH"; // STRING - Путь к рингтону который проигрывается при показе уведомления
    public static final String NOTIFICATIONS_SOUND_NAME = "NOTIFICATIONS_SOUND_NAME"; // STRING - Имя рингтона
    public static final String IS_UPDATE_CALENDAR_DATA_CHANGE_TIME_NOIFICATION = "IS_UPDATE_CALENDAR_DATA_CHANGE_TIME_NOIFICATION"; // BOOLEAN - Надо ли обновлять календарь после показа нотификации и откладывания
}