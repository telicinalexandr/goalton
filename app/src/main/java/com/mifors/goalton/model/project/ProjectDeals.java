package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

/**
 * Created by gly8 on 15.09.17.
 */
@SuppressWarnings("ALL")
@Table(name = "ProjectDeals")
public class ProjectDeals extends ExtendedModelUnique implements InterfaceSpinnerItem {
    @Column(name = "name")
    private String name;
    @Column(name = "company_id")
    private long companyId;
    @Column(name = "project_id")
    private long projectId;
    @Column(name = "owner_user_id")
    private long ownerUserId;
    @Column(name = "creation_date")
    private String creationDate;
    @Column(name = "amount")
    private String amount;
    @Column(name = "close_date")
    private String closeDate;
    @Column(name = "stage")
    private long stage;
    @Column(name = "status")
    private int status;
    @Column(name = "sort")
    private int sort;
    @Column(name = "actual_close_date")
    private String actualCloseDate;

    //==========================================
    // Getter setter
    //==========================================
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return String.valueOf(getServerId());
    }

    @Override
    public void setValue(String value) {

    }

    public long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public long getStage() {
        return stage;
    }

    public void setStage(long stage) {
        this.stage = stage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getActualCloseDate() {
        return actualCloseDate;
    }

    public void setActualCloseDate(String actualCloseDate) {
        this.actualCloseDate = actualCloseDate;
    }
}
