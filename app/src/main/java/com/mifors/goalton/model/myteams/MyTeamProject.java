package com.mifors.goalton.model.myteams;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

import java.util.List;

/**
 * Created by gly8 on 23.06.17.
 */
@SuppressWarnings("ALL")
@Table(name = "MyTeamProject")
public class MyTeamProject extends ExtendedModelUnique {
    @Column(name = "teamId")
    private long teamId;
    @Column(name = "userId")
    private long userId;
    @Column(name = "projectId")
    private long projectId;
    @Column(name = "rightToView")
    private int rightToView;
    @Column(name = "rightToEdit")
    private int rightToEdit;
    @Column(name = "rightToEditProject")
    private int rightToEditProject;
    @Column(name = "rightToInvite")
    private int rightToInvite;
    @Column(name = "name")
    private String name;

    //==========================================
    // Methods
    //==========================================
    public static List<MyTeamProject> findItemsByTeamServerId(long teamId) {
        return new Select().from(MyTeamProject.class).where("teamId = ?", teamId).execute();
    }


    //==========================================
    // Getter setter
    //==========================================
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public int getRightToView() {
        return rightToView;
    }

    public void setRightToView(int rightToView) {
        this.rightToView = rightToView;
    }

    public int getRightToEdit() {
        return rightToEdit;
    }

    public void setRightToEdit(int rightToEdit) {
        this.rightToEdit = rightToEdit;
    }

    public int getRightToEditProject() {
        return rightToEditProject;
    }

    public void setRightToEditProject(int rightToEditProject) {
        this.rightToEditProject = rightToEditProject;
    }

    public int getRightToInvite() {
        return rightToInvite;
    }

    public void setRightToInvite(int rightToInvite) {
        this.rightToInvite = rightToInvite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }
}
