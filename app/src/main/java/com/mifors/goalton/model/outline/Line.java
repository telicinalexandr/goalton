package com.mifors.goalton.model.outline;

import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.activity.ActivityEditLineOutline;
import com.mifors.goalton.adapters.MultiLevelExpIndListAdapter;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.utils.calendar.PlannerFilter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gly8 on 14.04.17.
 */
@SuppressWarnings("ALL")
@Table(name = "Line")
public class Line extends ExtendedModelUnique implements MultiLevelExpIndListAdapter.ExpIndData {

    private static final String TAG = "Goalton [" + Line.class.getSimpleName() + "]";

    public static final String DATE_FORMAT_DATE_END_AND_TIME = "yyyy-MM-dd kk:mm:ss";
    public static final String DATE_FORMAT_DATE_END_NOT_TIME = "yyyy-MM-dd";
    public static final String DATE_FORMAT = "dd MMM yy";
    public static final String DATE_FORMAT_TIME = "dd MMM yy HH:mm";
    public static final String DATE_EMPTY = "0000-00-00 00:00:00";
    public static final String INBOX_ITEM = "<span class=\"bold bg2\">INBOX</span>";

    public static enum StatusOffline {
        CREATE,
        UPDATE,
        UPDATE_SORT,
        REMOVE;
    }

    @Column(name = "mode")
    private ActivityEditLineOutline.Mode modeEdit;

    @Column(name = "offline_status")
    private StatusOffline offlineStatus; // Статус того что сделано с задачей в офлайне

    @Column(name = "offline_status_sort")
    private StatusOffline offlineStatusSort; // Статус того что изменилась сортировка задачи

    @Column(name = "offline_id")
    private long offlineId; // Временный id задачи для хранения в базе

    @Column(name = "status_new")
    private boolean isNew;

    @Expose
    @SerializedName("title")
    @Column(name = "title")
    private String title;

    @Column(name = "title_replacing_html_tags")
    private String titleReplacingTags; // Текст задачи с удаленными HTML тегами

    @Expose
    @SerializedName("project_id")
    @Column(name = "project_id", index = true)
    private long projectId;

    @Expose
    @SerializedName("sort")
    @Column(name = "sort")
    private double sort; // Корнеыой парент id = 0

    @Expose
    @SerializedName("parent_item_id")
    @Column(name = "parent_item_id", index = true)
    private long parentItemId; // Корнеыой парент id = 0

    @Expose
    @SerializedName("creator_user_id")
    @Column(name = "creator_user_id")
    private long creatorUserId;

    @Expose
    @SerializedName("is_completed")
    @Column(name = "is_completed")
    private String isCompleted;

    @Expose
    @SerializedName("is_important")
    @Column(name = "is_important")
    private String isImportant;

    @Expose
    @SerializedName("company_id")
    @Column(name = "company_id")
    private long companyId;

    @Expose
    @SerializedName("company_name")
    @Column(name = "company_name")
    private String companyName;

    @Expose
    @SerializedName("assign_user_id")
    @Column(name = "assign_user_id")
    private String assignUserId;

    @Expose
    @SerializedName("assign_user_username")
    @Column(name = "assign_user_username")
    private String assignUserUsername;

    @Expose
    @SerializedName("project_stage_id")
    @Column(name = "project_stage_id")
    private long projectStageId;

    @Expose
    @SerializedName("project_stage_name")
    @Column(name = "project_stage_name")
    private String projectStageName;

    @Expose
    @SerializedName("duration")
    @Column(name = "duration")
    private int duration;

    @Column(name = "level")
    private int level; // Уровень вложености

    @Column(name = "color") // Картинка задачи
    private int color = Integer.MIN_VALUE;

    //----------------------- Deadline dates ----------------
    @Column(name = "deadline_date_start") // Начало дедлайна
    private Calendar deadlineDateStart;

    @Expose
    @SerializedName("deadline_date")
    @Column(name = "deadline_date_start_str")
    private String deadlineDateStartStr;

    @Expose
    @SerializedName("deadline_time")
    @Column(name = "deadline_date_start_time_str")
    private String deadlineDateStartTimeStr;

    @Column(name = "deadline_date_end") // Окончание дедлайна
    private Calendar deadlineDateEnd;

    @Column(name = "deadline_date_end_str")
    private String deadlineDateEndStr;

    @Column(name = "deadline_date_time_end_str")
    private String deadlineDateEndTimeStr;

    @Expose
    @SerializedName("deadline_date_format")
    @Column(name = "deadline_date_format")
    private String deadlineDateFormat;

    @Expose
    @SerializedName("deadline_time_format")
    @Column(name = "deadline_time_format") // Время сервера
    private String deadlineDateFormatTime;

    @Expose
    @SerializedName("completed_date")
    @Column(name = "completed_date")
    private String completeDate; // Дата когда закончили задачу - устанавливается на сервере когда выставляем что задача выполнена

    @Expose
    @SerializedName("img")
    @Column(name = "img")
    private String img; // Корнеыой парент id = 0

    //----------------------- Deadline dates ----------------

    public Line() {
    }

    //====================================================================================
    // implement interface methods multiExpandableListView
    //====================================================================================
    private boolean group; // true - элемент сгрупирован (закрыт)  false - элемент не сгрупирован (открыт)

    @Override
    public boolean isGroup() {
        return group;
    }

    @Override
    public List<? extends MultiLevelExpIndListAdapter.ExpIndData> getChildren() {
        return getChildrenParentLine(this.getServerId());
    }

    @Override
    public void setIsGroup(boolean b) {
        group = b;
    }

    @Override
    public int getCountChildren() {
        return Line.getCountChildByLine(this);
    }

    /*====================================================================================*/
    // Methods
    /*====================================================================================*/
    public static Line getLineInbox(long projectId) {
        return new Select().from(Line.class)
                .where("project_id = ? AND title = ?", projectId, INBOX_ITEM)
                .executeSingle();
    }

    public static List<Line> getChildrenParentLine(long parentItemId) {
        return new Select().from(Line.class)
                .where("parent_item_id = ? AND (offline_status != 'REMOVE' OR offline_status IS NULL)", parentItemId)
                .orderBy("sort ASC").execute();
    }

    public static List<Line> getChildrenParentLineAndProjectID(long parentItemId, long projectId, long level) {
        return new Select()
                .from(Line.class)
                .where("parent_item_id = ?", parentItemId)
                .and("project_id = ? AND (offline_status != 'REMOVE' OR offline_status IS NULL)", projectId)
                .and("level >= ?", level)
                .orderBy("sort ASC")
                .execute();
    }

    public static Line getLineNewForProject(long projectId) {
        return new Select()
                .from(Line.class)
                .where("project_id = ?", projectId)
                .and("status_new = ?", true)
                .orderBy("sort ASC").executeSingle();
    }

    public static int getCountChildByLine(Line line) {
        return getCountChildByLineId(line.getServerId());
    }

    public static int getCountChildByLineId(long lineId) {
        return new Select().from(Line.class)
                .where("parent_item_id = ? AND (offline_status != 'REMOVE' OR offline_status IS NULL)", lineId)
                .count();
    }

    public static List<Line> getChildsByProjectIdAndStatusCompleted(long projectId) {
        return new Select().from(Line.class)
                .where("project_id = ? AND (offline_status != 'REMOVE' OR offline_status IS NULL)", projectId)
                .execute();
    }

    public static int getCountChildByLineIdAndStatusCompleted(long lineId, String isCompleted) {
        return new Select().from(Line.class)
                .where("parent_item_id = ? AND is_completed=? AND (offline_status != 'REMOVE' OR offline_status IS NULL) ", lineId, isCompleted)
                .count();
    }

    public static Line getParent(Line line) {
        return new Select().from(Line.class)
                .where("server_id = ?", line.getParentItemId())
                .executeSingle();
    }

    public static List<Line> getOfflineLines() {
        return new Select().from(Line.class)
                .where("offline_status NOT NULL")
                .and("title NOT NULL")
                .execute();
    }

    public static int getOfflineLinesCount() {
        return new Select().from(Line.class)
                .where("offline_status NOT NULL")
                .and("title NOT NULL")
                .count();
    }

    public static int getOfflineLinesUpdateSortCount() {
        return new Select().from(Line.class)
                .where("offline_status_sort = 'UPDATE_SORT'")
                .count();
    }

    public static List<Line> getOfflineLinesUpdateSort() {
        return new Select().from(Line.class)
                .where("offline_status_sort = 'UPDATE_SORT'")
                .execute();
    }

    // Количество линий созданных офлайн 
    public static int getCountCheckOfflineLines(long projectId) {
        return new Select().from(Line.class)
                .where("project_id = ? ", projectId, true)
                .and("is_offline_change = ?", true)
                .count();
    }

    // Повышение уровня детей рекурсивным способом
    // Вне зависимости от того какой уровень у них был раньше.
    public void recursiveUpdateChildsLevel(Line parent, @Nullable List<Line> childs) {
        List<Line> listChild = (List<Line>) parent.getChildren();
        for (Line line : listChild) {
            line.setLevel(parent.getLevel() + 1); // Все дети имеют уровень на 1 больше чем у родителя
            if (childs != null) {
                childs.add(line);
            }
            line.save();
            if (line.getCountChild() > 0) {
                recursiveUpdateChildsLevel(line, childs);
            }
        }
    }

    public static int getCountLinesFromDB(long projectServerId) {
        return new Select().from(Line.class).where("project_id = ?", projectServerId).count();
    }

    public boolean isLoadParent() {
        Line line = Line.findByServerId(Line.class, getParentItemId());
        return line == null;
    }

    public static boolean isLoadLines(long projectId) {
        int countLinesRootElements = new Select().from(Line.class).where("project_id = ? AND level = 1", projectId).count();
        if (countLinesRootElements == 0) {
            return true;
        } else {
            Project project = Project.findByServerId(Project.class, projectId);
            if (!TextUtils.isEmpty(project.getCountItems()) && (Integer.parseInt(project.getCountItems()) != project.getCountLinesDb())) {
                return true;
            }
        }

        return false;
    }

    public static void deleteAllByProjectId(long projectId) {
        new Delete().from(Line.class).where("project_id = ? ", projectId).execute();
    }

    @Override
    public String toString() {
        return "\r\n{ sort = " + sort +
                " level = " + level +
                " serverId = " + getServerId() +
                " parentItemId = " + getParentItemId() +
                " offlineStatusSort = " + getOfflineStatusSort() +
                " oflineStatus = " + getOfflineStatus() +
                " modeEdit = " + getModeEdit() +
                " title = " + getTitle() +
//                " modeEdit = " + getModeEdit() +
//                " serverId = " + getServerId() +
//                " level = "+level+
//                " sort = "+sort+
//                " deadlineStart = " + ManagerCalendar.convertCalendarToString(getDeadlineDateStart()) +
//                " deadlineDateEnd = " + ManagerCalendar.convertCalendarToString(getDeadlineDateEnd()) +
                "}";
    }

    public String convertCalendarToDateDeadLine(Calendar calendar) {
        return ManagerCalendar.convertCalendarToString(calendar, "dd/MM/yy");
    }

    public int getBetweenHourDeadLineDatesStartEnd() {
        if (getDeadlineEndDateInt() < getDeadlineStartTimeInt()) {
            return 24 - getDeadlineStartTimeInt();
        } else {
            return getDeadlineEndDateInt() - getDeadlineStartTimeInt();
        }
    }

    public int getDeadlineEndDateInt() {
        try {
            if (deadlineDateEnd != null) {
                return deadlineDateEnd.get(Calendar.HOUR_OF_DAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getDeadlineStartTimeInt() {
        try {
            if (deadlineDateStart != null) {
                return deadlineDateStart.get(Calendar.HOUR_OF_DAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Line)) {
            return false;
        }
        return this.getServerId() == ((Line) obj).getServerId();
    }

    @Override
    public int hashCode() {
        int result = 31;
        return Long.bitCount(result * getServerId());
    }

    public void removeDeadlineDates() {
        setDeadlineDateEnd(null);
        setDeadlineDateEndStr("");
        setDeadlineDateEndTimeStr("");
        setDuration(0);
    }

    // Возвращает задачи которые относятся к календарю на переданный день с выборкой по фильтру
    public static List<Line> getPlannerLinesForDayNotDeadlineByFilter(long startTime, long endTime, PlannerFilter filter) {
        StringBuilder query = new StringBuilder("deadline_date_start >= ? AND deadline_date_start < ? AND deadline_date_end IS NULL AND (offline_status != 'REMOVE' OR offline_status IS NULL) ");
        query.append(filter.getQueryProfiles());
        String q = createQueryPathByPlannerFilter(query.toString(), filter);
        return new Select().from(Line.class).where(q, startTime, endTime).execute();
    }

    // Возвращает задачи которые относятся к календарю на переданный день
    public static List<Line> getPlannerLinesForDeadlineDateByFilter(long startTime, long endTime, PlannerFilter filter) {
        StringBuilder query = new StringBuilder("deadline_date_start >= ? AND deadline_date_start < ? AND deadline_date_end IS NOT NULL AND (offline_status != 'REMOVE' OR offline_status IS NULL)");
        query.append(filter.getQueryProfiles());
        String q = createQueryPathByPlannerFilter(query.toString(), filter);
        return new Select().from(Line.class).where(q, startTime, endTime).execute();
    }

    // Создание запроса по выбранным проектам и пользователям
    private static String createQueryPathByPlannerFilter(String query, PlannerFilter plannerFilter) {
        if (plannerFilter == null) {
            return query;
        }

        StringBuilder builderProjects = new StringBuilder();
        StringBuilder builderAssigns = new StringBuilder();
        int countProjects = Project.getCount(Project.class);
        // Если всего один проект и id = 0 то показываем все
        if (plannerFilter.getProjectsChecked().size() == 1
                && plannerFilter.getProjectsChecked().get(0) == 0
                || (plannerFilter.getProjectsChecked().size() - 1 == countProjects)) {

        } else {
            ArrayList<Long> arrayList = plannerFilter.getCopyArrayProjectStages();
            arrayList.remove(new Long(0));
            if (arrayList.size() == 1) {
                builderProjects.append(" AND ").append("project_id = ").append(arrayList.get(0));
            } else if (arrayList.size() > 1) {
                builderProjects.append(" AND (");
                for (int i = 0; i < arrayList.size(); i++) {
                    if (arrayList.get(i) != 0) {
                        builderProjects.append("project_id = ").append(arrayList.get(i));
                        if (i == arrayList.size() - 1) {

                        } else {
                            builderProjects.append(" OR ");
                        }
                    }
                }
                builderProjects.append(")");
            }
        }

        if (!TextUtils.isEmpty(builderProjects)) {
            query += builderProjects.toString();
        }

        if (!TextUtils.isEmpty(builderAssigns)) {
            query += builderAssigns.toString();
        }

        query += " ORDER BY deadline_date_start ASC";
        return query;
    }

    // Возвращает задачи которые относятся к календарю на переданный день
    public static List<Line> getPlannerLinesForDayNotDeadline(long startTime, long endTime) {
        return new Select().from(Line.class).where("deadline_date_start >= ? AND deadline_date_start < ?", startTime, endTime).execute();
    }

    public void updateDatesTextByCalendar() {
        updateDatesTextByCalendarNotSave();
        if (getServerId() != 0) {
            save();
        }
    }

    public void updateDatesTextByCalendarNotSave() {
        if (getDeadlineDateStart() != null) {
            setDeadlineDateStartStr(ManagerCalendar.convertCalendarToString(getDeadlineDateStart(), DATE_FORMAT_DATE_END_NOT_TIME));
            setDeadlineDateStartTimeStr(ManagerCalendar.convertCalendarToString(getDeadlineDateStart(), "HH:mm:ss"));
            setDeadlineDateFormat(ManagerCalendar.convertCalendarToString(getDeadlineDateStart(), DATE_FORMAT));
        } else {
            setDeadlineDateStartStr(null);
            setDeadlineDateStartTimeStr(null);
            setDeadlineDateFormat(null);
        }

        if (getDeadlineDateEnd() != null) {
            setDeadlineDateEndStr(ManagerCalendar.convertCalendarToString(getDeadlineDateEnd(), DATE_FORMAT_DATE_END_NOT_TIME + " HH:mm:ss"));
            setDeadlineDateEndTimeStr(ManagerCalendar.convertCalendarToString(getDeadlineDateEnd(), "HH:mm:ss"));
        } else {
            setDeadlineDateEndStr(null);
            setDeadlineDateEndTimeStr(null);
        }
    }

    // Удаление задач у переданной недели
    public static void deleteLinesByWeek(int weekOfYear, int year) {

        ActiveAndroid.beginTransaction();
        try {
            List<Line> list = getLinesByWeek(weekOfYear, year);
            for (int i = 0; i < list.size(); i++) {
                list.get(i).delete();
            }
            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }
    }

    public static List<Line> getLinesByWeek(int weekOfYear, int year) {
        weekOfYear++;
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        // НЕ УДАЛЯТЬ!! WARNING this line is required to make the code work.
        // Might have something to do with an inner function called
        // complete which gets called on get.
        calendar.get(Calendar.DAY_OF_WEEK);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        long timeStartWeek = calendar.getTimeInMillis();
        long endTimeWeek = timeStartWeek + (1000 * 60 * 60 * 24 * 6);
        calendar.setTimeInMillis(endTimeWeek);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        return Line.getPlannerLinesForDayNotDeadline(timeStartWeek, endTimeWeek);
    }

    public static HashMap<Long, Integer> getHasmapLevels(int week, int year) {
        List<Line> list = getLinesByWeek(week, year);
        HashMap<Long, Integer> map = new HashMap();
        for (Line line : list) {
            map.put(new Long(line.getServerId()), new Integer(line.getLevel()));
        }
        return map;
    }

    public static void deleteByServerId(long serverId) {
        Line line = Line.findByServerId(Line.class, serverId);
        if (line != null) {
            line.delete();
        }
    }

    public static void deleteChildrens(long parentId) {
        new Delete().from(Line.class).where("parent_item_id = ?", parentId).execute();
    }

    public void updateFinishDateForCurrentDuration() {
        if (duration == 0) {
            setDeadlineDateEnd(null);
        } else if (getDeadlineDateStart() != null) {
            Calendar dateEnd = Calendar.getInstance();
            dateEnd.setTimeInMillis(getDeadlineDateStart().getTimeInMillis());
            dateEnd.add(Calendar.MINUTE, duration);
            setDeadlineDateEnd(dateEnd);
        } else {
            setDeadlineDateEnd(null);
        }
    }

    public void updateReplaceTitleFromCurentTitle() {
        if (TextUtils.isEmpty(getTitle())) {
            setTitleReplacingTags("");
        } else {
            setTitleReplacingTags(Html.fromHtml(getTitle().replace("<br />", "")).toString());
        }
    }

    public void update(Line line) {
        this.setOfflineStatus(line.getOfflineStatus());
        this.setOfflineStatusSort(line.getOfflineStatusSort());
        this.setOfflineId(line.getOfflineId());
        this.setTitle(line.getTitle());
        this.setTitleReplacingTags(line.getTitleReplacingTags());
        this.setProjectId(line.getProjectId());
        this.setSort(line.getSort());
        this.setLevel(line.getLevel());
        this.setParentItemId(line.getParentItemId());
        this.setCreatorUserId(line.getCreatorUserId());
        this.setIsCompleted(line.getIsCompleted());
        this.setIsImportant(line.getIsImportant());
        this.setCompanyId(line.getCompanyId());
        this.setCompanyName(line.getCompanyName());
        this.setAssignUserId(line.getAssignUserId());
        this.setAssignUserUsername(line.getAssignUserUsername());
        this.setProjectStageId(line.getProjectStageId());
        this.setProjectStageName(line.getProjectStageName());
        this.setDuration(line.getDuration());
        this.setLevel(line.getLevel());
        this.setColor(line.getColor());
        this.setDeadlineDateStart(line.getDeadlineDateStart());
        this.setDeadlineDateStartStr(line.getDeadlineDateStartStr());
        this.setDeadlineDateStartTimeStr(line.getDeadlineDateStartTimeStr());
        this.setDeadlineDateEnd(line.getDeadlineDateEnd());
        this.setDeadlineDateEndStr(line.getDeadlineDateEndStr());
        this.setDeadlineDateEndTimeStr(line.getDeadlineDateEndTimeStr());
        this.setDeadlineDateFormat(line.getDeadlineDateFormat());
        this.setDeadlineDateFormatTime(line.getDeadlineDateFormatTime());
        this.setCompleteDate(line.getCompleteDate());
    }

    //====================================================================================
    // Getter setter
    //====================================================================================

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setTitleReplacingTags(String titleReplacingTags) {
        this.titleReplacingTags = titleReplacingTags;
    }

    public String getTitleReplacingTags() {
        return titleReplacingTags;
    }

    // Setter
    public void setTitle(String title) {
        this.title = title;
    }

    public void setParentItemId(long parentItemId) {
        this.parentItemId = parentItemId;
    }

    public void setCreatorUserId(long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public void setIsCompleted(String isCompleted) {
        this.isCompleted = isCompleted;
    }

    public void setIsImportant(String isImportant) {
        this.isImportant = isImportant;
    }

    public void setDeadlineDateStartStr(String deadlineDateStartStr) {
        this.deadlineDateStartStr = deadlineDateStartStr;
    }

    public void setDeadlineDateStartTimeStr(String deadlineDateStartTimeStr) {
        this.deadlineDateStartTimeStr = deadlineDateStartTimeStr;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public void setCompanyId(long companyId) {
        this.companyId = companyId;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setAssignUserId(String assignUserId) {
        this.assignUserId = assignUserId;
    }

    public void setAssignUserUsername(String assignUserUsername) {
        this.assignUserUsername = assignUserUsername;
    }

    public void setDeadlineDateFormat(String deadlineDateFormat) {
        this.deadlineDateFormat = deadlineDateFormat;
    }

    public void setProjectStageId(long projectStageId) {
        this.projectStageId = projectStageId;
    }

    public void setProjectStageName(String projectStageName) {
        this.projectStageName = projectStageName;
    }

    // Getter
    public String getTitle() {
        return title;
    }

    public long getParentItemId() {
        return parentItemId;
    }

    public long getCreatorUserId() {
        return creatorUserId;
    }

    public String getIsCompleted() {
        return isCompleted;
    }

    public String getIsImportant() {
        return isImportant;
    }

    public String getDeadlineDateStartStr() {
        return deadlineDateStartStr;
    }

    public String getDeadlineDateStartTimeStr() {
        return deadlineDateStartTimeStr;
    }

    public String getDeadlineDateFormat() {
        return deadlineDateFormat;
    }

    public long getProjectId() {
        return projectId;
    }

    public long getCompanyId() {
        return companyId;
    }

    public long getOfflineId() {
        return offlineId;
    }

    public void setOfflineId(long offlineId) {
        this.offlineId = offlineId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getAssignUserId() {
        return assignUserId;
    }

    public String getAssignUserUsername() {
        return assignUserUsername;
    }

    public long getProjectStageId() {
        return projectStageId;
    }

    public String getProjectStageName() {
        return projectStageName;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void setSort(double sort) {
        this.sort = sort;
    }

    public double getSort() {
        return sort;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    // 1- выполнено, 0-невыполнено
    public boolean isComplete() {
        return "1".equals(getIsCompleted());
    }

    public int getCountChild() {
        return Line.getCountChildByLine(this);
    }

    public int getCountChildByStatusCompleted(String status) {
        return Line.getCountChildByLineIdAndStatusCompleted(this.getServerId(), status);
    }

    public ActivityEditLineOutline.Mode getModeEdit() {
        return modeEdit;
    }

    public void setModeEdit(ActivityEditLineOutline.Mode modeEdit) {
        this.modeEdit = modeEdit;
    }

    @Override
    public long getServerId() {
        if (super.getServerId() == 0) {
            return offlineId;
        }
        return super.getServerId();
    }

    public boolean isImportant() {
        return "1".equals(getIsImportant());
    }

    public void setDeadlineDateEndTimeStr(String deadlineDateEndTimeStr) {
        this.deadlineDateEndTimeStr = deadlineDateEndTimeStr;
    }

    public String getDeadlineDateEndTimeStr() {
        return deadlineDateEndTimeStr;
    }

    public String getDeadlineDateEndStr() {
        return deadlineDateEndStr;
    }

    public void setDeadlineDateEndStr(String deadlineDateEndStr) {
        this.deadlineDateEndStr = deadlineDateEndStr;
    }

    public Calendar getDeadlineDateStart() {
        return deadlineDateStart;
    }

    public void setDeadlineDateStart(Calendar deadlineDateStart) {
        this.deadlineDateStart = deadlineDateStart;
    }

    public Calendar getDeadlineDateEnd() {
        return deadlineDateEnd;
    }

    public void setDeadlineDateEnd(Calendar deadlineDateEnd) {
        this.deadlineDateEnd = deadlineDateEnd;
    }

    public String getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(String completeDate) {
        this.completeDate = completeDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDeadlineDateFormatTime() {
        return deadlineDateFormatTime;
    }

    public void setDeadlineDateFormatTime(String deadlineDateFormatTime) {
        this.deadlineDateFormatTime = deadlineDateFormatTime;
    }

    public StatusOffline getOfflineStatus() {
        return offlineStatus;
    }

    public void setOfflineStatus(StatusOffline offlineStatus) {
        this.offlineStatus = offlineStatus;
    }

    public StatusOffline getOfflineStatusSort() {
        return offlineStatusSort;
    }

    public void setOfflineStatusSort(StatusOffline offlineStatusSort) {
        this.offlineStatusSort = offlineStatusSort;
    }
}