package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModel;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gly8 on 31.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "ProjectRight")
public class ProjectRight extends ExtendedModel implements InterfaceSpinnerItem {

    /**
     * team_id: "5526",
     * name: "sadsdfsdf",
     * owner_user_id: "5505",
     * username: "telicin",
     * email: "telicin@mifors.com",
     **/

    @Column(name = "parent_project_id")
    private long parentProjectId;  // server_id проекта к которому относятся эти настройки

    @Expose
    @SerializedName("team_id")
    @Column(name = "team_id")
    private long teamId; // В базе на сервере является foregin key

    @Expose
    @SerializedName("owner_user_id")
    @Column(name = "owner_user_id") // В базе на сервере является foregin key
    private long ownerUserId; // Id владельца

    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;

    @Expose
    @SerializedName("username")
    @Column(name = "username")
    private String username;

    @Expose
    @SerializedName("email")
    @Column(name = "email")
    private String email;

    @Expose
    @SerializedName("users")
    private ArrayList<ProjectRightUser> listRights;

    //==========================================
    // Methods
    //==========================================
    public static ProjectRight findByTeamId(long teamId) {
        return new Select().from(ProjectRight.class).where("team_id=?", teamId).executeSingle();
    }

    public static ProjectRight findByTeamIdAndOwnerUser(long teamId, long ownerUserId) {
        return new Select().from(ProjectRight.class).where("team_id=? AND owner_user_id=?", teamId, ownerUserId).executeSingle();
    }

    public static List<ProjectRight> findByParentProjectId(long projectId) {
        return new Select().from(ProjectRight.class).where("parent_project_id = ?", projectId).execute();
    }

    public static void deleteAllByProject(long projectId){
        deleteByKeyAndValue(ProjectRight.class, "parent_project_id", projectId);
    }

    //==========================================
    // Gette setter
    //==========================================

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<ProjectRightUser> getListRights() {
        if (listRights == null || listRights.size() == 0) {
            setListRights((ArrayList<ProjectRightUser>) ProjectRightUser.getAllRightsByProjectRight(this));
        }
        return listRights;
    }

    public void setListRights(ArrayList<ProjectRightUser> listRights) {
        this.listRights = listRights;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return String.valueOf(teamId);
    }

    @Override
    public void setValue(String value) {

    }

    public long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public long getParentProjectId() {
        return parentProjectId;
    }

    public void setParentProjectId(long parentProjectId) {
        this.parentProjectId = parentProjectId;
    }

}