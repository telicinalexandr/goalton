package com.mifors.goalton.model.team;


import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gly8 on 27.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "TeamProfile")
public class TeamProfile extends ExtendedModel {
    @Expose
    @SerializedName("user_id")
    @Column(name = "jsonServerId")
    private long jsonServerId;

    @Expose
    @SerializedName("username")
    @Column(name = "name")
    private String username;

    @Expose
    @SerializedName("email")
    @Column(name = "email")
    private String email;


    @Expose
    @SerializedName("is_active")
    @Column(name = "is_active")
    private int isActive;

    @Expose
    @SerializedName("users_team_id")
    @Column(name = "users_team_id", unique = true)
    private int usersTeamId;

    @Expose
    @SerializedName("projects")
    ArrayList<TeamProject> teamProjects;

    @Column(name = "team")
    private Team team;

    //==========================================
    // METHODS
    //==========================================
    public TeamProfile() {
    }

    public List<TeamProfile> getAll() {
        return new Select().from(TeamProfile.class).execute();
    }

    public static void deleteByUserTeamId(long userTeamId) {
        new Delete().from(TeamProfile.class).where("users_team_id = ?", userTeamId).execute();
    }

    public TeamProject getProjectById(long projectId){
        if (teamProjects != null) {
            for (TeamProject p : teamProjects) {
                if (p.getProjectId() == projectId) {
                    return p;
                }
            }
        }
        return null;
    }

    public static TeamProfile findByUserTeamId(long userTeamId){
        return new Select().from(TeamProfile.class).where("users_team_id = ?", userTeamId).executeSingle();
    }

    public static List<TeamProfile> findByTeam(long teamId){
        return new Select().from(TeamProfile.class).where("team = ?", teamId).execute();
    }

    //==========================================
    // Getter setter
    //==========================================
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getUsersTeamId() {
        return usersTeamId;
    }

    public long getJsonServerId() {
        return jsonServerId;
    }

    public void setJsonServerId(long jsonServerId) {
        this.jsonServerId = jsonServerId;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setUsersTeamId(int usersTeamId) {
        this.usersTeamId = usersTeamId;
    }

    public ArrayList<TeamProject> getTeamProjects() {
        if (teamProjects == null || teamProjects.size() == 0) {
            teamProjects = (ArrayList<TeamProject>)TeamProject.findByTeamProfileId(usersTeamId);
        }
        return teamProjects;
    }

    public void setTeamProjects(ArrayList<TeamProject> teamProjects) {
        this.teamProjects = teamProjects;
    }
}
