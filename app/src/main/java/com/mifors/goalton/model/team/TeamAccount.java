package com.mifors.goalton.model.team;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gly8 on 28.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "TeamAccount")
public class TeamAccount extends ExtendedModelUnique
{
    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;

    @Expose
    @SerializedName("id") // Хранит значение id объекта с сервера
    @Column(name = "jsonServerId")
    private long jsonServerId;

    @Expose
    @SerializedName("owner_user_id")
    @Column(name = "owner_user_id")
    private String ownerUserId;

    @Expose
    @SerializedName("tariff_id")
    @Column(name = "tariff_id")
    private String tariffId;

    @Expose
    @SerializedName("tariff_name")
    @Column(name = "tariff_name")
    private String tariffName;

    @Expose
    @SerializedName("tariff_limit_item")
    @Column(name = "tariff_limit_item")
    private String tariffLimitItem;

    @Expose
    @SerializedName("tariff_limit_user")
    @Column(name = "tariff_limit_user")
    private String tariffLimitUser;

    @Expose
    @SerializedName("count_user")
    @Column(name = "count_user")
    private String countUser;

    @Expose
    @SerializedName("count_project")
    @Column(name = "count_project")
    private String countProject;

    @Expose
    @SerializedName("count_item")
    @Column(name = "count_item")
    private String countItem;

    @Expose
    @SerializedName("teams")
    ArrayList<Team> teams;
    //==========================================
    // METHODS
    //==========================================
    public static TeamAccount finByServerId(long uuid) {
        return findByServerId(TeamAccount.class, uuid );
    }

    public List<TeamAccount> getAll() {
        return new Select().from(TeamAccount.class).execute();
    }

    //==========================================
    // GETTER SETTER
    //==========================================
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(String ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getTariffLimitItem() {
        return tariffLimitItem;
    }

    public void setTariffLimitItem(String tariffLimitItem) {
        this.tariffLimitItem = tariffLimitItem;
    }

    public String getTariffLimitUser() {
        return tariffLimitUser;
    }

    public void setTariffLimitUser(String tariffLimitUser) {
        this.tariffLimitUser = tariffLimitUser;
    }

    public String getCountUser() {
        return countUser;
    }

    public void setCountUser(String countUser) {
        this.countUser = countUser;
    }

    public String getCountProject() {
        return countProject;
    }

    public void setCountProject(String countProject) {
        this.countProject = countProject;
    }

    public String getCountItem() {
        return countItem;
    }

    public void setCountItem(String countItem) {
        this.countItem = countItem;
    }

    public long getJsonServerId() {
        return jsonServerId;
    }

    public void setJsonServerId(long jsonServerId) {
        this.jsonServerId = jsonServerId;
    }

    public ArrayList<Team> getTeams(){
        if (teams == null){
            teams = (ArrayList)Team.getTeamsByAccount(this);
        }
        return teams;
    }
    public void setTeams(ArrayList<Team> teams){
        this.teams = teams;
    }
}
