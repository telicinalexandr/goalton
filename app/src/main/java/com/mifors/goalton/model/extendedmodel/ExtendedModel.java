package com.mifors.goalton.model.extendedmodel;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

/**
 * Created by zettig on 28.11.2016.
 */

@SuppressWarnings("ALL")
public class ExtendedModel extends Model {

    //==========================================
    // Methods
    //==========================================

    public static <T extends ExtendedModel> int clearTable(Class<T> type) {
        ActiveAndroid.beginTransaction();
        try {
            List<T> list = new Select().from(type).execute();
            for (T t : list) {
                t.delete();
            }

            ActiveAndroid.setTransactionSuccessful();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return 0;
    }

    public static <T extends ExtendedModel> int getCount(Class<T> type) {
        int count = new Select().from(type).count();
        return count;
    }

    //==========================================
    // Find
    //==========================================
    public static <T extends ExtendedModel> T finbyDevId(Class<T> type, String devId) {
        return new Select().from(type).where("devid = ?", devId).executeSingle();
    }

    public static <T extends ExtendedModel> T finByKeySingle(Class<T> type, String columnName, Object value) {
        return new Select().from(type).where(columnName + "= ?", value).executeSingle();
    }

    public static <T extends ExtendedModel> List<T> finByKeyExecute(Class<T> type, String columnName, Object value) {
        List<T> list = new Select().from(type).where(columnName + "= ?", value).execute();
        return list;
    }

    /**
     * Совершает поиск по переданному имени колонки и значению
     *
     * @param type       - class у которого нужно найти объект
     * @param columnName - поле по которому делается поиск
     * @param serverId   - уникальный id объекта пришедшего с сервера
     * @return возвращает объект из базы. Может вернуть null.
     */
    public static <T extends ExtendedModel> T findByFieldAndLongValue(Class<T> type, String columnName, long serverId) {
        return new Select().from(type).where(columnName + " = ? ", serverId).executeSingle();
    }

    public static <T extends ExtendedModel> List<T> getAll(Class<T> type) {
        return new Select().from(type).execute();
    }

    //==========================================
    // Delete
    //==========================================
    public static <T extends ExtendedModel> void deleteByKeyAndValue(Class<T> type, String columnName, Object value) {
        List<T> list = new Select().from(type).where(columnName + "= ?", value).execute();
        for (T instance : list) {
            instance.delete();
        }
    }

    public static void deleteAll(Class<? extends ExtendedModel> type) {
        new Delete().from(type).execute();
    }
}