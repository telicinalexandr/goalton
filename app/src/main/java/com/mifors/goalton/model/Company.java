package com.mifors.goalton.model;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.List;

/**
 * Created by gly8 on 18.04.17.
 */
@SuppressWarnings("ALL")
@Table(name = "Company")
public class Company extends ExtendedModelUnique implements InterfaceSpinnerItem{

    @Expose
    @SerializedName("id")
    @Column(name = "jsonServerId")
    private long jsonServerId;
    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;
    @Expose
    @SerializedName("owner_user_id")
    @Column(name = "owner_user_id")
    private String owner_user_id;
    @Expose
    @SerializedName("owner_team_id")
    @Column(name = "owner_team_id")
    private String owner_team_id;
    @Expose
    @SerializedName("comment")
    @Column(name = "comment")
    private String comment;
    @Expose
    @SerializedName("address")
    @Column(name = "address")
    private String address;
    @Expose
    @SerializedName("industry_id")
    @Column(name = "industry_id")
    private String industry_id;
    @Expose
    @SerializedName("industry")
    @Column(name = "industry")
    private String industry;
    @Expose
    @SerializedName("comment2")
    @Column(name = "comment2")
    private String comment2;
    @Expose
    @SerializedName("assigned_to")
    @Column(name = "assigned_to")
    private String assigned_to;
    @Expose
    @SerializedName("birthdate_company")
    @Column(name = "birthdate_company")
    private String birthdate_company;
    @Expose
    @SerializedName("uppername")
    @Column(name = "uppername")
    private String uppername;

    @Expose
    @SerializedName("is_projects")
    @Column(name = "is_projects")
    private boolean isProjects;
    //==========================================
    // Methods
    //==========================================
    public static List<Company> getAllByProjects(){
        return new Select().from(Company.class).where("is_projects = ?", true).execute();
    }

    //==========================================
    // GETTER SETER
    //==========================================

    public void setJsonServerId(long jsonServerId) {
        this.jsonServerId = jsonServerId;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return String.valueOf(getServerId());
    }

    @Override
    public void setValue(String value) {

    }

    public void setOwner_user_id(String owner_user_id) {
        this.owner_user_id = owner_user_id;
    }

    public void setOwner_team_id(String owner_team_id) {
        this.owner_team_id = owner_team_id;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIndustry_id(String industry_id) {
        this.industry_id = industry_id;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setComment2(String comment2) {
        this.comment2 = comment2;
    }

    public void setAssigned_to(String assigned_to) {
        this.assigned_to = assigned_to;
    }

    public void setBirthdate_company(String birthdate_company) {
        this.birthdate_company = birthdate_company;
    }

    public void setUppername(String uppername) {
        this.uppername = uppername;
    }

    public long getJsonServerId() {
        return jsonServerId;
    }

    public String getName() {
        return name;
    }

    public String getOwner_user_id() {
        return owner_user_id;
    }

    public String getOwner_team_id() {
        return owner_team_id;
    }

    public String getComment() {
        return comment;
    }

    public String getAddress() {
        return address;
    }

    public String getIndustry_id() {
        return industry_id;
    }

    public String getIndustry() {
        return industry;
    }

    public String getComment2() {
        return comment2;
    }

    public String getAssigned_to() {
        return assigned_to;
    }

    public String getBirthdate_company() {
        return birthdate_company;
    }

    public String getUppername() {
        return uppername;
    }

    public void setIsProjects(boolean prjects) {
        isProjects = prjects;
    }
}
