package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelFindByUniqueField;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.List;

/**
 * Created by gly8 on 31.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "ProjectSubject")
public class ProjectSubject extends ExtendedModelFindByUniqueField implements InterfaceSpinnerItem {

    @Column(name = "parent_project_id")
    private long parentProjectId;  // server_id проекта к которому относятся эти настройки

    @Expose
    @SerializedName("id")
    @Column(name = "server_id", unique = true)
    private long serverId;

    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;

    @Column(name = "project_id")
    private long projectId;

    //==========================================
    // Methods
    //==========================================
    public static List<ProjectSubject> getSubjectByProjectId(long projectId) {
        return new Select().from(ProjectSubject.class).where("project_id = ?", projectId).execute();
    }

    public static void deleteAllByProject(long projectId){
        new Delete().from(ProjectSubject.class).where("project_id = ?", projectId).execute();
    }

    //==========================================
    // Getter setetr
    //==========================================
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return String.valueOf(serverId);
    }

    @Override
    public void setValue(String value) {

    }

    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }

    public long getParentProjectId() {
        return parentProjectId;
    }

    public void setParentProjectId(long parentProjectId) {
        this.parentProjectId = parentProjectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }
}
