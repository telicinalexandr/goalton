package com.mifors.goalton.model.team;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gly8 on 27.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "Team")
public class Team extends ExtendedModelUnique {
    @Expose
    @SerializedName("id")
    @Column(name = "jsonServerId")
    private long jsonServerId;

    @Expose
    @Column(name = "name")
    private String name;

    @Expose
    @SerializedName("users")
    @Column(name = "profiles", onDelete = Column.ForeignKeyAction.CASCADE)
    ArrayList<TeamProfile> profiles;

    @Column(name = "teamAccount", onDelete = Column.ForeignKeyAction.CASCADE)
    private TeamAccount teamAccount;

    //==========================================
    // Methods
    //==========================================
    public List<Team> getAll() {
        return new Select().from(Team.class).execute();
    }

    public static Team finByServerId(long uuid) {
        return findByServerId(Team.class, uuid);
    }

    public static List<Team> getTeamsByAccount(TeamAccount teamAccount){
        return new Select().from(Team.class).where("teamAccount = ?", teamAccount.getId()).execute();
    }

    //==========================================
    // Getter setter
    //==========================================
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<TeamProfile> getProfiles() {
        if (profiles == null){
            profiles = (ArrayList<TeamProfile>) TeamProfile.findByTeam(this.getId());
        }
        return profiles;
    }

    public void setProfiles(ArrayList<TeamProfile> profiles) {
        this.profiles = profiles;
    }

    public long getJsonServerId() {
        return jsonServerId;
    }

    public void setJsonServerId(long jsonServerId) {
        this.jsonServerId = jsonServerId;
    }

    public TeamAccount getTeamAccount() {
        return teamAccount;
    }

    public void setTeamAccount(TeamAccount teamAccount) {
        this.teamAccount = teamAccount;
    }
}
