package com.mifors.goalton.model.user;

import android.text.TextUtils;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

/**
 * Created by gly8 on 20.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "Profile")
public class Profile extends ExtendedModelUnique {
    @Expose
    @Column(name = "username")
    private String username;
    @SerializedName("auth_key")
    @Expose
    @Column(name = "auth_key")
    private String authKey;

    @SerializedName("password_hash")
    @Expose
    @Column(name = "password_hash")
    private String passwordHash;

    @SerializedName("password_reset_token")
    @Expose
    @Column(name = "password_reset_token")
    private String passwordResetToken;

    @SerializedName("email")
    @Expose
    @Column(name = "email")
    private String email;

    @SerializedName("status")
    @Expose
    @Column(name = "status")
    private int status;

    @SerializedName("created_at")
    @Expose
    @Column(name = "created_at")
    private long createdAt;

    @SerializedName("updated_at")
    @Expose
    @Column(name = "updated_at")
    private long updatedAt;

    @SerializedName("bg")
    @Expose
    @Column(name = "bg")
    private String bg;

    @SerializedName("firstname")
    @Expose
    @Column(name = "firstname")
    private String firstname;

    @SerializedName("lastname")
    @Expose
    @Column(name = "lastname")
    private String lastname;

    @SerializedName("timezone")
    @Expose
    @Column(name = "timezone")
    private String timezone;

    @SerializedName("country")
    @Expose
    @Column(name = "country")
    private String country;

    @SerializedName("get_todolist")
    @Expose
    @Column(name = "get_todolist")
    private int getTodolist;

    @SerializedName("company_name")
    @Expose
    @Column(name = "company_name")
    private String companyName;

    @SerializedName("company_size")
    @Expose
    @Column(name = "company_size")
    private String companySize;

    private String password;
    @Column(name = "is_my_profile")
    private boolean isMyProfile;


    //==========================================
    // Methods
    //==========================================
    public Profile() {
    }

    @Override
    public String toString() {
        return "[Profile] username = "+username+"  email = "+email;
    }

    public static Profile getMyProfile() {
        return new Select().from(Profile.class).where("is_my_profile=?", true).executeSingle();
    }

    public String getFio(){
        String fio="";
        if (!TextUtils.isEmpty(getFirstname()) ) {
             fio = getFirstname();
        }

        if (!TextUtils.isEmpty(getLastname())) {
            fio += " "+getLastname();
        }
        return fio;
    }
    //==========================================
    // Getter setter
    //==========================================

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getPasswordResetToken() {
        return passwordResetToken;
    }

    public void setPasswordResetToken(String passwordResetToken) {
        this.passwordResetToken = passwordResetToken;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getGetTodolist() {
        return getTodolist;
    }

    public void setGetTodolist(int getTodolist) {
        this.getTodolist = getTodolist;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanySize() {
        return companySize;
    }

    public void setCompanySize(String companySize) {
        this.companySize = companySize;
    }

    public boolean isMyProfile() {
        return isMyProfile;
    }

    public void setIsMyProfile(boolean myProfile) {
        isMyProfile = myProfile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
