package com.mifors.goalton.model.team;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

import java.util.List;

/**
 * Created by gly8 on 28.03.17.
 */

@SuppressWarnings("ALL")
@Table(name = "TeamProject")
public class TeamProject extends ExtendedModelUnique {

    @Expose
    @SerializedName("project_id")
    @Column(name = "project_id")
    private long projectId;

    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;

    @Expose
    @SerializedName("right_to_view")
    @Column(name = "right_to_view")
    private String rightToView;

    @Expose
    @SerializedName("right_to_edit")
    @Column(name = "right_to_edit")
    private String rightToEdit;

    @Expose
    @SerializedName("right_to_edit_project")
    @Column(name = "right_to_edit_project")
    private String rightToEditProject;

    @Expose
    @SerializedName("right_to_invite")
    @Column(name = "right_to_invite")
    private String rightToInvite;

    @Expose
    @SerializedName("arUserProject")
    private TeamPermissionProfile profilesPermissions;

    @Column(name = "team_id")
    private long teamId;

    @Column(name = "team_profile_id")
    private long teamProfileId;

    //==========================================
    // Methods
    //==========================================

    public List<TeamProject> getAll() {
        return new Select().from(TeamProject.class).execute();
    }

    public static TeamProject findByProjectId(long projectId){
        return new Select().from(TeamProject.class).where("project_id = ?", projectId).executeSingle();
    }

    public static List<TeamProject> findByUserTeamId(long userTeamId){
        return new Select().from(TeamProject.class).where("team_id = ?", userTeamId).execute();
    }

    public static List<TeamProject> findByTeamProfileId(long userTeamId){
        return new Select().from(TeamProject.class).where("team_profile_id = ?", userTeamId).execute();
    }

    //==========================================
    // Getter setter
    //==========================================
    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRightToView() {
        return rightToView;
    }

    public void setRightToView(String rightToView) {
        this.rightToView = rightToView;
    }

    public String getRightToEdit() {
        return rightToEdit;
    }

    public void setRightToEdit(String rightToEdit) {
        this.rightToEdit = rightToEdit;
    }

    public String getRightToEditProject() {
        return rightToEditProject;
    }

    public void setRightToEditProject(String rightToEditProject) {
        this.rightToEditProject = rightToEditProject;
    }

    public String getRightToInvite() {
        return rightToInvite;
    }

    public void setRightToInvite(String rightToInvite) {
        this.rightToInvite = rightToInvite;
    }

//    public TeamProfile getTeamProfile() {
//        return teamProfile;
//    }
//
//    public void setTeamProfile(TeamProfile teamProfile) {
//        this.teamProfile = teamProfile;
//    }

    public TeamPermissionProfile getProfilesPermissions() {
        return profilesPermissions;
    }

    public void setProfilesPermissions(TeamPermissionProfile profilesPermissions) {
        this.profilesPermissions = profilesPermissions;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public long getTeamProfileId() {
        return teamProfileId;
    }

    public void setTeamProfileId(long teamProfileId) {
        this.teamProfileId = teamProfileId;
    }
}
