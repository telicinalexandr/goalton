package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModel;

import java.util.List;

/**
 * Created by gly8 on 03.04.17.
 */

@SuppressWarnings("ALL")
public class ProjectRightUser extends ExtendedModel {
    /**
     * {
     * team_id: "5526",
     * user_id: "5506",
     * username: "lolik",
     * email: "Zema34200@mail.ru",
     * right_to_view: 1,
     * right_to_edit: 1,
     * right_to_edit_project: 0,
     * right_to_invite: 0
     * }
     **/

    @Column(name = "parent_project_id")
    private long parentProjectId;  // server_id проекта к которому относятся эти настройки

    @Expose
    @SerializedName("team_id")
    @Column(name = "team_id")
    private long teamId;

    @Expose
    @SerializedName("user_id")
    @Column(name = "user_id")
    private long userId;

    @Expose
    @SerializedName("username")
    @Column(name = "username")
    private String username;

    @Expose
    @SerializedName("email")
    @Column(name = "email")
    private String email;

    @Expose
    @SerializedName("right_to_view")
    @Column(name = "right_to_view")
    private String rightToView="0";

    @Expose
    @SerializedName("right_to_edit")
    @Column(name = "right_to_edit")
    private String rightToEdit="0";

    @Expose
    @SerializedName("right_to_edit_project")
    @Column(name = "right_to_edit_project")
    private String rightToEditProject="0";

    @Expose
    @SerializedName("right_to_invite")
    @Column(name = "right_to_invite")
    private String rightToInvite="0";

    @Column(name = "project_right", onDelete = Column.ForeignKeyAction.CASCADE)
    private ProjectRight projectRight;

    //==========================================
    // Methods
    //==========================================

    public static ProjectRightUser findByProjectRight(long teamId, long userId, ProjectRight projectRight) {
        return new Select().from(ProjectRightUser.class).where("team_id=? AND user_id=? AND project_right=?", teamId, userId, projectRight.getId()).executeSingle();
    }

    public static ProjectRightUser findByUserIdANDTeamId(long teamId, long userId) {
        return new Select().from(ProjectRightUser.class).where("team_id=? AND user_id=?", teamId, userId).executeSingle();
    }

    public static List<ProjectRightUser> getAllRightsByProjectRight(ProjectRight projectRight) {
        return new Select().from(ProjectRightUser.class).where("project_right = ?", projectRight.getId()).execute();
    }

    public static void deleteAllByProject(long projectId){
        deleteByKeyAndValue(ProjectRight.class, "parent_project_id", projectId);
    }

    @Override
    public String toString() {
        return "{username = "+username+"  rightToView = "+rightToView+"   rightToEdit = "+rightToEdit+"  rightToEditProject = "+rightToEditProject+"  rightToInvite = "+rightToInvite+"}";
    }

    //==========================================
    // Getter seter
    //==========================================
    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRightToView() {
        return rightToView;
    }

    public void setRightToView(String rightToView) {
        this.rightToView = rightToView;
    }

    public String getRightToEdit() {
        return rightToEdit;
    }

    public void setRightToEdit(String rightToEdit) {
        this.rightToEdit = rightToEdit;
    }

    public String getRightToEditProject() {
        return rightToEditProject;
    }

    public void setRightToEditProject(String rightToEditProject) {
        this.rightToEditProject = rightToEditProject;
    }

    public String getRightToInvite() {
        return rightToInvite;
    }

    public void setRightToInvite(String rightToInvite) {
        this.rightToInvite = rightToInvite;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public ProjectRight getProjectRight() {
        return projectRight;
    }

    public void setProjectRight(ProjectRight projectRight) {
        this.projectRight = projectRight;
    }

    public long getParentProjectId() {
        return parentProjectId;
    }

    public void setParentProjectId(long parentProjectId) {
        this.parentProjectId = parentProjectId;
    }

}
