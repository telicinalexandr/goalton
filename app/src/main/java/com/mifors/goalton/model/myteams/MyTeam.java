package com.mifors.goalton.model.myteams;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

/**
 * Created by gly8 on 23.06.17.
 */
@SuppressWarnings("ALL")
@Table(name = "MyTeam")
public class MyTeam extends ExtendedModelUnique {

    @Column(name = "user_id")
    private long userId;
    @Column(name = "team_id")
    private long teamId;
    @Column(name = "is_active")
    private int isActive;
    @Column(name = "name")
    private String name;
    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
