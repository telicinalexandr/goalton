package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gly8 on 30.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "Project")
public class Project extends ExtendedModelUnique implements InterfaceSpinnerItem{

    @Expose
    @SerializedName("id")
    @Column(name = "jsonServerId")
    private long jsonServerId;

    @Expose
    @SerializedName("owner_user_id")
    @Column(name = "owner_user_id")
    private long ownerUserId;
    @Expose
    @SerializedName("right_to_edit_project")
    @Column(name = "right_to_edit_project")
    private String rightToEditProject;
    @Expose
    @SerializedName("right_to_edit")
    @Column(name = "right_to_edit")
    private String rightToEdit;
    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;
    @Expose
    @SerializedName("img")
    @Column(name = "img")
    private String img;
    @Expose
    @SerializedName("type")
    @Column(name = "type")
    private String type;
    @Expose
    @SerializedName("count_items")
    @Column(name = "count_items")
    private String countItems;
    @Expose
    @SerializedName("count_items_is_overdue")
    @Column(name = "count_items_is_overdue")
    private String countItemsIsOverdue;
    @Expose
    @SerializedName("count_items_is_completed")
    @Column(name = "count_items_is_completed")
    private String countItemsIsCompleted;
    @Expose
    @SerializedName("percent_complete")
    @Column(name = "percent_complete")
    private int percentComplete;
    @Expose
    @SerializedName("other_user_name")
    @Column(name = "other_user_name")
    private String otherUserName;

    @Column(name = "urlImage")
    private String urlImage;

    // Поля объекта в настройках
    @Expose
    @SerializedName("creator_user_id")
    @Column(name = "creator_user_id")
    private long creatorUserId;
    @Expose
    @SerializedName("team_id")
    @Column(name = "team_id")
    private long teamId;
    @Expose
    @SerializedName("is_active")
    @Column(name = "is_active")
    private int isActive;
    @Expose
    @SerializedName("subject_id")
    @Column(name = "subject_id")
    private long subjectId;
    @Expose
    @SerializedName("show_completed")
    @Column(name = "show_completed")
    private int showCompleted;
    @Expose
    @SerializedName("show_columns")
    @Column(name = "show_columns")
    private String showColumns;
    @Expose
    @SerializedName("show_columns_tl")
    @Column(name = "show_columns_tl")
    private String showColumnsTl;

    @SerializedName("users")
    private ArrayList<ProjectRight> listRights;

    @SerializedName("is_revert_cover")
    private boolean isRevertCover; // Была ли повернута обложка

    //==========================================
    // Methods
    //==========================================
    public static void deleteFromBd(long serverId) {
        Project prj = findByServerId(Project.class, serverId);
        if (prj != null) {
            prj.delete();
        }
    }

    @Override
    public String toString() {
        return ""+name;
    }

    // Возвращает все проекты кроме новых
    public static List<Project> getAllAndNotNew() {
        return new Select().from(Project.class).where(KEY_SERVER_ID+" > 0").execute();
    }

    public int getCountLinesDb(){
        return new Select().from(Line.class).where("project_id = ?", getServerId()).count();
    }

    //==========================================
    // Getter setter
    //==========================================

    public boolean isShowCompletedLines(){
        return getShowCompleted() > 0;
    }

    public long getJsonServerId() {
        return jsonServerId;
    }

    public void setJsonServerId(long jsonServerId) {
        this.jsonServerId = jsonServerId;
    }

    public long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public String getRightToEditProject() {
        return rightToEditProject;
    }

    public void setRightToEditProject(String rightToEditProject) {
        this.rightToEditProject = rightToEditProject;
    }

    public String getRightToEdit() {
        return rightToEdit;
    }

    public void setRightToEdit(String rightToEdit) {
        this.rightToEdit = rightToEdit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return String.valueOf(getServerId());
    }

    @Override
    public void setValue(String value) {

    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountItems() {
        return countItems;
    }

    public void setCountItems(String countItems) {
        this.countItems = countItems;
    }

    public String getCountItemsIsOverdue() {
        return countItemsIsOverdue;
    }

    public void setCountItemsIsOverdue(String countItemsIsOverdue) {
        this.countItemsIsOverdue = countItemsIsOverdue;
    }

    public String getCountItemsIsCompleted() {
        return countItemsIsCompleted;
    }

    public void setCountItemsIsCompleted(String countItemsIsCompleted) {
        this.countItemsIsCompleted = countItemsIsCompleted;
    }

    public int getPercentComplete() {
        return percentComplete;
    }

    public void setPercentComplete(int percentComplete) {
        this.percentComplete = percentComplete;
    }

    public String getOtherUserName() {
        return otherUserName;
    }

    public void setOtherUserName(String otherUserName) {
        this.otherUserName = otherUserName;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public long getCreatorUserId() {
        return creatorUserId;
    }

    public void setCreatorUserId(long creatorUserId) {
        this.creatorUserId = creatorUserId;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public int getShowCompleted() {
        return showCompleted;
    }

    public void setShowCompleted(int showCompleted) {
        this.showCompleted = showCompleted;
    }

    public String getShowColumns() {
        return showColumns;
    }

    public void setShowColumns(String showColumns) {
        this.showColumns = showColumns;
    }

    public String getShowColumnsTl() {
        return showColumnsTl;
    }

    public void setShowColumnsTl(String showColumnsTl) {
        this.showColumnsTl = showColumnsTl;
    }

    public boolean isNewProject() {
        return getServerId() == 0;
    }

    public boolean isRevertCover() {
        return isRevertCover;
    }

    public void setRevertCover(boolean revertCover) {
        isRevertCover = revertCover;
    }
}
