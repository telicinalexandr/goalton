package com.mifors.goalton.model.extendedmodel;

import com.activeandroid.query.Select;

/**
 * Created by gly8 on 03.04.17.
 */

@SuppressWarnings("ALL")
public class ExtendedModelFindByUniqueField extends ExtendedModel {

    /**
     * Ищет объект по переданым параметрам если не находит возвращает
     * только созданый объект.
     *
     * @param type       - class у которого нужно найти объект
     * @param columnName - поле по которому делается поиск
     * @param value      - уникальное значение по которому делается поиск
     * @return - объет из базы или только содзанный в методе и записанный в базу
     */
    public static <T extends ExtendedModelUnique> T findOrCreateByField(Class<T> type, String columnName, long value) {
        T instance = new Select().from(type).where(columnName + " = ? ", value).executeSingle();
        if (instance == null) {
            try {
                instance = type.newInstance();
                instance.setServerId(value);
                instance.save();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }
}
