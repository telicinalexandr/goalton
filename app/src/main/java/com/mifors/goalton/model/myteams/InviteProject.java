package com.mifors.goalton.model.myteams;

import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

/**
 * Created by gly8 on 23.06.17.
 */

@SuppressWarnings("ALL")
public class InviteProject implements InterfaceSpinnerItem {
    private String value;
    private String name;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
