package com.mifors.goalton.model.myteams;

import java.util.Calendar;

/**
 * Created by gly8 on 23.06.17.
 */

@SuppressWarnings("ALL")
public class Invite {
    private long id;
    private long ownerUserId;//": 9321,

    private long teamId;//": 9397,

    private long projectId;//": 15396,

    private long userId;//": 3,

    private String email;//": "topman@buketclub.ru",

    private String status;//": "invite",

    private String createdAtText;//": "2017-06-23 20:08:11",
    private String updatedAtText;//": "2017-06-23 20:08:11"
    private Calendar createdA;
    private Calendar updateAt;

    private String nameProject;
    private String nameTeam;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAtText() {
        return createdAtText;
    }

    public void setCreatedAtText(String createdAtText) {
        this.createdAtText = createdAtText;
    }

    public String getUpdatedAtText() {
        return updatedAtText;
    }

    public void setUpdatedAtText(String updatedAtText) {
        this.updatedAtText = updatedAtText;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Calendar getCreatedA() {
        return createdA;
    }

    public void setCreatedA(Calendar createdA) {
        this.createdA = createdA;
    }

    public Calendar getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Calendar updateAt) {
        this.updateAt = updateAt;
    }

    public String getNameProject() {
        return nameProject;
    }

    public void setNameProject(String nameProject) {
        this.nameProject = nameProject;
    }

    public String getNameTeam() {
        return nameTeam;
    }

    public void setNameTeam(String nameTeam) {
        this.nameTeam = nameTeam;
    }
}
