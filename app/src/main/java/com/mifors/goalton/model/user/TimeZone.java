package com.mifors.goalton.model.user;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.mifors.goalton.model.extendedmodel.ExtendedModel;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

/**
 * Created by gly8 on 24.03.17.
 */

@SuppressWarnings("ALL")
@Table(name = "TimeZone")
public class TimeZone extends ExtendedModel implements InterfaceSpinnerItem {
    @Column(name = "name")
    private String name;
    @Column(name = "value")
    private String value;

    //==========================================
    // Methods
    //==========================================

    public static TimeZone findCountryByValue(String value) {
        return new Select().from(TimeZone.class).where("value=?", value).executeSingle();
    }

    //==========================================
    // Getter setter
    //==========================================
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }
}
