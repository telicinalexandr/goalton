package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.List;

/**
 * Created by gly8 on 31.03.17.
 */
@SuppressWarnings("ALL")
@Table(name = "ProjectStage")
public class ProjectStage extends ExtendedModelUnique implements InterfaceSpinnerItem {

    public enum StageField {
        RENAME(1),
        MAX_DAYS(2),
        WIP_DAYS(3);

        private int number;

        StageField(int number) {
            this.number = number;
        }

        public int getNumber() {
            return number;
        }
    }

    /**
     * {
     * id: 30307,
     * project_id: 8618,
     * name: "todo",
     * sort: 0,
     * period: null,
     * limit_item: null
     * },
     **/

    @Column(name = "parent_project_id")
    private long parentProjectId;  // server_id проекта к которому относятся эти настройки

    @Expose
    @SerializedName("id")
    @Column(name = "jsonServerid")
    private long jsonServerid;
    @Expose
    @SerializedName("project_id")
    @Column(name = "project_id")
    private long projectId;
    @Expose
    @SerializedName("name")
    @Column(name = "name")
    private String name;
    @Expose
    @SerializedName("sort")
    @Column(name = "sort")
    private String sort;
    @Expose
    @SerializedName("period")
    @Column(name = "period")
    private String period;
    @Expose
    @SerializedName("limit_item")
    @Column(name = "limit_item")
    private String limitItem;

    @Column(name = "is_delete")
    private boolean isDelete;

    @Column(name = "is_new")
    private boolean isNew;

    //==========================================
    // Methods
    //==========================================

    public static List<ProjectStage> findByProjectId(long projectId) {
        return new Select().from(ProjectStage.class).where("project_id = ?", projectId).execute();
    }

    public static ProjectStage findByProjectIdAndServerId(long projectId, long serverId) {
        return new Select().from(ProjectStage.class).where("project_id = ? AND "+KEY_SERVER_ID+" = ?", projectId, serverId).executeSingle();
    }

    public static List<ProjectStage> findByProjectIdAndNotDelete(long projectId) {
        return new Select().from(ProjectStage.class).where("project_id = ? AND is_delete=?", projectId, false).execute();
    }

    // Количество этапов в проекте
    public static int countStagesProject(long projectId) {
        return new Select().from(ProjectStage.class).where("project_id = ?", projectId).count();
    }

    // Количество созданных этапов в проекте
    public static int countNewStagesToProject(long projectId) {
        return new Select().from(ProjectStage.class).where("project_id = ? AND is_new = ?", projectId, true).count();
    }

    // Все новые этапы проекта
    public static List<ProjectStage> getAllNewStages(long projectId) {
        return new Select().from(ProjectStage.class).where("project_id = ? AND is_new = ?", projectId, true).execute();
    }

    public static void deleteAllByProjectNewItems(long projectId) {
        new Delete().from(ProjectStage.class).where("project_id = ? AND is_new = ?", projectId, true).execute();
    }

    public static void deleteAllByProject(long projectId) {
        new Delete().from(ProjectStage.class).where("project_id = ?", projectId).execute();
    }

    public static List<ProjectStage> getAllByProjectId(long projectId) {
        return new Select().from(ProjectStage.class).where("project_id = ?", projectId).execute();
    }

    //==========================================
    // Getter seter
    //==========================================
    public long getJsonServerid() {
        return jsonServerid;
    }

    public void setJsonServerid(long jsonServerid) {
        this.jsonServerid = jsonServerid;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getValue() {
        return String.valueOf(getServerId());
    }

    @Override
    public void setValue(String value) {

    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getLimitItem() {
        return limitItem;
    }

    public void setLimitItem(String limitItem) {
        this.limitItem = limitItem;
    }

    public long getParentProjectId() {
        return parentProjectId;
    }

    public void setParentProjectId(long parentProjectId) {
        this.parentProjectId = parentProjectId;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean getIsNewLocal() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}