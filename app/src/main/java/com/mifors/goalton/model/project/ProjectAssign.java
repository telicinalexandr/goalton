package com.mifors.goalton.model.project;

import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;
import com.mifors.goalton.ui.spinner.interfaces.InterfaceSpinnerItem;

import java.util.List;

/**
 * Created by gly8 on 11.07.17.
 */
@SuppressWarnings("ALL")
@Table(name = "ProjectAssign")
public class ProjectAssign extends ExtendedModelUnique implements InterfaceSpinnerItem{
    @Column(name = "user_uame")
    private String userName;

    @Column(name = "project_id")
    private long projectID;

    public String getUserName() {
        return userName;
    }

    //==========================================
    // Methods
    //==========================================
    public ProjectAssign() {
    }

    public ProjectAssign(String userName, long projectID) {
        this.userName = userName;
        this.projectID = projectID;
    }

    //==========================================
    // Getter setter
    //==========================================
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getProjectID() {
        return projectID;
    }

    public void setProjectID(long projectID) {
        this.projectID = projectID;
    }

    public static List<ProjectAssign> findByProjectID(long projectId) {
        return new Select().from(ProjectAssign.class).where("project_id = ?", projectId).execute();
    }

    @Override
    public String getName() {
        return userName;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getValue() {
        return String.valueOf(getServerId());
    }

    @Override
    public void setValue(String value) {

    }
}
