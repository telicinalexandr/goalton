package com.mifors.goalton.model.extendedmodel;

import com.activeandroid.annotation.Column;
import com.activeandroid.query.Select;

/**
 * Created by ivanisaev on 13.08.15.
 */
@SuppressWarnings("ALL")
public abstract class ExtendedModelUnique extends ExtendedModelFindByUniqueField {

    public static final String KEY_SERVER_ID = "server_id";

    @Column(name = KEY_SERVER_ID, unique = true, index = true)
    private long serverId;

    //==========================================
    // Methods
    //==========================================
    // Поиск
    public static <T extends ExtendedModelUnique> T findOrCreate(Class<T> type, long serverId) {
        return findOrCreateByField(type, KEY_SERVER_ID, serverId);
    }

    // Поиск
    public static <T extends ExtendedModel> T findByServerId(Class<T> type, long uuid) {
        return new Select().from(type).where(KEY_SERVER_ID+"=?", uuid).executeSingle();
    }

    // Поиск
    public static <T extends ExtendedModel> T findById(Class<T> type, long id) {
        return new Select().from(type).where("id=?", id).executeSingle();
    }

    // Поиск
    public static <T extends ExtendedModel> T find(Class<T> type, long uuid, String key) {
        return new Select().from(type).where(key + "=?", uuid).executeSingle();
    }

    // Удаление
    public static <T extends ExtendedModel> T  deleteByServerId(Class<T> type, long serverId){
        T instance = new Select().from(type).where(KEY_SERVER_ID+" = ? ", serverId).executeSingle();
        if (instance != null) {
            instance.delete();
        }
        return null;
    }

    // Удаление по ключу
    public static <T extends ExtendedModel> T  deleteByKey(Class<T> type, String nameColumn, Object key){
        T instance = new Select().from(type).where(nameColumn+" = ? ", key).executeSingle();
        if (instance != null) {
            instance.delete();
        }
        return null;
    }
    //==========================================
    // Geter seter
    //==========================================
    public long getServerId() {
        return serverId;
    }

    public void setServerId(long serverId) {
        this.serverId = serverId;
    }
}