package com.mifors.goalton.model.team;

import com.activeandroid.annotation.Column;
import com.activeandroid.query.Select;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mifors.goalton.model.extendedmodel.ExtendedModelUnique;

import java.util.List;

/**
 * Created by gly8 on 28.03.17.
 */

@SuppressWarnings("ALL")
public class TeamPermissionProfile extends ExtendedModelUnique {
    @Expose
    @SerializedName("id") // Хранит значение id объекта с сервера
    @Column(name = "jsonServerId")
    private long jsonServerId;

    @Expose
    @SerializedName("user_id")
    @Column(name = "user_id")
    private String userId;
    @Expose
    @SerializedName("project_id")
    @Column(name = "project_id")
    private String projectId;

    @Expose
    @SerializedName("right_to_view")
    @Column(name = "right_to_view")
    private String rightToView;

    @Expose
    @SerializedName("right_to_edit")
    @Column(name = "right_to_edit")
    private String rightToEdit;

    @Expose
    @SerializedName("right_to_edit_project")
    @Column(name = "right_to_edit_project")
    private String rightToEditProject;

    @Expose
    @SerializedName("right_to_invite")
    @Column(name = "right_to_invite")
    private String rightToInvite;

    @Column(name = "teamProject", onDelete = Column.ForeignKeyAction.CASCADE)
    private TeamProject teamProject;
    //==========================================
    // Methods
    //==========================================

    public List<TeamPermissionProfile> getAll() {
        return new Select().from(TeamAccount.class).execute();
    }

    public static TeamPermissionProfile getByUserIdAndProjectId(long userTeamId, long projectId){
        return new Select().from(TeamPermissionProfile.class).where("user_id = ? AND project_id = ?", userTeamId, projectId).executeSingle();
    }

    //==========================================
    // Getter setter
    //==========================================
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getRightToView() {
        return rightToView;
    }

    public void setRightToView(String rightToView) {
        this.rightToView = rightToView;
    }

    public String getRightToEdit() {
        return rightToEdit;
    }

    public void setRightToEdit(String rightToEdit) {
        this.rightToEdit = rightToEdit;
    }

    public String getRightToEditProject() {
        return rightToEditProject;
    }

    public void setRightToEditProject(String rightToEditProject) {
        this.rightToEditProject = rightToEditProject;
    }

    public String getRightToInvite() {
        return rightToInvite;
    }

    public void setRightToInvite(String rightToInvite) {
        this.rightToInvite = rightToInvite;
    }

    public long getJsonServerId() {
        return jsonServerId;
    }

    public void setJsonServerId(long jsonServerId) {
        this.jsonServerId = jsonServerId;
    }

    public TeamProject getTeamProject() {
        return teamProject;
    }

    public void setTeamProject(TeamProject teamProject) {
        this.teamProject = teamProject;
    }
}
