package com.mifors.goalton;

/**
 * Created by gly8 on 07.08.17.
 */

@SuppressWarnings("ALL")
public final class Constants {
    public static final int INVITE_ACCESS = 101;
    public static final String INVITE_ACCEPT = "accepted";
    public static final String INVITE_REJECTED = "rejected";
}
