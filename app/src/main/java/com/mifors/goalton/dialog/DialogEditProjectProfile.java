package com.mifors.goalton.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.model.project.Project;
import com.mifors.goalton.model.project.ProjectRightUser;


public class DialogEditProjectProfile extends DialogFragment {
    private Project project;

    private ProjectRightUser projectRightUser;
    private CheckBox viewItems, editItems, editProject, invite;
    private View rootView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_edit_team_permission, container, false);
        init();
        return rootView;
    }

    public void init() {
        initViews();
        initData();
        if (projectRightUser.getUserId() == ManagerAccount.getMyProfile().getServerId()) {
            disableViews();
        }
    }

    private void initViews() {
        rootView.findViewById(R.id.dialog_permission_ok).setOnClickListener(onBtnOkClick);
        rootView.findViewById(R.id.dialog_permission_cancel).setOnClickListener(onBtnCancelClick);

        viewItems = rootView.findViewById(R.id.permission_view_items);
        editItems = rootView.findViewById(R.id.permission_edit_items);
        editProject = rootView.findViewById(R.id.permission_edit_project);
        invite = rootView.findViewById(R.id.permission_invite);

        viewItems.setOnCheckedChangeListener(onViewItemsChecked);
        editItems.setOnCheckedChangeListener(onEditItemsChecked);
        editProject.setOnCheckedChangeListener(onEditProjectChecked);

    }

    private void disableViews() {
        viewItems.setEnabled(false);
        editItems.setEnabled(false);
        editProject.setEnabled(false);
        invite.setEnabled(false);
    }

    private void initData() {
        ((TextView) rootView.findViewById(R.id.permission_project_name)).setText(project.getName());
        ((TextView) rootView.findViewById(R.id.permission_profile_name)).setText(projectRightUser.getUsername());

        if (projectRightUser.getRightToEditProject().equals("1")) {
            editProject.setChecked(true);
        } else if (projectRightUser.getRightToEdit().equals("1")) {
            editItems.setChecked(true);
        } else if (projectRightUser.getRightToView().equals("1")) {
            viewItems.setChecked(true);
        }

        if (projectRightUser.getRightToInvite().equals("1")) {
            invite.setChecked(true);
        }
    }

    /////////
    //
    // LISTENERS
    //
    /////////

    private View.OnClickListener onBtnCancelClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    private View.OnClickListener onBtnOkClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String viewI = viewItems.isChecked() ? "1" : "0";
            String editI = editItems.isChecked() ? "1" : "0";
            String editP = editProject.isChecked() ? "1" : "0";
            String inv = invite.isChecked() ? "1" : "0";

            projectRightUser.setRightToView(viewI);
            projectRightUser.setRightToEdit(editI);
            projectRightUser.setRightToEditProject(editP);
            projectRightUser.setRightToInvite(inv);
            projectRightUser.save();

            dismiss();
        }
    };

    private CompoundButton.OnCheckedChangeListener onViewItemsChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (!isChecked) {
                editItems.setChecked(false);
                editProject.setChecked(false);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onEditItemsChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                viewItems.setChecked(true);
            } else {
                editProject.setChecked(false);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onEditProjectChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                editItems.setChecked(true);
            }
        }
    };

    //==========================================
    // Getter setter
    //==========================================
    public void setProject(Project project) {
        this.project = project;
    }

    public void setProjectRightUser(ProjectRightUser projectRightUser) {
        this.projectRightUser = projectRightUser;
    }
}
