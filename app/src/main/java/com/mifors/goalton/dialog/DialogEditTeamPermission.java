package com.mifors.goalton.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.core.ManagerAccount;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.model.team.TeamPermissionProfile;
import com.mifors.goalton.model.team.TeamProfile;
import com.mifors.goalton.model.team.TeamProject;
import com.mifors.goalton.network.HttpParam;


public class DialogEditTeamPermission extends DialogFragment {
    public static final String PROJECT_ID_KEY = "PROJECT_ID_KEY";
    public static final String USER_TEAM_ID_KEY = "USER_TEAM_ID_KEY";
    TeamProject teamProject;
    CheckBox viewItems, editItems, editProject, invite;
    View rootView;
    TeamProfile teamProfile;
    TeamPermissionProfile permissionProfile;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
        long id = getArguments().getLong(PROJECT_ID_KEY);
        long userTeamId = getArguments().getLong(USER_TEAM_ID_KEY);
        teamProfile = TeamProfile.findByUserTeamId(userTeamId);
        teamProject = teamProfile.getProjectById(id);
        permissionProfile = TeamPermissionProfile.getByUserIdAndProjectId(teamProfile.getJsonServerId(),teamProject.getProjectId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dialog_edit_team_permission, container, false);
        initViews();
        initData();
        if (teamProfile.getJsonServerId() == ManagerAccount.getMyProfile().getServerId()){
            disableViews();
        }
        setRetainInstance(false);
        return rootView;
    }

    private void initViews() {
        rootView.findViewById(R.id.dialog_permission_ok).setOnClickListener(onBtnOkClick);
        rootView.findViewById(R.id.dialog_permission_cancel).setOnClickListener(onBtnCancelClick);

        viewItems = rootView.findViewById(R.id.permission_view_items);
        editItems = rootView.findViewById(R.id.permission_edit_items);
        editProject = rootView.findViewById(R.id.permission_edit_project);
        viewItems.setOnCheckedChangeListener(onViewItemsChecked);
        editItems.setOnCheckedChangeListener(onEditItemsChecked);
        editProject.setOnCheckedChangeListener(onEditProjectChecked);
        invite = rootView.findViewById(R.id.permission_invite);
    }
    private void disableViews(){
        viewItems.setEnabled(false);
        editItems.setEnabled(false);
        editProject.setEnabled(false);
        invite.setEnabled(false);
    }

    private void initData() {
        ((TextView) rootView.findViewById(R.id.permission_project_name)).setText(teamProject.getName());
        ((TextView) rootView.findViewById(R.id.permission_profile_name)).setText(teamProfile.getUsername());

        if (permissionProfile != null) {
            if (permissionProfile.getRightToEditProject().equals("1")) {
                editProject.setOnCheckedChangeListener(null);
                editProject.setChecked(true);
                editProject.setOnCheckedChangeListener(onEditProjectChecked);
            }

            if (permissionProfile.getRightToEdit().equals("1")) {
                editItems.setOnCheckedChangeListener(null);
                editItems.setChecked(true);
                editItems.setOnCheckedChangeListener(onEditItemsChecked);
            }

            if (permissionProfile.getRightToView().equals("1")) {
                viewItems.setOnCheckedChangeListener(null);
                viewItems.setChecked(true);
                viewItems.setOnCheckedChangeListener(onViewItemsChecked);
            }

            if (permissionProfile.getRightToInvite().equals("1")) {
                invite.setChecked(true);
            }
        }
    }

    /////////
    //
    // LISTENERS
    //
    /////////

    private View.OnClickListener onBtnCancelClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    private View.OnClickListener onBtnOkClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String viewI = "0";
            String editI = "0";
            String editP = "0";
            String inv = "0";
            if (viewItems.isChecked()){ viewI = "1";}
            if (editItems.isChecked()){ editI = "1";}
            if (editProject.isChecked()){ editP = "1";}
            if (invite.isChecked()) {inv = "1";}

            ManagerMyTeams.responseChangePermissionTeamProfile(callbackChangePermission,
                    teamProfile.getJsonServerId(),
                    teamProject.getProjectId(),
                    viewI,editI,editP,inv);
        }
    };

    private Handler callbackChangePermission = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpParam.RESPONSE_OK) {
                if (permissionProfile != null) {
                    if (viewItems.isChecked()) {
                        permissionProfile.setRightToView("1");
                    } else {
                        permissionProfile.setRightToView("0");
                    }
                    if (editItems.isChecked()) {
                        permissionProfile.setRightToEdit("1");
                    } else {
                        permissionProfile.setRightToEdit("0");
                    }
                    if (editProject.isChecked()) {
                        permissionProfile.setRightToEditProject("1");
                    } else {
                        permissionProfile.setRightToEditProject("0");
                    }
                    if (invite.isChecked()) {
                        permissionProfile.setRightToInvite("1");
                    } else {
                        permissionProfile.setRightToInvite("0");
                    }
                    permissionProfile.save();
                }
            }
            dismiss();
        }
    };

    private CompoundButton.OnCheckedChangeListener onViewItemsChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (!isChecked) {
                editItems.setChecked(false);
                editProject.setChecked(false);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onEditItemsChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                viewItems.setChecked(true);
            } else {
                editProject.setChecked(false);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener onEditProjectChecked = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                editItems.setChecked(true);
            }
        }
    };
}
