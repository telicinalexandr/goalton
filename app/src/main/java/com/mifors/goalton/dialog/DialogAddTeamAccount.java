package com.mifors.goalton.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.adapters.AdapterSpinnerAccounts;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerMyTeams;
import com.mifors.goalton.model.team.TeamAccount;
import com.mifors.goalton.network.HttpParam;

import java.util.List;


public class DialogAddTeamAccount extends DialogFragment {
    private static final String TAG = "Goalton [" + DialogAddTeamAccount.class.getSimpleName() + "]";

    public static final int RESULT_CODE_UPDATE = 1004;

    RadioGroup radioGroup;
    Spinner spinner;
    TextView btnOk;
    TextView btnCancel;
    TextInputEditText edName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_add_team_account, container, false);
        btnOk = view.findViewById(R.id.dialog_add_team_ok);
        btnOk.setOnClickListener(onBtnOkClick);
        btnCancel = view.findViewById(R.id.dialog_add_team_cancel);
        btnCancel.setOnClickListener(onBtnCalcelClick);
        edName = view.findViewById(R.id.dialog_add_team_name);
        spinner = view.findViewById(R.id.add_team_dialog_spinner);
        List<TeamAccount> list = TeamAccount.getAll(TeamAccount.class);
        TeamAccount[] accounts = new TeamAccount[list.size()];
        spinner.setAdapter(new AdapterSpinnerAccounts(getActivity(), R.layout.item_spinner_accounts, list.toArray(accounts)));
        radioGroup = view.findViewById(R.id.add_team_dialog_radiogroup);
        radioGroup.setOnCheckedChangeListener(onRaidoGroupChanged);
        return view;
    }

    //////
    //
    // LISTENERS
    //
    /////

    private View.OnClickListener onBtnOkClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (TextUtils.isEmpty(edName.getText())) {
                ManagerApplication.getInstance().showToast(R.string.error_empty_team_name);
                return;
            }
            String name = edName.getText().toString().trim();
            if (radioGroup.getCheckedRadioButtonId() == R.id.add_team_dialog_team) {
                long accountId = ((TeamAccount) spinner.getSelectedItem()).getJsonServerId();
                ManagerMyTeams.responseCreateTeam(callbackResponseCreate, accountId, name);
            } else {
                ManagerMyTeams.responseCreateAccount(callbackResponseCreate, name);
            }
        }
    };

    private View.OnClickListener onBtnCalcelClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    private RadioGroup.OnCheckedChangeListener onRaidoGroupChanged = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
            if (checkedId == R.id.add_team_dialog_team) {
                spinner.setVisibility(View.VISIBLE);
            } else {
                spinner.setVisibility(View.INVISIBLE);
            }
        }
    };

    private Handler callbackResponseCreate = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == HttpParam.RESPONSE_OK){
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_CODE_UPDATE,null);
                dismiss();
            } else {
                ManagerApplication.getInstance().showToast(R.string.message_error_connect_to_server);
            }
        }
    };
}
