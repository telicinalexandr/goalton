package com.mifors.goalton.utils;

/**
 * Created by kirpich on 28.08.2017.
 */

@SuppressWarnings("ALL")
public class ResponseRunnabel implements Runnable
{
    private String name;

    public ResponseRunnabel() {
    }

    public ResponseRunnabel(String name) {
        this.name = name;
    }

    @Override
    public void run() {
    }

    public Runnable getRunnable(){
        return this;
    }

    @Override
    public String toString() {
        return "ResponseRunnabel{name='" + name + "\r\n}";
    }
}
