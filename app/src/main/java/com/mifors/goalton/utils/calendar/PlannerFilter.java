package com.mifors.goalton.utils.calendar;

import com.mifors.goalton.R;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.core.ManagerAccount;

import java.util.ArrayList;

import static com.mifors.goalton.SharedKeys.CALENDAR_FILTER_ARRAY_PROJECTS;
import static com.mifors.goalton.SharedKeys.CALENDAR_FILTER_TYPE_PROJECTS;

/**
 * Created by gly8 on 19.09.17.
 */

@SuppressWarnings("ALL")
public class PlannerFilter
{
    private ArrayList<Long> projectsChecked;
    private int typeShowRadioButon;

    public PlannerFilter() {
        restore();
    }

    public static void reset() {
        PlannerFilter plannerFilter = new PlannerFilter();
        plannerFilter.getProjectsChecked().clear();
        plannerFilter.addIdProject(new Long(0));
        plannerFilter.setTypeShowRadioButon(R.id.btn_3);
        plannerFilter.save();
    }

    public void restore() {
        setProjectsChecked(ManagerApplication.getInstance().getSharedManager().getArrayLong(CALENDAR_FILTER_ARRAY_PROJECTS));
        setTypeShowRadioButon(ManagerApplication.getInstance().getSharedManager().getValueInteger(CALENDAR_FILTER_TYPE_PROJECTS));
    }

    public void save() {
        ManagerApplication.getInstance().getSharedManager().saveArrayLong(projectsChecked, CALENDAR_FILTER_ARRAY_PROJECTS);
        ManagerApplication.getInstance().getSharedManager().putKeyInteger(CALENDAR_FILTER_TYPE_PROJECTS, getTypeShowRadioButon());
    }

    public void addIdProject(Long projectId) {
        if (!getProjectsChecked().contains(projectId)) {
            getProjectsChecked().add(projectId);
        }
    }

    public void removeIdProject(Long projectId) {
        getProjectsChecked().remove(projectId);
    }

    public boolean containsProjectId(Long projectId){
        return getProjectsChecked().contains(projectId);
    }

    public ArrayList<Long> getCopyArrayProjectStages(){
        ArrayList<Long> arrayList = new ArrayList<>();
        arrayList.addAll(projectsChecked);
        return arrayList;
    }

    public boolean isShowAllProjects() {
        return projectsChecked != null && projectsChecked.contains(new Long(0));
    }

    public StringBuilder getQueryProfiles(){
        StringBuilder appeand = new StringBuilder();
        switch (getTypeShowRadioButon()) {
            case R.id.btn_1:
                appeand.append(" AND assign_user_id = ").append(ManagerAccount.getMyProfile().getServerId());
                break;
            case R.id.btn_2:
                appeand.append(" AND creator_user_id = ").append(ManagerAccount.getMyProfile().getServerId());
                break;
        }

        return appeand;
    }

    //==========================================
    // Getter setter
    //==========================================
    public ArrayList<Long> getProjectsChecked() {
        if (projectsChecked == null) {
            projectsChecked = new ArrayList<>();
        }
        return projectsChecked;
    }

    public void setProjectsChecked(ArrayList<Long> projectsChecked) {
        this.projectsChecked = projectsChecked;
    }

    public int getTypeShowRadioButon() {
        return typeShowRadioButon;
    }

    public void setTypeShowRadioButon(int typeShowRadioButon) {
        this.typeShowRadioButon = typeShowRadioButon;
    }
}
