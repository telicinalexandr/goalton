package com.mifors.goalton.utils.calendar;

import android.text.TextUtils;

import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.model.outline.Line;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by gly8 on 26.09.17.
 */
// Описывает группу элементов в промежуток времени между часами
@SuppressWarnings("ALL")
public class CalendarObjectGroupLines {
    private ArrayList<Line> arrayList;
    private int maxEndTime;
    private String timeEndStr = "";
    private Calendar maxCalendar;

    public CalendarObjectGroupLines(ArrayList<Line> arrayList, int maxEndTime, Calendar calendar) {
        this.arrayList = arrayList;
        this.maxEndTime = maxEndTime;
        this.maxCalendar = calendar;
    }

    public ArrayList<Line> getArrayList() {
        return arrayList;
    }

    public int getMaxEndTime() {
        return maxEndTime;
    }

    public String getTimeEnd() {
        if (TextUtils.isEmpty(timeEndStr)) {
            timeEndStr = ManagerCalendar.bulBul(maxEndTime);
        }
        return timeEndStr;
    }

    @Override
    public String toString() {
        return "timeEnd = " + getTimeEnd() + "  maxEndTime = " + maxEndTime + "  countChild = " + arrayList.size()+" array = "+arrayList;
    }

    public Calendar getMaxCalendar() {
        return maxCalendar;
    }

    public void setMaxCalendar(Calendar maxCalendar) {
        this.maxCalendar = maxCalendar;
    }
}