package com.mifors.goalton.utils;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;

import com.activeandroid.ActiveAndroid;
import com.mifors.goalton.activity.ActivityEditLineOutline;
import com.mifors.goalton.interfaces.InterfaceManagerCreateLine;
import com.mifors.goalton.managers.core.ManagerEditLine;
import com.mifors.goalton.managers.core.ManagerOutline;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.network.HttpParam;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gly8 on 07.11.17.
 */

public class ServiceSyncTasks extends Service implements InterfaceManagerCreateLine {

    public static final String ACTION_SYNC = "com.mifors.goalton.SYNC_TASK";
    public static final String EXTRA_KEY_IS_FINISH = "EXTRA_IS_FINISH";
    public static final String EXTRA_KEY_IS_UPDATE = "EXTRA_IS_UPDATE";
    public static final String EXTRA_KEY_NAME_TASK_UPDATE = "EXTRA_NAME_TASK_UPDATE";
    public static final String EXTRA_KEY_PROJECT_ID_UPDATE = "EXTRA_KEY_PROJECT_ID_UPDATE";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startSend();
        return super.onStartCommand(intent, flags, startId);
    }

    // Задачи которые несмогли синхронизироватся удаляются для того чтобы не выпасть в рекурсию
    private ArrayList<Line> arrayListRemove = new ArrayList();
    private Line tempLineSend;

    private void startSend() {
        List<Line> list = Line.getOfflineLines();
        list.removeAll(arrayListRemove);
        Intent intent = new Intent(ACTION_SYNC);
        if (list.size() > 0) {
            tempLineSend = list.get(0);
            intent.putExtra(EXTRA_KEY_IS_UPDATE, true);
            intent.putExtra(EXTRA_KEY_NAME_TASK_UPDATE, tempLineSend.getTitle());
            intent.putExtra(EXTRA_KEY_PROJECT_ID_UPDATE, tempLineSend.getProjectId());
            if (tempLineSend.getOfflineStatus() == Line.StatusOffline.CREATE &&
                    (tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.CREATE_LINE_OUTLINE || tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.UPDATE_LINE_OUTLINE)) {
                ManagerEditLine managerEditLine = new ManagerEditLine(this, tempLineSend);
                managerEditLine.responseCreateItemOutline();
            } else if (tempLineSend.getOfflineStatus() == Line.StatusOffline.CREATE &&
                    (tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.CREATE_LINE_CALENDAR || tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.UPDATE_LINE_CALENDAR)) {
                ManagerEditLine managerEditLine = new ManagerEditLine(this, tempLineSend);
                managerEditLine.setTempLine(tempLineSend);
                managerEditLine.responseCreateItemPlanner();
            } else if (tempLineSend.getOfflineStatus() == Line.StatusOffline.UPDATE &&
                    (tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.CREATE_LINE_CALENDAR || tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.UPDATE_LINE_CALENDAR)) {
                ManagerEditLine managerEditLine = new ManagerEditLine(this, tempLineSend);
                managerEditLine.setTempLine(tempLineSend);
                managerEditLine.responseCreateItemPlanner();
            } else if (tempLineSend.getOfflineStatus() == Line.StatusOffline.UPDATE &&
                    (tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.CREATE_LINE_OUTLINE || tempLineSend.getModeEdit() == ActivityEditLineOutline.Mode.UPDATE_LINE_OUTLINE)) {
                ManagerEditLine managerEditLine = new ManagerEditLine(this, tempLineSend);
                managerEditLine.setTempLine(tempLineSend);
                managerEditLine.responseUpdateItem();
            } else if (tempLineSend.getOfflineStatus() == Line.StatusOffline.REMOVE) {
                ManagerOutline.responseDeleteLine(tempLineSend, new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.what == HttpParam.RESPONSE_OK) {
                            if (msg.obj != null) {
                                Line line = Line.findByServerId(Line.class, (Long) msg.obj);
                                if (line != null) {
                                    line.delete();
                                }
                            }
                        } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                            arrayListRemove.add(tempLineSend);
                        }
                        startSend();
                        super.handleMessage(msg);
                    }
                });
            }
        } else {
            arrayListRemove = new ArrayList();
            tempLineSend = null;
            // Есть ли задачи для редактирования сортировки
            if (Line.getOfflineLinesUpdateSortCount() > 0) {
                List<Line> listSort = Line.getOfflineLinesUpdateSort();
                ManagerOutline.responseUpdateSort(listSort, new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        super.handleMessage(msg);
                        if (msg.what == HttpParam.RESPONSE_OK) {
                            ActiveAndroid.beginTransaction();
                            try {
                                List<Line> listSort = Line.getOfflineLinesUpdateSort();
                                for (Line line : listSort) {
                                    line.setOfflineStatusSort(null);
                                    line.save();
                                }
                                ActiveAndroid.setTransactionSuccessful();
                            } finally {
                                ActiveAndroid.endTransaction();
                                startSend();
                            }
                        } else if (msg.what == HttpParam.RESPONSE_ERROR) {
                            finishSend();
                        }
                    }
                });
            } else {
                intent.putExtra(EXTRA_KEY_IS_FINISH, true);
            }
        }

        sendBroadcast(intent);
    }

    private void finishSend() {
        Intent intent = new Intent(ACTION_SYNC);
        intent.putExtra(EXTRA_KEY_IS_FINISH, true);
        sendBroadcast(intent);
    }

    @Override
    public void finishResponse(Line line) {
        startSend();
    }

    @Override
    public void showProgressBar(int resString) {

    }

    @Override
    public void updateStatusImportantCompletion() {

    }
}
