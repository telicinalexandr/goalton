package com.mifors.goalton.utils;

import com.mifors.goalton.model.project.ProjectStage;

import java.util.Comparator;


public class ComparatorProjectsStages implements Comparator<ProjectStage> {
    @Override
    public int compare(ProjectStage o1, ProjectStage o2) {
        return o1.getSort().compareTo(o2.getSort());
    }
}
