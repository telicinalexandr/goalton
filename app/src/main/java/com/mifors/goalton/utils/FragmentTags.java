package com.mifors.goalton.utils;


public class FragmentTags {
    public static final String FRAGMENT_SETTINGS = "FRAGMENT_SETTINGS";
    public static final String FRAGMENT_MY_PROJECTS = "FRAGMENT_MY_PROJECTS";
    public static final String FRAGMENT_CALENDAR = "FRAGMENT_CALENDAR";
}
