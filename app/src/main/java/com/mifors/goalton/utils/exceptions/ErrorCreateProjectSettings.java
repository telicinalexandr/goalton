package com.mifors.goalton.utils.exceptions;

/**
 * Created by gly8 on 11.04.17.
 */

@SuppressWarnings("ALL")
public class ErrorCreateProjectSettings extends Exception {
    public static final String EMPTY_NAME = "empty_name";
    public ErrorCreateProjectSettings(String message) {
        super(message);
    }
}
