package com.mifors.goalton.utils.calendar;

import com.mifors.goalton.fragment.calendar.FragmentDay;

import java.util.Observable;

/**
 * Created by gly8 on 28.09.17.
 */

@SuppressWarnings("ALL")
public class ObservableFragmentDays extends Observable {
    private static final String TAG = "Goalton [" + ObservableFragmentDays.class.getSimpleName() + "]";

    public void sendNotify() {
        setChanged();
        notifyObservers();
    }

    public void sendNotifyWeekDays(int week) {
        setChanged();
        notifyObservers(week);
    }

    public void sendNotifyUpdateCurrentHour() {
        setChanged();
        notifyObservers(FragmentDay.NOTIFY_CURRENT_HOUR);
    }

    public void sendNotifyHideMinutes() {
        setChanged();
        notifyObservers(FragmentDay.NOTIFY_HIDE_MINUTES_TITLE);
    }

}