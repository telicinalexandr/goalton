package com.mifors.goalton.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mifors.goalton.managers.ManagerApplication;

import java.util.Observable;


/**
 * Created by Вадим on 12.09.2016.
 */
public class ReciverCoonnectNetwork extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        boolean isOnline = ManagerApplication.getInstance().isConnectToInternet(false);
        NetworkObservable.getInstance().setChanged();
        NetworkObservable.getInstance().notifyObservers(isOnline);
    }

    public static class NetworkObservable extends Observable {
        private static NetworkObservable instance = null;

        private NetworkObservable() {
            // Exist to defeat instantiation.
        }

        @Override
        public void setChanged() {
            super.setChanged();
        }

        public static NetworkObservable getInstance(){
            if(instance == null){
                instance = new NetworkObservable();
            }
            return instance;
        }
    }

}
