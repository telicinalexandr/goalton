package com.mifors.goalton.utils.notifications;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mifors.goalton.activity.ActivityTimeNotification;
import com.mifors.goalton.managers.ManagerApplication;
import com.mifors.goalton.managers.utils.ManagerCalendar;
import com.mifors.goalton.model.outline.Line;

/**
 * Created by gly8 on 19.02.18.
 */

public class ReciverTimeNotifications extends BroadcastReceiver {
    private static final String TAG = "Goalton [" + ReciverTimeNotifications.class.getSimpleName() + "]";
    public static final String KEY_ID_LINE = "KEY_ID_LINE";

    @Override
    public void onReceive(Context context, Intent intent) {
        // Этот метод будет вызываться по событию, сочиним его позже
//        long idLine = intent.getLongExtra(KEY_ID_LINE, 0);
//        String bundleName = context.getPackageName();
//        Log.v(TAG, "[onReceive] nameActivity =   bundle = " + bundleName);
//        Intent i = new Intent();
//        i.setClassName(bundleName, bundleName + ".activity.ActivityTimeNotification");
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        i.putExtra(ActivityTimeNotification.KEY_ID_LINE, idLine);
//        context.startActivity(i);
    }

    public static void addNotification(Line line) {
//        if (line != null && line.getDeadlineDateStart() != null) {
//            AlarmManager am = (AlarmManager) ManagerApplication.getInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE); // Получаем сервис
//            Intent intentTime = createIntent(ManagerApplication.getInstance().getApplicationContext(), String.valueOf(line.getServerId()), line.getServerId());
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(ManagerApplication.getInstance().getApplicationContext(), 0, intentTime, PendingIntent.FLAG_UPDATE_CURRENT); // Регестрируем действие
//            Log.v(TAG, "[createIntent] addNotification " + ManagerCalendar.convertCalendarToString(line.getDeadlineDateStart()));
//            am.set(AlarmManager.RTC_WAKEUP, line.getDeadlineDateStart().getTimeInMillis(), pendingIntent);
//        }
    }

    private static Intent createIntent(Context context, String action, long extra) {
        Intent intent = new Intent(context, ReciverTimeNotifications.class);
        intent.setAction(action);
        intent.putExtra(ReciverTimeNotifications.KEY_ID_LINE, extra);
        return intent;
    }

    public static void deleteNotification(Line line) {
//        if (line != null && line.getDeadlineDateStart() != null) {
//            AlarmManager am = (AlarmManager) ManagerApplication.getInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE); // Получаем сервис
//            Intent intentTime = createIntent(ManagerApplication.getInstance().getApplicationContext(), String.valueOf(line.getServerId()), line.getServerId());
//            PendingIntent pendingIntent = PendingIntent.getBroadcast(ManagerApplication.getInstance().getApplicationContext(), 0, intentTime, PendingIntent.FLAG_UPDATE_CURRENT); // Регестрируем действие
//            am.cancel(pendingIntent);
//        }
    }
}