package com.mifors.goalton.utils;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.StrikethroughSpan;
import android.text.util.Linkify;
import android.widget.TextView;

import com.mifors.goalton.R;
import com.mifors.goalton.model.Company;
import com.mifors.goalton.model.outline.Line;
import com.mifors.goalton.model.project.ProjectAssign;

import java.util.Calendar;

/**
 * Created by gly8 on 11.08.17.
 */

@SuppressWarnings("ALL")
public class OutlineLineUIHelper {
    int[] arrBackgroundsColorTop = new int[]{
            R.drawable.shape_outline_root_bg_round_top_color_1,
            R.drawable.shape_outline_root_bg_round_top_color_2,
            R.drawable.shape_outline_root_bg_round_top_color_3,
            R.drawable.shape_outline_root_bg_round_top_color_4,
            R.drawable.shape_outline_root_bg_round_top_color_5,
            R.drawable.shape_outline_root_bg_round_top_color_6
    };

    int[] arrBackgroundsColorAll = new int[]{
            R.drawable.shape_outline_root_bg_round_all_color_1,
            R.drawable.shape_outline_root_bg_round_all_color_2,
            R.drawable.shape_outline_root_bg_round_all_color_3,
            R.drawable.shape_outline_root_bg_round_all_color_4,
            R.drawable.shape_outline_root_bg_round_all_color_5,
            R.drawable.shape_outline_root_bg_round_all_color_6
    };

    int[] arrBackgroundsColorBottom = new int[]{
            R.drawable.shape_outline_root_bg_round_bottom_color_1,
            R.drawable.shape_outline_root_bg_round_bottom_color_2,
            R.drawable.shape_outline_root_bg_round_bottom_color_3,
            R.drawable.shape_outline_root_bg_round_bottom_color_4,
            R.drawable.shape_outline_root_bg_round_bottom_color_5,
            R.drawable.shape_outline_root_bg_round_bottom_color_6
    };

    int[] arrBackgroundsColorCenter = new int[]{
            R.drawable.shape_outline_root_bg_center_color_1,
            R.drawable.shape_outline_root_bg_center_color_2,
            R.drawable.shape_outline_root_bg_center_color_3,
            R.drawable.shape_outline_root_bg_center_color_4,
            R.drawable.shape_outline_root_bg_center_color_5,
            R.drawable.shape_outline_root_bg_center_color_6
    };

    int[] arrBackgroundsColorroundAllTransparent = new int[]{
            R.drawable.shape_outline_root_bg_round_all_20percenttransparent_color_1,
            R.drawable.shape_outline_root_bg_round_all_20percenttransparent_color_2,
            R.drawable.shape_outline_root_bg_round_all_20percenttransparent_color_3,
            R.drawable.shape_outline_root_bg_round_all_20percenttransparent_color_4,
            R.drawable.shape_outline_root_bg_round_all_20percenttransparent_color_5,
            R.drawable.shape_outline_root_bg_round_all_20percenttransparent_color_6
    };

    public static int[] arrSpinnerColor = new int[]{
            R.drawable.shape_outline_circle_color_1,
            R.drawable.shape_outline_circle_color_2,
            R.drawable.shape_outline_circle_color_3,
            R.drawable.shape_outline_circle_color_4,
            R.drawable.shape_outline_circle_color_5,
            R.drawable.shape_outline_circle_color_6
    };



    // CheckBox color
    private int[][] states = new int[][]
            {
                    new int[]{android.R.attr.state_checked, android.R.attr.state_pressed}, // checked/pressed
                    new int[]{android.R.attr.state_pressed}, // pressed
                    new int[]{android.R.attr.state_checked}, // checked
                    new int[]{android.R.attr.state_enabled}  // disabled
            };
    // CheckBox color
    private int[] colors = new int[]
            {
                    Color.BLACK,
                    Color.parseColor("#3e63d0"),
                    Color.parseColor("#3e63d0"),
                    Color.BLACK
            };
    // CheckBox color
    private int[] colorsOverdue = new int[]
            {
                    Color.parseColor("#8b0100"), // Синий
                    Color.parseColor("#3e63d0"), // Красный
                    Color.parseColor("#3e63d0"),
                    Color.parseColor("#8b0100")
            };

    // Отрисовка заголовка в соответсвии с статусом задачи
    public void checkTitle(Line line, TextView title) {
            Spannable spannable;
            if (TextUtils.isEmpty(line.getTitleReplacingTags())) {
                spannable = new SpannableString("");
            } else {
                spannable = new SpannableString(line.getTitleReplacingTags());
            }

            if (line.isComplete()) {
                spannable.setSpan(new StrikethroughSpan(), 0, line.getTitleReplacingTags().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                title.setText(spannable);
                title.setTextColor(ContextCompat.getColor(title.getContext(), R.color.blue_light_very_dark));
            } else {
                spannable.removeSpan(new StrikethroughSpan());
                title.setText(spannable);
                title.setTextColor(ContextCompat.getColor(title.getContext(), android.R.color.black));
            }

            title.setMovementMethod(LinkMovementMethod.getInstance());
            title.setLinksClickable(true);
            Linkify.addLinks(title, Linkify.ALL);
    }


    public int getColor(int color) {
        return Color.parseColor(getColorHex(color));
    }

    public String getColorHex(int color) {
        switch (color) {
            case 1:
                return "#e9e9e9";
            case 2:
                return "#FAF5B3";
            case 3:
                return "#FDB64E";
            case 4:
                return "#CCCCFF";
            case 5:
                return "#E7CDDD";
            case 6:
                return "#9BCA3C";
        }
        return "";
    }
/**1 - FFFFFF
 2 - FAF5B3
 3 - FDB64E
 4 - CCCCFF
 5 - F2D74E
 6 - 9BCA3C
 */
    public int getColorRoundBottom(int color) {
        return getColor(arrBackgroundsColorBottom, color);
    }

    public int getColorRoundTop(int color) {
        return getColor(arrBackgroundsColorTop, color);
    }

    public int getColorRoundAll(int color) {
        return getColor(arrBackgroundsColorAll, color);
    }

    public int getColorRoundAllDateSetContainer(int color) {
        return getColor(arrBackgroundsColorroundAllTransparent, color);
    }

    public int getColorRoundCenter(int color) {
        return getColor(arrBackgroundsColorCenter, color);
    }

    private int getColor(int[] arrColor, int color) {
        if (color == Integer.MIN_VALUE)  {
            color = 0;
        } else if (color > 0) {
            color -=1;
        }

        if (color < arrColor.length) {
            return arrColor[color];
        }
        return arrColor[(color - 1)];
    }

    // Проверка и установка задач с просроком по дедлайну
    public void checkDeadlineProsecution(AppCompatCheckBox checkBox, Line line){
        if (line.getDeadlineDateStart() != null) {
            if (Calendar.getInstance().getTimeInMillis() > line.getDeadlineDateStart().getTimeInMillis()) {
                checkBox.setSupportButtonTintList(new ColorStateList(states, colorsOverdue));
            } else {
                checkBox.setSupportButtonTintList(new ColorStateList(states, colors));
            }
        } else {
            checkBox.setSupportButtonTintList(new ColorStateList(states, colors));
        }
    }

    public StringBuilder getAssignName(Line line){
        StringBuilder builder = new StringBuilder();https://vk.com/id94474764

        if (!TextUtils.isEmpty(line.getAssignUserId())) {
            if (!TextUtils.isEmpty(line.getAssignUserUsername())) {
                builder.append(line.getAssignUserUsername());
            } else {
                ProjectAssign projectAssign = ProjectAssign.findByServerId(ProjectAssign.class, Long.parseLong(line.getAssignUserId()));
                if (projectAssign != null && !TextUtils.isEmpty(projectAssign.getUserName())) {
                    builder.append(projectAssign.getUserName());
                }
            }
        }

        if (line.getCompanyId() != 0) {
            Company company = Company.findByServerId(Company.class, line.getCompanyId());
            if (company != null) {
                builder.append(" ").append(company.getName()).append(" ");
            } else if (!TextUtils.isEmpty(line.getAssignUserId())) {
                builder.append(" ").append(line.getAssignUserUsername()).append(" ");
            }
        }
        return builder;
    }
}