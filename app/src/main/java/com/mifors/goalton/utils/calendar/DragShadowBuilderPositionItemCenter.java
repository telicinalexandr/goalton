package com.mifors.goalton.utils.calendar;

import android.graphics.Point;
import android.view.View;

/**
 * Created by gly8 on 23.10.17.
 */

public class DragShadowBuilderPositionItemCenter extends View.DragShadowBuilder {
    int widthView = 0, heightView = 0;

    public DragShadowBuilderPositionItemCenter(View view) {
        super(view);
        widthView = view.getWidth();
        heightView = view.getHeight();
    }

    @Override
    public void onProvideShadowMetrics(Point shadowSize, Point touchPoint) {
        shadowSize.set(widthView, heightView);
        touchPoint.set(widthView / 2, 0);
    }
}